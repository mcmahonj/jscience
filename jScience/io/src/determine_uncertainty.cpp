// STL
#include <cmath>             // std::pow(), ::round()
#include <tuple>             // std::tuple<>, ::make_tuple()

// jScience
#include "jScience/math.hpp" // exponent()


//
// DESC:
//
// -----
//
// NOTE: To use:
//
//    std::ofstream ofs(filename);
//
//    ofs << std::fixed;
//
//    report_uncertainty();
//
//    if( _exp < 0 )
//    {
//        ofs << std::setprecision(std::abs(_exp)) << x << "(" << i_dx << ")";
//    }
//    else
//    {
//        ofs << std::setprecision(0) << x << "(" << i_dx << ")";
//    }
//
// -----
//
// NOTE: x and dx are passed by value, because they are modified herein (and x returned).
//
std::tuple<
           double, // x
           int,    // i_dx
           // -----
           int     // _exp
           > determine_uncertainty(
                                   double x,
                                   double dx
                                   )
{
    // double x;
    int i_dx;
    // -----
    int _exp;

    // DETERMINE EXPONENT (DECIMAL PLACE) OF UNCERTAINTY
    _exp = exponent(
                    dx
                    );

    // REMOVE EXPONENT
    dx /= std::pow(10.,_exp);

    // ... AND ROUND
    dx = std::round(dx);

    i_dx = static_cast<int>(dx);

    // IF 10, ...
    if( i_dx == 10 )
    {
        // ... SET i_dx BACK TO 1
        i_dx = 1;

        // ... AND INCREASE EXPONENT BY 1
        _exp++;
    }

    // ADJUST (ACTUAL) VALUE TO PRECISION OF UNCERTAINTY
    x /= std::pow(10.,_exp);

    x = std::round(x);

    x *= std::pow(10.,_exp);

    // RETURN
    return std::make_tuple(
                           x,
                           i_dx,
                           // -----
                           _exp
                           );
}
