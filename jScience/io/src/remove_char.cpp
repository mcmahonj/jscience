/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <1/03/2009
// LAST UPDATE:  6/18/2014

// STL
#include <algorithm> // std::remove()
#include <string>    // std::string, .erase()


//========================================================================
//========================================================================
//
// NAME: std::string remove_char(char c, std::string str)
//
// DESC: Removes the character c from a string (str).
//
//========================================================================
//========================================================================
std::string remove_char(char c, std::string str)
{
    str.erase( std::remove( str.begin(), str.end(), c ), str.end() ) ;
    return str ;
}
