/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/18/2014
// LAST UPDATE: 5/18/2014

// STL
#include <string>       // std::string

// Boost
#include <boost/lexical_cast.hpp>


//========================================================================
//========================================================================
//
// NAME: bool is_number( const std::string str )
//
// DESC: Determines whether a string contains a valid number (and only a valid number).
//
// NOTES:
//     ! << JMM >> :: I do NOT think this subroutine is the most efficient for checking for a number, and in the future should be replaced with a full efficient solution 
//
//========================================================================
//========================================================================
bool is_number( const std::string str )
{
    // note: try{} catch{} should be used sparingly, but in this instance, it is correct because we need to try lexical_cast() as is
    try
    {
        boost::lexical_cast<double>(str);
    }
    catch (...)
    {
        return false;
    }
    
    return true;
}
