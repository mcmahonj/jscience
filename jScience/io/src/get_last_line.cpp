// STL
#include <fstream>   // std::ifstream
#include <limits>    // std::numeric_limits<>
#include <string>    // std::string


//
// DESC: Gets the last line of an ifstream (file, etc.).
//
std::istream& ignoreline(std::ifstream& is, std::ifstream::pos_type& pos)
{
    pos = is.tellg();
    return is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


//
// DESC: Gets the last line of an ifstream (file, etc.).
//
std::string get_last_line(std::ifstream& is)
{
    std::string line;
    
    std::ifstream::pos_type pos = is.tellg();
    
    std::ifstream::pos_type last_pos;
    while( is >> std::ws && ignoreline(is, last_pos) )
    {
        pos = last_pos;
    }
    
    is.clear();
    is.seekg(pos);
    
    std::getline(is, line);
    
    return line;
}


//
// DESC: Gets the last line of an ifstream (file, etc.).
//
std::string last_line(
                      const std::string filename
                      )
{
    std::ifstream ifs(filename);
    
    std::string lline = get_last_line(ifs);
    
    return lline;
}


// NOTE: the following algorithm is an alternative implementation that I found online.
/*
//
// DESC: Gets the last line of an ifstream (file, etc.).
//
std::string last_line(
                      const std::string filename
                      )
{
    std::string lline;
    
    std::ifstream ifs(filename);
    
    if(ifs.is_open())
    {
        ifs.seekg(-1,std::ios_base::end);                // go to one spot before the EOF
        
        bool keepLooping = true;
        while(keepLooping)
        {
            char ch;
            ifs.get(ch);                            // Get current byte's data
            
            if((int)ifs.tellg() <= 1)               // If the data was at or before the 0th byte
            {
                ifs.seekg(0);                       // The first line is the last line
                keepLooping = false;                // So stop there
            }
            else if(ch == '\n')                     // If the data was a newline
            {
                keepLooping = false;                // Stop at the current position.
            }
            else                                    // If the data was neither a newline nor at the 0 byte
            {
                ifs.seekg(-2,std::ios_base::cur);   // Move to the front of that data, then to the front of the data before it
            }
        }
        
        std::getline(ifs,lline);                    // Read the current line
        
        ifs.close();
    }
    
    return lline;
}
*/