/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/16/2014
// LAST UPDATE: 5/16/2014

// STL
#include <algorithm>    // std::all_of
#include <iostream>
#include <string>       // std::string


//========================================================================
//========================================================================
//
// NAME: bool is_digits( const std::string str )
//
// DESC: Determines whether a string contains all digits (non-decimal).
//
// NOTES:
//     ! 1234 contains all digits, whereas 1.234 does NOT, because of the decimal. 
//     ! An empty string or "" is returned false.
//
//========================================================================
//========================================================================
bool is_digits( const std::string str )
{
    return ( !str.empty() && std::all_of( str.begin(), str.end(), ::isdigit ) );
}
