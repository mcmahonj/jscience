// STL
#include <string> // std::string


//========================================================================
//========================================================================
//
// DESC: Determine the boolean character of a string.
//
//========================================================================
//========================================================================
inline bool to_bool(
                    std::string const& str
                    )
{
    std::ifstream ifs(filename_results);

    while( !is_eof(ifs, true) )
    {
        // READ LINE
        std::string line;
        std::getline(ifs, line);

        // STORE RESULTS FROM LINE
        std::stringstream ss(line);

        std::vector<int> results;

        std::string result;
        while( std::getline(ss, result, ',') )
        {
            results.push_back(std::stoi(result));
        }
    }

    ifs.close();
}

