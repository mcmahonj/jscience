// STL
#include <cmath>              // std::abs()
#include <iomanip>            // std::setprecision()
#include <ios>                // std::fixed
#include <sstream>            // std::stringstream
#include <string>             // std::string
#include <tuple>              // std::tie()

// this
#include "jScience/io/io.hpp" // determine_uncertainty()


//
// DESC:
//
// -----
//
// NOTE: x and dx are passed by value, because they are modified herein.
//
std::string report_uncertainty(
                               double x,
                               double dx
                               )
{
    std::string x_dx;

    // DETERMINE UNCERTAINTY
    int i_dx;
    // -----
    int _exp;
    std::tie(
             x,
             i_dx,
             // -----
             _exp
             ) = determine_uncertainty(
                                       x,
                                       dx
                                       );

    // ASSIGN TO A std::stringstream
    std::stringstream ss;

    ss << std::fixed;

    if( _exp < 0 )
    {
        ss << std::setprecision(std::abs(_exp)) << x << "(" << i_dx << ")";
    }
    else
    {
        ss << std::setprecision(0) << x << "(" << i_dx << ")";
    }

    // ASSIGN TO A std::string
    x_dx = ss.str();

    // RETURN
    return x_dx;
}
