/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/11/2014
// LAST UPDATE: 6/11/2014

// STL
#include <string>   // std::string
#include <vector>   // std::vector


//========================================================================
//========================================================================
//
// NAME: std::string combine_words( const std::vector<std::string> &words, const char delim = ' ' )
//
// DESC: Combines a vector of words, using a delimiter of delim.
//
//========================================================================
//========================================================================
std::string combine_words( const std::vector<std::string> &words, const char delim = ' ' )
{
    std::string str = std::string();
    
    for( auto &word : words )
    {
        str += (delim + word);
    }
    
    return str;
}