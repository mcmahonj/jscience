// STL
#include <iomanip> // std::quoted()
#include <sstream> // std::stringstream, .eof(), .peek(), .get()
#include <string>  // std::string, ::getline()
#include <vector>  // std::vector<>


//
// DESC: Parse a CSV line.
//
// NOTE: This subroutine (should) conform to RFC 4180.
//
// NOTE: ... See:
//
//     https://en.wikipedia.org/wiki/Comma-separated_values
//
// TODO: NOTE: ... Though, I am sure that not all subtleties are (yet) handled.
//
std::vector<std::string> parse_CSV_line(
                                        const std::string line
                                        )
{
    std::vector<std::string> CSV_tokens;

    std::stringstream ss(line);

    while( !ss.eof() )
    {
        std::string CSV_token;

        // GET INITIAL WHITESPACE (IF EXISTS)
        std::string CSV_ws;

        while( ss.peek() == ' ' )
        {
            CSV_ws.push_back(' ');

            ss.get();
        }

        // GET REMAINDER
        if( ss.peek() == '"' )
        {
            // NOTE: Whitespace surrounding quotes is ignored.

            // GET VALUE BETWEEN QUOTES
            ss >> std::quoted(CSV_token);

            // ADD QUOTES BACK IN
            CSV_token.insert(CSV_token.begin(),'"');
            CSV_token.push_back('"');

            std::string discard;
            std::getline(ss, discard, ',');
        }
        else
        {
            // NOTE: Without quotes, surrounding whitespace is important.

            std::string _CSV_token;
            std::getline(ss, _CSV_token, ',');

            CSV_token.append(CSV_ws);
            CSV_token.append(_CSV_token);
        }

        CSV_tokens.push_back(CSV_token);
    }

    return CSV_tokens;
}
