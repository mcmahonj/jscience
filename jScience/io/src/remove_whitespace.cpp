// STL
#include <algorithm> // std::remove_if()
#include <string>    // std::string, string.erase(), string.begin(), string.end()
#include <vector>    // std::vector


//========================================================================
//========================================================================
//
// NAME: std::string remove_whitespace(std::string str)
//
// DESC: Removes the whitespace from a string.
//
//========================================================================
//========================================================================
std::string remove_whitespace(std::string str)
{
    str.erase(std::remove_if(str.begin(), str.end(),
                             [](char c){ return (c =='\r' || c =='\t' || c == ' ' || c == '\n');}),
              str.end() );

    return str;
}


//========================================================================
//========================================================================
//
// NAME: std::vector<std::string> remove_whitespace(std::vector<std::string>)
//
// DESC: Removes the whitespace from each string in a vector.
//
//========================================================================
//========================================================================
std::vector<std::string> remove_whitespace(std::vector<std::string> vstr)
{
    for( auto &str : vstr )
    {
        str = remove_whitespace(str);
    }

    return vstr;
}
