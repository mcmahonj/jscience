#ifndef IO_HPP
#define IO_HPP


// STL
#include <fstream> // std::ifstream
#include <string>  // std::string
#include <tuple>   // std::tuple<>
#include <vector>  // std::vector<>


std::vector<std::string> parse_CSV_line(
                                        const std::string line
                                        );


//========================================================================
// NAME: std::string combine_words( const std::vector<std::string> &words, const char delim = ' ' )
// DESC: Combines a vector of words, using a delimiter of delim.
//========================================================================
std::string combine_words( const std::vector<std::string> &words, const char delim = ' ' );


//========================================================================
// DESC: Gets the last line of an ifstream (file, etc.).
//========================================================================
std::string get_last_line(std::ifstream& is);

std::string last_line(
                      const std::string filename
                      );

//========================================================================
// NAME: std::string remove_char(char c, std::string str)
// DESC: Removes the character c from a string (str).
//========================================================================
std::string remove_char(char c, std::string str);


//========================================================================
// NAME: std::string remove_whitespace(std::string str)
// DESC: Removes the whitespace from a string.
//========================================================================
std::string remove_whitespace(std::string str);

//========================================================================
// NAME: std::vector<std::string> remove_whitespace(std::vector<std::string> vstr)
// DESC: Removes the whitespace from each string in a vector.
//========================================================================
std::vector<std::string> remove_whitespace(std::vector<std::string> vstr);


//========================================================================
// NAME: std::vector<std::string> remove_empty(std::vector<std::string> vstr)
// DESC: Given a vector of strings, returns a vector with the empty elements removed, using the erase-remove idiom.
//========================================================================
std::vector<std::string> remove_empty(std::vector<std::string> vstr);


std::string report_uncertainty(
                               double x,
                               double dx
                               );

std::tuple<
           double, // x
           int,    // i_dx
           // -----
           int     // _exp
           > determine_uncertainty(
                                   double x,
                                   double dx
                                   );


//========================================================================
// NAME: bool is_digits( const std::string str )
// DESC: Determines whether a string contains all digits (non-decimal).
// NOTES:
//     ! 1234 contains all digits, whereas 1.234 does NOT, because of the decimal.
//========================================================================
bool is_digits( const std::string str );


//========================================================================
// NAME: bool is_number( const std::string str )
// DESC: Determines whether a string contains a valid number (and only a valid number).
//========================================================================
bool is_number( const std::string str );


//========================================================================
// DESC: Determine the boolean character of a string.
//========================================================================
bool to_bool(
             std::string const& str
             );
#include "jScience/io/src/to_bool.cpp"


#endif
