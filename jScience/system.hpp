#ifndef SYSTEM_HPP
#define SYSTEM_HPP

int setenv(
           const char *name,
           const char *value,
           const int   overwrite
           );

#endif
