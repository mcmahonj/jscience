/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/31/2014
// LAST UPDATE: 6/29/2014

#include "jScience/materials/Material.hpp"

// STL
#include <complex>  // std::complex
#include <vector>   // std::vector

// jScience
#include "jScience/materials/Drude.hpp"   // calc_epsr()
#include "jScience/materials/Lorentz.hpp" // calc_epsr()


Material::Material()
{
    epsr = 0.0;
    Drude_poles.clear();
    Lorentz_poles.clear();
}

Material::Material( const double ep, const std::vector<Drude> &Dp, const std::vector<Lorentz> &Lp )
{   
    epsr = ep;
    Drude_poles = Dp;
    Lorentz_poles = Lp;
}

Material::~Material() {};


//=========================================================
//=========================================================
//
// NAME: std::complex<double> calc_epsr( const double omega, const Material &mat )
//
// DESC: Calculates the relative permittivity (epsilon_r) for the material model mat at frequency omega (in eV).
//
//=========================================================
//=========================================================
std::complex<double> calc_epsr( const double omega, const Material &mat )
{
    std::complex<double> epsr = mat.epsr;
    
    for( auto &Dp : mat.Drude_poles )
    {
        epsr += ( calc_epsr(omega, Dp) - 1.0 );
    }
    
    for( auto &Lp : mat.Lorentz_poles )
    {
        epsr += ( calc_epsr(omega, Lp) - 1.0 );
    }
    
    return epsr;
}
