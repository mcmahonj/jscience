/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/31/2014
// LAST UPDATE: 6/29/2014

#include "jScience/materials/Drude.hpp"

// STL
#include <complex>  // std::complex

// jScience
#include "jScience/physics/consts.hpp" // COMPLEXJ


Drude::Drude() : Drude(0.0, 0.0) {};

Drude::Drude(const double om, const double ga)
{   
    omega_p = om;
    gamma   = ga;
}

Drude::~Drude() {};


//=========================================================
//=========================================================
//
// NAME: std::complex<double> calc_epsr( const double omega, const Drude &pole )
//
// DESC: Calculates the relative permittivity (epsilon_r) for a Drude pole Dp at frequency omega (in eV).
//
//=========================================================
//=========================================================
std::complex<double> calc_epsr( const double omega, const Drude &Dp )
{
    return ( 1.0 - Dp.omega_p*Dp.omega_p/(omega*(omega + COMPLEXJ*Dp.gamma)) );
}

