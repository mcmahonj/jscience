/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/31/2014
// LAST UPDATE: 5/31/2014

#ifndef DRUDE_H
#define DRUDE_H


// STL
#include <complex>


//=========================================================
// NAME: Drude model
// DESC: Defines a Drude model.
// NOTES:
//     ! The permittivity, defined by this model, is:
//          eps(omega) = 1 - omega_p^2/[omega*(omega + i*gamma)]
//=========================================================
class Drude
{
    
public:
    
    double omega_p;
    double gamma;
    
    
    Drude();
    Drude(const double om, const double ga);
    ~Drude();

};


std::complex<double> calc_epsr( const double omega, const Drude &Dp );


#endif
