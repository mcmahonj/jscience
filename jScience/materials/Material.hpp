/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/31/2014
// LAST UPDATE: 6/29/2014

#ifndef MATERIAL_H
#define MATERIAL_H

// STL
#include <complex>     // std::complex
#include <vector>      // std::vector

// jScience
#include "Drude.hpp"   // Drude
#include "Lorentz.hpp" // Lorentz


//=========================================================
// Drude model
// Desc: A material model consisting of an infinite-frequency permittivity and a number of Drude and Lorentz poles.
//=========================================================
class Material
{
    
public:
    
    double epsr;                        // Infinite-frequency permittivity
    std::vector<Drude> Drude_poles;     // Drude poles
    std::vector<Lorentz> Lorentz_poles; // Lorentz poles
    
    
    Material();
    Material( const double ep, const std::vector<Drude> &Dp, const std::vector<Lorentz> &Lp );
    ~Material();

};


std::complex<double> calc_epsr( const double omega, const Material &mat );


#endif
