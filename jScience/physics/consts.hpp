
#ifndef CONSTS_H
#define CONSTS_H


// STL
#include <complex>

// TODO: I am not sure how all of these constants (e.g., math vs physics) should be distributed

// TODO: The constants should be simplified so that they are all given just once in particular units (e.g., Kb just in J/K), and then all of the conversions are also given

//extern const long double PI;
extern const double PI;
extern const double EULERC, NM;
extern const double RAD_TO_DEG, DEG_TO_RAD;
extern const double golden_ratio;
extern const double golden_ratio2;

extern const std::complex<double> COMPLEXZERO, COMPLEXONE, COMPLEXJ;
extern const double CSPEED, CSPEED4, EPS0, EPS0R, MU0, MU0R, Z0, PLANCK, PLANCKEV, a0, HBAR, HBAREV, ECHARGE, EMASS, PMASS, PMASS_EMASS, au_u, EV, Eh, GN;
extern const double ALPHA;
extern const double M_PER_BOHR, AA_PER_BOHR;
extern const double AA_PER_NM;
extern const double JTOEV, J_PER_EV, RYD_PER_J, RYD_PER_eV, Ha_PER_RYD, RYD_BOHR_PER_GAUSS_M, eV_PER_kcal_PER_mol, Ha_PER_kJ_PER_mol, kg_PER_amu, Hz_per_cmm1;
extern const double kB, kB_Ha, kB_eV, kB_J_K;
extern const double GPa_PER_Ha_PER_BOHR3;


#endif
