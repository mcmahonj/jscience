/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/18/2014
// LAST UPDATE: 10/28/2014

#ifndef LATTICE_H
#define LATTICE_H


// STL
#include <string> // std::string
#include <vector> // std::vector

// jScience
#include "jScience/geometry/Vector.hpp" // Vector
#include "jScience/physics/MillerIndices.hpp" // MillerIndices


//========================================================================
// NAME: void read_lattice( const std::string filename, std::vector<Vector<double>> &a )
// DESC: Reads in a lattice, as specified by:
//
//          alat
//            a(0).x  a(0).y  a(0).z
//            a(1).x  a(1).y  a(1).z
//            a(2).x  a(2).y  a(2).z
// NOTES:
//     i. alat should by in units of A, and a() should be in units of alat
//========================================================================
void read_lattice( const std::string filename, std::vector<Vector<double>> &a );

//========================================================================
// NAME: std::vector<Vector<double>> reciprocal_primvec( const std::vector<Vector<double>> &a )
// DESC: Calculates the reciprocal primitive vectors b of a lattice, given its primitive vectors a.
//========================================================================
std::vector<Vector<double>> reciprocal_primvec( const std::vector<Vector<double>> &a );

//========================================================================
//========================================================================
//
// NAME: void lattice_planes( const std::vector<Vector<double>> &a, const int max_h, const int max_k, const int max_l, std::vector<Vector<double>> &g, std::vector<MillerIndices> &hkl )
//
// DESC: Given the lattice vectors a, calculate and return the lattice planes g and corresponding Miller indices hkl (up to supplied max. values -- e.g., -max_h <= h <= max_h), sorted by |g|.
//
// NOTES:
//     ! The Miller indices are not written in their lowest terms (i.e., all terms are returned)
//     ! See:
//          http://en.wikipedia.org/wiki/Miller_index
//
//========================================================================
//========================================================================
void lattice_planes( const std::vector<Vector<double>> &a, const int max_h, const int max_k, const int max_l, std::vector<Vector<double>> &g, std::vector<MillerIndices> &hkl );

//========================================================================
// NAME: Vector<double> rij_min_img( const Vector<double> &ri, const Vector<double> &rj, const std::vector<Vector<double>> &a )
// DESC: Determine the vector pointing from ri to rj, using the minimum-image convention with primitive vectors a.
//========================================================================
Vector<double> rij_min_img( const Vector<double> &ri, const Vector<double> &rj, const std::vector<Vector<double>> &a );

//========================================================================
// NAME: double MSD( const std::vector<Vector<double>> &r, const std::vector<Vector<double>> &r0, const std::vector<Vector<double>> &a )
// DESC: Returns the mean squared displacement of a collection of particles (r) from their lattice positions (r0), given the primitive vectors a, in the minimum-image convention.
//========================================================================
double MSD( const std::vector<Vector<double>> &r, const std::vector<Vector<double>> &r0, const std::vector<Vector<double>> &a );

//========================================================================
// NAME: double dnn( const std::vector<Vector<double>> &r, const std::vector<Vector<double>> &a )
// DESC: Given a set of particle coordinates (r) and lattice primitive vectors (a), calculate the average nearest-neighbor separation, in the minimum image concention.
//========================================================================
double dnn( const std::vector<Vector<double>> &r, const std::vector<Vector<double>> &a );


#endif	
