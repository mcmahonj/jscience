/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with SimStats. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 10/28/2014
// LAST UPDATE: 10/28/2014

#ifndef RHOK_H
#define RHOK_H


// STL
#include <complex> // std::complex
#include <vector>  // std::vector

// jScience
#include "jScience/geometry/Vector.hpp" // Vector, dot_product()
#include "jScience/physics/consts.hpp"  // COMPLEXZERO, COMPLEXJ


//========================================================================
// NAME: std::complex<double> rhok( const Vector<double> &k, const std::vector<Vector<double>> &r )
// DESC: Given a reciprocal lattice vector k, calculates rho(k) = sum_j exp(-i*k*r_j).
//========================================================================
std::complex<double> rhok( const Vector<double> &k, const std::vector<Vector<double>> &r );

//========================================================================
// NAME: std::vector<std::complex<double>> rhok( const std::vector<Vector<double>> &vk, const std::vector<Vector<double>> &r )
// DESC: Given a vector of reciprocal lattice vectors vk, calculates a corresponding vector of rho(k).
//========================================================================
std::vector<std::complex<double>> rhok( const std::vector<Vector<double>> &vk, const std::vector<Vector<double>> &r );


#endif	
