/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with jScience. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 10/28/2014
// LAST UPDATE: 10/29/2014

#ifndef MILLER_INDICES_H
#define MILLER_INDICES_H


// STL
#include <iostream>


//=========================================================
// MILLER INDICES
//=========================================================
class MillerIndices
{
    
public:
    
    int h;
    int k;
    int l;
    
    
    MillerIndices( const int hh, const int kk, const int ll );
    ~MillerIndices();
    
    friend std::ostream& operator<<(std::ostream &os, const MillerIndices &MI);

};


#endif	
