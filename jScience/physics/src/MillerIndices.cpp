
#include "jScience/physics/MillerIndices.hpp" // MillerIndices

// STL
#include <iostream>


MillerIndices::MillerIndices( const int hh, const int kk, const int ll )
{
    h = hh;
    k = kk;
    l = ll;
}


MillerIndices::~MillerIndices() {};


//========================================================================
//========================================================================
//
// NAME: std::ostream& operator<<(std::ostream &os, const MillerIndices &MI)
//
// DESC: Ostream overload for MillerIndices.
//
// NOTES:
//     ! If h, k, or l is less than 0 or greater than 9, it is placed in parenthesis ...
//     ! ... standard notation is to put a bar above it; see:
//          http://en.wikipedia.org/wiki/Miller_index
//
//========================================================================
//========================================================================
std::ostream& operator<<(std::ostream &os, const MillerIndices &MI)
{
    if( (MI.h < 0) || (MI.h > 9) )
    {
        os << "(" << MI.h << ")";
    }
    else
    {
        os << MI.h;
    }
    
    if( (MI.k < 0) || (MI.k > 9) )
    {
        os << "(" << MI.k << ")";
    }
    else
    {
        os << MI.k;
    }
    
    if( (MI.l < 0) || (MI.l > 9) )
    {
        os << "(" << MI.l << ")";
    }
    else
    {
        os << MI.l;
    }
}
