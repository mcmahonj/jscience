
#include "jScience/physics/consts.hpp"

// STL
#include <cmath> // std::sqrt()

// BOOST
#include <boost/math/constants/constants.hpp>


// STANDARD CONSTANTS
// ! PI is a tricky constant to calculate. We could use Machin's formula (good to ~500 digits), or perhaps acos(-1.0) which would be as accurate as the compiler would support, or even acosl(-1.0L) for long double precision. But using boost seems the ``easiest''.
//const double PI=16.0*atan(0.2)-4.0*atan(1.0/239.0);
//const long double PI = boost::math::constants::pi<long double>();
const double PI = boost::math::constants::pi<double>();
const double EULERC = 0.57721566490153286060651209008;
const double NM=1.0e-9;
const double golden_ratio = (1.0 + std::sqrt(5.0))/2.0;
const double golden_ratio2 = 1.0 - 1.0/golden_ratio;

// RADIAN<-->DEGREE CONVERSIONS
const double RAD_TO_DEG = 180.0/PI;
const double DEG_TO_RAD = PI/180.0;

// COMPLEX CONSTANTS
const std::complex<double> COMPLEXZERO(0.0,0.0);
const std::complex<double> COMPLEXONE(1.0,0.0);
const std::complex<double> COMPLEXJ(0.0,1.0);

// PHYSICAL CONSTANTS
// CSPEED is a physical constant defined by 299792458 m/s, which is exact
const double CSPEED = 299792458.0;
const double CSPEED4=pow(CSPEED, 4);
// MU0 is a defined constant of 4*pi*10^-7 N/A^2 (kg*m*s^-2) = 4*pi*10^-7 H/m
const double MU0=4.0*PI*(1.0e-7);
// EPS0 is a defined as 1/(c^2*MU0) F/m (A^2*s^4*kg^-1*m^-3)
const double EPS0 = 1.0/(CSPEED*CSPEED*MU0);
const double MU0R=1.0;
const double EPS0R=1.0;
const double Z0=sqrt(MU0/EPS0);

const double PLANCK=6.62606896e-34;   // h
const double PLANCKEV=4.13566733e-15; // h
const double HBAR=1.054571629e-34;    // hbar (in J*s)
const double HBAREV=6.58211899e-16;   // hbar

const double a0=5.2917720859e-11;
const double ECHARGE=1.602176487e-19;
const double EMASS=9.10938215e-31;
const double PMASS=1.672621637e-27;
const double PMASS_EMASS=1836.15267389; // https://en.wikipedia.org/wiki/Proton-to-electron_mass_ratio
                                        // NOTE: This should be more accurate in the 6th decimal place than PMASS/EMASS (TODO: NOTE: Not sure why.).
const double au_u = 1822.888486192; // 1822.888486192(53); see: https://en.wikipedia.org/wiki/Unified_atomic_mass_unit
const double EV = 1.6021765314e-19;
const double Eh = 4.359744650e-18; // in J (2010 CODATA recommended value)

// see: https://en.wikipedia.org/wiki/Fine-structure_constant
const double ALPHA = ECHARGE*ECHARGE/((4*PI*EPS0)*HBAR*CSPEED);

// NEWTON'S CONSTANT
const double GN = 6.67428e-11;

// CONVERSION FACTORS
const double M_PER_BOHR  = 5.2917720859e-11;
const double AA_PER_BOHR = (M_PER_BOHR*(1.0e10));
const double AA_PER_NM   = 10.0;

const double JTOEV = 6.241506363e18;
const double J_PER_EV = 1.0/JTOEV;
const double RYD_PER_J = 4.587420897e+17;
const double RYD_PER_eV = 0.073498618;
const double Ha_PER_RYD = 0.5;
// ! the following is sometimes called the electrostatic constant, which converts Gauss units to standard (which I have taken to be Ry)
const double RYD_BOHR_PER_GAUSS_M = (1.0/(4.0*PI*EPS0))*ECHARGE*ECHARGE*RYD_PER_J/M_PER_BOHR;
const double eV_PER_kcal_PER_mol = 0.0433641;
const double Ha_PER_kJ_PER_mol = 1.0/2625.49962;
const double Hz_per_cmm1 = CSPEED*100.0;

// STATISTICAL MECHANICS STUFF
// Boltzmann's constant in eV/K (2010 CODTA value from Wikipedia)
const double kB     = 8.617332478e-5; // in eV/K
const double kB_Ha  = 3.1668114e-6; // https://en.wikipedia.org/wiki/Boltzmann_constant#Value_in_different_units
const double kB_eV  = kB_Ha*(1.0/Ha_PER_RYD)*(1.0/RYD_PER_eV);
const double kB_J_K = kB_eV*J_PER_EV;

const double kg_PER_amu = 1.660538921e-27; // uncertainty of (73)

// ! from the Abinit documentation
const double GPa_PER_Ha_PER_BOHR3 = 29421.033;

