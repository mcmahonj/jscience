/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with SimStats. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 10/29/2014
// LAST UPDATE: 10/29/2014


// STL
#include <vector>  // std::vector

// jScience
#include "jScience/geometry/Vector.hpp" // Vector, dot_product()


//========================================================================
//========================================================================
//
// NAME: void wrap_coord( std::vector<Vector<double>> &r, const std::vector<Vector<double>> &a )
//
// DESC: Given the lattice vectors a, wrap the coordinates r using periodic boundary conditions.
//
// JMM: NOT DONE
//
//========================================================================
//========================================================================
void wrap_coord( std::vector<Vector<double>> &r, const std::vector<Vector<double>> &a )
{
    
}
