/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/18/2014
// LAST UPDATE: 7/18/2014


// STL
#include <vector> // std::vector

// jScience
#include "jScience/geometry/Vector.hpp" // Vector, cross_product(), dot_product()
#include "jScience/physics/consts.hpp"  // PI


//========================================================================
//========================================================================
//
// NAME: std::vector<Vector<double>> reciprocal_primvec( const std::vector<Vector<double>> &a )
//
// DESC: Calculates the reciprocal primitive vectors b of a lattice, given its primitive vectors a.
//
// NOTES:
//     ! See:
//          http://en.wikipedia.org/wiki/Reciprocal_lattice
//
//========================================================================
//========================================================================
std::vector<Vector<double>> reciprocal_primvec( const std::vector<Vector<double>> &a )
{
    std::vector<Vector<double>> b(3);
    
    b[0] = 2.0*PI*cross_product(a[1], a[2])/dot_product(a[0], cross_product(a[1], a[2]));
    b[1] = 2.0*PI*cross_product(a[2], a[0])/dot_product(a[1], cross_product(a[2], a[0]));
    b[2] = 2.0*PI*cross_product(a[0], a[1])/dot_product(a[2], cross_product(a[0], a[1]));
    
    return b;
}
