/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of FDTD++.
 
 FDTD++ is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 FDTD++ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with FDTD++. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 10/28/2014
// LAST UPDATE: 10/28/2014


// STL
#include <vector>    // std::vector

// jScience
#include "jScience/geometry/Vector.hpp"       // Vector, magnitude()
#include "jScience/physics/lattice.hpp"       // reciprocal_primvec()
#include "jScience/physics/MillerIndices.hpp" // MillerIndices

#include "jutility.hpp"                       // sort_indices_asc()


//========================================================================
//========================================================================
//
// NAME: void lattice_planes( const std::vector<Vector<double>> &a, const int max_h, const int max_k, const int max_l, std::vector<Vector<double>> &g, std::vector<MillerIndices> &hkl )
//
// DESC: Given the lattice vectors a, calculate and return the lattice planes g and corresponding Miller indices hkl (up to supplied max. values -- e.g., -max_h <= h <= max_h), sorted by |g|.
//
// NOTES:
//     ! The Miller indices are not written in their lowest terms (i.e., all terms are returned)
//     ! See:
//          http://en.wikipedia.org/wiki/Miller_index
//
//========================================================================
//========================================================================
void lattice_planes( const std::vector<Vector<double>> &a, const int max_h, const int max_k, const int max_l, std::vector<Vector<double>> &g, std::vector<MillerIndices> &hkl )
{
    g.clear();
    hkl.clear();
    
    std::vector<Vector<double>> b = reciprocal_primvec(a);
    
    // JMM: there MUST be a better way to do this and perform the sort by gmag in place below, such that we don't need to declare all these tmp arrays
    std::vector<MillerIndices>  hkl_tmp;
    std::vector<Vector<double>> g_tmp;
    std::vector<double>         gmag;
    
    for( int h = -max_h; h <= max_h; ++h )
    {
        for( int k = -max_k; k <= max_k; ++k )
        {
            for( int l = -max_l; l <= max_l; ++l )
            {
                //if((h == 0) && (k == 0) && (l == 0))
                //{
                //    continue;
                //}
                
                hkl_tmp.push_back( MillerIndices(h, k, l) );
                g_tmp.push_back( static_cast<double>(h)*b[0] + static_cast<double>(k)*b[1] + static_cast<double>(l)*b[2] );
                gmag.push_back( magnitude( g_tmp.back() ) );
            }
        }
    }
    
    for( auto &idx : sort_indices_asc(gmag) )
    {
        hkl.push_back( hkl_tmp[idx] );
        g.push_back( g_tmp[idx] );
    }
}

