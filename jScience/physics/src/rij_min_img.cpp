/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/18/2014
// LAST UPDATE: 7/18/2014


// STL
#include <vector> // std::vector

// jScience
#include "jScience/geometry/Vector.hpp" // Vector, dot_product(), magnitude()


//========================================================================
//========================================================================
//
// NAME: Vector<double> rij_min_img( const Vector<double> &ri, const Vector<double> &rj, const std::vector<Vector<double>> &a )
//
// DESC: Determine the vector pointing from ri to rj, using the minimum-image convention with primitive vectors a.
//
//========================================================================
//========================================================================
Vector<double> rij_min_img( const Vector<double> &ri, const Vector<double> &rj, const std::vector<Vector<double>> &a )
{
    Vector<double> rij = rj - ri;
    
    for( int i = 0; i < 3; ++i )
    {
        double rij_a = dot_product(rij, a[i]);
        double mag_a = magnitude(a[i]);
        
        if( (rij_a/mag_a) > (mag_a/2.0) )
        {
            rij -= a[i];
        }
        else if( (rij_a/mag_a) < (-mag_a/2.0) )
        {
            rij += a[i];
        }
    }
    
    return rij;
}
