/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 10/28/2014
// LAST UPDATE: 10/29/2014


// STL
#include <fstream> // std::ifstream
#include <string>  // std::string
#include <vector>  // std::vector

// jScience
#include "jScience/geometry/Vector.hpp" // Vector


//========================================================================
//========================================================================
//
// NAME: bool read_xyz_step( std::ifstream &ifs, std::vector<std::string> &symb, std::vector<Vector<double>> &r )
//
// DESC: Given an open std::ifstream, read a step from a .xyz file, returning a list of symbols and positions. The format is:
//
//          # atoms
//          comment
//          atom_symb_11 x_11 y_11 z_11
//          atom_symb_12 x_12 y_12 z_12
//          .
//          .
//          .
//          # atoms
//          comment
//          atom_symb_21 x_21 y_21 z_21
//          atom_symb_22 x_22 y_22 z_22
//          .
//          .
//          .
//
// NOTES:
//     ! See:
//          http://en.wikipedia.org/wiki/XYZ_file_format
//     ! The comment line is discarded.
//     ! The coordinates should be in A.
//
//========================================================================
//========================================================================
bool read_xyz_step( std::ifstream &ifs, std::vector<std::string> &symb, std::vector<Vector<double>> &r )
{
    symb.clear();
    r.clear();
    
    // JMM: I don't think this is the best error check ...
    unsigned int na;
    if( !(ifs >> na) )
    {
        return false;
    }
    
    std::string commment;
    //ifs >> na;
    ifs >> commment;
    
    for( unsigned int i = 0; i < na; ++i )
    {
        std::string    symbi;
        Vector<double> ri;

        ifs >> symbi;
        ifs >> ri.x;
        ifs >> ri.y;
        ifs >> ri.z;
        
        symb.push_back( symbi );
        r.push_back( ri );
    }
    
    return true;
}

