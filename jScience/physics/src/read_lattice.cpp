/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with SimStats. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 10/28/2014
// LAST UPDATE: 10/28/2014


// STL
#include <fstream> // std::ifstream, std::ios::in
#include <string>  // std::string
#include <vector>  // std::vector

// jScience
#include "jScience/geometry/Vector.hpp" // Vector, dot_product()


//========================================================================
//========================================================================
//
// NAME: void read_lattice( const std::string filename, std::vector<Vector<double>> &a )
//
// DESC: Reads in a lattice, as specified by:
//
//          alat
//            a(0).x  a(0).y  a(0).z
//            a(1).x  a(1).y  a(1).z
//            a(2).x  a(2).y  a(2).z
//
// NOTES:
//     i. alat should by in units of A, and a() should be in units of alat
//
//========================================================================
//========================================================================
void read_lattice( const std::string filename, std::vector<Vector<double>> &a )
{
    std::ifstream ifs( filename, std::ios::in );
    // JMM: should check here that the file is open ...
    
    a.resize(3);
    
    double alat;
    ifs >> alat;

    for( int i = 0; i < 3; ++i )
    {
        ifs >> a[i].x;
        ifs >> a[i].y;
        ifs >> a[i].z;
        
        a[i] *= alat;
    }
    
    ifs.close();
}
