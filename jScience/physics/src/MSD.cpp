/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/18/2014
// LAST UPDATE: 7/18/2014


// STL
#include <vector> // std::vector

// jScience
#include "jScience/geometry/Vector.hpp" // Vector, dot_product()
#include "jScience/physics/lattice.hpp" // rij_min_img()


//========================================================================
//========================================================================
//
// NAME: double MSD( const std::vector<Vector<double>> &r, const std::vector<Vector<double>> &r0, const std::vector<Vector<double>> &a )
//
// DESC: Returns the mean squared displacement of a collection of particles (r) from their lattice positions (r0), given the primitive vectors a, in the minimum-image convention.
//
//========================================================================
//========================================================================
double MSD( const std::vector<Vector<double>> &r, const std::vector<Vector<double>> &r0, const std::vector<Vector<double>> &a )
{
    if( r.size() != r0.size() )
    {
        // << JMM: throw error, don't just exit! >>
        exit(0);
    }
    
    double sum = 0.0;
    
    for( decltype(r.size()) i = 0; i < r.size(); ++i )
    {
        Vector<double> r_minus_r0 = rij_min_img(r0[i], r[i], a);
        
        sum += dot_product(r_minus_r0, r_minus_r0);
    }
    
    return ( sum/static_cast<double>(r.size()) );
}
