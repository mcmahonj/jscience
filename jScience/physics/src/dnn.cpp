/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/18/2014
// LAST UPDATE: 7/18/2014


// STL
#include <limits> // std::numeric_limits
#include <vector> // std::vector

// jScience
#include "jstats.hpp"                   // mean()
#include "jScience/geometry/Vector.hpp" // Vector, magnitude()
#include "jScience/physics/lattice.hpp" // rij_min_img()


//========================================================================
//========================================================================
//
// NAME: double dnn( const std::vector<Vector<double>> &r, const std::vector<Vector<double>> &a )
//
// DESC: Given a set of particle coordinates (r) and lattice primitive vectors (a), calculate the average nearest-neighbor separation, in the minimum image concention.
//
//========================================================================
//========================================================================
double dnn( const std::vector<Vector<double>> &r, const std::vector<Vector<double>> &a )
{
    std::vector<double> min_rij(r.size(), std::numeric_limits<double>::max());
    
    for( decltype(r.size()) i = 0; i < r.size(); ++i )
    {
        for( decltype(r.size()) j = 0; j < r.size(); ++j )
        {
            if( i == j )
            {
                continue;
            }
            
            Vector<double> rij = rij_min_img(r[i], r[j], a);
            
            min_rij[i] = std::min( magnitude(rij), min_rij[i] );
        }
    }
    
    return mean(min_rij);
}
