/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 10/7/2013
// LAST UPDATE: 6/16/2014


template<class T>
Tensor<T>::Tensor() {};

template<class T>
Tensor<T>::Tensor(T txx, T txy, T txz, T tyx, T tyy, T tyz, T tzx, T tzy, T tzz)
{
    xx = txx;
    xy = txy;
    xz = txz;
    
    yx = tyx;
    yy = tyy;
    yz = tyz;
    
    zx = tzx;
    zy = tzy;
    zz = tzz;
}

template<class T>
Tensor<T>::~Tensor() {};
