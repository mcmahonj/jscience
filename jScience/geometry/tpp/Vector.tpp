/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/13/2014
// LAST UPDATE: 6/25/2014

#include "jScience/geometry/Vector.hpp"

// STL
#include <cmath>     // std::sqrt
#include <complex>   // std::complex


template<typename T>
Vector<T>::Vector() {};

template<typename T>
Vector<T>::Vector(T vx, T vy, T vz)
{
    x = vx;
    y = vy;
    z = vz;
}

template<typename T>
Vector<T>::~Vector() {};


template<typename T>
Vector<T>& Vector<T>::operator+=( const Vector<T> &other )
{
    x += other.x;
    y += other.y;
    z += other.z;
    
    return (*this);
}

template<typename T>
Vector<T>& Vector<T>::operator-=( const Vector<T> &other )
{
    x -= other.x;
    y -= other.y;
    z -= other.z;
    
    return (*this);
}

/*
template<typename T>
template<typename Tb>
Vector<T>& Vector<T>::operator*=( Tb b )
{
    x *= b;
    y *= b;
    z *= b;
    
    return (*this);
}
*/

template<typename T>
Vector<T>& Vector<T>::operator*=( T b )
{
    x *= b;
    y *= b;
    z *= b;
    
    return (*this);
}

template<typename T>
Vector<T>& Vector<T>::operator/=( T b )
{
    x /= b;
    y /= b;
    z /= b;
    
    return (*this);
}

template<typename T>
Vector<T> operator+( Vector<T> a, const Vector<T> &b )
{
    return ( a += b );
}

template<typename T>
Vector<T> operator-( Vector<T> a, const Vector<T> &b )
{
    return ( a -= b );
}

/*
template<typename T, typename Tb>
Vector<T> operator*( Tb b, Vector<T> a )
{
    return ( a *= b );
}
*/

/*
template<typename T, typename Tb>
Vector<T> operator*( Vector<T> a, Tb b )
{
    return ( b*a );
}
*/

template<typename T>
Vector<T> operator*( T a, Vector<T> b )
{
    return ( b *= a );
}

template<typename T>
Vector<T> operator*( Vector<T> a, T b )
{
    return ( a *= b );
}


template<typename T>
Vector<T> operator/( Vector<T> a, T b )
{
    return ( a /= b );
}


template<typename T>
double magnitude( const Vector<T> &v )
{
    return std::sqrt( dot_product(v, v) );
}

template<typename T>
Vector<T> real( Vector<std::complex<T>> v )
{
    return Vector<T>( std::real(v.x), std::real(v.y), std::real(v.z) );
}

template<typename T>
Vector<T> imag( Vector<std::complex<T>> v )
{
    return Vector<T>( std::imag(v.x), std::imag(v.y), std::imag(v.z) );
}

template<typename T>
Vector<std::complex<T>> conj( Vector<std::complex<T>> v )
{
    return Vector<std::complex<T>>( std::conj(v.x), std::conj(v.y), std::conj(v.z) );
}

template<typename T>
T dot_product( const Vector<T> &a, const Vector<T> &b )
{
    return ( a.x*b.x + a.y*b.y + a.z*b.z );
}

template<typename T>
Vector<T> cross_product( const Vector<T> &a, const Vector<T> &b )
{
    return Vector<T>( (a.y*b.z - a.z*b.y), (a.z*b.x - a.x*b.z), (a.x*b.y - a.y*b.x) );
}
