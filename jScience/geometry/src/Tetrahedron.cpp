/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/13/2014
// LAST UPDATE: 6/15/2014

#include "jScience/geometry/Tetrahedron.hpp"

// STL
#include <cmath>                  // std::fabs

// jScience
#include "jScience/geometry/Vector.hpp"   // Vector


Tetrahedron::Tetrahedron() : Polyhedron()
{
    V.resize(4, Vector<double>(0.0, 0.0, 0.0));
}


//========================================================================
//========================================================================
//
// NAME: double Tetrahedron::volume()
//
// DESC: Returns the volume of a tetrahedron.
//
// NOTES:
//     ! See:
//          http://en.wikipedia.org/wiki/Tetrahedron#Volume
//
//========================================================================
//========================================================================
double Tetrahedron::volume()
{
    return( std::fabs( dot_product( (V[0] - V[3]), cross_product( (V[1] - V[3]), (V[2] - V[3]) ) ) )/6.0 );
}
