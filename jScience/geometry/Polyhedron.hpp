/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/13/2014
// LAST UPDATE: 6/14/2014

#ifndef POLYHEDRON_H
#define POLYHEDRON_H


// STL
#include <vector>              // std::vector

// jScience
#include "jScience/geometry/Solid.hpp"  // Vector
#include "jScience/geometry/Vector.hpp" // Vector


//=========================================================
// NAME: Polyhedron
// DESC: A polyhedron (solid in 3D with flat faces, straight edges, and vertices).
//=========================================================
class Polyhedron : public Solid
{
    
public:
   
    std::vector<Vector<double>> V;
   
    
    Polyhedron();
    Polyhedron( const std::vector<Vector<double>> &vtx );
    virtual ~Polyhedron() {};
    
};


#endif
