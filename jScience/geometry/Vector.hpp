/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/13/2014
// LAST UPDATE: 8/19/2014

#ifndef VECTOR_H
#define VECTOR_H


// STL
#include <complex> // std::complex
#include <vector>  // std::vector


//=========================================================
// NAME: Vector<>
// DESC: A 3D vector class.
//=========================================================
template<typename T>
class Vector
{
    
public:
    
    T x;
    T y;
    T z;
    
    Vector();
    Vector(T vx, T vy, T vz);
    ~Vector();
    
    Vector<T>& operator+=( const Vector<T> &other );
    Vector<T>& operator-=( const Vector<T> &other );
    
/*
    template<typename Tb>
    Vector<T>& operator*=( Tb b );
*/    
    Vector<T>& operator*=( T b );
    Vector<T>& operator/=( T b );
};


template<typename T>
Vector<T> operator+( Vector<T> a, const Vector<T> &b );

template<typename T>
Vector<T> operator-( Vector<T> a, const Vector<T> &b );

/*
template<typename T, typename Tb>
Vector<T> operator*( Tb b, Vector<T> a );
*/
  
template<typename T>
Vector<T> operator*( T a, Vector<T> b );
    
template<typename T>
Vector<T> operator*( Vector<T> a, T b );
    
inline Vector<std::complex<double>> operator*( std::complex<double> b, Vector<double> a )
{
    return Vector<std::complex<double>>( (b*a.x), (b*a.y), (b*a.z) );
}
    
inline Vector<std::complex<double>> operator/( Vector<std::complex<double>> a, double b )
{
    return Vector<std::complex<double>>( (a.x/b), (a.y/b), (a.z/b) );
}

template<typename T>
Vector<T> operator/( Vector<T> a, T b );

/*
template<typename T, typename Tb>
Vector<T> operator*( Vector<T> a, Tb b );
*/

template<typename T>
double magnitude( const Vector<T> &v );

template<typename T>
Vector<T> real( Vector<std::complex<T>> v );

template<typename T>
Vector<T> imag( Vector<std::complex<T>> v );

template<typename T>
Vector<std::complex<T>> conj( Vector<std::complex<T>> v );
    
template<typename T>
T dot_product( const Vector<T> &a, const Vector<T> &b );

template<typename T>
Vector<T> cross_product( const Vector<T> &a, const Vector<T> &b );


#include "tpp/Vector.tpp"

    
#endif



