#ifndef JSCIENCE_MATH_UTILITY_HPP
#define JSCIENCE_MATH_UTILITY_HPP


// STL
#include <tuple>        // std::tuple<>
#include <vector>       // std::vector<>


double deg2rad(
               const double deg
               );

double rad2deg(
               const double rad
               );

// TODO: should this go in sampling in stats++?
std::tuple<
           std::vector<int>,
           std::vector<int>,
           std::vector<int>
           > ngroups(const int x, const int n);


#endif

