//
// DESC: Kronecker delta.
//
// INPUT:
//
//     delta_ij = 0 if i != j
//                1 if i == j
//
int Kronecker_delta(
                    const int i,
                    const int j
                    )
{
    if(i != j)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
