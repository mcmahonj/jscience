// TODO: there are *much* faster algoriths than this naive one --- e.g., see: https://en.wikipedia.org/wiki/Baillie%E2%80%93PSW_primality_test

// STL
#include <cmath> // std::sqrt()


bool is_prime(
              const int n
              )
{
    if(n <= 1)
    {
        // NOTE: 1 *is not* a prime number
        
        return false;
    }
    else if(n == 2)
    {
        // NOTE: 2 *is* a prime number
        
        return true;
    }
    else if(n%2 == 0)
    {
        // NOTE: any number > 2 and divisible by 2 is (obviously) prime
        
        // NOTE: this check could be merged into the for loop below, but this saves us a couple of operations
        
        return false;
    }
    else
    {
        int max_i = static_cast<int>(std::sqrt(static_cast<double>(n)) + 1.);
        
        // NOTE: only odd divisors need to be checkes
        for(int i = 3; i <= max_i; i+=2)
        {
            if(n%i == 0)
            {
                return false;
            }
        }
        
        return true;
    }
}