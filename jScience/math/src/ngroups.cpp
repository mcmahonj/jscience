/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 9/8/2015
// LAST UPDATE: 9/8/2015


// STL
#include <tuple>        // std::tuple<>
#include <vector>       // std::vector<>


//========================================================================
//========================================================================
//
// NAME: std::tuple<
//                  std::vector<int>,
//                  std::vector<int>,
//                  std::vector<int>
//                  > ngroups(const int x, const int n)
//
// DESC: Divides a number x into n groups, distributing remainders equally.
//
//========================================================================
//========================================================================
std::tuple<
           std::vector<int>,
           std::vector<int>,
           std::vector<int>
           > ngroups(const int x, const int n)
{
    std::vector<int> begin(n);
    std::vector<int> end(n);
    // size[] is declared below, so that it can be initialized
    
    int nper = x/n;
    int nrem = x%n;
    
    std::vector<int> size(n, nper);
    
    for(int i = 0; i < n; ++i)
    {
        if(i == 0)
        {
            begin[i] = 0;
        }
        else
        {
            begin[i] = end[i-1] + 1;
        }
        
        end[i] = begin[i] + nper - 1;
        
        // uniformly distribute remaining pieces
        if(i < nrem)
        {
            ++end[i];
            ++size[i];
        }
    }
    
    return std::make_tuple(begin, end, size);
}
