// STL
#include <cmath> // log10


//
// DESC: Calculate the exponent of a number.
//
// INPUT:
//
//     x ::
//
// OUTPUT:
//
//     e(+/-)n
//
int exponent(
             const double x
             )
{
    double y = log10(x);

    if( y < 0. )
    {
        y -= 1.;
    }

    return ( static_cast<int>(y) );
}
