//
// DESC: Heaviside step function.
//
// INPUT:
//
//     int convention :: convention for zero argument
//                       0 == H(0) == 1/2 (often used)
//                       1 == H(0) == 1   (when H needs to be right-continuous)
//                       2 == H(0) == 0   (when H needs to be left-continuous)
//
double hsf(
           const double x,
           const int    convention
           )
{
    if(x > 0.)
    {
        return 1.;
    }
    else if(x < 0.)
    {
        return 0.;
    }
    else
    {
        switch(convention)
        {
        case 1:
            return 1.;
        case 2:
            return 0.;
        case 0:
        default:
            return 0.5;
        }
    }
}
