// this
#include "jScience/physics/consts.hpp" // PI


double deg2rad(
               const double deg
               )
{
    return (PI*deg/180.);
}
