// this
#include "jScience/physics/consts.hpp" // PI


double rad2deg(
               const double rad
               )
{
    return (180.*rad/PI);
}
