/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/9/2014
// LAST UPDATE: 7/9/2014

#ifndef BESSEL_H
#define BESSEL_H


//========================================================================
// NAME: template<typename T>
//       T cyl_bessel_j( const int v, const T x )
// DESC: Bessel function of the first kind.
//========================================================================
template<typename T>
T cyl_bessel_j( const int v, const T x );
#include "tpp/cyl_bessel_j.tpp"

//========================================================================
// NAME: template<typename T>
//       T cyl_dbessel_j( const int v, const T x )
// DESC: Derivative of the Bessel function of the first kind.
//========================================================================
template<typename T>
T cyl_dbessel_j( const int v, const T x );
#include "tpp/cyl_dbessel_j.tpp"


#endif
