/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/9/2014
// LAST UPDATE: 7/9/2014

#include "jScience/math/Neumann.hpp"

// STL
#include <cmath>     // std::abs, std::pow
#include <stdexcept> // std::domain_error

// jScience
#include "jScience/math/Bessel.hpp"    // cyl_bessel_j
#include "jScience/math/factorial.hpp" // factorial()
#include "jScience/physics/consts.hpp" // PI, EULERC


static double hs( const unsigned int s );

#include <iostream>
//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       T cyl_neumann( const int v, const T x )
//
// DESC: Bessel function of the second kind (also known as a Neumann function).
//
// NOTES:
//     ! The Bessel function of the second kind is defined as ...
//			Yv(x) = (2.0*Jv(x)/pi)*(ln(x/2) + gamma_e)
//				+ (x^v/pi)*sum{m=0, m=inf}[(((-1)^(m-1))*(hs(m) +
//					hs(m+n))*x^(2*m))/((2.0^(2*m+n))*m!*(m+n)!)]
//				- (x^-v/pi)*sum{m=0, m=(n-1)}[((n-m-1)!*x^(2*m))/((2.0^(2*m-n))*m!)]
//     ... which converges for all arguments, given enough terms
//     ! See:
//          - http://en.wikipedia.org/wiki/Bessel_function
//          - The included slides in articles/ for difficulties in checking for convergence)
//     ! Y-v = ((-1)^v)*Yv
//
//========================================================================
//========================================================================
template<typename T>
T cyl_neumann( const int v, const T x )
{
    if( x == T(0) )
    {
        throw std::domain_error( "The Hankel function is singular at x = 0." );
    }
    
    const unsigned int max_m = 20;
    
    //*********************************************************
    
    T Yv = T(0);
    
    unsigned int absv = std::abs(v);
    
    T x2  = x*x;
    
    T      pow_x   = static_cast<T>(1.0);
    double pow_2pn = std::pow(2.0, absv);
    double pow_2mn = std::pow(2.0, -static_cast<int>(absv));
    
    // << JMM: A convergence check would be better here, but how to do it? >>
    T sum1 = T(0);
    T sum2 = T(0);
    for( unsigned int m = 0; m <= max_m; ++m )
    {
        if( m != 0 )
        {
            pow_x   *= x2;
            pow_2pn *= 4.0;
        }

        if( ((m-1) % 2) != 0 ) // (-1)^(m-1)
        {
            sum1 -= (hs(m) + hs(m+absv))*pow_x/(pow_2pn*static_cast<double>(factorial(m))*static_cast<double>(factorial(m+absv)));
        }
        else
        {
            sum1 += (hs(m) + hs(m+absv))*pow_x/(pow_2pn*static_cast<double>(factorial(m))*static_cast<double>(factorial(m+absv)));
        }
        
        if( m < absv )
        {
            if( m != 0 )
            {
                pow_2mn *= 4.0;
            }
            
            sum2 += static_cast<double>(factorial(absv - m - 1))*pow_x/(pow_2mn*static_cast<double>(factorial(m)));
        }
    }
    
    sum1 *= std::pow(x, absv)/PI;
    sum2 *= std::pow(x, -static_cast<int>(absv))/PI;
    
    // note: cyl_bessel_j must be called with |v| here
    Yv = (2.0*cyl_bessel_j(absv, x)/PI)*(std::log(x/2.0) + EULERC) + sum1 - sum2;
    
    // Y-v = ((-1)^v)*Yv
    if( (v < 0) && ((v % 2) != 0) )
    {
        Yv *= -1.0;
    }
    
    return Yv;
}

//========================================================================
//========================================================================
//
//	NAME: static double hs( const unsigned int s )
//
//	DESC: Calculates the hs function for use in the calculation of the Bessel functions of the second kind.
//
//	NOTES:
//     ! The hs function is calculated as ...
//          hs = 1/1 + 1/2 + ... + 1/s
//     ... with hs(0) = 0.0
//
//========================================================================
//========================================================================
static double hs( const unsigned int s )
{
    double sum = 0.0;
    
    for( unsigned int i = 1; i <= s; ++i )
    {
        sum += 1.0/static_cast<double>(i);
    }
    
    return sum;
}
