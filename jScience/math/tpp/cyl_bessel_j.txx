/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/8/2014
// LAST UPDATE: 7/9/2014

#include "jScience/math/Bessel.hpp"

// jScience
#include "jScience/math/factorial.hpp" // factorial()


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       T cyl_bessel_j( const int v, const T x )
//
// DESC: Bessel function of the first kind.
//
// NOTES:
//     ! The Bessel function of the first kind is defined as ...
//			Jv(x) = sum{m=0, m=inf}[(-1)^m*(x/2)^(2*m+v)/(m!*gamma(m+v+1))]
//     ... which converges for all arguments, given enough terms
//     ! See:
//          - http://en.wikipedia.org/wiki/Bessel_function
//          - The included slides in articles/ for difficulties in checking for convergence)
//     ! J-v = ((-1)^v)*Jv
//
//========================================================================
//========================================================================
template<typename T>
T cyl_bessel_j( const int v, const T x )
{
    const unsigned int max_m = 10;
    
    //*********************************************************
    
    T Jv = T(0);
    
    if( x == T(0) )
    {
        if( v == 0 )
        {
            return static_cast<T>(1.0);
        }
        else
        {
            return static_cast<T>(0.0);
        }
    }
    
    unsigned int absv = std::abs(v);
    
    T x_2  = x/2.0;
    T x_22 = x_2*x_2;
    
    T pow_x_2 = std::pow(x_2, absv);
    
    // << JMM: A convergence check would be better here, but how to do it? >>
    for( unsigned int m = 0; m <= max_m; ++m )
    {
        if( m != 0 )
        {
            pow_x_2 *= x_22;
        }

        if( (m % 2) != 0 ) // (-1)^m
        {
            // note: in the denominator we must calculate gamma(m + absv +1), but this is just (m + absv)!
            Jv -= pow_x_2/( static_cast<double>(factorial(m))*static_cast<double>(factorial(m + absv)) );
        }
        else
        {
            Jv += pow_x_2/( static_cast<double>(factorial(m))*static_cast<double>(factorial(m + absv)) );
        }
    }
    
    // J-v = ((-1)^v)*Jv
    if( (v < 0) && ((v % 2) != 0) )
    {
        Jv *= -1.0;
    }
    
    return Jv;
}