/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/8/2014
// LAST UPDATE: 7/8/2014

#include "jScience/math/Hankel.hpp" // cyl_hankel_1

// STL
#include <complex>   // std::complex
#include <stdexcept> // std::domain_error


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::complex<double> cyl_dhankel_1( const int v, const T x )
//
// DESC: Derivative of the Hankel function of the first kind.
//
// NOTES:
//     ! The derivative of the Hankel function is defined identically to the derivative of the Bessel function:
//          Hdv(x) = Hv-1(x) - v*Hv(x)/x
//     ! There is a more general equation that does not take the derivative with respect to the entire argument
//
//========================================================================
//========================================================================
template<typename T>
std::complex<double> cyl_dhankel_1( const int v, const T x )
{
    if( x == T(0) )
    {
        throw std::domain_error( "The Hankel function is singular at x = 0." );
    }
    
    return (cyl_hankel_1((v-1), x) - static_cast<double>(v)*cyl_hankel_1(v, x)/x);
}
