/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/8/2014
// LAST UPDATE: 7/9/2014

#include "jScience/math/Bessel.hpp" // cyl_bessel_j


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       T cyl_dbessel_j( const int v, const T x )
//
// DESC: Derivative of the Bessel function of the first kind.
//
// NOTES:
//     ! The derivative of the Bessel function is defined as:
//          Jdv(x) = Jv-1(x) - v*Jv(x)/x
//
//========================================================================
//========================================================================
template<typename T>
T cyl_dbessel_j( const int v, const T x )
{
    T Jdv = cyl_bessel_j((v-1), x);
    
    // note: there are two options here if x = T(0), which give the same result:
    //     0*J0(0)/0 = 0, because n = 0
    //     v*Jv(0)/0 = 0, because Jv(0) = 0
    if( x != T(0) )
    {
        Jdv -= static_cast<T>(v)*cyl_bessel_j(v, x)/x;
    }
    
    return Jdv;
}
