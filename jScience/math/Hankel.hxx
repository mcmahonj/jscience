/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 7/9/2014
// LAST UPDATE: 7/9/2014

#ifndef HANKEL_H
#define HANKEL_H


// STL
#include <complex>   // std::complex


//========================================================================
// NAME: template<typename T>
//       std::complex<double> cyl_hankel_1( const int v, const T x )
// DESC: Hankel function of the first kind.
//========================================================================
template<typename T>
std::complex<double> cyl_hankel_1( const int v, const T x );
#include "tpp/cyl_hankel_1.tpp"

//========================================================================
// NAME: template<typename T>
//       std::complex<double> cyl_dhankel_1( const int v, const T x )
// DESC: Derivative of the Hankel function of the first kind.
//========================================================================
template<typename T>
std::complex<double> cyl_dhankel_1( const int v, const T x );
#include "tpp/cyl_dhankel_1.tpp"

// i. the Hankel functions of the first and second kind are defined as:
//			Hn_1(z) = Jn(z) + jYn(z)
//			Hn_2(z) = Jn(z) - jYn(z)


#endif
