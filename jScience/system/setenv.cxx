// STL
#include <cstdlib>  // std::getenv
#include <stdlib.h> // getenv_s()

#include <stdio.h>
#include <stdlib.h>
int setenv(
           const char *name,
           const char *value,
           const int   overwrite
           )
{
    int errcode = 0;

    if( !overwrite )
    {
        size_t envsize = 0;

        errcode = getenv_s(&envsize, NULL, 0, name);

        if( errcode || envsize )
        {
            return errcode;
        }
    }

    return _putenv_s(name, value);
}
