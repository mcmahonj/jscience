/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/29/2014
// LAST UPDATE: 12/31/2014


// STL
#include <algorithm>               // std::shuffle
#include <vector>                  // std::vector

// jScience
#include "jScience/stats/prng.hpp" // g_prng, g_prng_seeded, seed_prng()


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       void shuffle_data(std::vector<T> &data)
//
// DESC: Shuffle a vector of data.
//
//========================================================================
//========================================================================
template<typename T>
void shuffle_data(std::vector<T> &data)
{
    if( !g_prng_seeded )
    {
        seed_prng();
    }
    
    std::shuffle(data.begin(), data.end(), g_prng);
}