/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/29/2014
// LAST UPDATE: 12/30/2014


// STL
#include <vector>                  // std::vector

// jScience
#include "jScience/stats/prng.hpp" // g_prng, g_prng_seeded, seed_prng()


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<T> sample_data( const bool wreplace, const std::vector<T> &data, const unsigned int n )
//
// DESC: Sample data with or without replacement.
//
// NOTES:
//     ! This is essentially a wrapper routine.
//
//========================================================================
//========================================================================
template<typename T>
std::vector<T> sample_data( const bool wreplace, const std::vector<T> &data, const unsigned int n )
{
    std::vector<T> samples;
    
    if( wreplace )
    {
        samples = sample_data_wreplace(data, n);
    }
    else
    {
        samples = sample_data_woreplace(data, n);
    }
    
    return samples;
}