/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/29/2014
// LAST UPDATE: 12/31/2014


// STL
#include <algorithm>    // std::min_element
#include <cmath>        // std::round, std::floor
#include <iterator>     // std::begin, std::end
#include <stdexcept>    // std::invalid_argument()
#include <vector>       // std::vector

// jScience
#include "jScience/stl/vector.hpp"   // magnitude()
#include "jstats.hpp"   // sum()
#include "jutility.hpp" // sort_indices_desc()


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<std::vector<T>> partition_data( const std::vector<T> &data, std::vector<double> f )
//
// DESC: Partition data according to the fraction(s) f.
//
// INPUT:
//     std::vector<T> data   : Data to be partitioned [0,...,(nd-1)]
//          (nd == number of data points)
//     std::vector<double> f : The size of the partions [0,...,(np-1)]
//          (np == number of partitions)
//
// OUTPUT:
//     std::vector<std::vector<T>> partitions : Partitioned data [0,...,(np-1)][0,...,(ndp-1)]
//          (ndp == number data points in a given partitions)
//
// NOTES:
//     ! The number of partitions is inferred from the size of f.
//     ! f is normalized in this routine, it need *not* sum to 1, as passed.
//
//========================================================================
//========================================================================
template<typename T>
std::vector<std::vector<T>> partition_data( const std::vector<T> &data, std::vector<double> f )
{
    std::vector<std::vector<T>> partitions(f.size());
 
    // CHECK/NORMALIZE f
    if( *std::min_element( std::begin(f), std::end(f) ) < 0.0 )
    {
        throw std::invalid_argument( "partition_data(): Received request for a negative partition" );
    }
    
    if( sum(f) <= 0.0 )
    {
        throw std::invalid_argument( "partition_data(): Sum of partitions is <= 0.0" );
    }
    
    f /= sum(f);
    
    // CALCULATE PARTITION SIZES
    
    // first calculate a lower bound to each partition size ...
    std::vector<unsigned int> n;
    std::vector<double>       nrem;
    
    for( auto &fi : f )
    {
        double       di = fi*data.size();
        // std::floor() is *not* needed, since static_cast<> truncates; it does make the intent clearer
        unsigned int ni = static_cast<unsigned int>( std::floor(di) );
        
        n.push_back( ni );
        nrem.push_back( di - static_cast<double>(ni) );
    }
    
    // ... and then add remaining points
    unsigned int nt = sum(n);
    
    if( nt != data.size() )
    {
        std::vector<size_t> indx = sort_indices_desc(nrem);
        
        for( unsigned int i = 0; i < (data.size() - nt); ++i )
        {
            ++n[indx[i]];
        }
    }
    
    // ASSIGN DATA TO THE PARTITIONS
    unsigned int cntr = 0;
    
    for( decltype(n.size()) i = 0; i < n.size(); ++i  )
    {
        for( unsigned int j = 0; j < n[i]; ++j )
        {
            partitions[i].push_back( data[cntr] );
            
            ++cntr;
        }
    }
    
    return partitions;
}