/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 1/10/2015
// LAST UPDATE: 1/12/2015


// STL
#include <vector>     // std::vector

// jScience
#include "jScience/stl/vector.hpp" // transpose()
#include "jstats.hpp"              // mean(), stddev()


//========================================================================
//========================================================================
//
// NAME: std::vector<std::vector<double>> covariance_matrix( const std::vector<std::vector<double>> &x )
//
// DESC: Computes the *sample* covariance matrix.
//
// INPUT:
//          std::vector<std::vector<double>> x :: A 2D vector containing N observations of K variables [0,...,N][0,...,K]       
//
// NOTES:
//     ! See: http://en.wikipedia.org/wiki/Sample_mean_and_covariance
//          
//
//========================================================================
//========================================================================
std::vector<std::vector<double>> covariance_matrix( const std::vector<std::vector<double>> &x )
{
    auto N = x.size();
    auto K = x[0].size();
    
    std::vector<std::vector<double>> Q(K, std::vector<double>(K));
    
    // precompute the means
    std::vector<double> xmean;
    
    std::vector<std::vector<double>> xT = transpose(x);

    for( auto &xTi : xT )
    {
        xmean.push_back( mean(xTi) );
    }
    
    for( decltype(x[0].size()) j = 0; j < K; ++j )
    {
        for( decltype(x[0].size()) k = j; k < K; ++k )
        {
            Q[j][k] = 0.0;
            
            for( decltype(x.size()) i = 0; i < N; ++i )
            {
                Q[j][k] += (x[i][j] - xmean[j])*(x[i][k] - xmean[k]);
            }
            
            Q[j][k] /= static_cast<double>(N-1);
            
            Q[k][j] = Q[j][k];
        }
    }
    
    return Q;
}
