/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 1/12/2015
// LAST UPDATE: 1/12/2015


// STL
#include <vector>     // std::vector

// jScience
#include "jScience/stats/stats.hpp" // covariance_matrix()
#include "jScience/stl/vector.hpp"  // transpose()
#include "jstats.hpp"               // mean(), stddev()


//========================================================================
//========================================================================
//
// NAME: std::vector<std::vector<double>> covariance_matrix( const std::vector<std::vector<double>> &x )
//
// DESC: Given a *sample* of
//
//========================================================================
//========================================================================
std::vector<std::vector<double>> correlation_matrix( const std::vector<std::vector<double>> &x )
{
    std::vector<std::vector<double>> Q = covariance_matrix(x);
    
    // precompute the means
    std::vector<double> xsd;
    
    std::vector<std::vector<double>> xT = transpose(x);
    
    for( auto &xTi : xT )
    {
        xsd.push_back( stddev(xTi) );
    }
    
    for( decltype(Q.size()) j = 0; j < Q.size(); ++j )
    {
        for( decltype(Q.size()) k = j; k < Q.size(); ++k )
        {
            Q[j][k] /= (xsd[j]*xsd[k]);
            
            Q[k][j] = Q[j][k];
        }
    }
    
    return Q;    
}
