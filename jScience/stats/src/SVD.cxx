/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 1/12/2015
// LAST UPDATE: 1/12/2015


// STL
#include <utility>  // std::pair<>, std::make_pair()
#include <vector>   // std::vector<>

// This
#include "jScience/stats/wrappers.hpp" // SVD()


//========================================================================
//========================================================================
//
// NAME: std::tuple<Matrix, std::vector<double>, Matrix> SVD( const DataMatrix &X )
//
// DESC: Wrapper to SVD(), taking a DataMatrix, instead of a Matrix.
//
// *see jScience/linalg/decomposition.hpp for notes*
//
//========================================================================
//========================================================================
std::tuple<Matrix, std::vector<double>, Matrix> SVD( const DataMatrix &X )
std::pair<Matrix> PCA( const DataMatrix &X )
{
//    Matrix XT = transpose(X);
    
    Matrix U, VT;
    std::vector<double> S;
    std::tie(U, S, VT) = SVD(X);
    
    return std::make_pair(VT, S);
}