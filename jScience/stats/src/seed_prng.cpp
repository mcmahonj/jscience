/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/29/2014
// LAST UPDATE: 12/29/2014


// STL
#include <algorithm>  // std::generate_n
#include <array>      // std::array
//#include <cstdint>  // std::uint_fast64_t
#include <functional> // std::ref
#include <random>     // std::mt19937_64, std::random_device, std::seed_seq

// jScience
#include "jScience/stats/prng.hpp"


//========================================================================
//========================================================================
//
// NAME: void seed_prng()
//
// DESC: Loads up the seed sequence with enough random data to fill the std::mt19937(_64) state.
//
// NOTES:
//     ! See:
//          http://stackoverflow.com/questions/24334012/best-way-to-seed-mt19937-64-for-monte-carlo-simulations
//
//========================================================================
//========================================================================
void seed_prng()
{
    std::array<int, std::mt19937_64::state_size> seed_data;
    std::random_device rd;
    std::generate_n(seed_data.data(), seed_data.size(), std::ref(rd));
    std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
    
//    // std::random_device returns an unsigned int (32 bits), so we must call it twice to get 64 bits
//    std::random_device rd;
//    std::uint_fast64_t seed = rd();
//    seed = (seed << 32) | rd();
    
//    g_prng.seed(seed);
    
    g_prng.seed(seq);
    
    g_prng_seeded = true;
}
