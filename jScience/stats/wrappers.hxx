/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 1/12/2015
// LAST UPDATE: 1/12/2015

#ifndef STATS_WRAPPERS_H
#define STATS_WRAPPERS_H


// STL
#include <tuple>   // std::tuple<>
#include <vector>  // std::vector<>

// This
#include "jScience/stl/Matrix.hpp"       // Matrix
#include "jScience/stats/DataMatrix.hpp" // DataMatrix


//========================================================================
//========================================================================
//
// NAME: std::tuple<Matrix, std::vector<double>, Matrix> SVD( const DataMatrix &X )
//
// DESC: Wrapper to SVD(), taking a DataMatrix, instead of a Matrix.
//
// *see jScience/linalg/decomposition.hpp for notes*
//
//========================================================================
//========================================================================
std::tuple<Matrix, std::vector<double>, Matrix> SVD( const DataMatrix &X );


#endif

