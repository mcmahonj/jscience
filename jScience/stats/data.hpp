/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/29/2014
// LAST UPDATE: 12/29/2014

#ifndef DATA_H
#define DATA_H


// STL
#include <vector> // std::vector


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       void shuffle_data(std::vector<T> &data)
//
// DESC: Shuffle a vector of data.
//
//========================================================================
//========================================================================

// TODO: shuffle_data() (as all of jScience stats) should go to stats++
// TODO: a whole set of subroutines for shuffle stuff should be developed (easily setup a shuffle index, shuffle data, call all appropriate headers, etc.)

template<typename T>
void shuffle_data(std::vector<T> &data);
#include "tpp/shuffle_data.tpp"


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<T> sample_data( const bool wreplace, const std::vector<T> &data, const unsigned int n )
//
//       template<typename T>
//       std::vector<T> sample_data_wreplace( const std::vector<T> &data, const unsigned int n )
//
//       template<typename T>
//       std::vector<T> sample_data_woreplace( const std::vector<T> &data, const unsigned int n )
//
// DESC: Sample data with or without replacement.
//
//========================================================================
//========================================================================
template<typename T>
std::vector<T> sample_data( const bool wreplace, const std::vector<T> &data, const unsigned int n );
#include "tpp/sample_data.tpp"

template<typename T>
std::vector<T> sample_data_wreplace( const std::vector<T> &data, const unsigned int n );
#include "tpp/sample_data_wreplace.tpp"

template<typename T>
std::vector<T> sample_data_woreplace( std::vector<T> data, const unsigned int n );
#include "tpp/sample_data_woreplace.tpp"


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<std::vector<T>> partition_data( const std::vector<T> &data, std::vector<double> f )
//
// DESC: Partition data according to the fraction(s) f.
//
// INPUT:
//     std::vector<T> data   : Data to be partitioned [0,...,(nd-1)]
//          (nd == number of data points)
//     std::vector<double> f : The size of the partions [0,...,(np-1)]
//          (np == number of partitions)
//
// OUTPUT:
//     std::vector<std::vector<T>> partitions : Partitioned data [0,...,(np-1)][0,...,(ndp-1)]
//          (ndp == number data points in a given partitions)
//
// NOTES:
//     ! The number of partitions is inferred from the size of f.
//     ! f is normalized internally
//
//========================================================================
//========================================================================
template<typename T>
std::vector<std::vector<T>> partition_data( const std::vector<T> &data, std::vector<double> f );
#include "tpp/partition_data.tpp"


#endif

