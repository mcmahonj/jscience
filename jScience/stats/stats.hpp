#ifndef STATS_HPP
#define STATS_HPP

// STL
#include <vector> // std::vector<>


std::vector<std::vector<double>> covariance_matrix( const std::vector<std::vector<double>> &x );

std::vector<std::vector<double>> correlation_matrix( const std::vector<std::vector<double>> &x );

#endif
