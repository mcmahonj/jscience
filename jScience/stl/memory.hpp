/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/29/2015
// LAST UPDATE: 5/29/2015

#ifndef MEMORY_HPP
#define MEMORY_HPP


// STL
#include <vector> // std::vector<>


template<typename T>
std::vector<T> rowMajOrd_to_colMajOrd(const std::vector<T> &v,
                                      const std::vector<typename std::vector<T>::size_type> &N);
#include "tpp/rowMajOrd_to_colMajOrd.tpp"

template<typename T>
std::vector<T> colMajOrd_to_rowMajOrd(const std::vector<T> &v,
                                      const std::vector<typename std::vector<T>::size_type> &N);
#include "tpp/colMajOrd_to_rowMajOrd.tpp"


#endif
