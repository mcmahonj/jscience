// STL
#include <fstream> // std::ifstream
#include <ios>     // std::streamoff
#include <string>  // std::string, std::getline()

// jScience
#include "jScience/io/io.hpp" // remove_whitespace()

// checks whether an ifs is at the end of a file
// FIXME: I think there must be a more elegant solution (ideally using just the STL), I just don't know it
inline bool is_eof(
                   std::ifstream& ifs, // ifs to check
                   const bool ell      // whether an empty line(s) signifies the end of file
                   )
{
    std::streamoff place = ifs.tellg();

    std::string tmp;

    // check for the failure to read the next line
    if( !std::getline(ifs, tmp) )
    {
        return true;
    }

    // check for a empty last line(s)
    if(ell)
    {
        while(true)
        {
            tmp = remove_whitespace(tmp);

            if(!tmp.empty())
            {
                break;
            }
            else if(!std::getline(ifs, tmp)) // NOTE: by this else statement, tmp is known to be empty
            {
                return true;
            }
        }
    }

    ifs.clear(); // clear possible eofbit
    ifs.seekg(place);

    return false;
}
