// STL
#include <string>  // std::string


//
// DESC: Determine the suffix of a filename (i.e., the last chars after a '.')
//
inline std::string suffix(
                          const std::string filename
                          )
{
    std::string suffix_r;

    int i = (filename.length()-1);

    while((i >= 0) && (filename.at(i) != '.'))
    {
        suffix_r.push_back(filename.at(i));

        --i;
    }

    std::string suffix_;
    for(int i = (suffix_r.length()-1); i >= 0; --i)
    {
        suffix_.push_back(suffix_r.at(i));
    }

    return suffix_;
}
