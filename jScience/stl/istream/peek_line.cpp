// STL
#include <fstream> // std::ifstream
#include <ios>     // std::ios_base
#include <istream> // std::tellg(), seekg()
#include <string>  // std::string, ::getline()


//
// DESC: Peek the next line of an std::ifstream.
//
inline std::string peek_line(
                             std::ifstream& ifs
                             )
{
    std::string line;

    // GET CURRENT POSITION
    int len = ifs.tellg();

    // READ LINE
    std::getline(ifs, line);

    // RETURN TO POSITION BEFORE "READ LINE"
    ifs.seekg(len, std::ios_base::beg);

    return line;
}
