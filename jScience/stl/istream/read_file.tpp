#include "jScience/stl/istream.hpp" // this, is_eof()

// STL
#include <fstream>                  // std::ifstream
#include <string>                   // std::string
#include <vector>                   // std::vector<>


//
// DESC: Reads a one-column or one-row file.
//
template<typename T>
std::vector<T> read_file(
                         const std::string filename
                         )
{
    std::vector<T> data;

    std::ifstream ifs(filename);

    while( !is_eof(ifs, true) )
    {
        T pt;

        ifs >> pt;

        data.push_back(pt);
    }

    ifs.close();

    return data;
}
