/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/15/2014
// LAST UPDATE: 6/15/2014

#ifndef PTR_H
#define PTR_H


// STL
#include <memory> // std::unique_ptr


/*
template<typename Derived, typename Base, typename Del>
std::unique_ptr<Derived, Del> static_unique_ptr_cast( std::unique_ptr<Base, Del>&& p );
#include "tpp/static_unique_ptr_cast.tpp"

template<typename Derived, typename Base, typename Del>
std::unique_ptr<Derived, Del> dynamic_unique_ptr_cast( std::unique_ptr<Base, Del>&& p );
#include "tpp/dynamic_unique_ptr_cast.tpp"
*/

template<typename Derived, typename Base>
std::unique_ptr<Derived> static_unique_ptr_cast( std::unique_ptr<Base>&& p );
#include "tpp/static_unique_ptr_cast.tpp"

template<typename Derived, typename Base>
std::unique_ptr<Derived> dynamic_unique_ptr_cast( std::unique_ptr<Base>&& p );
#include "tpp/dynamic_unique_ptr_cast.tpp"


#endif
