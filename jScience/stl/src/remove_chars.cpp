/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 3/6/2015
// LAST UPDATE: 3/6/2015


// STL
#include <algorithm> // std::remove_if()
#include <string>    // std::string, .erase(), .find()


//========================================================================
//========================================================================
//
// NAME: std::string remove_chars( std::string str, const std::string &chars )
//
// DESC: Removes a set of characters from a string.
//
//========================================================================
//========================================================================
std::string remove_chars( std::string str, const std::string &chars )
{
    // this implementation makes use of the erase--remove idiom
    str.erase(
              std::remove_if(
                             str.begin(),
                             str.end(),
                             [chars](char c)
                             {
                                 if( chars.find(c) != std::string::npos )
                                 {
                                     return true;
                                 }
                                 else
                                 {
                                     return false;
                                 }
                             }),
              str.end()
    );
    
    return str;
}