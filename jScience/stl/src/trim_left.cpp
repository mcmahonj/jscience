/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of FDTD++.
 
 FDTD++ is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 FDTD++ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with FDTD++. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 4/8/2015
// LAST UPDATE: 4/8/2015


// STL
#include <algorithm>  // std::find_if()
#include <functional> // std::bind1st()
#include <string>     // std::string, std::string::erase()


//========================================================================
//========================================================================
//
// NAME: std::string trim_right( std::string str )
//
// DESC: Remove leading spaces from a string
//
// NOTES:
//     - This subroutine is functionally equivalent to boost::algorithm::trim_left().
//
//========================================================================
//========================================================================
std::string trim_left( std::string str )
{
    str.erase( str.begin(), std::find_if( str.begin(), str.end(), std::bind1st(std::not_equal_to<char>(), ' ') ) );
    
    return str;
}
