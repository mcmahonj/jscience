/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 2/26/2015
// LAST UPDATE: 2/26/2015


// STL
#include <unordered_map> // std::unordered_map<>
#include <vector>        // std::vector<>


//========================================================================
//========================================================================
//
// NAME: std::unordered_map<int,int> count_occurences( const std::vector<int> &v )
//
// DESC: Counts the number of occurences in a vector.
//
//========================================================================
//========================================================================
std::unordered_map<int,int> count_occurences( const std::vector<int> &v )
{
    std::unordered_map<int,int> m;
    
    for( auto itr = v.begin(); itr != v.end(); ++itr )
    {
        ++m[*itr];
    }
    
    return m;
}