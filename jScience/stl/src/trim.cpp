/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of FDTD++.
 
 FDTD++ is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 FDTD++ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with FDTD++. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 4/8/2015
// LAST UPDATE: 4/11/2015


// STL
#include <string> // std::string

// this
#include "jScience/stl/string.hpp" // trim_left(), trim_right()


//========================================================================
//========================================================================
//
// NAME: std::string trim( std::string str )
//
// DESC: Removes leading and trailing spaces from a string.
//
// NOTES:
//     - This subroutine is functionally equivalent to boost::algorithm::trim().
//
//========================================================================
//========================================================================
std::string trim( std::string str )
{
    str = trim_left(str);
    str = trim_right(str);
        
    return str;
}
