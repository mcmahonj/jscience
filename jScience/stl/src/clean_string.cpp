
// STL
#include <string>          // std::string

// jScience
#include "jScience/stl/string.hpp" // reduce(), trim()


// DESC: Cleans a string, by:
//           - removing extra whitespace
//           - trimming leading and trailing whitespacee
//           - ensuring that a single space exists after each comma and period
//           - ensuring that the string does not begin with a comma or period
std::string clean_string( std::string str )
{
    // remove extra whitespace
    str = reduce(str);
    
    // trim leading and trailing whitespace
    str = trim(str);
    
    // ensure that one space exists after each comma and period (except the last one, of course)
    for( int i = 0; i < (static_cast<int>(str.length())-1); ++i )
    {
        if( (str[i] == ',') || (str[i] == '.') )
        {
            if( str[i+1] != ' ' )
            {
                str.insert( (i+1), " " );
            }
        }
    }
    
    if( str.empty() )
    {
        return std::string();
    }
    
    // ensure that we don't begin with a comma or period
    if( !str.empty() )
    {
        if( (str.front() == ',') || (str.front() == '.') )
        {
            if( str.length() > 1 )
            {
                str.erase( str.begin(), (str.begin()+1) );
            }
            else
            {
                str.erase( str.begin() );
            }
        }
    }

    return str;
}



