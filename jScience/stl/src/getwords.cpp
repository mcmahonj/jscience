/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of FDTD++.
 
 FDTD++ is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 FDTD++ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with FDTD++. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 4/9/2015
// LAST UPDATE: 4/9/2015


// STL
#include <algorithm>    // std::remove_if()
#include <sstream>      // std::stringstream
#include <string>       // std::string, std::getline(), std::string::erase()
#include <vector>       // std::vector<>

// jScience
#include "jutility.hpp" // append()


//========================================================================
//========================================================================
//
// NAME: std::vector<std::string> getwords( const std::string str, const std::string delim )
//
// DESC: Gets all (each) words from a string, delimited by any number of characters.
//
// INPUT:
//          std::string              str   : string to parse words from
//          std::string              delim : string containing any number of delimiters (default of '\n')
//
// OUTPUT:
//          std::vector<std::string> words : parsed words
//
//========================================================================
//========================================================================
std::vector<std::string> getwords( const std::string str, const std::string delim )
{
    std::vector<std::string> words;
    
    std::stringstream ss(str);
    
    std::string line;
    
    if( delim.empty() )
    {
        while( std::getline(ss, line) )
        {
            words.push_back(line);
        }
    }
    else if( delim.length() == 1 )
    {
        while( std::getline(ss, line, delim[0]) )
        {
            words.push_back(line);
        }
    }
    else
    {
        while( std::getline(ss, line, delim[0]) )
        {
            std::string delim_new = delim;
            delim_new.erase( delim_new.begin() );
            
            std::vector<std::string> words_new = getwords(line, delim_new);
            
            append( words_new, words );
        }
    }
    
    // remove empty words
    words.erase( std::remove_if(words.begin(),
                                words.end(),
                                [](std::string const& word)
                                { return word.empty(); }
                                ), words.end() );
    
    return words;
}
