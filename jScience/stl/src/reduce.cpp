/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of FDTD++.
 
 FDTD++ is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 FDTD++ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with FDTD++. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 4/7/2015
// LAST UPDATE: 4/8/2015


// STL
#include <regex>           // std::regex_replace(), std::regex()
#include <string>          // std::string, std::string::erase()


//========================================================================
//========================================================================
//
// NAME: std::string reduce( std::string str )
//
// DESC: Removes extra whitespace in a string.
//
//========================================================================
//========================================================================
std::string reduce( std::string str )
{
    return ( std::regex_replace(str, std::regex("[ ]+"), " ") );
}



