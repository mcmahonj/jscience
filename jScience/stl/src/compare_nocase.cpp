/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 1/14/2015
// LAST UPDATE: 1/14/2015


// STL
#include <algorithm> // std::transform
#include <string>    // std::string


//========================================================================
//========================================================================
//
// NAME: bool compare_nocase( std::string str1, std::string str2 )
//
// DESC: Compares two std::string objects, ignoring case.
//
// NOTES:
//     ! This does *NOT* work for general UTF-8.
//
//========================================================================
//========================================================================
bool compare_nocase( std::string str1, std::string str2 )
{
    std::transform( str1.begin(), str1.end(), str1.begin(), ::tolower );
    std::transform( str2.begin(), str2.end(), str2.begin(), ::tolower );
    
    return (str1 == str2);
}