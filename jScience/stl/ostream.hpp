/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 9/19/2015
// LAST UPDATE: 9/19/2015

#ifndef JSCIENCE_STL_OSTREAM_HPP
#define JSCIENCE_STL_OSTREAM_HPP


// STL
#include <ostream> // std::ostream


// note: _NullBuffer (and _NullStream below) contain _ prior to their class names, because the user does not need access to the classes themselves, but rather an instance of them (e.g., NullStream below)
class _NullBuffer : public std::streambuf
{

public:
    
    int overflow(int c);
    
};

class _NullStream : public std::ostream
{

public:
    
    _NullStream();
    
private:
    
    _NullBuffer nb;
};

extern _NullStream NullStream;


#endif

