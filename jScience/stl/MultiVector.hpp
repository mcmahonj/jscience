// TODO: What is needed is to be able to do fast assignment of rows/columns/etc. --- e.g., in the Matrix class (derived from here), it would be convenient to do A.row(i) = Vector<double>(...) <-- as of now, one may *think* you could do that, but A.row(i) just returns a COPY of what is at row(i)

// TODO: there is an issue with the initialization of MultiVector<>, because one MUST provide an initialization of the values of the vector, otherwise it gets declared one dimension too small

// TODO: There must be a better and generalized way to declare an arbitrary-sized MultiVector, rather than implement each dimension by hand

// TODO: NOTE: I am wondering whether the functionality of outputting a Matrix<> to a file should be built in functionality of Matrix<> (or MultiVector<>)
// TODO: NOTE: ... because, I know that I have used this functionality more than once


#ifndef MULTI_VECTOR_H
#define MULTI_VECTOR_H


// STL
#include <vector> // std::vector

// Boost
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>        // serialize std::vector<>


//=========================================================
//
// Multi(dimensional) Vector
//
// NOTES:
//     ! Despite the Boost.MultiArray interface of [i][j]...[k], there are arguments in favor of using (i,j,...,k). See: http://www.parashift.com/c++-faq/nondependent-name-lookup-members.html
//
//=========================================================
template<typename T>
class MultiVector
{

public:

    MultiVector();
    MultiVector(const unsigned int r, T init);
    MultiVector(const unsigned int r, const unsigned int c, T init);
    MultiVector(const unsigned int r, const unsigned int c, const unsigned int d, T init);
    MultiVector(const unsigned int r, const unsigned int c, const unsigned int d, const unsigned int h, T init);
    MultiVector(const unsigned int r, const unsigned int c, const unsigned int d, const unsigned int h, const unsigned int m, T init);
    MultiVector(const unsigned int r, const unsigned int c, const unsigned int d, const unsigned int h, const unsigned int m, const unsigned int _n, T init);
    MultiVector(const std::vector<typename std::vector<T>::size_type> &d, const std::vector<T> &v = std::vector<T>(), const char f = 'r');
    ~MultiVector();

    unsigned int   get_ndim() const;
    unsigned int   size(const unsigned int d) const;
    // << jmm: the following is scheduled for removal >>
    unsigned int   get_size(const unsigned int d) const;
    std::vector<T> get_data() const;

    typename std::vector<T>::reference operator()(const unsigned int i);
    typename std::vector<T>::const_reference operator()(const unsigned int i) const;

    typename std::vector<T>::reference operator()(const unsigned int i, const unsigned int j);
    typename std::vector<T>::const_reference operator()(const unsigned int i, const unsigned int j) const;

    typename std::vector<T>::reference operator()(const unsigned int i, const unsigned int j, const unsigned int k);
    typename std::vector<T>::const_reference operator()(const unsigned int i, const unsigned int j, const unsigned int k) const;

    typename std::vector<T>::reference operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l);
    typename std::vector<T>::const_reference operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l) const;

    typename std::vector<T>::reference operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m);
    typename std::vector<T>::const_reference operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m) const;

    typename std::vector<T>::reference operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m, const unsigned int _n);
    typename std::vector<T>::const_reference operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m, const unsigned int _n) const;

protected:

    std::vector<T> data;

private:

    std::vector<typename std::vector<T>::size_type> n;

    typename std::vector<T>::reference at(const unsigned int i);
    typename std::vector<T>::reference at(const unsigned int i, const unsigned int j);
    typename std::vector<T>::reference at(const unsigned int i, const unsigned int j, const unsigned int k);
    typename std::vector<T>::reference at(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l);
    typename std::vector<T>::reference at(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m);
    typename std::vector<T>::reference at(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m, const unsigned int _n);

    // note: conversion to row-major and column-major order formats are defined as friends, because it is more efficient to access the private members data and n
    template<typename Y>
    friend std::vector<Y> to_rowMajOrd( const MultiVector<Y> &A );
//    #include "tpp/to_RowMajOrd.tpp"

    template<typename Y>
    friend std::vector<Y> to_colMajOrd( const MultiVector<Y> &A );
//    #include "tpp/to_ColMajOrd.tpp"

    // (Boost) SERIALIZATION
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(
                   Archive            &ar,
                   const unsigned int  version
                   )
    {
        ar & this->data;

        ar & this->n;
    }

};

#include "tpp/MultiVector.tpp"

#include "tpp/to_rowMajOrd.tpp"
#include "tpp/to_colMajOrd.tpp"


#endif

