#ifndef JSCIENCE_STL_ISTREAM_HPP
#define JSCIENCE_STL_ISTREAM_HPP


// STL
#include <fstream> // std::ifstream
#include <string>  // std::string
#include <vector>  // std::vector<>


std::string suffix(
                   const std::string filename
                   );
#include "jScience/stl/istream/suffix.cpp"


std::string peek_line(
                      std::ifstream& ifs
                      );
#include "jScience/stl/istream/peek_line.cpp"


bool is_eof(
            std::ifstream& ifs, // ifs to check
            const bool ell      // whether an empty line signifies the end of file
            );
#include "jScience/stl/istream/is_eof.cpp"


template<typename T>
std::vector<T> read_file(
                         const std::string filename
                         );
#include "jScience/stl/istream/read_file.tpp"


#endif

