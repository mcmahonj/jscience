#ifndef VECTOR3D_H
#define VECTOR3D_H


// STL
#include <complex>       // std::complex
#include <unordered_map> // std::unordered_map<>
#include <vector>        // std::vector


// TODO: NOTE: Perhaps should consider the following STL std::vector<> subroutines become just wrappers to the Vector<> subroutines.
//
// TODO: NOTE: ... Further, these wrappers actually become part of the linear algebra package of jScience.


//========================================================================
// NAME: template <typename T>
//       std::vector<T> abs(const std::vector<T> &x)
// DESC: Returns a vector containing the absolute values of the elements of x.
// NOTES:
//     ! This is essentially an overload to the C++ std::abs(), which only handles a valarray
//     ! This is similar to the MATLAB function abs()
//========================================================================
template <typename T>
std::vector<T> abs(const std::vector<T> &x);
#include "tpp/abs.tpp"


//
// DESC: Various operator overloads.
//
template<typename T>
std::vector<T> operator+(
                         const std::vector<T> &a,
                         const std::vector<T> &b
                         );

template<typename T>
std::vector<T> operator-( const std::vector<T> &a, const std::vector<T> &b );

template<typename T>
void operator+=(
                std::vector<T>       &a,
                const std::vector<T> &b
                );

template<typename T>
void operator*=(
                const T              &a,
                const std::vector<T> &b
                );

template<typename T>
void operator*=(
                std::vector<T> &a,
                const T        &b
                );

template<typename T>
void operator/=( std::vector<T> &a, const T &b );

template<typename T>
std::vector<T> operator*(
                         const T              &a,
                         const std::vector<T> &b
                         );

template<typename T>
std::vector<T> operator/( const std::vector<T> &a, const T &b );

#include "tpp/vector.tpp"


//========================================================================
// NAME: template <typename T>
//       std::vector<T> rdivide(const std::vector<T> &a, const std::vector<T> &b)
// DESC: Returns an element-by-element division of two vectors.
// NOTES:
//     ! This is similar to the MATLAB function rdivide() (./).
//========================================================================
template <typename T>
std::vector<T> rdivide(const std::vector<T> &a, const std::vector<T> &b);
#include "tpp/rdivide.tpp"


//========================================================================
// NAME: template <typename T>
//       std::vector<T> diff(const std::vector<T> &v)
// DESC: Returns the diff function for the vector x.
//
//       Suppose: x = [x0 x1 ... x{n-1}]
//       Then   : y = diff(x) = [(x1-x0) (x2-x1) ... (x{n-1}-x{n-2})]
//========================================================================
template <typename T>
std::vector<T> diff(const std::vector<T> &x);
#include "tpp/diff.tpp"


//========================================================================
// NAME: template <typename T>
//       double magnitude(const std::vector<T> &x)
// DESC: Returns the magnitude of the vector x.
//========================================================================
template <typename T>
double magnitude(const std::vector<T> &x);
#include "tpp/magnitude.tpp"


//========================================================================
// NAME: template <typename T>
//       std::vector<std::vector<T>> transpose( const std::vector<std::vector<T>> &x )
// DESC: Returns the transpose of the 2D vector x.
//========================================================================
template <typename T>
std::vector<std::vector<T>> transpose( const std::vector<std::vector<T>> &x );
#include "tpp/transpose.tpp"


//========================================================================
// NAME: std::unordered_map<int,int> count_occurences( const std::vector<int> &v )
// DESC: Counts the number of occurences in a vector.
//========================================================================
std::unordered_map<int,int> count_occurences( const std::vector<int> &v );


//========================================================================
// DESC: Extracts the subvector from imin to imax of x.
//========================================================================
template <typename T>
std::vector<T> subvector(
                         const std::vector<T>                     &x,
                         const typename std::vector<T>::size_type  imin,
                         const typename std::vector<T>::size_type  imax
                         );
#include "jScience/stl/tpp/subvector.tpp"


#endif

