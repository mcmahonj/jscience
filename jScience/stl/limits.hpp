#ifndef JSCIENCE_STL_LIMITS_HPP
#define JSCIENCE_STL_LIMITS_HPP

// STL
#include <limits>      // std::numeric_limits<>::is_integer
#include <type_traits> // std::enable_if<>


template<class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type almost_equal(
                                                                                      const T   x,
                                                                                      const T   y,
                                                                                      const int ulp
                                                                                      );
#include "jScience/stl/tpp/almost_equal.tpp"

#endif // JSCIENCE_STL_LIMITS_HPP
