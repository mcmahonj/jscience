/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/15/2014
// LAST UPDATE: 6/15/2014

#include "../ptr.hpp"

// STL
#include <memory> // std::unique_ptr

/*
//========================================================================
//========================================================================
//
// NAME: template<typename Derived, typename Base, typename Del>
//       std::unique_ptr<Derived, Del> dynamic_unique_ptr_cast( std::unique_ptr<Base, Del>&& p )
//
// DESC: Dynamic casts a std::unique_ptr to a Derived type
//
//========================================================================
//========================================================================
template<typename Derived, typename Base, typename Del>
std::unique_ptr<Derived, Del> dynamic_unique_ptr_cast( std::unique_ptr<Base, Del>&& p )
{
    if(Derived *result = dynamic_cast<Derived *>(p.get()))
    {
        p.release();
        return std::unique_ptr<Derived, Del>(result, std::move(p.get_deleter()));
    }
    
    return std::unique_ptr<Derived, Del>(nullptr, p.get_deleter());
}
*/

// << JMM: don't we need to inject the base deleter into the derived object, like above >>
//========================================================================
//========================================================================
//
// NAME: template<typename Derived, typename Base>
//       std::unique_ptr<Derived, Del> dynamic_unique_ptr_cast( std::unique_ptr<Base, Del>&& p )
//
// DESC: Dynamic casts a std::unique_ptr to a Derived type
//
//========================================================================
//========================================================================
template<typename Derived, typename Base>
std::unique_ptr<Derived> dynamic_unique_ptr_cast( std::unique_ptr<Base>&& p )
{
    if( Derived *result = dynamic_cast<Derived *>(p.get()) )
    {
        p.release();
        
        return std::unique_ptr<Derived>(result);
    }
    
    return std::unique_ptr<Derived>(nullptr);
}