/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 1/10/2015
// LAST UPDATE: 1/10/2015


// STL
#include <stdexcept> // std::invalid_argument
#include <vector>    // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       std::vector<std::vector<T>> transpose( const std::vector<std::vector<T>> &x )
//
// DESC: Returns the transpose of the vector x.
//
// NOTES:
//     ! It is almost never a good idea to use 2D vectors.
//     ! I believe that there are MUCH faster algorithms than this. See: stackoverflow.
//
//========================================================================
//========================================================================
template <typename T>
std::vector<std::vector<T>> transpose( const std::vector<std::vector<T>> &x )
{
    auto nr = x.size();
    
    if( nr == 0 )
    {
        std::vector<std::vector<T>>();
    }
    
    auto nc = x[0].size();
    
    std::vector<std::vector<T>> xT(nc, std::vector<T>(nr));
    
    for( decltype(x.size()) i = 0; i < x.size(); ++i )
    {
        if( x[i].size() != nc )
        {
            throw std::invalid_argument( "2D vector passed to transpose() is not rectangular" );
        }
        
        for( decltype(x[i].size()) j = 0; j < x[i].size(); ++j )
        {
            xT[j][i] = x[i][j];
        }
    }
        
    return xT;
}
