    template<typename T>
    std::vector<double> normalize(
                                  const std::vector<T> &x,
                                  const int             p = 1
                                  );


// STL
#include <vector>     // std::vector<>

// jScience
#include "jstats.hpp" // sum() << jmm: temp >>


//
// DESC: Normalize a vector so that the elements xi^p sum to one.
//
template<typename T>
inline std::vector<double> distribution::normalize(
                                                   const std::vector<T> &x,
                                                   const int             p = 1
                                                   )
{
    std::vector<double> x_norm;

    double xnorm = pnorm(x, p);

    for( auto &xi : x )
    {
        x_norm.push_back( (xi/xnorm) );
    }

    return x_norm;
}
