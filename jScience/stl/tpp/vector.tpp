// note: in the operator overloads, static_casts are ignored, which CAN affect the results. For example, consider int i = 10 and double d = 2.2. i *= d (= 22), but if we pre-static_cast d, then i = 20. We therefore let the compile determine the appropriate casts.
// << JMM >> :: I am not sure how to best overload operators in the following case: a is a complex vector, b is a real vector and c is a complex number, and we want a += c*b, because the rhs should be converted to c and returned

//#include "vector.hpp"

// STL
#include <stdexcept> // std::runtime_error()


//=========================================================
// STL VECTOR
//=========================================================
// << JMM >> :: I am not sure if one can (well, should) override the C++ STL, to use the standard methodology of relying on the -=, +=, etc. operators to handle -, +, etc.


//
// DESC: Various operator overloads.
//
template<typename T>
std::vector<T> operator+(
                         const std::vector<T> &a,
                         const std::vector<T> &b
                         )
{
    if( a.size() != b.size() )
    {
        throw std::runtime_error( ("error in operator+() for two STL vectors: a.size() != b.size()") );
    }

    std::vector<T> c;

    for( auto i = 0; i < a.size(); ++i )
    {
        c.push_back( (a[i] + b[i]) );
    }

    return c;
}

template<typename T>
std::vector<T> operator-( const std::vector<T> &a, const std::vector<T> &b )
{
    // << JMM >> :: This does not handle the case when a or b is a scalar, like times() or rdivide() does

    if( a.size() != b.size() )
    {
        throw std::runtime_error( ("error in operator-() for two STL vectors: a.size() != b.size()") );
    }

    std::vector<T> c;

    for( auto i = 0; i < a.size(); ++i )
    {
        c.push_back( a[i] - b[i] );
    }

    return c;
}

template<typename T>
void operator+=(
                std::vector<T>       &a,
                const std::vector<T> &b
                )
{
    a = a + b;
}

template<typename T>
void operator*=(
                const T        &a,
                std::vector<T> &b
                )
{
    for( auto &bi : b )
    {
        bi *= a;
    }
}

template<typename T>
void operator*=(
                std::vector<T> &a,
                const T        &b
                )
{
    a = b*a;
}

template<typename T>
void operator/=( std::vector<T> &a, const T &b )
{
    for( auto &ai : a )
    {
        ai /= b;
    }
}

template<typename T>
std::vector<T> operator*(
                         const T              &a,
                         const std::vector<T> &b
                         )
{
    std::vector<T> c;

    for( auto &bi : b )
    {
        c.push_back( (a*bi) );
    }

    return c;
}

template<typename T>
std::vector<T> operator/( const std::vector<T> &a, const T &b )
{
    std::vector<T> c;

    for( auto &ai : a )
    {
        c.push_back(ai/b);
    }

    return c;
}
