// STL
#include <cstdlib>     // std::fabs()
#include <limits>      // std::numeric_limits<>::is_integer, ::epsilon(), ::min()
#include <type_traits> // std::enable_if<>


//
// DESC: Determine whether two variables (class T) are "almost equal".
//
// =====
//
// NOTE: This algorithm comes from:
//
//     https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
//
template<class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type almost_equal(
                                                                                      const T   x,
                                                                                      const T   y,
                                                                                      const int ulp
                                                                                      )
{
    // the machine epsilon has to be scaled to the magnitude of the values used
    // and multiplied by the desired precision in ULPs (units in the last place)
    return std::fabs(x-y) <= std::numeric_limits<T>::epsilon() * std::fabs(x+y) * ulp
        // unless the result is subnormal
        || std::fabs(x-y) < std::numeric_limits<T>::min();
}
