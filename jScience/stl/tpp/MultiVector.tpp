/*
 Copyright 2014-Present Algorithms in Motion LLC

 This file is part of jScience.

 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/23/2014
// LAST UPDATE: 6/27/2015

#include "../MultiVector.hpp"

// STL
#include <iostream>  // std::cout, std::endl
#include <stdexcept> // std::runtime_error()
#include <string>    // std::to_string()

// jScience
#include "jScience/stl/memory.hpp" // colMajOrd_to_rowMajOrd()


template<typename T>
MultiVector<T>::MultiVector()
{
    n.clear();
    data.clear();
}

template<typename T>
MultiVector<T>::MultiVector(const unsigned int r, T init) : MultiVector<T>::MultiVector()
{
    n.push_back(r);

    data.resize(r, init);
}

template<typename T>
MultiVector<T>::MultiVector(const unsigned int r, const unsigned int c, T init) : MultiVector<T>::MultiVector()
{
    n.push_back(r);
    n.push_back(c);

    data.resize(r*c, init);
}

template<typename T>
MultiVector<T>::MultiVector(const unsigned int r, const unsigned int c, const unsigned int d, T init) : MultiVector<T>::MultiVector()
{
    n.push_back(r);
    n.push_back(c);
    n.push_back(d);

    data.resize(r*c*d, init);
}

template<typename T>
MultiVector<T>::MultiVector(const unsigned int r, const unsigned int c, const unsigned int d, const unsigned int h, T init) : MultiVector<T>::MultiVector()
{
    n.push_back(r);
    n.push_back(c);
    n.push_back(d);
    n.push_back(h);

    data.resize(r*c*d*h, init);
}

template<typename T>
MultiVector<T>::MultiVector(const unsigned int r, const unsigned int c, const unsigned int d, const unsigned int h, const unsigned int m, T init) : MultiVector<T>::MultiVector()
{
    n.push_back(r);
    n.push_back(c);
    n.push_back(d);
    n.push_back(h);
    n.push_back(m);

    data.resize(r*c*d*h*m, init);
}

template<typename T>
MultiVector<T>::MultiVector(const unsigned int r, const unsigned int c, const unsigned int d, const unsigned int h, const unsigned int m, const unsigned int _n, T init) : MultiVector<T>::MultiVector()
{
    n.push_back(r);
    n.push_back(c);
    n.push_back(d);
    n.push_back(h);
    n.push_back(m);
    n.push_back(_n);

    data.resize(r*c*d*h*m*_n, init);
}

template<typename T>
MultiVector<T>::MultiVector(const std::vector<typename std::vector<T>::size_type> &d, const std::vector<T> &v, const char f) : MultiVector<T>::MultiVector()
{
    typename std::vector<T>::size_type size;

    if( d.size() > 0 )
    {
        size = 1;
    }
    else
    {
        size = 0;
    }

    for( auto &di : d )
    {
        // NOTE: without allowing for zero dimensions, there is no way to initialize an empty MultiVector
//        if( di == 0 )
//        {
//            throw std::runtime_error( "error in MultiVector<>: cannot have zero size along a dimension" );
//        }

        this->n.push_back(di);

        size *= di;
    }

    // assign the initialization vector (if specified)
    if( v.size() > 0 )
    {
        if( v.size() != size )
        {
            throw std::runtime_error( "error in MultiVector<>: initialization vector is not of the right size" );
        }

        if( (f == 'r') || (f == 'R') )
        {
            data = v;
        }
        else if( (f == 'c') || (f == 'C') )
        {
            data = colMajOrd_to_rowMajOrd(v, this->n);
        }
        else
        {
            throw std::runtime_error( "error in MultiVector<>: initialization by a vector must specify either row-major ('r'/'R') or column-major ('c'/'C') format (" + std::to_string(f) + " specified)" );
        }
    }
    else
    {
        this->data.resize(size, T(0));
    }
}

template<typename T>
MultiVector<T>::~MultiVector() {};


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       unsigned int MultiVector<T>::get_ndim() const
//
// DESC: Returns the number of dimensions of the MultiVector.
//
//========================================================================
//========================================================================
template<typename T>
unsigned int MultiVector<T>::get_ndim() const
{
    return n.size();
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       unsigned int MultiVector<T>::size(const unsigned int d) const
//
// DESC: Returns the size of the MultiVector along dimension d.
//
//========================================================================
//========================================================================
template<typename T>
unsigned int MultiVector<T>::size(const unsigned int d) const
{
    return n.at(d);
}

// << jmm: the following is being phased out>>
template<typename T>
unsigned int MultiVector<T>::get_size(const unsigned int d) const
{
    return n.at(d);
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<T> MultiVector<T>::get_data() const
//
// DESC: Returns the data vector.
//
//========================================================================
//========================================================================
template<typename T>
std::vector<T> MultiVector<T>::get_data() const
{
    return data;
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       T& Matrix<T>::operator()(const unsigned int i, const unsigned int j)
//
//       template<typename T>
//       T const& Matrix<T>::operator()(const unsigned int i, const unsigned int j) const
//
// DESC: Returns a reference or value to the (i, j) position, respectively.
//
//========================================================================
//========================================================================
template<typename T>
typename std::vector<T>::reference MultiVector<T>::operator()(const unsigned int i)
{
    return at(i);
}

template<typename T>
typename std::vector<T>::const_reference MultiVector<T>::operator()(const unsigned int i) const
{
    return const_cast<MultiVector&>(*this).at(i);
}

// -----

template<typename T>
typename std::vector<T>::reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j)
{
    return at(i, j);
}

template<typename T>
typename std::vector<T>::const_reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j) const
{
    return const_cast<MultiVector&>(*this).at(i, j);
}

// -----

template<typename T>
typename std::vector<T>::reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j, const unsigned int k)
{
    return at(i, j, k);
}

template<typename T>
typename std::vector<T>::const_reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j, const unsigned int k) const
{
    return const_cast<MultiVector&>(*this).at(i, j, k);
}

// -----

template<typename T>
typename std::vector<T>::reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l)
{
    return at(i, j, k, l);
}

template<typename T>
typename std::vector<T>::const_reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l) const
{
    return const_cast<MultiVector&>(*this).at(i, j, k, l);
}

// -----

template<typename T>
typename std::vector<T>::reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m)
{
    return at(i, j, k, l, m);
}

template<typename T>
typename std::vector<T>::const_reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m) const
{
    return const_cast<MultiVector&>(*this).at(i, j, k, l, m);
}

// -----

template<typename T>
typename std::vector<T>::reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m, const unsigned int _n)
{
    return at(i, j, k, l, m, _n);
}

template<typename T>
typename std::vector<T>::const_reference MultiVector<T>::operator()(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m, const unsigned int _n) const
{
    return const_cast<MultiVector&>(*this).at(i, j, k, l, m, _n);
}

//========================================================================
//========================================================================
//
// NAME: .at()
//
// DESC: Returns the value of data at position (i, j, k, ...)
//
// NOTES:
//     ! Recall that in C++, data is best stored in row-major order.
//     ! See:
//          http://en.wikipedia.org/wiki/Row-major_order
//
//========================================================================
//========================================================================
template<typename T>
typename std::vector<T>::reference MultiVector<T>::at(const unsigned int i)
{
    return data.at( i );
}

template<typename T>
typename std::vector<T>::reference MultiVector<T>::at(const unsigned int i, const unsigned int j)
{
    return data.at( i*n.at(1) + j );
}

template<typename T>
typename std::vector<T>::reference MultiVector<T>::at(const unsigned int i, const unsigned int j, const unsigned int k)
{
    return data.at( (i*n.at(1) + j)*n.at(2) + k );
}

template<typename T>
typename std::vector<T>::reference MultiVector<T>::at(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l)
{
    return data.at( ((i*n.at(1) + j)*n.at(2) + k)*n.at(3) + l );
}

template<typename T>
typename std::vector<T>::reference MultiVector<T>::at(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m)
{
    return data.at( (((i*n.at(1) + j)*n.at(2) + k)*n.at(3) + l)*n.at(4) + m );
}

template<typename T>
typename std::vector<T>::reference MultiVector<T>::at(const unsigned int i, const unsigned int j, const unsigned int k, const unsigned int l, const unsigned int m, const unsigned int _n)
{
    return data.at( ((((i*n.at(1) + j)*n.at(2) + k)*n.at(3) + l)*n.at(4) + m)*n.at(5) + _n );
}
