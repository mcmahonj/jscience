/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/28/2015
// LAST UPDATE: 5/29/2015


// STL
#include <vector> // std::vector<>

// jScience
#include "jScience/stl/memory.hpp"      // rowMajOrd_to_colMajOrd()
#include "jScience/stl/MultiVector.hpp" // MultiVector<>


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<T> to_colMajOrd( const MultiVector<T> &A )
//
// DESC: Returns the data in A in column-major order.
//
// NOTES:
//     ! A subroutine such as this is useful for calling Fortran functions.
//
//========================================================================
//========================================================================
template<typename T>
std::vector<T> to_colMajOrd(const MultiVector<T> &A)
{
    return rowMajOrd_to_colMajOrd(A.data, A.n);
}
