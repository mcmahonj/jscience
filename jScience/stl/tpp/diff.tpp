/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/4/2014
// LAST UPDATE: 5/4/2014


// STL
#include <vector> // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       std::vector<T> diff(const std::vector<T> &x)
//
// DESC: Returns the diff function for the vector x.
//
//       Suppose: x = [x0 x1 ... x{n-1}]
//       Then   : y = diff(x) = [(x1-x0) (x2-x1) ... (x{n-1}-x{n-2})]
//
// NOTES:
//     ! This function is similar to that in MATLAB as diff()
//
//========================================================================
//========================================================================
template <typename T>
std::vector<T> diff(const std::vector<T> &x)
{
    // note: the following implicitely handles when x contains only one element, returning an empty vector y
    
    std::vector<T> y;
    
    for( auto i = 1; i < x.size(); ++i )
    {
        y.push_back( (x[i] - x[i-1]) );
    }
    
    return y;
}
