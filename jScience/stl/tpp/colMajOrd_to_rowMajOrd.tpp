/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/29/2015
// LAST UPDATE: 5/29/2015


// STL
#include <vector> // std::vector<>


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<T> colMajOrd_to_rowMajOrd(const std::vector<T> &v)
//
// DESC: Converts a vector v, stored in column-major oder, to the corresponding vector stored in row-major order.
//
// NOTES:
//     ! A subroutine such as this is useful for converting the output from Fortran functions.
//     ! See: http://en.wikipedia.org/wiki/Row-major_order
//
//========================================================================
//========================================================================
template<typename T>
std::vector<T> colMajOrd_to_rowMajOrd(const std::vector<T> &v,
                                      const std::vector<typename std::vector<T>::size_type> &N)
{
    std::vector<T> rv(v.size());
    
    // for each element of the 1D data array ...
    for( typename std::vector<T>::size_type i = 0; i < v.size(); ++i )
    {
        // ... first find the corresponding indices
        std::vector<typename std::vector<T>::size_type> idxs;
        
        typename std::vector<T>::size_type rem = i;
        
        for( typename std::vector<typename std::vector<T>::size_type>::const_iterator j = N.begin(); j != N.end(); ++j )
        {
            typename std::vector<T>::size_type y = static_cast<typename std::vector<T>::size_type>(rem/(*j));
            
            idxs.push_back( (rem - y*(*j)) );
            
            rem = y;
        }
        
        // ... and then use these indices to find the corresponding row-major order index
        typename std::vector<T>::size_type rv_idx = 0;
        
        for( decltype(N.size()) k = 0; k < N.size(); ++k )
        {
            typename std::vector<T>::size_type P = 1;

            for( decltype(N.size()) l = (k+1); l < N.size(); ++l )
            {
                P *= N[l];
            }
            
            rv_idx += P*idxs[k];
        }
        
        rv[rv_idx] = v[i];
    }
    
    return rv;
}
