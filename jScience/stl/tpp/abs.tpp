/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/5/2014
// LAST UPDATE: 5/5/2014


// STL
#include <cmath>        // std::abs
#include <complex>      // std::abs (for complex)
#include <vector>       // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       std::vector<T> abs(const std::vector<T> &x)
//
// DESC: Returns a vector containing the absolute values of the elements of x.
//
// NOTES:
//     ! This is essentially an overload to the C++ std::abs(), which only handles a valarray
//     ! This is similar to the MATLAB function abs()
//
//========================================================================
//========================================================================
template <typename T>
std::vector<T> abs(const std::vector<T> &x)
{
    std::vector<T> xabs;
    
    for( auto &xi : x )
    {
        xabs.push_back(xi);
    }
    
    return xabs;
}

