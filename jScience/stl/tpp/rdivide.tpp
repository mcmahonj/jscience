/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/5/2014
// LAST UPDATE: 5/5/2014


// STL
#include <iostream>     // std::cout, std::endl
#include <vector>       // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       std::vector<T> rdivide(const std::vector<T> &a, const std::vector<T> &b)
//
// DESC: Returns an element-by-element division of two vectors.
//
// NOTES:
//     ! This is similar to the MATLAB function rdivide (./).
//
//========================================================================
//========================================================================
template <typename T>
std::vector<T> rdivide(const std::vector<T> &a, const std::vector<T> &b)
{
    std::vector<T> c;
    
    if( (a.size() == 0) || (b.size() == 0) )
    {
        std::cout << "Error in rdivide(): (a.size() == 0) || (b.size() == 0)! Exiting." << std::endl;
        exit(0);
    }
    else if( a.size() == b.size() )
    {
        for( auto i = 0; i < a.size(); ++i )
        {
            c.push_back( a[i]/b[i] );
        }
    }
    else if( a.size() == 1 )
    {
        for( auto &bi : b )
        {
            c.push_back( a[0]/bi );
        }
    }
    else if( b.size() == 1 )
    {
        for( auto &ai : a )
        {
            c.push_back( ai/b[0] );
        }
    }
    else
    {
        std::cout << "Error in rdivide(): (a.size() != b.size()) && !((a.size() == 1) || (b.size() == 1))! Exiting." << std::endl;
        exit(0);
    }
    
    return c;
}
