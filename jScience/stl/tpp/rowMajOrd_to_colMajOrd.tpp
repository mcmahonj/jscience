/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/29/2015
// LAST UPDATE: 5/29/2015


// STL
#include <vector> // std::vector<>


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<T> rowMajOrd_to_colMajOrd(const std::vector<T> &v)
//
// DESC: Converts a vector v, stored in row-major oder, to the corresponding vector stored in column-major order.
//
// NOTES:
//     ! A subroutine such as this is useful for calling Fortran functions.
//     ! See: http://en.wikipedia.org/wiki/Row-major_order
//
//========================================================================
//========================================================================
template<typename T>
std::vector<T> rowMajOrd_to_colMajOrd(const std::vector<T> &v,
                                      const std::vector<typename std::vector<T>::size_type> &N)
{
    std::vector<T> cv(v.size());
    
    // for each element of the 1D data array ...
    for( typename std::vector<T>::size_type i = 0; i < v.size(); ++i )
    {
        // ... first find the corresponding indices
        std::vector<typename std::vector<T>::size_type> idxs;
        
        typename std::vector<T>::size_type rem = i;
        
        for( typename std::vector<typename std::vector<T>::size_type>::const_reverse_iterator j = N.rbegin(); j != N.rend(); ++j )
        {
            typename std::vector<T>::size_type y = static_cast<typename std::vector<T>::size_type>(rem/(*j));
            
            idxs.insert( idxs.begin(), (rem - y*(*j)) );
            
            rem = y;
        }
        
        // ... and then use these indices to find the corresponding column-major order index
        typename std::vector<T>::size_type cv_idx = 0;
        
        for( decltype(N.size()) k = 0; k < N.size(); ++k )
        {
            typename std::vector<T>::size_type P = 1;

            for( decltype(N.size()) l = 0; l < k; ++l )
            {
                P *= N[l];
            }
            
            cv_idx += P*idxs[k];
        }
        
        cv[cv_idx] = v[i];
    }
    
    return cv;
}
