// STL
#include <vector>    // std::vector


// DESC: Extracts the subvector from imin to imax of x.
template <typename T>
std::vector<T> subvector(
                         const std::vector<T>                     &x,
                         const typename std::vector<T>::size_type  imin,
                         const typename std::vector<T>::size_type  imax
                         )
{
    typename std::vector<T>::const_iterator first = x.begin() + imin;
    typename std::vector<T>::const_iterator last  = x.begin() + (imax+1);

    return std::vector<T>(first, last);
}
