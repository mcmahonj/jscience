/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/30/2014
// LAST UPDATE: 12/30/2014


// STL
#include <cmath>        // std::sqrt

// jScience
#include "jstats.hpp"   // sum()
#include "jScience/stl/vector.hpp"   // times()


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       double magnitude(const std::vector<T> &x)
//
// DESC: Returns the magnitude of the vector x.
//
//========================================================================
//========================================================================
template <typename T>
double magnitude(const std::vector<T> &x)
{
    return std::sqrt( sum( times(x, x) ) );
}

