
#ifndef JSCIENCE_STL_STRING_HPP
#define JSCIENCE_STL_STRING_HPP


// STL
#include <string> // std::string
#include <vector> // std::vector<>


//========================================================================
//========================================================================
//
// NAME: std::string remove_chars( std::string str, const std::string &chars )
//
// DESC: Removes a set of characters from a string.
//
//========================================================================
//========================================================================
std::string remove_chars( std::string str, const std::string &chars );


//========================================================================
// NAME: bool compare_nocase( std::string str1, std::string str2 )
// DESC: Compares two std::string objects, ignoring case. 
// NOTES:
//     ! This does *NOT* work for general UTF-8.
//========================================================================
bool compare_nocase( std::string str1, std::string str2 );

//========================================================================
//
// NAME: std::vector<std::string> getwords( const std::string str, const std::string delim )
//
// DESC: Gets all (each) words from a string, delimited by any number of characters.
//
// INPUT:
//          std::string              str   : string to parse words from
//          std::string              delim : string containing any number of delimiters (default of '\n')
//
// OUTPUT:
//          std::vector<std::string> words : parsed words
//
//========================================================================
std::vector<std::string> getwords( const std::string line, const std::string delim = std::string( "\n" ) );


//========================================================================
//
// NAME: std::string reduce( std::string str )
//
// DESC: Removes extra whitespace in a string.
//
//========================================================================
std::string reduce( std::string str );


//========================================================================
//
// NAME: std::string trim_left( std::string str )
//       std::string trim_right( std::string str )
//       std::string trim( std::string str )
//
// DESC: Removes leading and trailing spaces from a string.
//
// NOTES:
//     - This subroutine is functionally equivalent to boost::algorithm::trim().
//
//========================================================================
std::string trim_left( std::string str );
std::string trim_right( std::string str );
std::string trim( std::string str );


// DESC: Cleans a string, by:
//           - removing extra whitespace
//           - trimming leading and trailing whitespacee
//           - ensuring that a single space exists after each comma and period
//           - ensuring that the string does not begin with a comma or period
std::string clean_string( std::string str );


#endif

