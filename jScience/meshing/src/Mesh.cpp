/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/12/2014
// LAST UPDATE: 6/15/2014

#include "jScience/meshing/Mesh.hpp"

// STL
#include <algorithm>   // std::find
#include <iostream>    // std::cout, std::endl
#include <memory>      // std::unique_ptr
#include <vector>      // std::vector

// jScience
#include "jScience/geometry/Polyhedron.hpp"  // Polyhedron
#include "jScience/geometry/Vector.hpp"      // Vector
#include "jScience/meshing/Element.hpp"      // Element
#include "jScience/meshing/meshing.hpp"      // is_VolumeElement()
#include "jScience/stl/ptr.hpp"              // dynamic_unique_ptr_cast()
 

Mesh::Mesh()
{
    nodes.clear();
    
    //elements.volume.clear();
    //elements.surface.clear();
    //elements.line.clear();
}

Mesh::~Mesh() {};


//========================================================================
//========================================================================
//
// NAME: void Mesh::add_node( const Node &node )
//
// DESC: Adds a node to the mesh.
//
//========================================================================
//========================================================================
void Mesh::add_node( const Node &node )
{
    nodes.push_back( node );
}


//========================================================================
//========================================================================
//
// NAME: void Mesh::add_element( const Element& element )
//
// DESC: Adds an element to the mesh.
//
//========================================================================
//========================================================================
void Mesh::add_element( std::unique_ptr<Element> &element )
{
    if( is_VolumeElement(*element) )
    { 
        elements.volume.push_back( std::move( dynamic_unique_ptr_cast<VolumeElement>( std::move(element) ) ) );
        
        for( decltype(elements.volume.back()->nodes.size()) i = 0; i < elements.volume.back()->nodes.size(); ++i )
        {
            elements.volume.back()->vtx[i] = nodes[elements.volume.back()->nodes[i]].vtx;
        }
    }
    else if( is_SurfaceElement(*element) )
    {
        elements.surface.push_back( std::move( dynamic_unique_ptr_cast<SurfaceElement>( std::move(element) ) ) );
        
        for( decltype(elements.surface.back()->nodes.size()) i = 0; i < elements.surface.back()->nodes.size(); ++i )
        {
            elements.surface.back()->vtx[i] = nodes[elements.surface.back()->nodes[i]].vtx;
        }
    }
}


//========================================================================
//========================================================================
//
// NAME: std::vector<std::unique_ptr<Element>>::size_type Mesh::find_volume_element( const Vector<double> &v )
//
// DESC: Given a vector v, (attempt to) find the volume element i that this is in.
//
// NOTES:
//     ! This uses a true/false return, rather than throw/catch, because it is *expected* that you won't find an element is common
//
// << JMM: Does this routine work for all objects? It should, right? >>
// << JMM: This routine is no good and hacky just for FDTD++ >>
//
//========================================================================
//========================================================================
bool Mesh::find_volume_element( const Vector<double> &v, std::vector<std::unique_ptr<VolumeElement>>::size_type &i )
{
    double tol = 0.01;

    for( i = 0; i < elements.volume.size(); ++i )
    {
        double vol = 0.0;
  
        // note: the following may not be the safest approach, as it modifies the actual objects. But, this does avoid unnecessary copies, etc.
        for( auto &vi : elements.volume[i]->vtx )
        {
            Vector<double> tmp_vi = vi;
            
            vi = v;
            
            //vol += elements.volume[i]->volume();
            vol += elements.volume[i]->calc_volume();
            
            vi = tmp_vi;
        }
        
        //double V = elements.volume[i]->volume();
        double V = elements.volume[i]->calc_volume();
        
        if( std::fabs(vol - V)/V < tol )
        {
            return true;
        }
    }

    return false;
} 


//========================================================================
//========================================================================
//
// NAME: int Mesh::get_volume_attrib( std::vector<std::unique_ptr<VolumeElement>>::size_type &i )
//
// DESC: Returns the volume element i attribute.
//
//========================================================================
//========================================================================
int Mesh::get_volume_attrib( std::vector<std::unique_ptr<VolumeElement>>::size_type &i )
{
    return elements.volume[i]->attrib;
}

