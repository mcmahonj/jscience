/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/12/2014
// LAST UPDATE: 6/15/2014

#ifndef MESH_H
#define MESH_H


// STL
#include <vector>                   // std::vector

// jScience
#include "jScience/geometry/Vector.hpp"      // Vector
#include "jScience/meshing/Element.hpp"      // Element
#include "jScience/meshing/MeshElements.hpp" // MeshElements
#include "jScience/meshing/Node.hpp"         // Node


//=========================================================
// NAME: Mesh
// DESC: A mesh, containing a list of nodes and elements.
//=========================================================
class Mesh
{
    
public:
    
    Mesh();
    ~Mesh();

    void add_node( const Node &node );
    void add_element( std::unique_ptr<Element> &element );
    
    bool find_volume_element( const Vector<double> &v, std::vector<std::unique_ptr<VolumeElement>>::size_type &i );
    int  get_volume_attrib( std::vector<std::unique_ptr<VolumeElement>>::size_type &i );
    
//private:
    
    std::vector<Node> nodes;
    MeshElements      elements;
    
};


#endif
