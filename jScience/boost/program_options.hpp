/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 2/5/2015
// LAST UPDATE: 2/5/2015

#ifndef PROGRAM_OPTIONS_H	
#define PROGRAM_OPTIONS_H


// boost
#include <boost/program_options.hpp> // boost::program_options


/* Function used to check that 'opt1' and 'opt2' are not specified at the same time. */
void conflicting_options( const boost::program_options::variables_map& vm, const char* opt1, const char* opt2 );

/* Function used to check that of 'for_what' is specified, then 'required_option' is specified too. */
void option_dependency( const boost::program_options::variables_map& vm, const char* for_what, const char* required_option );


#endif
