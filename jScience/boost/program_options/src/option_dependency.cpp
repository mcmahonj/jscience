/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of FDTD++.
 
 FDTD++ is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 FDTD++ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with FDTD++. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 2/5/2015
// LAST UPDATE: 2/5/2015


// STL
#include <stdexcept>  // std::logic_error()
#include <string>     // std::string()

// boost
#include <boost/program_options.hpp> // boost::program_options


// adapted from: http://www.boost.org/doc/libs/1_57_0/libs/program_options/example/real.cpp
void option_dependency( const boost::program_options::variables_map& vm, const char* for_what, const char* required_option )
{
    if( vm.count(for_what) && !vm[for_what].defaulted() )
    {
        if( (vm.count(required_option) == 0) || vm[required_option].defaulted() )
        {
            throw std::logic_error( std::string("Option '") + for_what + "' requires option '" + required_option + "'." );
        }
    }
}