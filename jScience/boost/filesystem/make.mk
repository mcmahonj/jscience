CPP_FILES := $(wildcard src/*.cpp)
OBJ_FILES  = $(patsubst src/%.cpp, $(MAKE_DIR)/obj/%.o, $(CPP_FILES))

all: $(OBJ_FILES)

$(MAKE_DIR)/obj/%.o   : src/%.cpp
	@echo $< " --> " $@
	@$(CXX) $(CXXFLAGS) -c $< -o $@