/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of FDTD++.
 
 FDTD++ is proprietary software: you can use it and/or modify it
 under the terms of the Algorithms in Motion License as published by
 Algorithms in Motion LLC, either version 1 of the License, or (at your
 option) any later version.
 
 FDTD++ is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 Algorithms in Motion License for more details.
 
 You should have received a copy of the Algorithms in Motion License
 along with FDTD++. If not, see <http://www.aimotionllc.com/licenses/>.
*/
// CREATED    : 1/31/2015
// LAST UPDATE: 2/3/2015


// STL
#include <algorithm>  // std::copy(), std::sort()
#include <iterator>   // std::back_inserter()
#include <stdexcept>  // std::runtime_error
#include <string>     // std::string
#include <tuple>      // std::tuple<>, std::make_tuple
#include <utility>    // std::pair<>, std::make_pair(), std::move()
#include <vector>     // std::vector<T>, std::vector::reserve()

// Boost
#include <boost/filesystem.hpp> // boost::filesystem


namespace fs = boost::filesystem;


static std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::string>> assign_paths( const std::vector<fs::path> &paths );


//========================================================================
//========================================================================
//
// NAME: std::pair<std::vector<std::string>, std::vector<std::string>> map_tree( const std::string dir, const bool recursive = true )
//
// DESC: Maps a directory tree.
//
// INPUT:
//          std::string dir                : directory to map
//          bool recursive                 : whether to map recursively
//
// OUTPUT:
//          std::vector<std::string> dirs  : directories
//          std::vector<std::string> files : files
//
//========================================================================
//========================================================================
std::pair<std::vector<std::string>, std::vector<std::string>> map_tree( const std::string dir, const bool recursive = true )
{
    std::vector<std::string> dirs, files;
    
    try
    {
        fs::path p(dir);
        
        if( fs::exists( p ) )
        {
            if( fs::is_directory( p ) )
            {
                std::vector<fs::path> paths;
                
                std::copy( fs::directory_iterator(p), fs::directory_iterator(), std::back_inserter(paths) );
                
                // sort the paths, since directory iteration is not ordered on some file systems
                std::sort( paths.begin(), paths.end() );
                
                std::vector<std::string> irreg;
                std::tie(dirs, files, irreg) = assign_paths(paths);
            }
/*
            else if( fs::is_regular_file(p) )
            {
                throw std::runtime_error( (p.string() + " is not a directory.") );
            }
*/
            else
            {
                throw std::runtime_error( (p.string() + " is not a directory.") );
            }
        }
        else
        {
            throw std::runtime_error( (p.string() + " does not exist.") );
        }
    }
    catch( const fs::filesystem_error& e )
    {
        throw;
    }
    
    if( recursive )
    {
        // because we append to dirs, loop over ONLY the direct child directories
        std::vector<std::string> dirs_orig = dirs;
        
        for( auto &dirsi : dirs_orig )
        {
            std::vector<std::string> child_dirs, child_files;
            std::tie(child_dirs, child_files) = map_tree( dirsi, true );

            dirs.reserve( dirs.size() + child_dirs.size() );
            std::move( child_dirs.begin(), child_dirs.end(), std::back_inserter(dirs) );
            
            files.reserve( files.size() + child_files.size() );
            std::move( child_files.begin(), child_files.end(), std::back_inserter(files) );
        }
    }
    
    return std::make_pair(dirs, files);
}


//========================================================================
//========================================================================
//
// NAME: static std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::string>> assign_paths( const std::vector<fs::path> &paths )
//
// DESC: Assigns a vector of paths to directories, files, or irregular objects (neither a regular file nor directory).
//
// INPUT:
//          std::vector<fs::path> paths : vector of path objects
//
// OUTPUT:
//          std::vector<std::string> dirs  : directories
//          std::vector<std::string> files : files
//          std::vector<std::string> irreg : irregular objects
//
//========================================================================
//========================================================================
static std::tuple<std::vector<std::string>, std::vector<std::string>, std::vector<std::string>> assign_paths( const std::vector<fs::path> &paths )
{
    std::vector<std::string> dirs, files, irreg;
    
    for( auto &path : paths )
    {
        // each path should be guaranteed to check, hence no check
        
        if( fs::is_regular_file(path) )
        {
            files.push_back( path.string() );
        }
        else if( fs::is_directory(path) )
        {
            dirs.push_back( path.string() );
        }
        else
        {
            irreg.push_back( path.string() );
        }
    }
    
    return std::make_tuple(dirs, files, irreg);
}