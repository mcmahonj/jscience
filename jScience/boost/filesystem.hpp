#ifndef FILESYSTEM_HPP
#define FILESYSTEM_HPP


// STL
#include <string>   // std::string
#include <utility>  // std::pair<>
#include <vector>   // std::vector


//
// DESC: Maps a directory tree.
//
// INPUT:
//          std::string dir                : directory to map
//          bool recursive                 : whether to map recursively
//
// OUTPUT:
//          std::vector<std::string> dirs  : directories
//          std::vector<std::string> files : files
//
// NEEDS (LINKING):
//
//     libboost_system
//     libboost_filesystem
//
std::pair<
          std::vector<std::string>,
          std::vector<std::string>
          > map_tree(
                     const std::string dir,
                     const bool recursive = true
                     );


#endif
