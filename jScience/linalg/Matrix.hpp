#ifndef MATRIX_H
#define MATRIX_H


// jScience
#include "jScience/stl/MultiVector.hpp" // MultiVector<>
#include "Vector.hpp"                   // Vector<>


//=========================================================
// Matrix
//=========================================================
template<typename T>
class Matrix : public MultiVector<T>
{
     
public:
    
    Matrix();
    Matrix(const unsigned int r,
           const unsigned int c,
           const std::vector<T> &v = std::vector<T>(),
           const char f = 'r');
    Matrix(const unsigned int r, const unsigned int c, T init);
    ~Matrix();
    
    Vector<T> row(const unsigned int i) const;
    Vector<T> column(const unsigned int j) const;
    
    Matrix<T>& operator+=(const Matrix<T>& B);
    Matrix<T>& operator-=(const Matrix<T>& B);
    Matrix<T>& operator*=(const T& a);
    Matrix<T>& operator/=(const T& a);
    
private:

    template<typename Y>
    friend Matrix<Y> operator+(Matrix<Y> A, const Matrix<Y>& B);
    
    template<typename Y>
    friend Matrix<Y> operator-(Matrix<Y> A, const Matrix<Y>& B);
    
    template<typename Y>
    friend Matrix<Y> operator*(Y a, Matrix<Y> B);

    template<typename Y>
    friend Matrix<Y> operator*(Matrix<Y> B, Y a);
    
    template<typename Y>
    friend Matrix<Y> operator/(Matrix<Y> B, Y a);
    
};

//template<typename T>
//Matrix<T> operator*(T a, Matrix<T>& B);

template<typename T>
Matrix<T> operator*(Matrix<T> A, const Matrix<T>& B);


#include "tpp/Matrix.tpp"

//template <typename T>
//Matrix<T> transpose( const Matrix<T> &A );
//#include "tpp/transpose.tpp"


#endif

