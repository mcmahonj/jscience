// STL
#include <string>              // std::string

// jScience
#include "jScience/linalg.hpp" // Matrix<>, Vector<>, normalize()


//
// DESC: Normalize a (stochastic) matrix so that the elements of A [row ("right") or column ("left")] sum to one.
//
// -----
//
// TODO: NOTE: I am not sure how to (best) normalize a doubly stochastic matrix (it seems that the order or row/column normalization would matter).
//
template <typename T>
Matrix<double> normalize_matrix(
                                const Matrix<T>   &A,
                                const std::string  direction
                                )
{
    Matrix<double> A_norm(A.size(0),A.size(1));

    if( direction == "right" )
    {
        for( auto i = 0; i < A.size(0); ++i )
        {
            Vector<double> Ai_norm = normalize(A.row(i));

            for( auto j = 0; j < A.size(1); ++j )
            {
                A_norm(i,j) = Ai_norm(j);
            }
        }
    }
    else if( direction == "left" )
    {
        for( auto j = 0; j < A.size(1); ++j )
        {
            Vector<double> Aj_norm = normalize(A.column(j));

            for( auto i = 0; i < A.size(0); ++i )
            {
                A_norm(i,j) = Aj_norm(j);
            }
        }
    }

    return A_norm;
}
