// jScience
#include "jScience/linalg.hpp" // Vector<>, pnorm()


//
// DESC: Normalize a vector so that the elements xi^p sum to one.
//
template <typename T>
Vector<double> normalize(
                         const Vector<T> &u,
                         const int        p
                         )
{
    Vector<double> u_norm(u.size());

    double unorm = pnorm(u, p);

    for( auto i = 0; i < u.size(); ++i )
    {
        u_norm(i) = static_cast<double>(u(i))/unorm;
    }

    return u_norm;
}
