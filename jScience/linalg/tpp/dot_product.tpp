
// jScience
#include "../Vector.hpp" // Vector<>


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       T dot_product(const Vector<T> &u, const Vector<T> &v)
//
// DESC: Calculate the dot product between vectors u and v.
//
//========================================================================
//========================================================================
template <typename T>
T dot_product(const Vector<T> &u, const Vector<T> &v)
{
    // TODO: the STL has std::inner_product() in <numeric> ... would calling that be faster than below? 
    
    T dp = T(0);
    
    for( auto i = 0; i < u.size(); ++i )
    {
        dp += (u(i)*v(i));
    }
    
    return dp;
}
