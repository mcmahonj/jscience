
#include "../Matrix.hpp"

// STL
#include <stdexcept> // std::runtime_error()

// jScience
#include "jScience/stl/MultiVector.hpp" // MultiVector


template<typename T>
Matrix<T>::Matrix() : Matrix<T>::Matrix(0,0) {};

template<typename T>
Matrix<T>::Matrix(const unsigned int r,
                  const unsigned int c,
                  const std::vector<T> &v,
                  const char f) : MultiVector<T>::MultiVector(std::vector<typename std::vector<T>::size_type>{r, c}, v, f) {};

template<typename T>
Matrix<T>::Matrix(const unsigned int r, const unsigned int c, T init) : MultiVector<T>::MultiVector(r, c, init) {};

template<typename T>
Matrix<T>::~Matrix() {};


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Vector<T> Matrix<T>::row(const unsigned int i) const
//
//       Vector<T> Matrix<T>::column(const unsigned int j) const
//
// DESC: Returns the row or column of a matrix.
//
//========================================================================
//========================================================================
template<typename T>
Vector<T> Matrix<T>::row(const unsigned int i) const
{
    Vector<T> x(this->size(1));
    
    for( auto j = 0; j < this->size(1); ++j )
    {
        x(j) = (*this)(i,j);
    }
    
    return x;
}

template<typename T>
Vector<T> Matrix<T>::column(const unsigned int j) const
{
    Vector<T> x(this->size(0));
    
    for( auto i = 0; i < this->size(0); ++i )
    {
        x(i) = (*this)(i,j);
    }
    
    return x;
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& B)
//
//       Matrix<T> operator+(Matrix<T> A, const Matrix<T>& B)
//
// DESC: Add two matrices.
//
//========================================================================
//========================================================================
template<typename T>
Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& B)
{
    if( B.data.size() != this->data.size() )
    {
        throw std::runtime_error( "Error in Matrix<T>& Matrix<T>::operator+=: Matrices A and B are not the same size." );
    }
    
    for( decltype(this->data.size()) i = 0; i < this->data.size(); ++i )
    {
        this->data[i] += B.data[i];
    }
    
    return (*this);
}

template<typename T>
Matrix<T> operator+(Matrix<T> A, const Matrix<T>& B)
{
    return (A += B);
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& B)
//
//       Matrix<T> operator-(Matrix<T> A, const Matrix<T>& B)
//
// DESC: Subtract two matrices.
//
//========================================================================
//========================================================================
template<typename T>
Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& B)
{
    if( B.data.size() != this->data.size() )
    {
        throw std::runtime_error( "Error in Matrix<T>& Matrix<T>::operator-=: Matrices A and B are not the same size." );
    }
    
    for( decltype(this->data.size()) i = 0; i < this->data.size(); ++i )
    {
        this->data[i] -= B.data[i];
    }
    
    return (*this);
}

template<typename T>
Matrix<T> operator-(Matrix<T> A, const Matrix<T>& B)
{
    return (A -= B);
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Matrix<T>::Matrix<T>& operator*=(const T& a)
//
//       Matrix<T> operator*(T a, Matrix<T> B)
//
// DESC: Multiply a matrix by a scalar.
//
//========================================================================
//========================================================================
template<typename T>
Matrix<T>& Matrix<T>::operator*=(const T& a)
{
    for( auto &x : this->data )
    {
        x *= a;
    }
    
    return (*this);
}

template<typename T>
Matrix<T> operator*(T a, Matrix<T> B)
{
    return (B *= a);
}

template<typename T>
Matrix<T> operator*(Matrix<T> B, T a)
{
    return (B *= a);
}

//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Matrix<T>& Matrix<T>::operator/=(const T& a)
//
//       Matrix<T> operator/(Matrix<Y> B, Y a)
//
// DESC: Divide a matrix by a scalar.
//
//========================================================================
//========================================================================
template<typename T>
Matrix<T>& Matrix<T>::operator/=(const T& a)
{
    for( auto &x : this->data )
    {
        x /= a;
    }
    
    return (*this);
}

template<typename T>
Matrix<T> operator/(Matrix<T> B, T a)
{
    return (B /= a);
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Matrix<T> operator*(Matrix<T> A, const Matrix<T>& B)
//
// DESC: Perform naive matrix multiplication.
//
//========================================================================
//========================================================================
template<typename T>
Matrix<T> operator*(Matrix<T> A, const Matrix<T>& B)
{
    // check that the matrices are of the right dimension to multiply
    if( A.get_size(1) != B.get_size(0) )
    {
        throw std::runtime_error( "Error in Matrix<T>::Matrix<T> operator*=(): Matrices A and B are not of the correct size." );
    }
    
    Matrix<T> C( A.get_size(0), B.get_size(1), T(0) );
    
    for( unsigned int i = 0; i < C.get_size(0); ++i )
    {
        for( unsigned int j = 0; j < C.get_size(1); ++j )
        {
            for( unsigned int k = 0; k < A.get_size(1); ++k )
            {
                C(i,j) += ( A(i,k)*B(k,j) );
            }
        }
    }
    
    return C;
}