/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/24/2015
// LAST UPDATE: 5/24/2015

#include "../Matrix.hpp"


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Matrix<T> transpose( const Matrix<T> &A )
//
// DESC: Returns the naive transpose of the matrix A.
//
//========================================================================
//========================================================================
template<typename T>
Matrix<T> transpose( const Matrix<T> &A )
{
    Matrix<T> AT( A.get_size(1), A.get_size(0) );
    
    for( unsigned int i = 0; i < A.get_size(1); ++i )
    {
        for( unsigned int j = 0; j < A.get_size(0); ++j )
        {
            AT(i, j) = A(j, i);
        }
    }
    
    return AT;
}