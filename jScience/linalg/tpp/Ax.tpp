/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/27/2015
// LAST UPDATE: 8/24/2015


// STL
#include <type_traits> // std::common_type<>

// jScience
#include "../Matrix.hpp" // Matrix<>
#include "../Vector.hpp" // Vector<>


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       Vector<T> operator*(const Vector<T> &x, const Matrix<T> &A)
//       Vector<T> operator*(const Matrix<T> &A, const Vector<T> &x)
//
// DESC: Calculates the matrix vector product Ax or x^TA.
//
//========================================================================
//========================================================================
template <typename T>
Vector<T> operator*(const Matrix<T> &A, const Vector<T> &x)
{
    // [m x n]*[n x 1] = [m x 1]
    
    Vector<T> Ax(A.size(0), T(0));
    
    for( auto i = 0; i < A.size(0); ++i )
    {
        for( auto j = 0; j < A.size(1); ++j )
        {
            Ax(i) += A(i,j)*x(j);
        }
    }
    
    return Ax;
}

// << jmm: ! the follow is ambiguous, so perhaps assuming a Vector<> is a column vectore would be best .. meaning the following does not work >> 

template<typename T, typename Y>
Vector<typename std::common_type<T,Y>::type> operator*(const Vector<T> &x, const Matrix<Y> &A)
{
    // [1 x m]*[m x n] = [1 x n]
    
    Vector<typename std::common_type<T,Y>::type> xA(A.size(1), T(0));
    
    for( auto i = 0; i < A.size(1); ++i )
    {
        for( auto j = 0; j < A.size(0); ++j )
        {
            xA(i) += x(j)*A(j,i);
        }
    }
    
    return xA;
}

