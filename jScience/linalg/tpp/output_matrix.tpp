// STL
#include <fstream>                         // std::ofstream
#include <string>                          // std::string

// Boost
#include <boost/archive/text_oarchive.hpp> // boost::archive::text_oarchive
#include <boost/serialization/string.hpp>  // serialize std::string
#include <boost/serialization/vector.hpp>  // serialize std::vector<>

// jScience
#include "jScience/linalg.hpp"             // Matrix<>


//
// DESC: Output a Matrix<> to a file.
//
// NOTE: This uses Boost (text archive).
//
// -----
//
template<typename T>
inline void output_matrix(
                          const Matrix<T>   &_matrix,
                          // -----
                          const std::string  filename
                          )
{
    std::ofstream ofs(filename);

    boost::archive::text_oarchive oa(ofs);

    oa << _matrix;

    ofs.close();
}
