// STL
#include <cmath>     // std::pow()
#include <cstdlib>   // std::abs()
#include <stdexcept> // std::runtime_error()

// jScience
#include "../Vector.hpp" // Vector<>


//
// DESC: Calculates the p-norm of a vector x ||x||_p.
//
template <typename T>
double pnorm(const Vector<T> &u, const int p)
{
    if( p < 1 )
    {
        throw std::runtime_error("Error in pnorm(): p must be >= 1");
    }
    // << jmm: would we save further by isolating the cause of p == 2, so that we can use std::sqrt()? >>
    else if( (p%2) == 0 )
    {
        double xp = 0.0;

        // << jmm: here and below we have to static cast to double, because we don't have a routine that handles powers of integers >>
        for( auto i = 0; i < u.size(); ++i )
        {
            xp += std::pow(static_cast<double>(u(i)), p);
        }

        return std::pow(xp, (1.0/static_cast<double>(p)));
    }
    else
    {
        double xp = 0.0;

        for( auto i = 0; i < u.size(); ++i )
        {
            xp += std::pow(std::abs(static_cast<double>(u(i))), p);
        }

        if( p == 1 )
        {
            return xp;
        }
        else
        {
            return std::pow(xp, (1.0/static_cast<double>(p)));
        }
    }
}
