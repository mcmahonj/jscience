/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 10/14/2015
// LAST UPDATE: 10/14/2015


// STL
#include <cmath>     // std::acos()

// jScience
#include "jScience/linalg.hpp" // Vector<>, pnorm()


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       double angle(const Vector<T> &u, const Vector<T> &v)
//
// DESC: Returns the angle between two vectors.
//
//========================================================================
//========================================================================
template <typename T>
double angle(const Vector<T> &u, const Vector<T> &v)
{
    return std::acos(dot_product(u,v)/(pnorm(u,2)*pnorm(v,2)));
}
