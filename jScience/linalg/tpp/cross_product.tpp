// jScience
#include "jScience/linalg/Vector.hpp" // Vector<>


//========================================================================
//========================================================================
//
// DESC: Calculate the cross product between vectors u and v.
//
//========================================================================
//========================================================================
template <typename T>
Vector<T> cross_product(
                        const Vector<T> &u,
                        const Vector<T> &v
                        )
{
    // NOTE: the cross product is only defined for three dimensional vectors (similar operations in other dimensions are named differently)
    
    Vector<T> x(3);
    
    x(0) = u(1)*v(2) - u(2)*v(1);
    x(1) = u(2)*v(0) - u(0)*v(2);
    x(2) = u(0)*v(1) - u(1)*v(0);
    
    return x;
}
