/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/27/2015
// LAST UPDATE: 8/24/2015


// STL
#include <type_traits>   // std::common_type<>

// jScience
#include "../Matrix.hpp" // Matrix<>
#include "../Vector.hpp" // Vector<>


//========================================================================
//========================================================================
//
// NAME: template<typename T, typename Y>
//       Vector<typename std::common_type<T,Y>::type> outer_product(const Vector<T> &u, const Vector<Y> &v)
//
// DESC: Returns the outer product between two vectors: A = u*v^T (where u and v are assumed to be column vectors)
//
//========================================================================
//========================================================================
template<typename T, typename Y>
Matrix<typename std::common_type<T,Y>::type> outer_product(const Vector<T> &u, const Vector<Y> &v)
{
    Matrix<typename std::common_type<T,Y>::type> A(u.size(), v.size());
    
    for( auto i = 0; i < u.size(); ++i )
    {
        for( auto j = 0; j < v.size(); ++j )
        {
            A(i,j) = u(i)*v(j);
        }
    }
    
    return A;
}
