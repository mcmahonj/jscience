// STL
#include <stdexcept>     // std::runtime_error()

// jScience
#include "../Vector.hpp" // Vector<>, Matrix<>


//
// DESC: Calculates the Hadamard product (element-by-element multiplication) between two vectors.
//
template <typename T>
Vector<T> Hadamard_product(
                           const Vector<T> &u,
                           const Vector<T> &v
                           )
{
    Vector<T> c(u.size());

    for( auto i = 0; i < u.size(); ++i )
    {
        c(i) = u(i)*v(i);
    }

    return c;
}


//
// DESC: Calculates the Hadamard product (element-by-element multiplication) between two matrices.
//
template <typename T>
Matrix<T> Hadamard_product(
                           const Matrix<T> &A,
                           const Matrix<T> &B
                           )
{
    if( (A.size(0) != B.size(0)) || (A.size(1) != B.size(1)) )
    {
        throw std::runtime_error("error in Hadamard_product(): matrices are not square");
    }

    Matrix<T> C(A.size(0),B.size(1));

    for(auto i = 0; i < A.size(0); ++i)
    {
        for(auto j = 0; j < B.size(1); ++j)
        {
            C(i,j) = A(i,j)*B(i,j);
        }
    }

    return C;
}
