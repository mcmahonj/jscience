// STL
#include <fstream>                         // std::ifstream
#include <string>                          // std::string

// Boost
#include <boost/archive/text_iarchive.hpp> // boost::archive::text_iarchive
#include <boost/serialization/string.hpp>  // serialize std::string
#include <boost/serialization/vector.hpp>  // serialize std::vector<>

// jScience
#include "jScience/linalg.hpp"             // Matrix<>


//
// DESC: Input a Matrix<> from a file.
//
// NOTE: This uses Boost (text archive).
//
// -----
//
template<typename T>
inline Matrix<T> input_matrix(
                              const std::string filename
                              )
{
    Matrix<T> _matrix;

    std::ifstream ifs(filename);

    boost::archive::text_iarchive ia(ifs);

    ia >> _matrix;

    ifs.close();

    return _matrix;
}
