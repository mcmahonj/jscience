// STL
#include <stdexcept>           // std::runtime_error()

// jScience
#include "jScience/linalg.hpp" // Hadamard_product(), transpose()

//
// DESC: Calculates the trace of the matrix A.
//
template <typename T>
T trace(
        const Matrix<T> &A
        )
{
    if( A.size(0) != A.size(1) )
    {
        throw std::runtime_error("error in trace(): matrix is not square");
    }

    T tr = T(0);

    for(auto i = 0; i < A.size(0); ++i)
    {
        tr += A(i,i);
    }

    return tr;
}

//
// DESC: Calculates the trace of the product of (same-sized) matrices A and B.
//
// NOTE: This is much faster then first calculating the matrix product A*B and then calling the above subroutine.
//
template <typename T>
T trace(
        const Matrix<T> &A,
        const Matrix<T> &B
        )
{

    // NOTE: Let the error check fall through to Hadamard_product().

//    if( (A.size(0) != B.size(0)) || (A.size(1) != B.size(1)) )
//    {
//        throw std::runtime_error("error in trace(): matrices are not square");
//    }

    T tr = T(0);

    Matrix<T> C = Hadamard_product(A, transpose(B));

    for(auto i = 0; i < C.size(0); ++i)
    {
        for(auto j = 0; j < C.size(1); ++j)
        {
            tr += C(i,j);
        }
    }

    return tr;
}
