/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 6/27/2015
// LAST UPDATE: 10/13/2015

// for all pairs of operations (+/+=, -/-=, etc.), see the notes corresponding to -/-=

#include "../Vector.hpp"

// STL
//#include <stdexcept> // std::runtime_error()
#include <type_traits> // std::common_type<>
#include <vector>    // std::vector<>

// jScience
#include "jScience/stl/MultiVector.hpp" // MultiVector


template<typename T>
Vector<T>::Vector() : MultiVector<T>::MultiVector() {};

template<typename T>
Vector<T>::Vector(const unsigned int n,
                  const T init) : MultiVector<T>::MultiVector(n, init) {};

template<typename T>
Vector<T>::Vector(const std::vector<T>& v) : MultiVector<T>::MultiVector(std::vector<typename std::vector<T>::size_type>{v.size()}, v, 'r') {};
                  
template<typename T>
Vector<T>::~Vector() {};


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       unsigned int Vector<T>::size() const
//
// DESC: Returns the size of the Vector.
//
//========================================================================
//========================================================================
template<typename T>
unsigned int Vector<T>::size() const
{
    // note: using this->data.size() instead of this->size(0) allows us whether a default Vector<> (0 size) is declared
    return this->data.size();
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<T> Vector<T>::std_vector() const
//
// DESC: Returns the std::vector<> object corresponding to a Vector<>.
//
//========================================================================
//========================================================================
template<typename T>
std::vector<T> Vector<T>::std_vector() const
{
    return this->data;
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Vector<T>& Vector<T>::operator+=(const Vector<T>& v)
//
//       Vector<T> operator+(Vector<T> u, const Vector<T>& v)
//
// DESC: Add two vectors.
//
//========================================================================
//========================================================================
template<typename T>
Vector<T>& Vector<T>::operator+=(const Vector<T>& v)
{
    for( decltype(this->data.size()) i = 0; i < this->data.size(); ++i )
    {
        this->data[i] += v.data[i];
    }
    
    return (*this);
}

template<typename T>
Vector<T> operator+(Vector<T> u, const Vector<T>& v)
{
    return (u += v);
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Vector<T>& Vector<T>::operator-=(const Vector<T>& v)
//
//       Vector<T> operator-(Vector<T> u, const Vector<T>& v)
//
// DESC: Subtracts two vectors.
//
//========================================================================
//========================================================================
template<typename T>
template<typename Y>
Vector<T>& Vector<T>::operator-=(const Vector<Y>& v)
{
    // see the note in operator-() ... because here we convert to the most common data type and then cast to the appropriate return type
    
    Vector<typename std::common_type<T,Y>::type> u_minus_v = (*this) - v;
    
    for(auto i = 0; i < u_minus_v.size(); ++i)
    {
        this->data[i] = static_cast<T>(u_minus_v(i));
    }
    
    return (*this);
}

template<typename Y, typename Z>
Vector<typename std::common_type<Y,Z>::type> operator-(Vector<Y> u, const Vector<Z>& v)
{
    // one cannot simply call (u -= v), because, for example, (Vector<int> - Vector<double>) should return a Vector<double>, but (Vector<int> -= Vector<double>) should return a Vector<int> 

    Vector<Y> u_data = u.get_data();
    Vector<Z> v_data = v.get_data();
    
    Vector<typename std::common_type<Y,Z>::type> u_minus_v(u.size());
    
    for(auto i = 0; i < u_minus_v.size(); ++i)
    {
        u_minus_v(i) = u_data(i) - v_data(i);
    }
    
    return u_minus_v;
}



//========================================================================
//========================================================================
//
// NAME: template<typename T, typename Y>
//       Vector<typename std::common_type<T,Y>::type>& Vector<T>::operator*=(const Y x)
//
//       Vector<typename std::common_type<T,Y>::type> operator/(Vector<T> u, const Y x)
//
// DESC: Multiply a vector by a number.
//
//========================================================================
//========================================================================
template<typename T>
template<typename Y>
Vector<T>& Vector<T>::operator*=(const Y x)
{
    Vector<typename std::common_type<T,Y>::type> ux = (*this)*x;
    
    for(auto i = 0; i < ux.size(); ++i)
    {
        this->data[i] = static_cast<T>(ux(i));
    }
    
    return (*this);
}

template<typename Y, typename Z>
Vector<typename std::common_type<Y,Z>::type> operator*(Vector<Y> u, const Z x)
{
    Vector<typename std::common_type<Y,Z>::type> ux(u.size());
    
    for(auto i = 0; i < ux.size(); ++i)
    {
        ux(i) = u(i)*x;
    }
    
    return ux;
}

template<typename Y, typename Z>
Vector<typename std::common_type<Y,Z>::type> operator*(const Z x, Vector<Y> u)
{
    return (u*x);    
}

template<typename Y>
Vector<Y> operator-(Vector<Y> u)
{
    return (u *= Y(-1));
}

//========================================================================
//========================================================================
//
// NAME: template<typename T, typename Y>
//       Vector<typename std::common_type<T,Y>::type>& Vector<T>::operator/=(const Y x)
//
//       Vector<typename std::common_type<T,Y>::type> operator/(Vector<T> u, const Y x)
//
// DESC: Divides a vector by a number.
//
//========================================================================
//========================================================================
template<typename T>
template<typename Y>
Vector<typename std::common_type<T,Y>::type>& Vector<T>::operator/=(const Y x)
{
    for( auto &di : this->data )
    {
        di /= x;
    }
    
    return (*this);
}

template<typename T, typename Y>
Vector<typename std::common_type<T,Y>::type> operator/(Vector<T> u, const Y x)
{
    return (u /= x);
}
