/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 1/12/2015
// LAST UPDATE: 1/12/2015

#ifndef DECOMPOSITION_H
#define DECOMPOSITION_H


// STL
#include <vector> // std::vector

// This
#include "jScience/linalg/Matrix.hpp"    // Matrix


//========================================================================
//========================================================================
//
// NAME: std::tuple<Matrix<double>, std::vector<double>, Matrix<double>> SVD( const Matrix<double> &A )
//
// DESC: Performs singular value decomposition (SVD) (using LAPACK).
//
// INPUT:
//          Matrix<double>      A   :: The MxN matrix to decompose via SVD
//
// OUTPUT:
//          Matrix<double>      U   :: The orthogonal MxM matrix U
//          std::vector<double> S   :: Singular values of A, sorted such that S(i) >= S(i+1)
//          Matrix<double>      V^T :: The orthogonal NxN matrix V^T
//
// NOTES:
//     ! See: http://www.netlib.org/clapack/old/double/dgesvd.c
//
//========================================================================
//========================================================================
std::tuple<Matrix<double>, std::vector<double>, Matrix<double>> SVD( const Matrix<double> &A );


#endif

