#ifndef JSCIENCE_LINALG_VECTOR_HPP
#define JSCIENCE_LINALG_VECTOR_HPP


// STL
#include <type_traits> // std::common_type<>
#include <vector>      // std::vector<>

// jScience
#include "jScience/stl/MultiVector.hpp" // MultiVector<>


//=========================================================
// Vector<>
//=========================================================
template<typename T>
class Vector : public MultiVector<T>
{
     
public:
    
    Vector();
    Vector(const unsigned int n,
           const T init = T(0));
    Vector(const std::vector<T>& v);
    ~Vector();
    
    using MultiVector<T>::size;
    unsigned int size() const;
    
    std::vector<T> std_vector() const;
    
//    void push_back(const T);
    
    Vector<T>& operator+=(const Vector<T>& v);
    
    template<typename Y>
    Vector<T>& operator-=(const Vector<Y>& v);
//    Vector<typename std::common_type<T,Y>::type>& operator-=(const Vector<Y>& v);
//    Vector<T>& operator-=(const Vector<T>& v);
    
//    template<typename Y>
//    Vector<typename std::common_type<T,Y>::type>& operator*=(const Y x);
    template<typename Y>
    Vector<T>& operator*=(const Y x);
    
    template<typename Y>
    Vector<typename std::common_type<T,Y>::type>& operator/=(const Y x);
    
private:

    template<typename Y>
    friend Vector<Y> operator+(Vector<Y> u, const Vector<Y>& v);
    
    template<typename Y, typename Z>
    friend Vector<typename std::common_type<Y,Z>::type> operator-(Vector<Y> u, const Vector<Z>& v);
    
//    template<typename Y>
//    friend Vector<Y> operator-(Vector<Y> u, const Vector<Y>& v);
    
    template<typename Y, typename Z>
    friend Vector<typename std::common_type<Y,Z>::type> operator*(Vector<Y> u, const Z x);
    
    template<typename Y, typename Z>
    friend Vector<typename std::common_type<Y,Z>::type> operator*(const Z x, Vector<Y> u);
    
    template<typename Y>
    friend Vector<Y> operator-(Vector<Y> u);
    
    template<typename Y, typename Z>
    friend Vector<typename std::common_type<Y,Z>::type> operator/(Vector<Y> u, const Z x);
    
};

#include "./tpp/Vector.tpp"


#endif

