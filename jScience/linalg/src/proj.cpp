
// jScience
#include "jScience/linalg.hpp" // Vector<>, scalar_proj()


Vector<double> proj(
                    const Vector<double> &a,
                    const Vector<double> &b
                    )
{
    return ( scalar_proj(a, b)*b );
}




