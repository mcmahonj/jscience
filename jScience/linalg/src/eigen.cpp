
// STL
#include <algorithm>    // std::max()
#include <complex>      // std::complex<>
#include <vector>       // std::vector<>
#include <tuple>        // std::tuple<>, std::make_tuple()

// jScience
#include "jScience/linalg/Matrix.hpp"   // Matrix<>
#include "jScience/stl/MultiVector.hpp" // MultiVector<>

// LAPACK/BLAS
// NOTE: with including Boost, it seems that the Boost header files must be called prior to LAPACK
#if defined(WLAPACKLINUX)
#include <lapacke/lapacke.h>
#elif defined(WLAPACKLINUX2)
#include <lapacke.h>
#elif defined(WLAPACKMAC)
#include <Accelerate/Accelerate.h>  // dgeev_()
#endif


// TODO: subroutines like this (matrix) should rely on Matrix<>, NOT MultiVector<>


//
// DESC: Computes, for an NxN real nonsymmetric matrix, the eigenvalues and optionally the left and/or right eigenvectors.
//
// INPUT:
//          Matrix<double>      A     :: The NxN matrix
//
// OUTPUT:
//          int                               INFO  :: DGEEV exit code
//          std::vector<std::complex<double>> W     :: eigenvalues
//          MultiVector<std::complex<double>> VL    :: left eigenvectors
//          MultiVector<std::complex<double>> VR    :: right eigenvectors
//
// NOTES:
//     ! See: http://www.netlib.org/lapack/double/dgeev.f
//
// -----
//
// TODO: NOTE: It does not look like the left eigenvectors are stored as row vectors (like they should be).
//
std::tuple<
           int,
           std::vector<std::complex<double>>,
           MultiVector<std::complex<double>>,
           MultiVector<std::complex<double>>
           > eigen(
                   const Matrix<double> &A
                   )
{
    char JOBVL = 'V';
    char JOBVR = 'V';

    int N = A.get_size(0);

    std::vector<double> vA = to_colMajOrd(A);

    int LDA = std::max(1, N);

    std::vector<double> WR(N), WI(N);

    int LDVL = N;
    int LDVR = N;
    std::vector<double> VL(LDVL*N), VR(LDVR*N);

    // note: the optimal LWORK was taken from what I use in (LAPACK) SVD() (with M=N)
    // << jmm: see the note on the website, it suggests calling another routine to determine the optimal value >>
    int LWORK = std::max( 1, 5*N );
    std::vector<double> WORK( std::max(1, LWORK) );

    int INFO;

#if defined(WLAPACKLINUX) || defined(WLAPACKLINUX2) || defined(WLAPACKMAC)
    // note: all values must be passed by address for LAPACK, not value
    dgeev_( &JOBVL, &JOBVR, &N, &*vA.begin(), &LDA, &*WR.begin(), &*WI.begin(), &*VL.begin(), &LDVL, &*VR.begin(), &LDVR, &*WORK.begin(), &LWORK, &INFO );

#else
    std::cerr << "careful: eigen() will not work because of no LAPACK";
#endif

    std::vector<std::complex<double>> W;

    unsigned int uLDVL = static_cast<unsigned int>(LDVL);
    unsigned int uLDVR = static_cast<unsigned int>(LDVR);
    unsigned int uN = static_cast<unsigned int>(N);

    MultiVector<double> mv_VL(std::vector<std::vector<double>::size_type>{uLDVL, uN}, VL, 'c');
    MultiVector<double> mv_VR(std::vector<std::vector<double>::size_type>{uLDVR, uN}, VR, 'c');

    MultiVector<std::complex<double>> cmv_VL(std::vector<std::vector<double>::size_type>{uLDVL, uN});
    MultiVector<std::complex<double>> cmv_VR(std::vector<std::vector<double>::size_type>{uLDVR, uN});

    for( int i = 0; i < N; ++i )
    {
        W.push_back( std::complex<double>(WR[i], WI[i]) );

        // note: complex eigenvalues are stored as conjugate pairs as (WR[i] + i*WI[i]) and (WR[i+1] - i*WI[i+1])
        // ... and, in this case eigenvectors are stored as VL[:,j] + i*VL[:,j+1] (must manually form the conjugate pair)
        if( std::imag(W.back()) != 0.0 )
        {
            W.push_back( std::complex<double>(WR[i+1], WI[i+1]) );

            for( int j = 0; j < LDVL; ++j )
            {
                cmv_VL(j, i)     = std::complex<double>( mv_VL(j, i), mv_VL(j, (i+1)) );
                cmv_VL(j, (i+1)) = std::complex<double>( mv_VL(j, i), -mv_VL(j, (i+1)) );
            }

            for( int j = 0; j < LDVR; ++j )
            {
                cmv_VR(j, i)     = std::complex<double>( mv_VR(j, i), mv_VR(j, (i+1)) );
                cmv_VR(j, (i+1)) = std::complex<double>( mv_VR(j, i), -mv_VR(j, (i+1)) );
            }

            ++i;
        }
        else
        {
            // TODO: NOTE: Shouldn't the left eigenvectors be stored as row vectors?
            for( int j = 0; j < LDVL; ++j )
            {
                cmv_VL(j, i) = mv_VL(j, i);
            }

            for( int j = 0; j < LDVR; ++j )
            {
                cmv_VR(j, i) = mv_VR(j, i);
            }
        }
    }

    return std::make_tuple(INFO, W, cmv_VL, cmv_VR);
}
