// jScience
#include "jScience/linalg.hpp" // Vector<>, Matrix<>, pnorm()

//
// DESC: Solves the set of n linear equations A*x=b. Here, A is input as its LU decomposition (determined by ludcmp), not itself.
//
// NOTES: See: Numerical Recipes in C, p. 44
//             Numerical Recipes 3rd ed. (C++), p. 53 (LUdcmp::solve())
//
Vector<double> lubksb(
                      const Matrix<double> &A_LU,
                      const int             n,
                      const Vector<int>    &indx,
                      const Vector<double> &b
                      )
{
    Vector<double> x = b;
    
    //=========================================================
    // FORWARD-SUBSTITUTION; Eq. (2.3.6)
    //=========================================================
    
    // NOTE: when ii is set to a positive value, it will become the index of the first nonvanishing element of b
    int ii = 0;
    
    for(int i = 0; i < n; ++i)
    {
        int ip = indx(i);
        
        double sum = x(ip);
        
        x(ip) = x(i);

        if(ii != 0)
        {
            for(int j = (ii-1); j < i; ++j)
            {
                sum -= A_LU(i,j)*x(j);
            }
        }
        else if(sum != 0.) // a nonzero element was encountered, so from now on we will have to do the sums in the loop above
        {
            ii = (i+1);
        }
        
        x(i) = sum;
    }
    
    //=========================================================
    // BACK-SUBSTITUTION; Eq. (2.3.7)
    //=========================================================
    
    for(int i = (n-1); i >= 0; --i)
    {
        double sum = x(i);
        
        for(int j = (i+1); j < n; ++j)
        {
            sum -= A_LU(i,j)*x(j);
        }
        
        x(i) = sum/A_LU(i,i);
    }
    
    return x;
}
