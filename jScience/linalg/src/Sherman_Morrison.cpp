// jScience
#include "jScience/linalg.hpp" // Matrix<>, Vector<>, outer_product(), dot_product()


//
// DESC: Computes the inverse of the sum of an invertible matrix A and the outer product uv^T, of vectors u and v.
//
// NOTE:
//
//                            A^-1 uv^T A^-1
//     (A + uv^T)^-1 = A^-1 - --------------
//                            1 + v^T A^-1 u
//
// INPUT:
//          Matrix<double>      A_inv     :: inverse of the matrix A
//          Vector<double>      u         :: vector u
//          Vector<double>      v         :: vector v
//
// OUTPUT:
//          Matrix<double>      A_uvT_inv :: inverse of the matrix (A + uv^T)
//
// NOTE: The main cases:
//
//     - If only u_i and v_j are non-zero, (u_i v_j) is added to a_i,j.
//     - If (u_i == 1) and other components of the vector equal 0, vector v is added to the row i in matrix A.
//     - If (v_j == 1) and other components of the vector equal 0, vector u is added to the column j in matrix A.
//     - If u and v are arbitrary vectors, row v (column u) is added to different rows (columns) with different multipliers.
//
// NOTE: The Sherman--Morrison formula is a special case of the Woodbury formula.
//
// NOTE: See: https://en.wikipedia.org/wiki/Sherman%E2%80%93Morrison_formula
//
Matrix<double> Sherman_Morrison(
                                const Matrix<double> &A_inv,
                                const Vector<double> &u,
                                const Vector<double> &v
                                )
{
    Matrix<double> A_uvT_inv;

    Vector<double> A_inv_u  = A_inv*u;
    Vector<double> vT_A_inv = v*A_inv;

    Matrix<double> A_inv_u_vT_A_inv = outer_product(A_inv_u,vT_A_inv);

    A_uvT_inv = A_inv - A_inv_u_vT_A_inv/(1. + dot_product(v,A_inv_u));

    return A_uvT_inv;
}
