
// STL
#include <algorithm>    // std::max(), std::min()
#include <iostream>  // std::cerr
#include <vector>       // std::vector<>
#include <tuple>        // std::tuple<>, std::make_tuple()

// jScience
#include "jScience/linalg/Matrix.hpp" // Matrix

// LAPACK/BLAS
// NOTE: with including Boost, it seems that the Boost header files must be called prior to LAPACK
#if defined(WLAPACKLINUX)
#include <lapacke/lapacke.h>
#elif defined(WLAPACKLINUX2)
#include <lapacke.h>
#elif defined(WLAPACKMAC)
#include <Accelerate/Accelerate.h>  // dgetrf_()
#endif

//========================================================================
//========================================================================
//
// NAME: std::tuple<int, Matrix<double>, std::vector<int>> LU_factorization( const Matrix<double> &A )
//
// DESC: Computed an LU factorization of a general MxN matrix A (using LAPACK).
//
// INPUT:
//          Matrix<double>      A    :: The MxN matrix to be factored
//
// OUTPUT:
//          int                 INFO :: DGETRF exit code
//          Matrix<double>      A    :: The factorization of the form: A = P*L*U
//          std::vector<int>    IPIV :: The pivot indices
//
// NOTES:
//     ! See: http://www.netlib.no/netlib/lapack/double/dgetrf.f
//
//========================================================================
//========================================================================
std::tuple<
           int,
           Matrix<double>,
           std::vector<int>
           > LU_factorization(
                              const Matrix<double> &A
                              )
{ 
    int M = A.get_size(0);
    int N = A.get_size(1);

    std::vector<double> vA = to_colMajOrd(A);

    int LDA = std::max(1, M);

    std::vector<int> IPIV(std::min(M, N));

    int INFO;

#if defined(WLAPACKLINUX) || defined(WLAPACKLINUX2) || defined(WLAPACKMAC)
    // all values must be passed by address for LAPACK, not value
    dgetrf_(&M, &N, &*vA.begin(), &LDA, &*IPIV.begin(), &INFO);
#else
    std::cerr << "careful: LU_factorization() will not work because of no LAPACK";
#endif
    
    return std::make_tuple(INFO, Matrix<double>(M, N, vA, 'c'), IPIV);
}
