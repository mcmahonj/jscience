// STL
#include <cmath>     // std::abs()
#include <stdexcept> // std::runtime_error()
#include <tuple>     // std::tuple<>, std::make_tuple()

// jScience
#include "jScience/linalg.hpp" // Matrix<>, Vector<>


//
// DESC: Given a matrix A, replace it by the LU decomposition of a rowwise permutation of itself.
//
// OUTPUT:
//
//     A_LU :: LU decomposition of a rowwise permutation of itself --- Eq. (2.3.14)
//     indx :: output vector that records the row permutation effected by the partial pivoting
//     d    :: +/-1, depending on whether the number of row interchanges was even or odd, respctively
//
// NOTE: See: Numerical Recipes 3rd ed. (C++), p. 52--53 (LUdcmp::LUdcmp())
// NOTE: ... Numerical Recipes in C, pp. 43--44 appears to be much more lengthy 
//
std::tuple<
           Matrix<double>,
           Vector<int>,
           int 
           > ludcmp(
                    const Matrix<double> &A,
                    const int             n,
                    const double          tiny
                    )
{
    Matrix<double> A_LU = A;
    Vector<int>    indx(n);
    int            d = 1;
    
    //=========================================================
    // FIND IMPLICIT SCALING OF EACH ROW
    //=========================================================
    
    Vector<double> vv(n);

    for(int i = 0; i < n; ++i)
    {
        double big = 0.;
        
        for(int j = 0; j < n; ++j)
        {
            double tmp = std::abs(A_LU(i,j));
            
            if(tmp > big)
            {
                big = tmp;
            }
        }

        if(big == 0.)
        {
            throw std::runtime_error("error in ludcmp(): singular matrix");
        }

        vv(i) = 1./big; 
    }

    //=========================================================
    // OUTERMOST kij LOOP
    //=========================================================
    
    for(int k = 0; k < n; ++k)
    {
        // search for largest pivot element
        double big = 0.;
        int imax;
        
        for(int i = k; i < n; ++i)
        {
            double tmp = vv(i)*std::abs(A_LU(i,k));
            
            if(tmp > big)
            {
                big = tmp;
                
                imax = i;
            }
        }
        
        // interchange rows, if we need to (and also parity of d and scale factor)
        if(k != imax)
        {
            for(int j = 0; j < n; ++j)
            {
                double tmp = A_LU(imax,j);
                A_LU(imax,j) = A_LU(k,j);
                A_LU(k,j) = tmp;
            }
            
            d *= -1;
            
            vv(imax) = vv(k);
        }
        
        indx(k) = imax;
        
        // NOTE: if the pivot element is zero, the matrix is singular. For some applications (on singular matrices) though, it is desirable to substitute tiny for zero.
        if(A_LU(k,k) == 0.)
        {
            A_LU(k,k) = tiny;
        }
        
        for(int i = (k+1); i < n; ++i)
        {
            double tmp = A_LU(i,k) /= A_LU(k,k);
            
            for(int j = (k+1); j < n; ++j)
            {
                A_LU(i,j) -= tmp*A_LU(k,j);
            }
        }
    }
    
    return std::make_tuple(
                           A_LU,
                           indx,
                           d
                           );
}

