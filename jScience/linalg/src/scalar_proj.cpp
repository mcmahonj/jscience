
// jScience
#include "jScience/linalg.hpp" // Vector<>, dot_product(), pnorm()


double scalar_proj(
                   const Vector<double> &a,
                   const Vector<double> &b
                   )
{
    return ( dot_product(a, b)/pnorm(b, 2) );
}




