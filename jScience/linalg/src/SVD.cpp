// STL
#include <algorithm>                  // std::max
#include <stdexcept>                  // std::invalid_argument
#include <string>                     // std::string, ::to_string()
#include <tuple>                      // std::tuple<>, ::make_tuple()

// jScience
#include "jScience/linalg/Matrix.hpp" // Matrix

// LAPACK/BLAS
#if defined(WLAPACKLINUX)
#include <lapacke/lapacke.h>
#elif defined(WLAPACKLINUX2)
#include <lapacke.h>
#elif defined(WLAPACKMAC)
#include <Accelerate/Accelerate.h>    // dgesvd_()
#endif


//
// DESC: Singular value decomposition (SVD) (using LAPACK).
//
// -----
//
// INPUT:
//          Matrix<double>      A   :: The MxN matrix to decompose via SVD
//
// OUTPUT:
//          Matrix<double>      U   :: The orthogonal MxM matrix U
//          std::vector<double> S   :: Singular values of A, sorted such that S(i) >= S(i+1)
//          Matrix<double>      V^T :: The orthogonal NxN matrix V^T
//
// =====
//
// NOTE: See:
//
//     http://www.netlib.org/clapack/old/double/dgesvd.c
//
std::tuple<
           Matrix<double>,
           std::vector<double>,
           Matrix<double>
           > SVD(
                 const Matrix<double> &A
                 )
{
    char JOBU  = 'A';
    char JOBVT = 'A';

    int M   = A.get_size(0);
    int N   = A.get_size(1);
    int LDA = std::max(1, static_cast<int>(A.get_size(0)));
    // unfortunately, the matrix A is stored in row-major order and we *need* its data in column-major order for Fortran
    std::vector<double> A_CMO;
    for( unsigned int j = 0; j < A.get_size(1); ++j )
    {
        for( unsigned int i = 0; i < A.get_size(0); ++i )
        {
            A_CMO.push_back( A(i,j) );
        }
    }

    int LDU = M;
    std::vector<double> U_CMO( LDU*M );

    std::vector<double> S( std::min(M,N) );

    int LDVT = N;
    std::vector<double> VT_CMO( LDVT*N );

    int LWORK = std::max( 1, std::max( (3*std::min(M,N) + std::max(M,N)), 5*std::min(M,N) ) );
    std::vector<double> WORK( std::max(1, LWORK) );

    int INFO;
#if defined(WLAPACKLINUX) || defined(WLAPACKLINUX2) || defined(WLAPACKMAC)
    // NOTE: For LAPACK, all variables must be passed by address (not value).
    //
    dgesvd_( &JOBU, &JOBVT, &M, &N, &*A_CMO.begin(), &LDA, &*S.begin(), &*U_CMO.begin(), &LDU, &*VT_CMO.begin(), &LDVT, &*WORK.begin(), &LWORK, &INFO );
#else
    throw std::runtime_error( "cannot call dgesvd_()" );
#endif
    if( INFO < 0 )
    {
        std::string arg = "The " + std::to_string(-INFO) + "the argument to dgesvd() in SVD() is invalid";
        throw std::invalid_argument( arg );
    }
    else if( INFO > 0 )
    {
        std::string arg = std::to_string(INFO) + " superdiagonals of an intermediate bidiagonal form B did not converge to 0 in SVD()";
        throw std::invalid_argument( arg );
    }

    // unfortunately (again), we now must convert VT_CMO and VT_CMO from column-major to row-major order
    Matrix<double> U( LDU, M );
    for( int i = 0; i < LDU; ++i )
    {
        for( int j = 0; j < M; ++j )
        {
            U(i, j) = U_CMO[j*M + i];
        }
    }

    Matrix<double> VT( LDU, N );
    for( int i = 0; i < LDVT; ++i )
    {
        for( int j = 0; j < N; ++j )
        {
            VT(i, j) = VT_CMO[j*N + i];
        }
    }

    return std::make_tuple(
                           U,
                           S,
                           VT
                           );
}
