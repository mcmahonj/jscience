#include <complex>
#include "jScience/stl/MultiVector.hpp" // MultiVector<>

// STL
#include <stdexcept>    // std::runtime_error()
#include <vector>       // std::vector<>
#include <tuple>        // std::tie()
#include <utility>      // std::pair<>, std::make_pair()

// jScience
#include "jScience/linalg.hpp" // Matrix<>, LU_factorization()
#include "jstats.hpp"          // sum()


//
// DESC: Compute the determinant of the matrix A (using LU decomposition).
//
// INPUT:
//          Matrix<double> A :: The NxN matrix to compute the determinant for
//
// OUTPUT:
//          int    info :: LU_factorization (and hence DGETRF()) exit code
//          double det  :: The determinant of A
//
std::pair<
          int,
          double
          > determinant(
                        const Matrix<double> &A
                        )
{
/*
    if(A.size(0) != A.size(1))
    {
        throw std::runtime_error("error in determinant(): trying to compute the determinant of a non-square matrix");
    }
*/
    double det;
    
    // perform LU decomposition (using LAPACK)
    int info;
    Matrix<double> LU;
    std::vector<int> ipiv;
    std::tie(
             info,
             LU,
             ipiv
             ) = LU_factorization(
                                  A
                                  );
    
    if(info != 0)
    {
        throw std::runtime_error("error in determinant(): info = " + std::to_string(info) + " on return from LU_factorization()");
    }
    
    // compute the determinant by taking the product of the diagonal elements of the factored matrix
    det = 1.;
    
    for(auto i = 0; i < LU.size(0); ++i)
    {
        // NOTE: 1 <= ipiv[i] <= N rather than 0 <= ipiv[i] < N because of Fortran
        if(ipiv[i] != (i+1))
        {
            det *= -LU(i,i);
        }
        else
        {
            det *= LU(i,i);
        }
    }
    
    return std::make_pair(
                          info,
                          det
                          );
 
    // TODO: NOTE: I am not sure whether the following approach is correct, *but* it definitely does not work ... so if it is correct, then something may be wrong in eigen()
/*
    int info;
    std::vector<std::complex<double>> W;
    MultiVector<std::complex<double>> VL;
    MultiVector<std::complex<double>> VR;
    std::tie(info, W, VL, VR) = eigen(A);
    
    double det = 1.0;
    
    for( auto &wi : W )
    {
        det *= std::real(wi);
    }
    
    return std::make_pair(info, det);
*/
}


