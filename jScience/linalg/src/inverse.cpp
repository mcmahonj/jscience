
// TODO: should create a series of inverse subroutines (for 1x1, 2x2, 3x3, 4x4, etc.) and then measure the performance difference below by having a switch that goes to these (explicit ones) vs calling LAPACK; I am thinking that for small matrices, explicit subroutines will be faster

// STL
#include <algorithm>    // std::max(), std::min()
#include <vector>       // std::vector<>
#include <tuple>        // std::tuple<>, std::make_tuple()

// jScience
#include "jScience/linalg.hpp"        // LU_factorization()
#include "jScience/linalg/Matrix.hpp" // Matrix<>

// LAPACK/BLAS
// NOTE: with including Boost, it seems that the Boost header files must be called prior to LAPACK
#if defined(WLAPACKLINUX)
#include <lapacke/lapacke.h>
#elif defined(WLAPACKLINUX2)
#include <lapacke.h>
#elif defined(WLAPACKMAC)
#include <Accelerate/Accelerate.h>
#endif


//========================================================================
//========================================================================
//
// NAME: std::tuple<int,
//                  int,
//                  Matrix<double>> inverse(const Matrix<double> &A)
//
// DESC: Computes the inverse a matrix A using LU factorization (via LAPACK).
//
// INPUT:
//          Matrix<double>      A     :: The NxN matrix to be inverted (via LAPACK)
//
// OUTPUT:
//          int                 INFO0 :: DGETRF exit code
//          int                 INFO  :: DGETRI exit code
//          Matrix<double>      A     :: The inverse of the matrix A
//
// NOTES:
//     ! See: http://www.netlib.org/lapack/double/dgetri.f
//
//========================================================================
//========================================================================
std::tuple<
           int,
           int,
           Matrix<double>
           > inverse(
                     const Matrix<double> &A
                     )
{
    int N   = A.get_size(0);
    int LDA = std::max(1, N);

    int INFO0;
    Matrix<double> fA;
    std::vector<int> IPIV;
    std::tie(INFO0, fA, IPIV) = LU_factorization(A);

    if( INFO0 != 0 )
    {
        return std::make_tuple(INFO0, INFO0, Matrix<double>());
    }

    std::vector<double> vfA = to_colMajOrd(fA);

    // note: the optimal LWORK was taken from what I use in (LAPACK) SVD() (with M=N)
    // << jmm: see the note on the website, it suggests calling another routine to determine the optimal value >>
    int LWORK = std::max( 1, 5*N );
    std::vector<double> WORK( std::max(1, LWORK) );

    int INFO;

#if defined(WLAPACKLINUX) || defined(WLAPACKLINUX2) || defined(WLAPACKMAC)
    // note: all values must be passed by address for LAPACK, not value
    dgetri_( &N, &*vfA.begin(), &LDA, &*IPIV.begin(), &*WORK.begin(), &LWORK, &INFO );
#else
    std::cerr << "careful: inverse() will not work because of no LAPACK";
#endif

    return std::make_tuple(INFO0, INFO, Matrix<double>(N, N, vfA, 'c'));
}




