#ifndef jScience_MATH_HPP
#define jScience_MATH_HPP


int exponent(
             const double x
             );


int Kronecker_delta(
                    const int i,
                    const int j
                    );


double hsf(
           const double x,
           const int    convention = 0
           );
// #include "jScience/math/src/hsf.cpp"


bool is_prime(
              const int n
              );
// #include "jScience/math/src/is_prime.cpp"


#endif // jScience_MATH_HPP

