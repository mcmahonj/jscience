// NOTE: I should implement lower/upper triangular matrices, that save on memory requirements by just a little over 2x

#ifndef LINALG_H
#define LINALG_H


// STL
#include <complex>                    // std::complex<>
#include <string>                     // std::string
#include <vector>                     // std::vector<>
#include <tuple>                      // std::tuple<>
#include <type_traits>                // std::common_type<>
#include <utility>                    // std::pair<>

// jScience
#include "jScience/linalg/Matrix.hpp" // Matrix<>
#include "jScience/linalg/Vector.hpp" // Vector<>


template <typename T>
double pnorm(const Vector<T> &u, const unsigned int p);
#include "./linalg/tpp/pnorm.tpp"

template <typename T>
Vector<double> normalize(
                         const Vector<T> &u,
                         const int        p = 1
                         );
#include "./linalg/tpp/normalize.tpp"

template <typename T>
Matrix<double> normalize_matrix(
                                const Matrix<T>   &A,
                                const std::string  direction
                                );
#include "./linalg/tpp/normalize_matrix.tpp"


template <typename T>
T dot_product(const Vector<T> &u, const Vector<T> &v);
#include "./linalg/tpp/dot_product.tpp"

template <typename T>
Vector<T> cross_product(
                        const Vector<T> &u,
                        const Vector<T> &v
                        );
#include "./linalg/tpp/cross_product.tpp"

template <typename T>
Vector<T> Hadamard_product(
                           const Vector<T> &u,
                           const Vector<T> &v
                           );

template <typename T>
Matrix<T> Hadamard_product(
                           const Matrix<T> &A,
                           const Matrix<T> &B
                           );
#include "./linalg/tpp/Hadamard_product.tpp"

// returns the angle between two vectors
template <typename T>
double angle(const Vector<T> &u, const Vector<T> &v);
#include "./linalg/tpp/angle.tpp"

template<typename T, typename Y>
Matrix<typename std::common_type<T,Y>::type> outer_product(const Vector<T> &u, const Vector<Y> &v);
#include "./linalg/tpp/outer_product.tpp"

template <typename T>
Matrix<T> transpose( const Matrix<T> &A );
#include "./linalg/tpp/transpose.tpp"

template <typename T>
T trace(
        const Matrix<T> &A
        );

template <typename T>
T trace(
        const Matrix<T> &A,
        const Matrix<T> &B
        );
#include "./linalg/tpp/trace.tpp"

template<typename T>
Vector<T> operator*(const Matrix<T> &A, const Vector<T> &x);
// ***
// << jmm: ! the follow is ambiguous, so perhaps assuming a Vector<> is a column vector would be best .. meaning the following does not work >>
// template<typename T>
// Vector<T> operator*(const Vector<T> &x, const Matrix<T> &A);
template<typename T, typename Y>
Vector<typename std::common_type<T,Y>::type> operator*(const Vector<T> &x, const Matrix<Y> &A);
// ***
#include "./linalg/tpp/Ax.tpp"

// scalar projection of a Vector<> a onto a non-zero Vector<> b
double scalar_proj(
                   const Vector<double> &a,
                   const Vector<double> &b
                   );

// projection of a Vector<> a along a non-zero Vector<> b
Vector<double> proj(
                    const Vector<double> &a,
                    const Vector<double> &b
                    );

// TODO: somehow I need to make the following (e.g., LU_factorization and ludcmp()) consistent --- i.e., the same names, etc., but just have one called vs the other if LAPACK is used vs it not.

std::tuple<int, Matrix<double>, std::vector<int>> LU_factorization(const Matrix<double> &A);

Vector<double> lubksb(
                      const Matrix<double> &A_LU,
                      const int             n,
                      const Vector<int>    &indx,
                      const Vector<double> &b
                      );

std::tuple<
           Matrix<double>,
           Vector<int>,
           int
           > ludcmp(
                    const Matrix<double> &A,
                    const int             n,
                    const double          tiny = 1.e-40
                    );

std::tuple<int,
           int,
           Matrix<double>> inverse(const Matrix<double> &A);

Matrix<double> Sherman_Morrison(
                                const Matrix<double> &A_inv,
                                const Vector<double> &u,
                                const Vector<double> &v
                                );

std::pair<int,
          double> determinant(const Matrix<double> &A);

std::tuple<int,
           std::vector<std::complex<double>>,
           MultiVector<std::complex<double>>,
           MultiVector<std::complex<double>>> eigen(const Matrix<double> &A);

// I/O
template<typename T>
Matrix<T> input_matrix(
                       const std::string filename
                       );
#include "jScience/linalg/tpp/input_matrix.tpp"

template<typename T>
void output_matrix(
                   const Matrix<T>   &_matrix,
                   // -----
                   const std::string  filename
                   );
#include "jScience/linalg/tpp/output_matrix.tpp"

#endif
