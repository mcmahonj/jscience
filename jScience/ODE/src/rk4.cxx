/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/12/2014
// LAST UPDATE: 12/12/2014


// STL
#include <vector> // std::vector


//========================================================================
//========================================================================
//
// NAME: void rk4(const std::vector<double> &y, const std::vector<double> &dydx, const double x, const double h, void derivs(const std::vector<double> &, const double, std::vector<double> &), std::vector<double> &yout)
//
// DESC: Given values for the variables y[0,...,n-1] and their derivatives dydx[0,...,n-1] known at x, use the fourth-order Runge--Kutta method to advance the solution over an interval h and return the incremented variables as yout[0,...,n-1]. The user supplies the routine derivs(y, x, dydx) which returns the derivative dy/dx at x.
//
//========================================================================
//========================================================================
void rk4(const std::vector<double> &y, const std::vector<double> &dydx, const double x, const double h, void derivs(const std::vector<double> &, const double, std::vector<double> &), std::vector<double> &yout)
{
    std::vector<double> dym(y.size(), 0.0), dyt(y.size(), 0.0), yt(y.size(), 0.0);
    
	double hh = h*0.5;
	double h6 = h/6.0;
	double xh = x + hh;
    
    // first step
	for( decltype(y.size()) i = 0; i < y.size(); ++i )
    {
        yt[i] = y[i] + hh*dydx[i];        
    }
    
    // second step    
	derivs(yt, xh, dyt);
    
	for( decltype(y.size()) i = 0; i < y.size(); ++i )
    {
        yt[i] = y[i] + hh*dyt[i];
    }
    
    // third step
    derivs(yt, xh, dym);
    
	for( decltype(y.size()) i = 0; i < y.size(); ++i )
    {
		yt[i]   = y[i] + h*dym[i];
		dym[i] += dyt[i];
    }
    
    // fourth step
	derivs(yt, (x+h), dyt);
    // accumulate increments with proper weights
	for( decltype(y.size()) i = 0; i < y.size(); ++i )
    {
		yout[i] = y[i] + h6*(dydx[i] + dyt[i] + 2.0*dym[i]);
    }
}


