/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/12/2014
// LAST UPDATE: 5/25/2015

#ifndef ODEINT_H
#define ODEINT_H


// STL
#include <functional> // std::function<>
#include <vector>     // std::vector


template<typename T>
std::vector<T> rk4(const std::vector<T> &y, const std::vector<T> &dydx, const double x, const double h, std::function<std::vector<T>(const std::vector<T> &, const double)> derivs);
#include "tpp/rk4.tpp"


#endif
