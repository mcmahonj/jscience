/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 10/13/2013
// LAST UPDATE: 10/13/2013


// STL
#include <algorithm> // std::sort
#include <set>       // std::set


//========================================================================
//========================================================================
//
// NAME: template <typename T> void remove_duplicates(std::vector<T> &v)
//       template <typename T> void remove_duplicates_huge(std::vector<T> &v)
//
// DESC: Removes duplicate elements from a vector. The vector is sorted, upon return. _huge is used for vectors with a lot of duplicates, but should not be used otherwise, due to poor cache locality.
//
//========================================================================
//========================================================================
template <typename T> void remove_duplicates(std::vector<T> &v)
{
    std::sort(v.begin(), v.end());
    v.erase( std::unique(v.begin(), v.end()), v.end() );
}

template <typename T> void remove_duplicates_huge(std::vector<T> &v)
{
    std::set<T> s(v.begin(), v.end());
    v.assign(s.begin(), s.end());
}