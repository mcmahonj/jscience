/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 11/8/2013
// LAST UPDATE: 11/8/2013


// STL
#include <utility>      // std::swap
#include <vector>       // std::vector

// THIS
#include "jrandnum.hpp" // rand_num_uniform_Mersenne_twister()


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       std::vector<T> get_n_rand_unique_elements(int n, std::vector<T> v)
//
// DESC: Get n random, unique elements from a vector v.
//
// NOTES:
//     ! This is a modified version of the Fisher--Yates algorithm, the Durstenfeld--Fisher--Yates or Knuth--Fisher--Yates algorithm.
//     ! v is passed by value, since it is re-arranged upon return.
//
//========================================================================
//========================================================================
template <typename T>
std::vector<T> get_n_rand_unique_elements(int n, std::vector<T> v)
{
    std::vector<T> elements;
    
    for( int i = 0; i < n; ++i )
    {
        int j = rand_num_uniform_Mersenne_twister( 0, (v.size()-1-i) );
        
        elements.push_back(v[j]);
        
        std::swap( v[j], v[v.size()-1-i] );
    }
    
    return elements;
}
