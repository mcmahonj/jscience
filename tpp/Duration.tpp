/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/26/2013
// LAST UPDATE: 12/31/2013

#include "Duration.hpp"

// STL
#include <cmath>    // std::round
#include <iostream> // std::cout, std::endl


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Duration& Duration::operator*=(const T &mult)
//
// DESC: Multiplication operator.
//
// NOTES: 
//     ! Different than the division operator below, there is no ambiguity with the parameters nY, nM, and nW, since values are not "rounded".
//
//========================================================================
//========================================================================
template<typename T>
Duration& Duration::operator*=(const T &mult)
{
    double dmult = static_cast<double>(mult);
    
    nY = static_cast<unsigned int>( std::round( dmult*static_cast<double>(nY) ) );
    nM = static_cast<unsigned int>( std::round( dmult*static_cast<double>(nM) ) );
    nW = static_cast<unsigned int>( std::round( dmult*static_cast<double>(nW) ) );
    nD = static_cast<unsigned int>( std::round( dmult*static_cast<double>(nD) ) );
    nh = static_cast<unsigned int>( std::round( dmult*static_cast<double>(nh) ) );
    nm = static_cast<unsigned int>( std::round( dmult*static_cast<double>(nm) ) );
    ns = static_cast<unsigned int>( std::round( dmult*static_cast<double>(ns) ) );
    
    return (*this);
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       Duration& Duration::operator/=(const T &rhs)
//
// DESC: Division operator.
//
//========================================================================
//========================================================================
template<typename T>
Duration& Duration::operator/=(const T &rhs)
{
    if( (nY > 0) || (nM > 0) || (nW > 0) )
    {
        std::cout << "Error in Duration::operator/=():( (nY > 0) || (nM > 0) || (nW > 0) ), which causes an ambiguous divide!" << std::endl;
        exit(0);
    }
    
    double drhs = static_cast<double>(rhs);
    
    if( drhs <= 0.0 )
    {
        std::cout << "Error in Duration::operator/=(): rhs <= 0!" << std::endl;
        exit(0);
    }
    
    std::time_t timestamp = this->to_timestamp();
    
    timestamp = static_cast<std::time_t>( std::round( static_cast<double>(timestamp)/drhs ) );
    
    nD = static_cast<unsigned int>( timestamp/86400 );
    
    unsigned int remainder = static_cast<unsigned int>( timestamp % 86400 );
    
    nh = remainder/3600;
    
    remainder = remainder % 3600;
    
    nm = remainder/60;
    
    ns = remainder % 60;

    return (*this);
}




