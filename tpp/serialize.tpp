/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/19/2014
// LAST UPDATE: 1/5/2015


// STL
#include <fstream>                             // std::ifstream

// Boost
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/filesystem.hpp>                // boost::filesystem::exists()


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       bool serialize_read( const std::string filename, T &obj, bool binary )
//
// DESC: Reads an object from file, previously serialized using Boost.
//
// NOTES:
//     ! The object T MUST contain the serialize() function for Boost.
//     ! The format (text or binary) MUST be the same as that used for serialize_write().
//
// << JMM: perhaps a check whether the file is binary should be implemented. This will break the efficiency though. >>
//
//========================================================================
//========================================================================
template <typename T>
bool serialize_read( const std::string filename, T &obj, bool binary )
{
    if( boost::filesystem::exists(filename) )
    {
        if( binary )
        {
            {
                // create and open an archive for input
                std::ifstream ifs(filename, std::ios::binary);
                boost::archive::binary_iarchive ia(ifs);
                
                // read class state from archive
                ia >> obj;
                
                // archive and stream closed when destructors are called
            }
        }
        else
        {
            {
                // create and open an archive for input
                std::ifstream ifs(filename);
                boost::archive::text_iarchive ia(ifs);
                
                // read class state from archive
                ia >> obj;
                
                // archive and stream closed when destructors are called
            }
        }
        
        return true;
    }
    else
    {
        return false;
    }
}


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       void serialize_write( const std::string filename, T &obj, bool binary )
//
// DESC: Writes a serialized object to file, using Boost, in either text or binary format.
//
// NOTES:
//     ! The object T MUST contain the serialize() function for Boost.
//     ! For portability (cross-platform, etc.), binary should be left false.
//
//========================================================================
//========================================================================
template <typename T>
void serialize_write( const std::string filename, T &obj, bool binary )
{
    if( binary )
    {
        std::ofstream ofs(filename, std::ios::trunc | std::ios::binary);
        
        // save data to archive
        {
            boost::archive::binary_oarchive oa(ofs);
            
            // write class instance to archive
            oa << obj;
            
            // archive and stream closed when destructors are called
        }
    }
    else
    {
        std::ofstream ofs(filename, std::ios::trunc);
        
        // save data to archive
        {
            boost::archive::text_oarchive oa(ofs);
            
            // write class instance to archive
            oa << obj;
            
            // archive and stream closed when destructors are called
        }
    }
}




