/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/23/2013
// LAST UPDATE: 12/23/2013


// STL
#include <vector>       // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename Y>
//       void RowMajOrd_to_Matrix(std::vector<Y> &a, unsigned int nr, Matrix<Y> &A)
//
// DESC: Convert the vector a, containing a matrix in row-major order, to a Matrix object.
//
// NOTES:
//     ! Since the data in a is in row-major order, we simply need to copy it to A.data.
//
//========================================================================
//========================================================================
template <typename Y>
void RowMajOrd_to_Matrix(std::vector<Y> &a, unsigned int nr, Matrix<Y> &A)
{
    A.resize(nr, (a.size()/nr));
    
    A.data = a;
}
