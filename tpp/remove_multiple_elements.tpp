/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 10/25/2013
// LAST UPDATE: 12/16/2013


// STL
#include <algorithm> // std::sort
#include <vector>    // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       void remove_multiple_elements(std::vector<std::vector<T>::size_type> indxs, std::vector<T> &v)
//
// DESC: Removes multiple elements from a vector.
//
// NOTES:
//     ! indxs MUST not contain duplicate elements -- if so, call remove_duplicates() first.
//
//========================================================================
//========================================================================
template <typename T>
void remove_multiple_elements(std::vector<typename std::vector<T>::size_type> indxs, std::vector<T> &v)
{
    std::sort(indxs.begin(), indxs.end(), std::greater<typename std::vector<T>::size_type>());
    
    for( auto &idx : indxs )
    {
        v.erase( (v.begin() + idx) );
    }
}
