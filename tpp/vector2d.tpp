/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 8/21/2013
// LAST UPDATE: 8/21/2013


//========================================================================
//========================================================================
//
// NAME: template <typename T> auto vector2D_size(std::vector<std::vector<T>> vec2D) -> decltype(vec2D.size())
//
// DESC: Calculate the number of elements in a 2D vector
//
//========================================================================
//========================================================================
template <typename T> auto vector2D_size(std::vector<std::vector<T>> vec2D) -> decltype(vec2D.size())
{
    decltype(vec2D.size()) total = 0;
    for( const auto &v : vec2D ) { total += v.size(); }
    
    return total;
}
