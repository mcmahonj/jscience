/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 11/21/2013
// LAST UPDATE: 11/21/2013

#include "Enum.hpp"

// << JMM >> :: this class needs to be expanded to restrict access to the constructor and add all iterator traits
// note: the capitalizations of Enum and Iterator distinguish them from the STL enum and iterator
template <typename T>
class Enum
{
    
public:
    
    class Iterator
    {
        
    public:
        
        Iterator(int v) : value(v) {};
        
        T operator*(void) const
        {
            return static_cast<T>(value);
        }
        
        void operator++(void)
        {
            ++value;
        }
        
        bool operator!=(const Iterator &rhs)
        {
            return value != rhs.value;
        }
        
    private:
        int value;
        
    };
    
};

template <typename T>
typename Enum<T>::Iterator begin(Enum<T>)
{
    return typename Enum<T>::Iterator( static_cast<int>(T::FIRST) );
}

template <typename T>
typename Enum<T>::Iterator end(Enum<T>)
{
    return typename Enum<T>::Iterator( static_cast<int>(T::LAST) + 1 );
}



