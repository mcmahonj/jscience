/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/4/2014
// LAST UPDATE: 5/4/2014


// STL
#include <vector>       // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       bool any(const std::vector<T> &x)
//
// DESC: Determines whether any elements of x are non-zero.
//
// NOTES:
//     ! This is similar to the MATLAB function any().
//
//========================================================================
//========================================================================
template <typename T>
bool any(const std::vector<T> &x)
{
    for( auto i = 0; i < x.size(); ++i )
    {
        if( x[i] != T(0) )
        {
            return true;
        }
    }
    
    return false;
}


//========================================================================
//========================================================================
//
// NAME: template <typename T, typename F>
//       bool any(const std::vector<T> &x, F func)
//
// DESC: Determines whether any elements of x satisfy the function func.
//
// NOTES:
//     ! This is similar to the MATLAB function any().
//
//========================================================================
//========================================================================
template <typename T, typename F>
bool any(const std::vector<T> &x, F func)
{
    for( auto i = 0; i < x.size(); ++i )
    {
        if( func(x[i]) )
        {
            return true;
        }
    }
    
    return false;
}
