/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/8/2014
// LAST UPDATE: 5/8/2014


// STL
#include <vector>       // std::vector

// jScience
#include "jrandnum.hpp" // rand_num_uniform_Mersenne_twister() 
#include "jutility.hpp" // get_n_rand_unique_elements()


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       void bootstrap(const bool replace, const std::vector<T> &x, const unsigned int n, std::vector<std::vector<T>> &resampled_x)
//
// DESC: Performs bootstrapping, with or without replacement, of the vector x, generating n samples and storing and resampled_x.
//
// NOTES:
//     ! 
//
//========================================================================
//========================================================================
template <typename T>
void bootstrap(const bool replace, const std::vector<T> &x, const unsigned int n, std::vector<std::vector<T>> &resampled_x)
{
    resampled_x.clear();
    
    if( replace )
    {
        for( int i = 0; i < n; ++i )
        {
            std::vector<T> xtmp;
            for( int j = 0; j < x.size(); ++j )
            {
                int r = rand_num_uniform_Mersenne_twister( 0, (x.size()-1) );
                xtmp.push_back(x[r]);
            }
            
            resampled_x.push_back(xtmp);
        }
    }
    else
    {
        for( int i = 0; i < n; ++i )
        {
            resampled_x.push_back( get_n_rand_unique_elements(x.size(), x) );
        }
    }
}

