/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/8/2014
// LAST UPDATE: 5/8/2014


// STL
#include <iostream>     // std::cout, std::endl
#include <vector>       // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       std::vector<double> mean(const std::vector<std::vector<T>> &x)
//
// DESC: Given a vector of vectors, return an average (element-wise) vector.
//
// NOTES:
//     ! This is similar to the MATLAB function mean().
//
//========================================================================
//========================================================================
template <typename T>
std::vector<double> mean(const std::vector<std::vector<T>> &x)
{
    // ENSURE THAT x IS NOT EMPTY ...
    if( x.size() == 0 )
    {
        std::cout << "Warning (error?) in mean(): Empty vector passed! Returning." << std::endl;
        return std::vector<T>();
    }
    
    auto n = x[0].size();
    if( n == 0 )
    {
        std::cout << "Warning (error?) in mean(): At least x[0] is empty (empty vector of vectors?)! Returning." << std::endl;
        return std::vector<T>();
    }
    
    // CALCULATE THE MEAN VECTOR FROM x ...
    std::vector<double> ret(n, 0.0);
    
    for( auto i = 0; i < x.size(); ++i )
    {
        if( x[i].size() != n )
        {
            std::cout << "Error in mean(): All vectors are not the same size! Exiting." << std::endl;
            exit(0);
        }
        
        for( auto j = 0; j < n; ++j )
        {
            ret[j] += static_cast<double>(x[i][j]);
        }
    }
    
    for( auto i = 0; i < n; ++i )
    {
        ret[i] /= static_cast<double>(x.size());
    }
    
    return ret;
}
