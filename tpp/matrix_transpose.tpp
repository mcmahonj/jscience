/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 4/30/2014
// LAST UPDATE: 4/30/2014

#include "Matrix.hpp"

// STL
//#include <utility>      // std::swap
//#include <vector>       // std::vector


template <typename T>
void transpose( const Matrix<T> &A, Matrix<T> &AT, unsigned int mstart, unsigned int mend, unsigned int nstart, unsigned int nend );


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       void matrix_transpose( const Matrix<T> &A, Matrix<T> &AT )
//
// DESC: Transpose the matrix A using a cache-oblivious algorithm, returning AT.
//
//========================================================================
//========================================================================
template <typename T>
void matrix_transpose( const Matrix<T> &A, Matrix<T> &AT )
{
    AT = Matrix<T>( A.get_ncols(), A.get_nrows() );
    
    transpose( A, AT, 0, (A.get_nrows()-1), 0, (A.get_ncols()-1) );
    
    // *****
    // << JMM: Can remove for production >> QUICK SANITY CHECK ...
    for( decltype(A.get_nrows()) i = 0; i < A.get_nrows(); ++i )
    {
        for( decltype(A.get_ncols()) j = 0; j < A.get_ncols(); ++j )
        {
            if( AT(j, i) != A(i, j) )
            {
                std::cout << "Error in matrix_transpose(): AT.at(j, i) != A.at(i, j)!" << std::endl;
                std::cout << "A.at(i, j)  = " << A(i, j) << std::endl;
                std::cout << "AT.at(j, i) = " << AT(j, i) << std::endl;
                exit(0);
            }
        }
    }
    // *****
}


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       void transpose( const Matrix<T> &A, Matrix<T> &AT, unsigned int mstart, unsigned int mend, unsigned int nstart, unsigned int nend )
//
// DESC: Transpose the matrix block A_mn using cache-oblivious matrix transposition, returning A_nm (AT).
//
// INPUT:
//     mstart : The index of the first row of the matrix A.
//     mend   : The index of the last row of the matrix A.
//     nstart : The index of the first column of the matrix A.
//     nend   : The index of the last column of the matrix A.
//
// NOTES:
//     See:
//          Harald Prokop. Cache-Oblivious Algorithms. Masters thesis, MIT. 1999.
//          M. Frigo, C. E. Leiserson, H. Prokop, and S. Ramachandran. Cache-oblivious algorithms. In Proceedings of the 40th IEEE Symposium on Foundations of Computer Science, 1999.
//
//========================================================================
//========================================================================
template <typename T>
void transpose( const Matrix<T> &A, Matrix<T> &AT, unsigned int mstart, unsigned int mend, unsigned int nstart, unsigned int nend )
{
    static const unsigned int threshold = 8*1024;
    
    unsigned int msize = mend - mstart + 1;
    unsigned int nsize = nend - nstart + 1;
    
    if( (msize*nsize*2) < threshold )
    {
        // naive matrix transposition ...
        for( unsigned int m = mstart; m <= mend; ++m )
        {
            for( unsigned int n = nstart; n <= nend; ++n )
            {
                AT(n, m) = A(m, n);
            }
        }
    }
    else if( msize >= nsize )
    {
        unsigned int mmid = (mstart + mend)/2;
        transpose(A, AT, mstart, mmid, nstart, nend);
        transpose(A, AT, (mmid+1), mend, nstart, nend);
    }
    else
    {
        unsigned int nmid = (nstart + nend)/2;
        transpose(A, AT, mstart, mend, nstart, nmid);
        transpose(A, AT, mstart, mend, (nmid+1), nend);
    }
}