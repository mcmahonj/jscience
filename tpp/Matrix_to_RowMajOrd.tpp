/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/22/2013
// LAST UPDATE: 12/22/2013


// STL
#include <vector>       // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename Y>
//       void Matrix_to_RowMajOrd(const Matrix<Y> &A, std::vector<Y> &a)
//
// DESC: Convert the data in matrix A to row-major order, returning the result in vector a.
//
// NOTES:
//     ! The data in matrix A SHOULD already be stored in row-major order.
//
//========================================================================
//========================================================================
template <typename Y>
void Matrix_to_RowMajOrd(const Matrix<Y> &A, std::vector<Y> &a)
{
    a = A.data;
}
