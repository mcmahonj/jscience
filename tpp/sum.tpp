/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 10/30/2013
// LAST UPDATE: 11/13/2013


// STL
#include <numeric> // std::accumulate
#include <vector>  // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename T> T sum(const std::vector<T> &v)
//
// DESC: Sums a vector v. For integers, the summation is (obviously) exact; for others, the summation is subject to roundoff error, but probably suitable for >99% of all tasks.
//
//========================================================================
//========================================================================
template <typename T>
T sum(const std::vector<T> &v)
{
    return std::accumulate(v.begin(), v.end(), T(0));
}

