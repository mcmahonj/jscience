/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/23/2013
// LAST UPDATE: 12/23/2013


// STL
#include <vector>       // std::vector


//========================================================================
//========================================================================
//
// NAME: template <typename Y>
//       void ColMajOrd_to_Matrix(std::vector<Y> &a, unsigned int nc, Matrix<Y> &A)
//
// DESC: Convert the vector a, containing a matrix in column-major order, to a Matrix object.
//
//========================================================================
//========================================================================
template <typename Y>
void ColMajOrd_to_Matrix(std::vector<Y> &a, unsigned int nc, Matrix<Y> &A)
{
    A.resize((a.size()/nc), nc);
    
    for( unsigned int j = 0; j < A.nc; ++j )
    {
        for( unsigned int i = 0; i < A.nr; ++i )
        {
            A.data[i*A.nr+j] = a[j*A.nc+i];
        }
    }
}
