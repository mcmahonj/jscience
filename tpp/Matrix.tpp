/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/22/2013
// LAST UPDATE: 5/12/2014

#include "Matrix.hpp"

// STL
#include <iostream> // std::cout, std::endl


template<typename T>
Matrix<T>::Matrix()
{
    nr = 0;
    nc = 0;

    data.clear();
}

template<typename T>
Matrix<T>::Matrix(unsigned int r, unsigned int c) 
{
    resize(r, c);
}

template<typename T>
Matrix<T>::~Matrix() {};


//========================================================================
//========================================================================
//
// NAME: template<typename T> void Matrix<T>::resize(unsigned int r, unsigned int c)
//
// DESC: Resizes the matrix to [r x c].
//
//========================================================================
//========================================================================
template<typename T>
void Matrix<T>::resize(unsigned int r, unsigned int c)
{
    if( (r < 1) || (c < 1) )
    {
        std::cout << "Error in Matrix<T>::resize(): ( (r < 1) || (c < 1) )!" << std::endl;
        exit(0);
    }
    
    nr = r;
    nc = c;
    
    data.clear();
    data.resize(nr*nc);
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       unsigned int Matrix<T>::get_nrows() const
//       unsigned int Matrix<T>::get_ncols() const
//
// DESC: Returns the number of rows or columns.
//
//========================================================================
//========================================================================
template<typename T>
unsigned int Matrix<T>::get_nrows() const
{
    return nr;
}

template<typename T>
unsigned int Matrix<T>::get_ncols() const
{
    return nc;
}


//========================================================================
//========================================================================
//
// NAME: template<typename T> T& Matrix<T>::operator()(unsigned int i, unsigned int j)
//
// DESC: Returns a reference to the (i, j) position.
//
//========================================================================
//========================================================================
template<typename T>
T& Matrix<T>::operator()(unsigned int i, unsigned int j)
{
    return at(i, j);
}


//========================================================================
//========================================================================
//
// NAME: template<typename T> T const& Matrix<T>::operator()(unsigned int i, unsigned int j) const
//
// DESC: Returns the value of the matrix at the (i, j) position.
//
//========================================================================
//========================================================================
template<typename T>
T const& Matrix<T>::operator()(unsigned int i, unsigned int j) const
{  
    return const_cast<Matrix&>(*this).at(i, j);
}


//========================================================================
//========================================================================
//
// NAME: template<typename T> T& Matrix<T>::at(unsigned int i, unsigned int j)
//
// DESC: Returns a reference to the (i, j) position.
//
// NOTES:
//     ! Recall that in C++, data is best stored in row-major order.
//
//========================================================================
//========================================================================
template<typename T>
T& Matrix<T>::at(unsigned int i, unsigned int j)
{
    if( (i >= nr) || (j >= nc) || (i < 0) || (j < 0) )
    {
        std::cout << "Error in Matrix<T>::operator(): (i, j) range is not valid!" << std::endl;
        exit(0);
    }
    
    return data[i*nc + j];
}


//========================================================================
//========================================================================
//
// NAME: template<typename T, typename Y>
//       Matrix<T>& operator+=(const Matrix<Y> &rhs)
//
//       template<typename T>
//       Matrix<T> operator+(Matrix<T> A, const Matrix<T> &B)
//
// DESC: Adds a matrix (rhs) to another matrix (*this, lhs).
//
//========================================================================
//========================================================================
template<typename T>
Matrix<T>& Matrix<T>::operator+=(const Matrix<T> &rhs)
{
    if( ((*this).get_nrows() != rhs.get_nrows()) || ((*this).get_ncols() != rhs.get_ncols()) )
    {
        std::cout << "Error in Matrix<T>& operator+=(): lhs and rhs do not have the same number of rows or columns! Exiting." << std::endl;
        exit(0);
    }
    
    // << JMM >> : can this be made cache oblivious??
    for( unsigned int i = 0; i < (*this).get_nrows(); ++i )
    {
        for( unsigned int j = 0; j < (*this).get_ncols(); ++j )
        {
            (*this)(i, j) += rhs(i, j);
        }
    }
    
    return (*this);
}

template<typename T>
Matrix<T> operator+(Matrix<T> A, const Matrix<T> &B)
{
    A += B;
    return A;
}



//========================================================================
//========================================================================
//
// NAME: template<typename Y>
//       Matrix<Y> operator*(const Y x, Matrix<Y> rhs)
//
//       Matrix<Y> operator*(Matrix<Y> lhs, const Y x)
//
// DESC: Multiplies a matrix (rhs) by a scalar x.
//
//========================================================================
//========================================================================
template<typename Y>
Matrix<Y> operator*(const Y x, Matrix<Y> rhs)
{
    for( auto &d : rhs.data )
    {
        d *= x;
    }
    
    return rhs;
}

template<typename Y>
Matrix<Y> operator*(Matrix<Y> lhs, const Y x)
{
    return (x*lhs);
}


// << JMM >> :: this needs to be converted to a cache-oblivious algorithm
//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       std::vector<T> operator*(const Matrix<T> &lhs, const std::vector<T> &rhs)
//
// DESC: Multiplies a matrix A and vector x.
//
//========================================================================
//========================================================================
template<typename T>
std::vector<T> operator*(const Matrix<T> &A, const std::vector<T> &x)
{
    if( A.get_ncols() != x.size() )
    {
        std::cout << "Error in std::vector<T> operator*(): A.get_ncols() != x.size()! Exiting." << std::endl;
        exit(0);
    }
    
    std::vector<T> b;
    
    for( auto i = 0; i < A.get_nrows(); ++i )
    {
        T bi = T(0);
        
        for( auto j = 0; j < A.get_ncols(); ++j )
        {
            bi += (A(i, j)*x[j]);
        }
        
        b.push_back(bi);
    }
    
    return b;
}
