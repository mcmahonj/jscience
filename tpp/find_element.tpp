/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 9/8/2013
// LAST UPDATE: 10/1/2013


//========================================================================
//========================================================================
//
// NAME: template <class T> int find_element_by_name(std::string name, const std::vector<T> &cls)
//
// DESC: Finds the element number of a vector of classes by name (m_name). A -1 is returned if the element is not found. 
//
//========================================================================
//========================================================================
template <class T> int find_element_by_name(std::string name, const std::vector<T> &cls)
{
    for( typename std::vector<T>::const_iterator it = cls.begin(), end = cls.end(); it != end; ++it )
    {
        if( it->m_name == name ) 
        { 
            return std::distance(cls.begin(), it); 
        }
    }
    
    return (-1);
}


//========================================================================
//========================================================================
//
// NAME: template <class T> int find_element_by_ID(std::string ID, const std::vector<T> &cls)
//
// DESC: Finds the element number of a vector of classes by ID (m_ID). A -1 is returned if the element is not found. 
//
//========================================================================
//========================================================================
template <class T> int find_element_by_ID(std::string ID, const std::vector<T> &cls)
{
    for( typename std::vector<T>::const_iterator it = cls.begin(), end = cls.end(); it != end; ++it )
    {
        if( it->m_ID == ID ) 
        { 
            return std::distance(cls.begin(), it); 
        }
    }
    
    return (-1);
}