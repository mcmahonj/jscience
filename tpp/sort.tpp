/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 8/11/2013
// LAST UPDATE: 1/1/2015

//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       std::vector<typename std::vector<T>::size_type> sort_indices_asc( const std::vector<T> &vec )
//
//       template <typename T>
//       std::vector<typename std::vector<T>::size_type> sort_indices_desc( const std::vector<T> &vec )
//
// DESC: Sort vector vec, returning the sorted indices idx
//
// NOTES:
//     !
//
//========================================================================
//========================================================================
template <typename T>
std::vector<typename std::vector<T>::size_type> sort_indices_asc( const std::vector<T> &vec )
{
    // INITIALIZE ORIGINAL INDEX LOCATIONS
    std::vector<size_t> idx( vec.size() );
    for(size_t i = 0; i != idx.size(); ++i) { idx[i] = i; }
    
    // SORT INDEX, BASED ON VALUES IN v
    sort( idx.begin(), idx.end(),
         [&vec](size_t i1, size_t i2) {return vec[i1] < vec[i2];} );
    
    return idx;
}

template <typename T>
std::vector<typename std::vector<T>::size_type> sort_indices_desc( const std::vector<T> &vec )
{
    // INITIALIZE ORIGINAL INDEX LOCATIONS
    std::vector<size_t> idx( vec.size() );
    for(size_t i = 0; i != idx.size(); ++i) { idx[i] = i; }
    
    // SORT INDEX, BASED ON VALUES IN v
    sort( idx.begin(), idx.end(),
         [&vec](size_t i1, size_t i2) {return vec[i1] > vec[i2];} );
    
    return idx;
}
