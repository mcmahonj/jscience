/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 9/2/2013
// LAST UPDATE: 9/2/2013


//========================================================================
//========================================================================
//
// NAME: template <typename T> auto interpolate( T begin, T end, T frac) -> decltype(begin)
//
// DESC: Interpolates the value frac of the way between two 1D data points
//
//========================================================================
//========================================================================
template <typename T> T interpolate_1D( T begin, T end, double frac)
{
    return static_cast<T>( frac*(end - begin) + begin );
}