/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 4/30/2014
// LAST UPDATE: 4/30/2014

#include "Matrix.hpp"

// STL
#include <algorithm>     // std::fill
#include <iostream>      // std::cout, std::endl


template <typename T>
void multiply( const Matrix<T> &A, const Matrix<T> &B, Matrix<T> &C, unsigned int mstart, unsigned int mend, unsigned int nstart, unsigned int nend, unsigned int pstart, unsigned int pend );


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       void matrix_multiply( const Matrix<T> &A, const Matrix<T> &B, Matrix<T> &C )
//
// DESC: Multiply the matrices A and B using a cache-oblivious algorithm, returning the product C.
//
//========================================================================
//========================================================================
template <typename T>
void matrix_multiply( const Matrix<T> &A, const Matrix<T> &B, Matrix<T> &C )
{
    if( A.get_ncols() != B.get_nrows() )
    {
        std::cout << "Error in matrix_multiply(): A.get_ncols() != B.get_nrows(), and thus cannot calculate the matrix multiplication!" << std::endl;
        exit(0);
    }
    
    // note: C MUST be filled with zeros here ...
    C = Matrix<T>( A.get_nrows(), B.get_ncols() );
    
    // *****
    // << JMM >> :: this MUST be zerod, hence the quick check
    for( int i = 0; i < C.get_nrows(); ++i )
    {
        for( int j = 0; j < C.get_ncols(); ++j )
        {
            if( C(i, j) != 0.0 )
            {
                std::cout << "Error in matrix_multiply(): C(i, j) != 0.0!" << std::endl;
                exit(0);
            }
        }
    }
    // *****
    
    multiply( A, B, C, 0, (A.get_nrows()-1), 0, (A.get_ncols()-1), 0, (B.get_ncols()-1) );
    
    
    // *****
    // << JMM: Can remove for production >> QUICK SANITY CHECK ...
    Matrix<T> Ctmp;
    Ctmp = Matrix<T>( A.get_nrows(), B.get_ncols() );
    
    for( unsigned int m = 0; m < A.get_nrows(); ++m )
    {
        for( unsigned int n = 0; n < A.get_ncols(); ++n )
        {
            for( unsigned int p = 0; p < B.get_ncols(); ++p )
            {
                Ctmp(m, p) += ( A(m, n)*B(n, p) );
            }
        }
    }
    
    for( int i = 0; i < C.get_nrows(); ++i )
    {
        for( int j = 0; j < C.get_ncols(); ++j )
        {
            if( C(i, j) != Ctmp(i,j) )
            {
                std::cout << "Error in matrix_multiply(): C(i, j) != Ctmp(i,j)!" << std::endl;
                std::cout << "C(i, j)  : " << C(i, j) << std::endl;
                std::cout << "Ctmp(i,j): " << Ctmp(i,j) << std::endl;
                exit(0);
            }
        }
    }
    // *****
    
}


//========================================================================
//========================================================================
//
// NAME: template <typename T>
//       void multiply( const Matrix<T> &A, const Matrix<T> &B, Matrix<T> &C, unsigned int mstart, unsigned int mend, unsigned int nstart, unsigned int nend, unsigned int pstart, unsigned int pend )
//
// DESC: Multiply the matrices A and B using a cache-oblivious algorithm, returning the product C_mp <- A_mn*B_np
//
// INPUT:
//     mstart : The index of the first row of the matrix A.
//     mend   : The index of the last row of the matrix A.
//     nstart : The index of the first column of the matrix A.
//     nend   : The index of the last column of the matrix A.
//     pstart : The index of the first column of the matrix B.
//     pend   : The index of the last column of the matrix B.
//
// NOTES:
//     See:
//          Harald Prokop. Cache-Oblivious Algorithms. Masters thesis, MIT. 1999.
//          M. Frigo, C. E. Leiserson, H. Prokop, and S. Ramachandran. Cache-oblivious algorithms. In Proceedings of the 40th IEEE Symposium on Foundations of Computer Science, 1999.
//
//========================================================================
//========================================================================
template <typename T>
void multiply( const Matrix<T> &A, const Matrix<T> &B, Matrix<T> &C, unsigned int mstart, unsigned int mend, unsigned int nstart, unsigned int nend, unsigned int pstart, unsigned int pend )
{
    static const unsigned int threshold = 32*1024;
    
    unsigned int msize = mend - mstart + 1;
    unsigned int nsize = nend - nstart + 1;
    unsigned int psize = pend - pstart + 1;
    
    if( (msize*nsize + nsize*psize + msize*psize) < threshold )
    {
        // naive matrix multiplication ...
        for( unsigned int m = mstart; m <= mend; ++m )
        {
            for( unsigned int n = nstart; n <= nend; ++n )
            {
                for( unsigned int p = pstart; p <= pend; ++p )
                {
                    C(m, p) += ( A(m, n)*B(n, p) );
                }
            }
        }
    }
    else if( (msize >= nsize) && (msize >= psize) )
    {
        unsigned int mmid = (mstart + mend)/2;
        multiply(A, B, C, mstart, mmid, nstart, nend, pstart, pend);
        multiply(A, B, C, (mmid+1), mend, nstart, nend, pstart, pend);
    }
    else if( nsize >= psize )
    {
        unsigned int nmid = (nstart + nend)/2;
        multiply(A, B, C, mstart, mend, nstart, nmid, pstart, pend);
        multiply(A, B, C, mstart, mend, (nmid+1), nend, pstart, pend);
    }
    else
    {
        unsigned int pmid = (pstart + pend)/2;
        multiply(A, B, C, mstart, mend, nstart, nend, pstart, pmid);
        multiply(A, B, C, mstart, mend, nstart, nend, (pmid+1), pend);
    }
}