/*
  Copyright (C) 2006 - 2008  Jeffrey M. McMahon

  This file is part of JSCIENCE.

  JSCIENCE is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  JSCIENCE is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with JSCIENCE.  If not, see <http://www.gnu.org/licenses/>.
*/


// !!! JMM: must verify
//========================================================================
//========================================================================
// NAME: void convert_date(string date, int * year, int * day)
// DESC: Converts the date ``date'' to integers describing year and day
// NOTES:
//========================================================================
//========================================================================
void days_old(string date, int * ndays)
{
    //=========================================================
    // GET YEAR, MONTH, DAY IN ``date''
    //=========================================================
    
    int year = date[0]*1000 + date[1]*100 + date[2]*10 + date[3];
    int mo = date[5]*10 + date[6];
    int day = date[8]*10 + date[9];
    
    //=========================================================
    // GET CURRENT YEAR, MONTH, DAY
    //=========================================================
    
    // GET CURRENT TIME
    time_t t = time(0);
    
    // SET UP A TIME STRUCTURE
    struct tm * now = localtime( & t );
    
    int year_current = now->tm_year + 1900;
    int mo_current = now->tm_mon + 1;
    int day_current = now->tm_mday;
    
    //=========================================================
    // GET DAYS BETWEEN ``date'' AND RIGHT NOW
    //=========================================================
    
    *ndays = rdn(year_current, mo_current, day_current) - rdn(year, mo, day);
    
    return;
}


// !!! JMM: must verify
// CALCULATE RATA DIE
// Rata Die is day one is 0001-01-01
int rdn(int y, int m, int d)
{
    if( m < 3 )
    {
        m += 12;
        y--;
    }
    
    return (d + (153*m - 457)/5 + 365*y + y/4 - y/100 + y/400 - 306);
}