/*
 Copyright 2013--Present JSCIENCE
 
 This file is distributed under the terms of the JSCIENCE License.
 
 You should have received a copy of the JSCIENCE License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 8/8/2013
// LAST UPDATE: 8/8/2013

#ifndef JWAVELETS2_H
#define JWAVELETS2_H

//************************************************************************
//************************************************************************
// INCLUDES
//************************************************************************
//************************************************************************

// UTILITY
#include <algorithm>
#include <string>
#include <vector>
// MATH
#include <cmath>
#include <complex>
// I/O
#include <iostream>
#include <fstream>

// JSCIENCE
#include "jmath_basic.h"

// !!! JMM: tmp
using namespace std;

//************************************************************************
//************************************************************************
// SUBROUTINES
//************************************************************************
//************************************************************************

void* swt(vector<double> & sig, int J, string nm, vector<double> & swt_output, int & length);
void* iswt(vector<double> &swtop,int J, string nm, vector<double> &iswt_output);
void upsamp(vector<double> & sig, int M, vector<double> & sig_u);
void* per_ext(vector<double> & sig, int a);

double convfft(vector<double> &a, vector<double> &b, vector<double> &c);
void* fft(vector<complex<double> > &data, int sign,unsigned  int N);

void denoise_signal(vector<double> & signal, int J, std::string wavelet_name, int winsize);

void circshift(vector<double> &sig_cir, int L);
void* bitreverse(vector<complex<double> > &sig);
double op_sum(double i, double j);
int vecsum(vector<double> &a, vector<double> &b, vector<double> &c);

void* gnuswtplot(int J);

int get_filter_coeff(string name, vector<double> & lp1, vector<double> & hp1, vector<double> & lp2, vector<double> & hp2);

#endif	
