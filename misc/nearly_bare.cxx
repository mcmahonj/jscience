/*
  Copyright (C) 2006 - 2008  Jeffrey M. McMahon

  This file is part of JSCIENCE.

  JSCIENCE is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  JSCIENCE is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with JSCIENCE.  If not, see <http://www.gnu.org/licenses/>.
*/


//************************************************************************
//------------------------------------------------------------------------
//
// NAME: JCRYSTLATT
//
// VERSION: Check www.thecomputationalphysicist.com for updated versions
//
// FILE: 
//     jfem_config.h
//
// AUTHOR: 
//     Jeffrey M. McMahon
//     jeffrey-mcmahon@northwestern.edu
//
//------------------------------------------------------------------------
//************************************************************************
//
// *last updated on 9/26/10
//
//------------------------------------------------------------------------
//************************************************************************
//
// NOTES:
//     i.
//
//
// TODO:
//     i.
//
//************************************************************************
//------------------------------------------------------------------------
//------------------------------------------------------------------------
//************************************************************************

//************************************************************************
//			INCLUDE FILES
//************************************************************************

#include "jcrystlatt.h"

//************************************************************************
//			CLASSES / STRUCTS
//************************************************************************


//************************************************************************
//			FULL CODE GLOBAL VARIABLES
//************************************************************************

//========================================================================
//========================================================================
//
// NAME: void conv_lattice_params(double [6], VECTOR_DOUBLE [3])
// DESC: Convert lattice parameters a, b, c, alpha, beta, gamma to lattice vectors
//
// NOTES:
//      a == celldm[0]
//      b == celldm[1]
//      c == celldm[2]
//      alpha == celldm[3] (angle between b and c)
//      beta == celldm[4] (angle between a and c)
//      gamma == celldm[5] (angle between a and b)
//
//     ii. the equations were taken from INPUT_PW.html from the Quantum Espresso v4.2.1 documents
//
//========================================================================
//========================================================================
void conv_lattice_params(double celldm[6], VECTOR_DOUBLE prim_vec[3])
{

  double tol = 1.0e-6;

  // ! using a, b, c, ... like this is slower and requires more memory than using celldm[] directly, but this way it is easy to see what is going on
  double a = celldm[0];
  double b = celldm[1];
  double c = celldm[2];
  double angle_bc = celldm[3]; // alpha in rad
  double angle_ac = celldm[4]; // beta in rad
  double angle_ab = celldm[5]; // gamma in rad

  prim_vec[0] = VECTOR_DOUBLE(a, 0.0, 0.0);
  prim_vec[1] = VECTOR_DOUBLE(b*cos(angle_ab), b*sin(angle_ab), 0.0);
  prim_vec[2] = VECTOR_DOUBLE(c*cos(angle_ac), c*( cos(angle_bc) - cos(angle_ac)*cos(angle_ab) )/sin(angle_ab), c*sqrt( 1.0 + 2.0*cos(angle_bc)*cos(angle_ac)*cos(angle_ab) - cos(angle_bc)*cos(angle_bc) - cos(angle_ac)*cos(angle_ac) - cos(angle_ab)*cos(angle_ab) )/sin(angle_ab) );

  //prim_vec[0] = VECTOR_DOUBLE(celldm[0], 0.0, 0.0);
  //prim_vec[1] = VECTOR_DOUBLE(celldm[1]*cos(celldm[5]), celldm[1]*sin(celldm[5]), 0.0);
  //prim_vec[2] = VECTOR_DOUBLE(celldm[2]*cos(celldm[4]), celldm[2]*( cos(celldm[3]) - cos(celldm[4])*cos(celldm[5]) )/sin(celldm[5]), celldm[2]*sqrt( 1.0 + 2.0*cos(celldm[3])*cos(celldm[4])*cos(celldm[5]) - cos(celldm[3])*cos(celldm[3]) - cos(celldm[4])*cos(celldm[4]) - cos(celldm[5])*cos(celldm[5]) )/sin(celldm[5]) );

  // CLEANUP VECTORS
  // i. if a vector has a component that is less than tol then remove it
  for(int i = 0; i < 3; ++i)
  {
    if( fabs(prim_vec[i].x) < tol)
    {
      prim_vec[i].x = 0.0;
    }

    if( fabs(prim_vec[i].y) < tol)
    {
      prim_vec[i].y = 0.0;
    }

    if( fabs(prim_vec[i].z) < tol)
    {
      prim_vec[i].z = 0.0;
    }
  }

  return;

}

