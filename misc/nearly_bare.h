/*
  Copyright (C) 2006 - 2008  Jeffrey M. McMahon

  This file is part of JSCIENCE.

  JSCIENCE is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  JSCIENCE is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with JSCIENCE.  If not, see <http://www.gnu.org/licenses/>.
*/


//************************************************************************
//------------------------------------------------------------------------
//
// NAME: JCRYSTLATT
//
// VERSION: Check www.thecomputationalphysicist.com for updated versions
//
// FILE: jcrystlatt.h
//
// AUTHOR: 	Jeffrey M. McMahon
//		mcmahon.physics@gmail.com
//
//------------------------------------------------------------------------
//************************************************************************
//
// *last updated on 9/26/10
//
//------------------------------------------------------------------------
//************************************************************************
//
// NOTES:   
//     i. 
//
//
// TODO:
//     i. 
//
//************************************************************************
//------------------------------------------------------------------------
//------------------------------------------------------------------------
//************************************************************************

#ifndef JCRYSTLATT_H	
#define JCRYSTLATT_H


//************************************************************************
//			INCLUDE FILES
//************************************************************************


//************************************************************************
//			CLASSES / STRUCTS
//************************************************************************



#endif	
