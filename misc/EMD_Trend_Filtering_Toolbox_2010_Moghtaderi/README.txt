EMD Trend Filtering

Azadeh Moghtaderi, Pierre Borgnat and Patrick Flandrin

October, 2011

%-----------------------------------------------------------
Implementation of the works in:

"Trend Filtering via Empirical Mode Decompositions", Azadeh Moghtaderi, Patrick Flandrin, Pierre Borgnat, Computational Statistics & Data Analysis, In Press, Corrected Proof, Available online 2 June 2011. doi:10.1016/j.csda.2011.05.015.

"Trend Filtering: Empirical Mode Decompositions vs. $\ell_1$ and Hodrick-Prescott", Azadeh Moghtaderi, Pierre Borgnat, Patrick Flandrin, Advances in Adaptive Data Analysis, Vol. 3, No. 1-2, p. 41-61, 2011. doi:10.1142/S1793536911000751.

%-----------------------------------------------------------
Uses the following freely available toolboxes:

1) EMD (Empirical Mode Decomposition) toolbox and developments:
Patrick Flandrin and Gabriel Rilling: 
URL: http://perso.ens-lyon.fr/patrick.flandrin/emd.html

2) TFTB (Time-Frequency ToolBox) toolbox:
F. Auger, O. Lemoine, P. Gon�alv�s, P. Flandrin;
URL: http://tftb.nongnu.org

%-----------------------------------------------------------

Examples:
	example_EMD_trend_filtering.m
	TrendEstSim.m






