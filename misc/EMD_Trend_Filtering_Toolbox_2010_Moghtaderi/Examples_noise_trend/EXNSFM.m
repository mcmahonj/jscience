function X = EXNSFM(N)


f1 = 0.02;
f2 = 0.0333;
f3 = 0.21;
f4 = 0.08;
f5 = 0.4;


for t = 0 : 400
    X(t+1) = 4.* cos(2 .* pi .* f1 .* t) + randn(1);
end

for t = 400:800
    X(t+1) = 5 .* cos(2 .* pi .* f2 .* t)+ randn(1);
end

for t = 800:1200
    X(t+1) = 10.* cos(2 .* pi .* f3 .* t)+ randn(1);
end

for t = 1200:1600
    X(t+1) = 3.* cos(2 .* pi .* f4 .* t)+ randn(1);
end

for t = 1600:N-1
    X(t+1) = 6.* cos(2 .* pi .* f5 .* t)+ randn(1);
end

