function X = EX2AR2(N,add)

% Author : Azadeh Moghtadeti (July 2009)
% This function creates a data set from a stationary AR(2) process.
% ---------------------------------------------------------
% INPUT :
% N = Sample size 
% add = The number of data skipped
% ---------------------------------------------------------
% OUTPUT :
% Y : Data set 
% --------------------------------------------------------

%standard deviation of teh while noise
stnois = 100;

% creating the stationary AR2
a = [0.2; 0.5];

X(1) = stnois.* randn;
X(2) = (a(1) .* X(1)) + (stnois.* randn);
for j=3:N+add
   X(j) = (X(j-2:j-1) * flipud(a)) + (stnois .* randn);
end

%output
X = X(add+1:N+add);
X = X.';

