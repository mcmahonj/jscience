function X = NSAR2(N,add,zpad)

% Author: Azadeh Moghtaderi, (January 2006)
% This function is making a non-stationary AR2 process 
% --------------------------------------------------
% OUTPUT:
% X : Nonstationary time series
% -------------------------------------------------
% INPUT :
% N = Sample size 
% add = The number of data skipped
% zpad : number of zero padding
% -------------------------------------------------

% calculating the fft frequencies
M = 2 .^ (floor(log(N)./ log(2)) + zpad);
while M < (2 * N)   
      M = 2 .^ (floor(log(N)./ log(2)) + zpad + 1); 
      % M is the number of fft points.
end

% select the proper number of points to plot with.
if (mod(M,2) == 0) 
    J = M ./ 2;
else
    J = floor (M ./ 2);
end
% -----------------------------------------------

fsmall = 0.002;
ampp = 0.3;
phi1 = 0.52;
a11 = phi1;
for t= (-1 * add):N-1
phi2(t+add+1) = ampp .* cos(2 .* pi * fsmall * t) ;
a21(t+add+1) = a11 - (phi2(t+add+1) .* a11);
a22(t+add+1) = phi2(t+add+1); 
end

a2 = [a21;a22]; 
a2 = a2.'; %size is Nx2

% ----------------------------------------------------
% Creating the non-stationary AR2 time series
% ----------------------------------------------------
stnois = 100;
epsilon = stnois .* randn(1,N+add);

% creating the data using the coefficients
X(1) = epsilon(1);
X(2) = (a2(2,1) .* X(1)) + epsilon(2);
X(3) = (a2(3,1) .* X(2)) + (a2(3,2) .* X(1)) + epsilon(3);
for t = 4 : N+add
    X(t) = (a2(t,1) .* X(t-1)) + (a2(t,2) .* X(t-2)) + epsilon(t);
end
X = X(add+1:N+add);


