function Y = ExTrendI(N,K)

% Author : Patrick Flandrin (2009)
% This function construct a random piecewise linear trend 
% ---------------------------------------------------------------------
% INPUT: 
% N : Sample size.
% K : Number of break points in the trend construction.
% -------------------------------------------------------
% OUTPUT:
% Y : Trend
% -------------------------------------------------------
% This is how we made Y1 to Y5

Y = zeros(1,N);
k = sort(fix(rand(1,K) .* N));
s = rand(1,K+1) - 0.5;

Y1 = rand - 0.5 ;
Y(1:k(1)) = Y1 + (1:k(1)) .* s(1);

for n = 2:K
    Y(k(n-1):k(n)) = Y(k(n-1)) + (1:k(n)-k(n-1)+1) .* s(n);
end
Y(k(K):N) = Y(k(K)) + (1:N-k(K)+1) .* s(K+1);

Y = Y - mean(Y);
M = max(abs(Y));
Y = Y/2/M;






