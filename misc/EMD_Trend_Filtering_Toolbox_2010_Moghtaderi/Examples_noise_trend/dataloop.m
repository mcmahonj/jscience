function Y = dataloop(N,Q)

% Author: Azadeh Moghtaderi (April 2008)
% This function generates Q realizations of a given process 
% ------------------------------------------------------------------------
% INPUT :
% N: sample size
% Q: number of realizations
% ------------------------------------------------------------------------
% OUTPUT:
% Y : data set of the size N x Q
% ------------------------------------------------------------------------
% NOTE :
% Choose the correct function for different examples in order to create X
% ------------------------------------------------------------------------

%Y = zeros(N,Q);
for b = 1:Q
    disp(b)
    % X = EXNSFM(N);
    % H = 0.7;
    % [B,X,w] = fbmcircul(N,H);% include H here or in the function
                              %parameters
    % X = NSAR2(N,30,1);
    X = EX1AR2(N,30);
    % X = EX2AR2(N,30);
    if size(X,2)>1,
        X=X.';
    end
    Y(:,b) = X;
end