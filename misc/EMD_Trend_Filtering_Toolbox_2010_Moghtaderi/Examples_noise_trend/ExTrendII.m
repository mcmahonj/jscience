function [Y6,Y7,Y8,Y9,Y10,Y11,Y13] = ExTrendII(N)

% Author: Azadeh Moghtaderi (January 2010) 
% This function creates trends
% --------------------------------------------
% INPUT:
% N : Sample size
% --------------------------------------------
% OUTPUT:
% Y6 - Y11: 5 different trends 
% ----------------------------------------------


load trend-Y5.mat
x = [1,64,97,563,753,944,1307,1308,1763,1959,1960,2000];
Lx = length(x);

for l = 1:Lx
y(l) = Y5(x(l));
end
xi = (1:N);
Y6 = interp1(x,y,xi,'spline');



QQ = 200;
fs1 = 0.002;
fs2 =0.001;
for t =0:N-1
    first = (t-1000).^2;
    value = (-1 .* first) ./ (2 .* (400 .^ 2)); 
    Y7(t+1) = exp(value);
    Y8(t+1) = 2 - Y7(t+1);
    Y9(t+1) = sqrt(1+(t./QQ));
    Y10(t+1) = 1.5 + cos(2 .* pi .* fs1 .* t);
    Y11(t+1) = 2 + sin(2 .* pi .* fs2 .* t);
    Y13(t+1) = 100 + ((t+10) / 500).^2; 
end


% plot(1:N,Y7)
% figure
% plot(1:N,Y8)
% figure
% plot(1:N,Y9)
% figure
% plot(1:N,Y10)
% figure
% plot(1:N,Y11)


