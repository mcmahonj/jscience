%% Test of EMD trend-filtering
%
% A. Moghtaderi, P. Flandrin, P. Borgnat
% 2011/10

% Example 1
load trend-Y3.mat
% load X3-N2000-r10000.mat
STD = 0.05 ;
N = length(Y3) ;
H = 0.7 ; 
[B0,X0,w0] = fbmcircul(N,H) ;  

Y = Y3;
% X = X3(:,5);
X = X0./std(X0)*STD + Y;

% Two possibilities: use linear or spline interpolation for EMD
% ->  this leads to different trend estimations (with linear, one finds 
% more piecewise linear looking trends).
%
% [CompMat1,InC,MInC,EsMF1,Rt1,Eg1,L,imf,Lthbar,Rthbar] = EgRtComp(X,'spline');
[CompMat1,InC,MInC,EsMF1,Rt1,Eg1,L,imf,Lthbar,Rthbar] = EgRtComp(X,'linear');

% Find the best index and its error using the theoretical result
%CompMat1
N = length(X);
Er = zeros(1,L);
m1 = 0;
while (m1 < L)
EsMF = zeros(1,N);
for l = L-m1:L
EsMF = EsMF + imf(l,:);
end
Er(m1+1) = sqrt(sum(abs(Y - EsMF).^2));
m1 = m1+1;
end
tInd = find(Er==(min(min(Er))));
tInd1 = L-tInd +1;
tEr1 = Er(tInd);
tEsMF1 = sum(imf(tInd1:L,:));
% ---------------------------------------
% Error for estimated trend
MInd1 = MInC;
Er1 = sqrt(sum(abs(Y - EsMF1).^2));
%Er1 = sqrt(sum(abs(Y - sum(imf(10:L,:))).^2));

disp('Index estimated from EMD trend-filtering')
MInC

disp('Index estimated from minimum of l2 distance using true trend (assuming it known!)')
tInd1

% ---------------------------------------
figure(10); clf
plot(1:N,X,'color',[1,1,1] .* 0,'linewidth',1)
A = axis;
axis([A(1) A(2) -0.6 0.6]);
set(gca,'fontsize',12);
xlabel('t')
% ylabel('C_t^{ (2)} and its estimate')
ylabel('X_t')

% ---------------------------------------
figure(11); clf
plot(1:N,Y,'--','color',[1,1,1] .* 0.7,'linewidth',2)
hold on
plot(1:N,EsMF1,'color',[1,1,1] .* 0,'linewidth',1)
plot(1:N,tEsMF1,'color','r','linewidth',1)

A = axis;
axis([A(1) A(2) -0.6 0.6]);
set(gca,'fontsize',12);
xlabel('t')
% ylabel('C_t^{ (2)} and its estimate')
ylabel('C_t^{ (1)} and its estimate')

legend('true trend','our method','l2-min with true')

figure(12); clf
plot(1:N,Y-EsMF1,'k');
set(gca,'fontsize',12);
xlabel('t')
% ylabel('C_t^{ (2)} and its estimate')
ylabel('C_t^{ (1)} -its estimate')

