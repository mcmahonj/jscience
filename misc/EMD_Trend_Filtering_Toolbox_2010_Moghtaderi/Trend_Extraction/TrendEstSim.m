function TrendEstSim

% Author: Azadeh Moghtaderi (April 2010), Pierre Borgnat (October 2011)
% This function estimates the trend using EMD and makes plots
% ----------------------------------------------------------


% Example 1 : Additive trend
load trend-Y3.mat
Y = Y3;

% load X3-N2000-r10000.mat
% X = X3(:,5);

N = 2000 ;
Z = EX1AR2(N,20) ;
Z = Z.';

SNR = 20 ;
X = sigmerge(Y.',Z.',SNR).' ; 

[CompMat1,InC,MInC,EsMF1,Rt1,Eg1,L,imf,Lthbar,Rthbar] = EgRtComp(X,'linear');

% Find the best index and its error using the theoretical result
%CompMat1
N = length(X);
Er = zeros(1,L);
m1 = 0;
while (m1 < L)
EsMF = zeros(1,N);
for l = L-m1:L
EsMF = EsMF + imf(l,:);
end
Er(m1+1) = sqrt(sum(abs(Y - EsMF).^2));
m1 = m1+1;
end
tInd = find(Er==(min(min(Er))));
tInd1 = L-tInd +1;
tEr1 = Er(tInd);
tEsMF1 = sum(imf(tInd1:L,:));
% ---------------------------------------
% Error for estimated trend
MInd1 = MInC;
Er1 = sqrt(sum(abs(Y - EsMF1).^2));
%Er1 = sqrt(sum(abs(Y - sum(imf(10:L,:))).^2));


disp('Index estimated from EMD trend-filtering')
MInC

disp('Index estimated from minimum of l2 distance using true trend (assuming it known!)')
tInd1

% ---------------------------------------
figure(1); clf
plot(1:N,Y,'--','color',[1,1,1] .* 0.7,'linewidth',2)
hold on
plot(1:N,EsMF1,'color',[1,1,1] .* 0.4,'linewidth',1)
A = axis;
axis([A(1) A(2) -0.6 0.6]);
set(gca,'fontsize',12);
xlabel('t')
% ylabel('C_t^{ (2)} and its estimate')
ylabel('C_t^{ (1)} and its estimate')

% ---------------------------------------
figure(2); clf
plot(1:N,X,'color',[1,1,1] .* 0.7,'linewidth',1)
hold on
plot(1:N,EsMF1,'color',[1,1,1] .* 0,'linewidth',1)
A = axis;
axis([A(1) A(2) -0.6 0.6]);
set(gca,'fontsize',12);
xlabel('t')
% ylabel('C_t^{ (2)} and its estimate')
ylabel('estimated C_t^{ (1)} and original signal')

% ---------------------------------------

figure(3); clf
Rt = [Rt1(1),Rt1];
subplot(2,1,1)
plot(1:L,log2(Eg1),'k','color',[1,1,1] .* 0.5,'linewidth',1)
hold on
plot(1:L,log2(Eg1),'ko','color',[1,1,1] .* 0.5,'linewidth',2)
hold on
indEg = find(CompMat1(:,1)) ;
if ~isempty(indEg)
    for ii=indEg,
    plot(ii,log2(Eg1(ii))+1.3,'kv','linewidth',2)
    end
end
% A = axis;
% axis([A(1) A(2) -5.1 10]);
xlabel('i')
ylabel('log_2 G^{ i}')
set(gca,'fontsize',12);
subplot(2,1,2)
plot(2:L,log2(Rt1),'ko','color',[1,1,1].* 0.4,'linewidth',2)
hold on
plot(1:L,log2(Rt),'k','color',[1,1,1].* 0.4,'linewidth',1)
plot(1:L,log2(Lthbar(1:L)),'--','color',[1,1,1].* 0,'linewidth',1)
hold on
plot(1:L,log2(Rthbar(1:L)),'--','color',[1,1,1].* 0,'linewidth',1)
hold on
indRt = find(CompMat1(:,2)) ;
if ~isempty(indRt)
    for ii=indRt,
    plot(ii,log2(Rt(ii))+0.2,'kv','linewidth',1)
    end
end
%A = axis;
%axis([A(1) A(2) 0.2 2.2]);
xlabel('i')
ylabel('log_2 R^{ i}') 
set(gca,'fontsize',12);

%% Example 1 : Multiplicative trend

load trend-Y8.mat
N = 2000 ;

H8 = 0.35;
[Q,Zfbm] = fbmcircul(N,H8) ;
ampl=1;
Zfbm = Zfbm./(std(Zfbm))*ampl ;

X8 = Y8.' .* Zfbm.';

Zlog = log(abs(Zfbm));
Xlog = log(abs(X8)) ;

mzlog = mean(Zlog);

mz = mean(Zfbm);

Ylog = log(abs(Y8));

Y = Ylog + mzlog; % theoretical trend which has to be compared to the estimation


[CompMat3,MInC,LEsMF3,Rt3,Eg3,L,imf2,Lthbar,Rthbar] = LEgRtComp(X8,'spline');
%CompMat3

LEsMF3 = LEsMF3 - mz;
% % Find the best index and its error using the theoretical result
N = length(X8);
Er = zeros(1,L);
m3 = 0;
while (m3 < L)
LEsMF = zeros(1,N);
for l = L-m3:L
LEsMF = LEsMF + imf2(l,:);
end
Er(m3+1) = sqrt(sum(abs(Y - LEsMF).^2));
m3 = m3+1;

end
tInd = find(Er==(min(min(Er))));
tInd3 = L-tInd +1;
tEr3 = Er(tInd);
tEsMF3 = sum(imf2(tInd3:L,:));

% ---------------------------------------
% Error for estimated trend
MInd3 = MInC;
Er3 = sqrt(sum(abs(Y - LEsMF3).^2));

figure(4); clf
plot(1:N,Y,'--','color',[1,1,1] .* 0.6,'linewidth',2)
hold on
plot(1:N,LEsMF3,'color',[1,1,1] .* 0,'linewidth',1)
%A = axis;
%axis([A(1) A(2) -0.1 0.8]);
set(gca,'fontsize',12);
xlabel('t')
%ylabel('log C_t^{ (5)} and its estimate')
ylabel('log C_t^{ (2)} and its estimate')

% ---------------------------------------
figure(5); clf
plot(1:N,log(abs(X8)),'color',[1,1,1] .* 0.7,'linewidth',1)
hold on
plot(1:N,LEsMF3,'color',[1,1,1] .* 0,'linewidth',1)
A = axis;
%axis([A(1) A(2) -0.6 0.6]);
set(gca,'fontsize',12);
xlabel('t')
% ylabel('C_t^{ (2)} and its estimate')
ylabel('estimated log(C_t^{ (1)}) and log of original signal')

%
figure(6); clf
Rt3(L-1) = Rt3(L-1) ./ (1.2 .* 10^15);
Rt = [Rt3(1),Rt3];
subplot(2,1,1)
plot(1:L,log2(Eg3),'k','color',[1,1,1] .* 0.4,'linewidth',1)
hold on
plot(1:L,log2(Eg3),'ko','color',[1,1,1] .* 0.4,'linewidth',2)
hold on
indEg3 = find(CompMat3(:,1)) ;
if ~isempty(indEg3)
    for ii=indEg3,
    plot(ii,log2(Eg3(ii))+1.3,'kv','linewidth',2)
    end
end
A = axis;
axis([1 11 A(3) 21]);
set(gca,'fontsize',12);
xlabel('i')
ylabel('log_2 G^{ i} ')
subplot(2,1,2)
plot(2:L,log2(Rt3),'ko','color',[1,1,1].* 0.4,'linewidth',2)
hold on
plot(1:L,log2(Rt),'k','color',[1,1,1].* 0.4,'linewidth',1)
plot(1:L,log2(Lthbar(1:L)),'--','color',[1,1,1].* 0,'linewidth',1)
hold on
plot(1:L,log2(Rthbar(1:L)),'--','color',[1,1,1].* 0,'linewidth',1)
indRt3 = find(CompMat3(:,2)) ;
if ~isempty(indRt3)
    for ii=indRt3,
    plot(ii,log2(Rt(ii))+0.2,'kv','linewidth',1)
    end
end
endA = axis;
axis([1 11 -0.5 3.8]);
set(gca,'fontsize',12);
xlabel('i')
ylabel('log_2 R^{ i} ') 


%%
return
