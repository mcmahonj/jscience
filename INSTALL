//=========================================================
// REQUIREMENTS
//=========================================================

- Unix-like (including Mac OS X) or Windows environment

- C++14 compiler (GCC 5.3.1 and 6.4.1-1 fully tested)

- Boost C++ libraries (1.58 fully tested)

- (Optional) LAPACK (3.5 fully tested)

- (Optional) libcurl (7.43 fully tested) 

- CMake (3.8.0-3 fully tested)

//=========================================================
// INSTALLATION
//=========================================================

1) Clone the jScience Git repository (see the Welcome email).

2) Modify the CMakeLists.txt in jscience/, *if necessary*:
     - Disable LAPACK/BLAS (comment out lines with flag -DWLAPACKMAC or -DWLAPACKLINUX/-DWLAPACKLINUX2).

3) In a (separate) build directory; in a terminal:
     $ CXX=[compiler] cmake jscience-path

5) Add to either .bashrc, .bash_profile, or .profile (as appropriate):
     export CPATH=jscience-path:$CPATH
     export CPATH=jscience-path/include:$CPATH
     export LIBRARY_PATH=jscience-build-path:$LIBRARY_PATH

//=========================================================
// NOTES & REFERENCES
//=========================================================

- Boost C++ libraries:
     http://www.boost.org/

- LAPACK:
     http://www.netlib.org/lapack/

- BLAS:
     http://www.netlib.org/blas/

- libcurl:
     http://curl.haxx.se/libcurl/





