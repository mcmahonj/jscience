
// STL
#include <algorithm> // std::max_element
#include <iostream>  // std::cout, std::endl
#include <limits>    // std::numeric_limits<T>::epsilon()
#include <vector>    // std::vector


// jScience
#include "jmath_basic.h"   // depsilon
#include "Matrix.hpp"      // Matrix
#include "SVD.hpp"         // SVD()


//========================================================================
//========================================================================
//
// NAME: int matrix_pseudoinverse( const Matrix<double> &A, Matrix<double> &Apsinv )
//
// DESC: Calculate the Moore-Pensrose pseudoinverse of the matrix A, using SVD.
//
// OUTOUT:
//     INFO : INFO, as returned by SVD (operations following successful SVD cannot fail)
//
// NOTES:
//     ! See:
//          http://en.wikipedia.org/wiki/Moore%E2%80%93Penrose_pseudoinverse
//
//========================================================================
//========================================================================
int matrix_pseudoinverse( const Matrix<double> &A, Matrix<double> &Apsinv )
{    
    int INFO;
    
    
    // FIRST USE SVD TO FACTORIZE THE MATRIX A ...
    Matrix<double> U, VT;
    std::vector<double> S;

    if( (INFO = SVD( A, U, S, VT )) != 0 )
    {
        std::cout << "Warning in matrix_pseudoinverse(): SVD returned " << INFO << std::endl;
        
        return INFO;
    }
 
    
    // CALCULATE V, SIGMA^-1, AND U^T ...
    Matrix<double> V, UT;
    matrix_transpose(VT, V);
    matrix_transpose(U, UT);
    
    // calculate tolerance for inverting SIGMA, following MATLAB, GNU Octave, and NumPy
    // see: http://en.wikipedia.org/wiki/Moore%E2%80%93Penrose_pseudoinverse
    // note: In general, the matrix S will not be singular. However, there might be column degeneracies in A, and we must zero some small diagonal elements of S.
    double t = std::numeric_limits<double>::epsilon()*static_cast<double>(std::max(A.get_nrows(), A.get_ncols()))*(*std::max_element(S.begin(), S.end()));
    
    // note the declaration as (n, m), instead of (m, n) ...
    Matrix<double> Sinv( A.get_ncols(), A.get_nrows() );
    for( decltype(S.size()) i = 0; i < S.size(); ++i )
    {
        if( std::fabs(S[i]) > t )
        {
            Sinv(i, i) = 1.0/S[i];
        }
    }
    
    
    // CALCULATE Apsinv = V*SIGMA^-1*U^T, AND RETURN ...
    Matrix<double> V_Sinv;
    matrix_multiply(V, Sinv, V_Sinv);
    matrix_multiply(V_Sinv, UT, Apsinv);
    

    // RETURN SUCCESS ...
    return 0;
}
