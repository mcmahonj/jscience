/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/12/2014
// LAST UPDATE: 5/12/2014


// STL
#include <vector>

// jScience
#include "Matrix.hpp" // Matrix, matrix_transpose(), matrix_multiply()


//========================================================================
//========================================================================
//
// NAME: void Hodrick_Prescott(const std::vector<double> &y, const double lambda, std::vector<double> &tau)
//
// DESC: Apply the Hodrick-Prescott (HP) filter to a time series y, returning the trend component tau.
//
// NOTES:
//     ! The HP filter assumes that the time series y is an additive mix of a trend component tau and cyclical component c, y = tau + c. If there is a multiplicative mix (as there often is), the logarithm of time series y should be passed to this subroutine.
//     ! See:
//          http://wiki.deepalgorithms.com/view/Hodrick-Prescott_filter
//          http://en.wikipedia.org/wiki/Hodrick%E2%80%93Prescott_filter
//
//========================================================================
//========================================================================
void Hodrick_Prescott(const std::vector<double> &y, const double lambda, std::vector<double> &tau)
{
    tau.clear();
    
    // CALCULATE I, D, D^T, and D^T*D ...
    Matrix<double> I = identity_matrix<double>( static_cast<unsigned int>(y.size()) );
 
    Matrix<double> D((y.size()-2), y.size());
    for( auto i = 0; i < (y.size()-2); ++i )
    {
        D(i, i)     = 1.0;
        D(i, (i+1)) = -2.0;
        D(i, (i+2)) = 1.0;
    }
    
    Matrix<double> DT;
    matrix_transpose(D, DT);
    
    Matrix<double> DTD;
    matrix_multiply(DT, D, DTD);
    
    // CALCULATE (I + 2*lambda*D^T*D)^-1 ..
    Matrix<double> Ip2lDTD = I + 2.0*lambda*DTD;
    
    Matrix<double> Ip2lDTD_inv;
    if( matrix_pseudoinverse(Ip2lDTD, Ip2lDTD_inv) != 0 )
    {
        std::cout << "Error in Hodrick_Prescott(): matrix_pseudoinverse() failed! Returning original series as trend." << std::endl;
        
        tau = y;
        
        return;
    }
    
    // CALCULATE (I + 2*lambda*D^T*D)^-1*y ...
    tau = Ip2lDTD_inv*y;
}




