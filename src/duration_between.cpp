/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/24/2013
// LAST UPDATE: 12/31/2013


// STL
#include <ctime>        // std::time_t

// THIS
#include "DateTime.hpp" // DateTime
#include "Duration.hpp" // Duration


//========================================================================
//========================================================================
//
// NAME: Duration duration_between(const DateTime &dt1, const DateTime &dt2)
//
// DESC: Returns the duration (in days, hours, mins., or secs.) between two DateTime objects.
//
// NOTES:
//     ! The returned Duration is not ALL inclusive -- e.g., the duration between two identical timestamps is NOT 1s.
//     ! duration_between() is used, rather than a subtraction operator between two DateTime objects, as the meaning is clearer, in my opinion
//
//========================================================================
//========================================================================
Duration duration_between(const DateTime &dt1, const DateTime &dt2)
{
    unsigned int ndays  = 0;
    unsigned int nhours = 0;
    unsigned int nmins  = 0;
    unsigned int nsecs  = 0;
    
    std::time_t time_between;
    if( dt2 > dt1 )
    {
        time_between = dt2.to_timestamp() - dt1.to_timestamp();
    }
    else if( dt1 > dt2 )
    {
        time_between = dt1.to_timestamp() - dt2.to_timestamp();
    }
    else
    {
        return Duration_none;
    }

    ndays = time_between/86400;

    if( (time_between = time_between % 86400) != 0 )
    {
        nhours = time_between/3600;

        if( (time_between = time_between % 3600) != 0 )
        {
            nmins = time_between/60;
            nsecs = time_between % 60;
        }
    }
    
    return Duration(0, 0, 0, ndays, nhours, nmins, nsecs);
}






