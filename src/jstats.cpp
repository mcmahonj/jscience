/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <7/4/2013
// LAST UPDATE:  11/13/2013

#include "jstats.hpp"

// STL
#include <algorithm>    // std::random_shuffle
#include <cmath>        // std::fabs()
#include <iostream>     // std::cout, std::endl
#include <limits>       // std::numeric_limits::lowest(), std::numeric_limits::max()

// JSCIENCE
#include "jrandnum.hpp" // rng routines


//========================================================================
//========================================================================
//
// NAME: double sum_exact(std::vector<double> x)
//
// DESC: Compute a ``faithful rounding'' of the sum of floating-point numbers (i.e., either the exact result or either one of the immediate floating-point neighbors of the exact result)
//
// NOTES:
//     ! this is an adaptation of FastAccSum() in:
//          S. M. Rump ``ULTIMATELY FAST ACCURATE SUMMATION'' SIAM J. Sci. Comput. 31. 3466--3502 (2009)
//     ! IMPORTANT: parenthesis are VERY important, such that certain operations are carried out before others
//
//========================================================================
//========================================================================
double sum_exact(std::vector<double> x)
{
    // IEEE 754 DOUBLE PRECISION ...
    static const double EPS            = (std::pow( 2.0, -53 ));   // RELATIVE ROUNDING ERROR UNIT
    static const double ETA            = (std::pow( 2.0, -1074 )); // SMALLEST POSITIVE (UNNORMALIZED) FLOATING-POINT NUMBER
    // SETUP STATIC CONSTANTS FOR USE BELOW
    static const double ETA_DIV_EPS    = (std::pow( 2.0, -1021 ));
    static const double TWOEPS         = (std::pow( 2.0, -52 ));
    static const double FOUREPS        = (std::pow( 2.0, -51 ));
    static const double FIVEEPS        = (5.0*EPS);
    static const double ONE_MINUS_EPS  = (1.0 - EPS);
    static const double ONE_MINUS_5EPS = (1.0 - FIVEEPS);
    static const double INV_TWOEPS     = (1.0/TWOEPS);

    //*********************************************************
    
    const double n    = (static_cast<double>(x.size()));
    const double nEPS = (n*EPS);
    
    double T      = 0.0;
    double sum_xi = 0.0;
    for( auto xi : x )
    {
        T      += std::fabs(xi);
        sum_xi += xi;
    }
    T /= (1.0 - nEPS); // sum_i[|xi|] <= T
    
    // IF NO ROUNDING ERROR, RETURN sum_xi DIRECTLY ...
    if( T <= ETA_DIV_EPS ) { return sum_xi; }

    // SETUP REMAINING CONSTANTS (NON-STATIC) FOR USE BELOW
    const double twon                     = (2.0*n);
    const double twonEPS                  = (twon*EPS);
    const double twon_nplus2_EPS          = (twon*(n + 2.0)*EPS);
    const double one_minus_3nplus1_EPS    = (1.0 - (3.0*n + 1.0)*EPS);
    const double threehalf_plus_4EPS_nEPS = ((3.0/2.0 + FOUREPS)*nEPS);
    
    double t, tp, tau, tau2, Phi;
    tp = 0.0;
    
    do
    {
        double sigma, sigma0, sigmap;
        sigma = sigma0 = (2.0*T)/one_minus_3nplus1_EPS;
              
        // [sigma', x] = ExtractVectorNew(sigma_0, x)
        for( auto &xi : x )
        {
            sigmap   = sigma + xi;
            double q = sigmap - sigma;
            xi      -= q;
            sigma    = sigmap;
        }
      
        tau = sigmap - sigma0; // exact
        t   = tp;
        tp  = t + tau;
        if( tp == 0.0 ) { return sum(x); }
        
        double q    = INV_TWOEPS*sigma0;
        double u    = std::fabs( q/ONE_MINUS_EPS - q ); // u = ufp(sigma_0)
        Phi         = (twon_nplus2_EPS*u)/ONE_MINUS_5EPS;
        double arg1 = threehalf_plus_4EPS_nEPS*sigma0;
        double arg2 = twonEPS*u; // sum_i[|xi|] <= T
        T           = std::min( arg1, arg2 );
    } while( (std::fabs(tp) < Phi) && ((4.0*T) > ETA_DIV_EPS) );

    // [t',tau_2] = FastTwoSum(t, tau)
    tau2 = (t - tp) + tau;
    
    // sum_xi MUST BE RECALCULATED ...
    sum_xi = 0.0;
    for( auto xi : x )
    {
        sum_xi += xi;
    }
    
    // RETURN THE FAITHFULLY ROUNDED sum_i[x_i]
    return (tp + (tau2 + sum_xi));
}


// PRELIMINARY VERSION OF SUM (ALGORITHM 4.6) WITH ASSOCIATED ROUTINES
/*
double sum(std::vector<double> x)
{
    // IEEE 754 DOUBLE PRECISION ...
    static const double EPS            = (std::pow( 2.0, -53 ));   // RELATIVE ROUNDING ERROR UNIT
    static const double ETA            = (std::pow( 2.0, -1074 )); // SMALLEST POSITIVE (UNNORMALIZED) FLOATING-POINT NUMBER
  
    //*********************************************************
    
    const double n    = (static_cast<double>(x.size()));
    
    double T = 0.0;
    for( auto xi : x )
    {
        T += std::fabs(xi);
    }
    T /= (1.0 - n*EPS);
    
    double t, t1, tau, Phi;
    t = 0.0;
    
    do
    {
        t1 = t;
        
        double sigma0 = (2.0*T)/(1.0 - (3.0*n + 1.0)*EPS);
        double sigman;
        ExtractVectorNew(sigma0, x, sigman, x);
        
        tau           = (sigman - sigma0);
        t             = (t + tau);
        
        Phi           = ((2.0*n*(n + 2.0)*EPS)*ufp(sigma0, EPS))/(1.0 - 5.0*EPS);
        
        double arg1, arg2;
        arg1          = ((3.0/2.0 + 4.0*EPS)*(n*EPS))*sigma0;
        arg2          = ((2.0*n)*EPS)*ufp(sigma0, EPS);
        T             = std::min( arg1, arg2 );
    } while( (std::fabs(t) < Phi) && ((4.0*T) > (ETA/EPS)) );
    
    // tau1 = t
    double tau1, tau2;
    FastTwoSum(t1, tau, tau1, tau2);

    double xi_sum = 0.0;
    for( auto xi : x )
    {
        xi_sum += xi;
    }
    
    // RETURN THE FAITHFULLY ROUNDED sum_i[x_i]
    return (tau1 + (tau2 + xi_sum));
}

void FastTwoSum(double a, double b, double &x, double &y)
{
    x = a + b;
    y = (a - x) + b;
}

void ExtractVector(double sigma, const std::vector<double> &p, double &tau, std::vector<double> &pp)
{
    tau = 0.0;
    
    for( decltype(p.size()) i = 0; i < p.size(); ++i )
    {
        double qi = (sigma + p[i]) - sigma;
        pp[i]     = (p[i] - qi);
        tau       = (tau + qi);
    }
}

void ExtractVectorNew(double sigma0, const std::vector<double> &p, double &sigman, std::vector<double> &pp)
{
    double sigma1 = sigma0;
    for( decltype(p.size()) i = 0; i < p.size(); ++i )
    {
        // [sigma_i, p_i'] = TwoSum(sigma_{i-1}, p_i)
        sigman    = (sigma1 + p[i]);
        double qi = (sigman - sigma1);
        pp[i]     = (p[i] - qi);
        
        sigma1    = sigman;
    }
}

double ufp(double p, const double EPS)
{
    const double psi = (1.0/(2.0*EPS)) + 1.0;
    
    double q = psi*p;
    return ( std::fabs(q - (1.0 - EPS)*q) );
}
*/

// << USE STATS++ >>
//========================================================================
//========================================================================
//
// NAME: double mean(const std::vector<double> &x)
//
// DESC: Calculates the mean of x. This ignores the issue of repeated roundoff error, but it probably suitable for >99% of tasks.
//
//========================================================================
//========================================================================
double mean(const std::vector<double> &x)
{    
    return ( sum(x)/static_cast<double>(x.size()) );
}

//========================================================================
//========================================================================
//
// NAME: double mean_exact(const std::vector<double> &x)
//
// DESC: Exact calculation of the mean of x.
//
//========================================================================
//========================================================================
double mean_exact(const std::vector<double> &x)
{
    return ( sum_exact(x)/static_cast<double>(x.size()) );
}


//========================================================================
//========================================================================
//
// NAME: double median(const std::vector<double> & x)
//
// DESC: Calculate the median of a vector x
//
// NOTES:
//     ! std::nth_element [O(N)] should be used over std::sort [O(N*log(N))] -- *BUT*, keep in mind that elements in the range [first, last) are rearranged, so DO NOT pass a vector by reference
//
//========================================================================
//========================================================================
double median(std::vector<double> x)
{
    // CALCULATE THE ELEMENT AT x.size()/2 AFTER BEGINNING (THIS DOESN'T MATTER IF x.size() IS EVEN OR ODD)
    std::nth_element( x.begin(), x.begin()+x.size()/2, x.end() );
    
    // GET THE MIDDLE (OR AVERAGE OF MIDDLE) ELEMENTS
    if( (x.size() % 2) != 0 ) { return x[x.size()/2]; }
    else                      { return (x[x.size()/2-1] + x[x.size()/2])/2.0; }   
}


//========================================================================
//========================================================================
//
// NAME: double variance(const std::vector<double> &x)
//
// DESC: Standard computation of the *sample* variance of a vector x. This ignores the issue of repeated roundoff error, but it probably suitable for many tasks and MUCH, MUCH faster than an exact calculation [variance()].
//
// NOTES:
//     ! a conversion from the sample variance to the entire-population variance can be performed by multiplying by (N-1)/N
//
//========================================================================
//========================================================================
double variance(const std::vector<double> &x)
{
    double xmean = mean(x);
    std::vector<double> diff(x.size());
    std::transform(x.begin(), x.end(), diff.begin(),
                   std::bind2nd(std::minus<double>(), xmean));
    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    
    return ( sq_sum/static_cast<double>(x.size() - 1) );
}


//========================================================================
//========================================================================
//
// NAME: double variance_exact(const std::vector<double> &x)
//
// DESC: Exact *sample* variance of a vector x.
//
// NOTES:
//     ! a conversion from the sample variance to the entire-population variance can be performed by multiplying by (N-1)/N
//
//========================================================================
//========================================================================
double variance_exact(const std::vector<double> &x)
{
    double xmean       = mean_exact(x);
    double xmean2      = xmean*xmean;
    double minus2xmean = -2.0*xmean;
    
    std::vector<double> xdiff;
    // ANALYTICALLY EXPANDING (xi - xmean)^2 PREVENTS ROUNDOFF OF (xi - xmean)
    for( auto xi : x )
    {
        xdiff.push_back( xi*xi );
        xdiff.push_back( xmean2 );
        xdiff.push_back( xi*minus2xmean );
    }
    
    return ( sum_exact(xdiff)/static_cast<double>(x.size() - 1) );
}


//========================================================================
//========================================================================
//
// NAME: double stddev(const std::vector<double> &x)
//
// DESC: Exact *sample* standard deviation of a vector x.
//
// NOTES:
//     ! a conversion from the sample std. dev. to the entire-population std. dev. can be performed by multiplying by sqrt((N-1)/N)
//
//========================================================================
//========================================================================
double stddev(const std::vector<double> &x)
{
    return sqrt( variance(x) );
}

//========================================================================
//========================================================================
//
// NAME: double stddev_exact(const std::vector<double> &x)
//
// DESC: Standard computation of the *sample* standard deviation of a vector x. This ignores the issue of repeated roundoff error, but it probably suitable for many tasks and MUCH, MUCH faster than an exact calculation [stddev()].
//
// NOTES:
//     ! a conversion from the sample std. dev. to the entire-population std. dev. can be performed by multiplying by sqrt((N-1)/N)
//
//========================================================================
//========================================================================
double stddev_exact(const std::vector<double> &x)
{
    return sqrt( variance_exact(x) );
}


//========================================================================
//========================================================================
//
// NAME: void test_statistical_routines()
//
// DESC: Runs some tests to estimate the accuracy of the statistical routines
//
//========================================================================
//========================================================================
void test_statistical_routines()
{
    const int N = 10000000;
    
    //*********************************************************
    
    // CALCULATION OF ROUND-OFF ERROR
    double a = rand_num_uniform_Mersenne_twister(-1E6, 1E6);
    double b = rand_num_uniform_Mersenne_twister(-1.0, 1.0);
    double x = a + b;
    double y = (a - x) + b;
    double z = x + y;
    
    std::cout << "ROUND OFF TEST: " << std::endl;
    std::cout << "     a               = " << a << std::endl;
    std::cout << "     b               = " << b << std::endl;
    std::cout << "     x = a + b       = " << x << std::endl;
    std::cout << "     y = (a - x) + b = " << y << std::endl;
    std::cout << "     z = x + y       = " << z << std::endl;
    std::cout << std::endl;

    // RECIPROCAL OF TRIANGULAR NUMBERS: sum_i[1/1 + 1/3 + 1/6 + 1/10 ...] = 2
    std::vector<double> invtri;
    int runsum = 0;
    for(int i = 1; i <= N; ++i)
    {
        runsum += i;
        invtri.push_back(1.0/static_cast<double>(runsum));
    }
    std::random_shuffle( invtri.begin(), invtri.end() );
    
    std::cout << "SUM OF RECIPROCAL OF TRIANGULAR NUMBERS: " << std::endl;
    std::cout << "     sum_exact = " << sum_exact(invtri) << std::endl;
    std::cout << "     sum       = " << sum(invtri) << std::endl;
    std::cout << std::endl;
    
    // NORMAL DISTRIBUTION OF RANDOM NUMBERS
    double dist_mean   = rand_num_uniform_Mersenne_twister(-10.0, 10.0);
    double dist_stddev = rand_num_uniform_Mersenne_twister( 1.0,  2.0);
    
    std::vector<double> randnums;
    for(int i = 0; i < N; ++i)
    {
        randnums.push_back( rand_num_normal_Mersenne_twister(dist_mean, dist_stddev) );
    }
    
    std::cout << "GENERATION OF RANDUM NUMBERS: " << std::endl;
    std::cout << "     dist_mean    = " << dist_mean << std::endl;
    std::cout << "     dist_stddev  = " << dist_stddev << std::endl;
    std::cout << "     mean_exact   = " << mean_exact(randnums) << std::endl;
    std::cout << "     mean         = " << mean(randnums) << std::endl;
    std::cout << "     stddev_exact = " << stddev_exact(randnums) << std::endl;
    std::cout << "     stddev       = " << stddev(randnums) << std::endl;
    std::cout << std::endl;
}


/*
// !!! JMM: I am not certain the following is correct -- must check before using in production
//========================================================================
//========================================================================
// NAME: double autocorrel_func( int k, double data [], int n )
// DESC: Get the autocorrelation function of data at lag k
// NOTES: 
//     !
//========================================================================
//========================================================================
double autocorrel_func( int k, double data [], int n )
{
    double rho = 0.0;
    
    double mu1, mu2, sum1, sum2, sum3;
   
    // GET MEAN VALUES OF DATA FROM t=1...(n-k) AND t=1+k...n (NO OFFSET)
    // ! keep in mind the c++ 0 offsets in this call
    mu1 = mean( data, 0, ((n-k)-1) );
    mu2 = mean( data, k, (n-1) );
    
    // CALCULATE THE 3 SUMMATIONS NEEDED FOR rho
    sum1 = sum2 = sum3 = 0.0;
    for(int t = 0; t < (n-k); ++t)
    {
        sum1 += ( (data[t] - mu1)*(data[t+k] - mu2) );
        sum2 += ( (data[t] - mu1)*(data[t] - mu1) );
    }
    // ! keep in mind that the following is k+1...n with no c++ offset
    for(int t = k; t < n; ++t)
    {
        sum3 += (data[t] - mu2)*(data[t] - mu2);
    }
    
    // CALCULATE rho
    rho = sum1/sqrt( sum2*sum3 );
    
    // CLEANUP & RETURN    
    return rho;
}
*/

