/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/2/2014
// LAST UPDATE: 5/12/2014


// STL
#include <vector>     // std::vector

// jScience
#include "Matrix.hpp" // Matrix


//========================================================================
//========================================================================
//
// NAME: int solve_Axb( const Matrix<double> &A, const std::vector<double> &b, std::vector<double> &x )
//
// DESC: Solve the linear equation Ax=b, using SVD to calculate the Moore-Penrose pseudoinverse of A.
//
// OUTPUT:
//     int :
//          0    == Success
//          else == Failure (or non-convergence) of matrix_pseudoinverse() (which ultimately is INFO of SVD)
//
//========================================================================
//========================================================================
int solve_Axb( const Matrix<double> &A, const std::vector<double> &b, std::vector<double> &x )
{
    x.clear();
    
    // CALCULATE THE PSEUDINVERSE OF A^T ...
    Matrix<double> Ainv;
    int INFO;
    if( (INFO = matrix_pseudoinverse(A, Ainv)) != 0 )
    {
        return INFO;
    }
    
    // CALCULATE x = A^-1*b ...
    x = Ainv*b;
    
    // RETURN SUCCESS ...
    return 0;
}


/*
//========================================================================
//========================================================================
//
// NAME: int solve_Axb( const Matrix<double> &A, const std::vector<double> &b, std::vector<double> &x )
//
// DESC: Solve the linear equation xA=b, using SVD to calculate the Moore-Penrose pseudoinverse of A.
//
// OUTPUT:
//     int :
//          0    == Success
//          else == Failure (or non-convergence) of matrix_pseudoinverse() (which ultimately is INFO of SVD)
//
//========================================================================
//========================================================================
int solve_xAb( const Matrix<double> &A, const std::vector<double> &b, std::vector<double> &x )
{

    // WE WANT TO SOLVE xA=b ...
    // ... THIS CAN BE WRITTEN: [xA=b] == [(xA)^T=b^T] == [A^Tx^T=b^T]
}
*/