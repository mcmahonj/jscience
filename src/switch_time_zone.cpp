// STL
#include <cstdlib>      // std::getenv
#include <ctime>        // std::time_t, std::tzset
#include <vector>       // std::vector

// this
#include "DateTime.hpp" // DateTime::TIME_ZONE, TZD_to_TZ()


static std::vector<char*> oldTZ;


//========================================================================
//========================================================================
//
// NAME: void switch_time_zone(DateTime::TIME_ZONE TZD)
//
// DESC: Switches the time zone to that specified by TZD.
//
// NOTES:
//     ! For a list of valid time zones, see:
//          http://en.wikipedia.org/wiki/List_of_tz_database_time_zones
//     ! After this is called, the environmental variable (on the computer) WILL be changed -- to restore the state, use unswitch_time_zone() BEFORE ending the program.
//
//========================================================================
//========================================================================
void switch_time_zone(DateTime::TIME_ZONE TZD)
{
    oldTZ.push_back(std::getenv("TZ"));

#if defined(_WIN32)
    _putenv_s("TZ", TZD_to_TZ.at(TZD).c_str());
#else
    setenv("TZ", TZD_to_TZ.at(TZD).c_str(), 1);
#endif // defined

    tzset();
}


//========================================================================
//========================================================================
//
// NAME: void unswitch_time_zone()
//
// DESC: Un-switches the time zone(s) set via a call(s) to switch_time_zone(). This function operates in a fashion similar to "Undo", where two successive calls to switch_time_zone() followed by two successive calls to unswitch_time_zone() restores the original state.
//
//========================================================================
//========================================================================
void unswitch_time_zone()
{
    if( (oldTZ.size() > 0) && oldTZ.back() )
    {
#if defined(_WIN32)
        _putenv_s("TZ", oldTZ.back());
#else
        setenv("TZ", oldTZ.back(), 1);
#endif // defined

        oldTZ.pop_back();
    }
    else
    {
#if defined(_WIN32)
        _putenv_s("TZ", "");
#else
        unsetenv("TZ");
#endif // defined
    }

    tzset();
}
