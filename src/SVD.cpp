
//#if defined(WLAPACKLINUX) || defined(WLAPACKMAC)

// STL
#include <iostream> // std::cout, std::endl
#include <vector>   // std::vector

// LAPACK/BLAS
#if defined(WLAPACKLINUX)
#include <lapacke/lapacke.h>
#elif defined(WLAPACKMAC)
#include <Accelerate/Accelerate.h>
#endif

// jScience
#include "Matrix.hpp"


//========================================================================
//========================================================================
//
// NAME: int SVD( const Matrix<double> &A, Matrix<double> &U, std::vector<double> &S, Matrix<double> &VT )
//
// DESC: Factorize a m x n matrix A using singular value decomposition (SVD): A = U * SIGMA * V^T.
//
// OUTPUT:
//     U  : A m x m orthogonal matrix, with the left-singular vectors of M in each column.
//     S  : The min(m, n) diagonal elements of SIGMA, an m x n matrix, sorted so that S(i) >= S(i+1)
//     VT : A n x n orthogonal matrix, which is the transpose of V, which contains the right-singular vectors of M in each column.
//
// NOTES:
//     ! See:
//          http://www.netlib.no/netlib/lapack/double/dgesvd.f
//
//========================================================================
//========================================================================
int SVD( const Matrix<double> &A, Matrix<double> &U, std::vector<double> &S, Matrix<double> &VT )
{
#if defined(WLAPACKLINUX) || defined(WLAPACKMAC)
    
    int INFO = 0;
    
    
    // SET TO COMPUTE BOTH U & V^T
    char JOBU  = 'A';
    char JOBVT = 'A';
    
    
    // SET ROWS (M), COLUMNS (N), AND LEADING DIMENSION OF A, AND STORE THE MATRIX A IN COLUMN-MAJOR ORDER (FOR FORTRAN)
    int M = A.get_nrows();
    int N = A.get_ncols();
    int LDA = M;
    
    std::vector<double> a;
    Matrix_to_ColMajOrd(A, a);
    
    
    // SET UP LEADING DIMENSIONS FOR U & V^T AND ALLOCATE ARRAYS TO STORE S, U, & V^T
    int LDU  = M;
    int LDVT = N;
    
    S.clear();
    S.resize( std::min(M, N) ); // use S directly
    std::vector<double> u( LDU*M );
    std::vector<double> vt( LDVT*N );
    
    
    // SET DIMENSION OF WORK ARRAY & SETUP WORK ARRAY
    int LWORK = std::max( 1, std::max( (3*std::min(M, N) + std::max(M, N)), 5*std::min(M, N) ) );
    std::vector<double> WORK( std::max(1, LWORK) );

    
    // CALL DGEEV
    dgesvd_( &JOBU, &JOBVT, &M, &N, &*a.begin(), &LDA, &*S.begin(), &*u.begin(), &LDU, &*vt.begin(), &LDVT, &*WORK.begin(), &LWORK, &INFO );

    if( INFO != 0 )
    {
        std::cout << "Warning in SVD(): DGESVD returned INFO=" << INFO << std::endl;
        
        // << JMM >> :: should we return here? Or does INFO>0 still have matrices that should be stored (just not corresponding to converged SVD)?
    }
    
    
    // CONVERT THE COLUM-MAJOR ORDER MATRICES BACK TO MATRIX FORM ...
    ColMajOrd_to_Matrix(u, M, U);
    ColMajOrd_to_Matrix(vt, N, VT);
    

    return INFO;
    
#else
    
    // << JMM:  Call my own SVD here >>
    std::cout << "Temporary error in SVD(): LAPACK SVD() called, but jScience compiled without LAPACK support!" << std::endl;
    exit(0);
    
#endif
}

