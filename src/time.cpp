/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 11/4/2013
// LAST UPDATE: 12/1/2013

// STL
#include <ctime>   // std::time_t
#include <sstream> // std::stringstream
#include <string>  // std::string


//========================================================================
//========================================================================
//
// NAME: std::string timestamp_to_yyyymmdd(std::time_t t)
//
// DESC: Returns a timestamp in the form YYYY-MM-DD.
//
//========================================================================
//========================================================================
std::string timestamp_to_yyyymmdd(std::time_t t)
{
    // note: the extra two spaces (12 instead of 10) are just for buffer
    char yyyymmdd[12];
    
    std::strftime(yyyymmdd, 12, "%F", std::localtime(&t));
    
    return static_cast<std::string>(yyyymmdd);
}

//========================================================================
//========================================================================
//
// NAME: std::string yyyymmdd_to_timestamp(std::time_t t)
//
// DESC: Returns a timestamp from a string in the form YYYY-MM-DD.
//
//========================================================================
//========================================================================
std::time_t yyyymmdd_to_timestamp(std::string yyyymmdd)
{
    std::stringstream tmp;
    tmp << yyyymmdd;
    
    std::string yyyy, mm, dd;
    getline(tmp, yyyy, '-');
    getline(tmp, mm, '-');
    getline(tmp, dd);
    
    std::time_t rawtime;
    std::time(&rawtime);
    
    struct tm *timeinfo;
    timeinfo = std::localtime(&rawtime);
    timeinfo->tm_year = std::stoi(yyyy) - 1900;
    timeinfo->tm_mon  = std::stoi(mm) - 1;
    timeinfo->tm_mday = std::stoi(dd);
    
    return mktime(timeinfo);
}


