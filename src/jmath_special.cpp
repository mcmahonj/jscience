/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <7/14/2013
// LAST UPDATE:  7/8/2014

#include "jmath_special.h"

// STL
#include <iostream>

// BOOST
#include <boost/math/special_functions/erf.hpp>

// jSCIENCE
#include "jScience/physics/consts.hpp"


//========================================================================
//========================================================================
// NAME: double jtanh(double x)
// DESC: Hyperbolic tangent function
// NOTES:
//     ! tanh(x) = sinh(x)/cosh(x) = (exp(x) - exp(-x))/(exp(x) + exp(-x)) = (exp(2x) - 1)/(exp(2x)+1)
//     ! the designator `j' in jtanh is to prevent confusion with any built in tanh
//     ! see:
//          https://en.wikipedia.org/wiki/Hyperbolic_function
//========================================================================
//========================================================================
double jtanh(double x)
{
    double exp2x = exp(2.0*x);
    
    return ( (exp2x - 1.0)/(exp2x + 1.0) );
}

//========================================================================
//========================================================================
// NAME: double dtanh(double x)
// DESC: Derivative of the hyperbolic tangent function
// NOTES:
//     ! dtanh(x) = sech^2(x)
//     ! the designator `j' in jtanh is to prevent confusion with any built in tanh
//     ! see:
//          https://en.wikipedia.org/wiki/Hyperbolic_function
//     !!! JMM: I think the derivative can also be written 1 - tanh^2(x), but even so, I am not sure which is faster
//========================================================================
//========================================================================
double dtanh(double x)
{
    double sechx = jsech(x);
    
    return ( sechx*sechx );
}


//========================================================================
//========================================================================
// NAME: double jsech(double x)
// DESC: Hyperbolic secent function
// NOTES:
//     ! sech(x) = 2/(exp(x) + exp(-x)) 
//     ! the designator `j' in jsech is to prevent confusion with any built in sech
//     ! see:
//          https://en.wikipedia.org/wiki/Hyperbolic_function
//========================================================================
//========================================================================
double jsech(double x)
{
    double expx = exp(x);
    
    return ( 2.0/( expx + 1.0/expx ) );
}


//========================================================================
//========================================================================
// NAME: double jerfinv(double z, double tol)
// DESC: Calculates the inverse error function of z
// NOTES:
//     ! the designator `j' is given to indicate that it is part of JSCIENCE
//     ! for a description of the algorithm, see:
//          http://en.wikipedia.org/wiki/Error_function#Inverse_functions
//     ! a tolerance of 1.0E-6 was selected, as this was the value found by
//     testing to give no noticeable difference in output with the default
//     number of sig figs (6) from 0.1 -- 1
//     !!! JMM: I do not know if this is better (faster and/or more accurate) than Boost
//========================================================================
//========================================================================
double jerfinv(double z)
{
    const int NPTS = 1000;  
    const double TOL = 1.0E-6;
    
    //*********************************************************
    
    // ON FIRST CALL, CALCULATE c_k COEFFICIENTS
    static bool firstcall = true;
    static double c[NPTS];
    
    if( firstcall )
    {
        c[0] = 1.0;
        
        for(int k = 1; k < NPTS; ++k)
        {
            c[k] = 0.0;
            
            // the range on m goes from 0 to (k-1)
            for(int m = 0; m < k; ++m) {c[k] += ( c[m]*c[k-1-m]/static_cast<double>( (m+1)*(2*m+1) ) ); }  
        }    
        
        firstcall = false;
    }

    //========================================================= 
    // NOW CALCULATE THE INVERSE ERROR FUNCTION
    //========================================================= 
    // ! the do while loop below basically has a range to include only enough terms up to NPTS
    
    int k = 0;
    double erfinv, prev_erfinv;
    
    // CALCULATE THE k=0 TERM
    erfinv = prev_erfinv = ( (c[k]/static_cast<double>(2*k+1))*pow( (sqrt(PI)*z/2.0), (2*k+1) ) ); 
    
    do 
    {
        // INCREMENT k
        k++;
        
        // ADD IN NEXT TERM
        erfinv += ( (c[k]/static_cast<double>(2*k+1))*pow( (sqrt(PI)*z/2.0), (2*k+1) ) );

        // CHECK FOR CONVERGENCE ...
        if( fabs(erfinv - prev_erfinv) < TOL ) { break; }
        
        // ... IF NOT, ASSIGN PREVIOUS VALUE AND GO AGAIN
        prev_erfinv = erfinv;
        
    } while( k < (NPTS-1) );
    
    //for(int k = 0; k < NPTS; ++k) {erfinv += ( (c[k]/static_cast<double>(2*k+1))*pow( (sqrt(PI)*z/2.0), (2*k+1) ) ); }
    
    return erfinv;
}



//========================================================================
//========================================================================
//
// NAME: double probit(double p)
//
// DESC: Calculates the probit function (inverse cumulative distribution function) associated with the standard normal distribution. p MUST lie in (0, 1).
//
// NOTES:
//     ! see:
//          http://en.wikipedia.org/wiki/Normal_distribution
//     ! p MUST lie in (0, 1)
//
//========================================================================
//========================================================================
double probit(double p)
{
    //return ( sqrt(2.0)*jerfinv( 2.0*p - 1.0) );
    return ( sqrt(2.0)*boost::math::erf_inv( 2.0*p - 1.0) );
}


//========================================================================
//========================================================================
// NAME:	double erfc_approx(double x)
// DESC:	Approximation to the complementary error function
// NOTES:
//     ! adapted from the Allen--Tindsley routine, taken from f.22 at:
//     http://www.ccl.net/cca/software/SOURCES/FORTRAN/allen-tildesley-book/index.shtml
//     ! the base reference is:
//     ABRAMOWITZ AND STEGUN, HANDBOOK OF MATHEMATICAL FUNCTIONS,
//          NATIONAL BUREAU OF STANDARDS, FORMULA 7.1.26
//     !!! I do not know if this is faster than the c++ built in erfc()
//     !!! I also do not know the accuracy of this expression
//========================================================================
//========================================================================
double erfc_approx(double x)
{
    const double A1 = 0.254829592;
    const double A2 = -0.284496736;
    const double A3 = 1.421413741;
    const double A4 = -1.453152027;
    const double A5 = 1.061405429;
    const double P =  0.3275911;    
    
    double T, x2, TP;
    
    T  = 1.0/(1.0 + P*x);
    x2 = x*x;
    TP = T*( A1 + T*( A2 + T*( A3 + T*( A4 + T*A5 ) ) ) );
    
    return ( TP*exp( -x2 ) );
}


/*
//========================================================================
//========================================================================
//
//	NAME:	static void get_g_2d(double k0, double xpos, double xposp, COMPLEX_DOUBLE& g)
//	DESC:	Gets the 2D vacuum Green's function.
//
//	INPUT:
//	
//	OUTPUT:
//		g:: the minimum value of two real numbers
//		
//	NOTES:	i. !!! this is a 2d Green's function, but it seems to depend only
//		on x
//		ii. this is the Green's function in vacuum so we use k0
//
//========================================================================
//========================================================================
void get_g(double k0, double xpos, double xposp, std::complex<double>& g)
{

  // get the free space wavelength
  double lambda = 2.0*PI/k0;

  // get the distance between x and x'
  double x = fabs(xpos - xposp);

  // get the n=0 Hankel function of the first kind with k0*x as the arguments
  std::complex<double> h0;
  get_hn(0, 1, k0*x, h0);


  g = (PI/lambda)*h0;

  return;
}
*/


/*
double digamma(int m)
{

  double digamma;

  int k;


  digamma = -EULERC;


  for(k = 1; k <= m-1; ++k)
  {
    digamma += 1.0/static_cast<double>(k);
  }
 

  return digamma;
}
 
 

double get_Pl(int l, double x);
double dfactorial2(int x);

  double Rinv;


cout << "here." << endl;
  if(R < 3.0)
  {

    double sr = get_length(r);
    double srp = get_length(r0);
    
    double rless, rgreat;
    if(sr > srp)
    {
      rless = srp;
      rgreat = sr;
    }
    else
    {
      rless = sr;
      rgreat = srp;
    }

    double s = sqrt(r.x*r.x + r.y*r.y);
    double sp = sqrt(r0.x*r0.x + r0.y*r0.y);

    double theta = acos(r.z/sr);
    double thetap = acos(r0.z/srp);

    double phi, phip;

    if(r.x < 0.0)
    {
      phi = PI - asin(r.y/s);
    }
    else
    {
      phi = asin(r.y/s);
    }

    if(r0.x < 0.0)
    {
      phip = PI - asin(r0.y/sp);
    }
    else
    {
      phip = asin(r0.y/sp);
    }

    int l, lmax = 10;
    Rinv = 0.0;
cout << "here2." << endl;
    double cosgamma = cos(theta)*cos(thetap) + sin(theta)*sin(thetap)*cos(phi - phip);
    for(l = 0; l < lmax; ++l)
    {
      cout << "get_Pl(l, cosgamma): " << get_Pl(l, cosgamma) << endl;
      Rinv += ( pow(rless, l)/pow(rgreat, l+1) )*get_Pl(l, cosgamma);
    }

  cout << "Rinv: " << Rinv << endl;
   cout << "1/r: " << 1.0/R << endl; 
  int gh;
  cin >> gh;
  }
  else
  {
    Rinv = 1.0/R;
  }
    //Rinv = 1.0/R;



//========================================================================
//========================================================================
//
//	NAME:	complex<double> get_Gmu(VECTOR_DOUBLE r, VECTOR_DOUBLE r0)
//	DESC:	Read in initial parameters
//
//	NOTES:
//
//
//========================================================================
//========================================================================
double get_Pl(int l, double x)
{
  double Pl = 0.0;

  int k;
  double dl = static_cast<double>(l);
  int krange = static_cast<int>(floor(dl/2.0));
 
  for(k = 0; k <= krange; ++k)
  {
    Pl += pow(-1.0, k)*(dfactorial2(2*(l - k)))*pow(x, l-2*k)/(dfactorial2(k)*dfactorial2(l-k)*dfactorial2(l-2*k));
  }

  Pl /= pow(2.0, l);

  return Pl;
}

//========================================================================
//========================================================================
//
//	NAME:	complex<double> get_Gmu(VECTOR_DOUBLE r, VECTOR_DOUBLE r0)
//	DESC:	Read in initial parameters
//
//	NOTES:
//
//
//========================================================================
//========================================================================
double dfactorial2(int x)
{

  if(x <= 1)
  {
    return 1.0;
  }

  return dfactorial2(x-1)*static_cast<double>(x);
}
*/

