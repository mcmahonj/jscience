/*
 Copyright 2014-Present Algorithms in Motion LLC

 This file is part of jScience.

 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <8/22/2013
// LAST UPDATE:  10/30/2013

#include "jminimization.h"

// STL
#include <functional> // std::function
#include <vector>     // std::vector


//========================================================================
//========================================================================
//
// NAME: void mnbrak(double *ax, double *bx, double *cx, double *fa, double *fb, double *fc, double (*func)(double))
//
// DESC: Given a function func, and given distinct initial points ax and bx, this routine searches in the downhill direction (defined by the function as evaluated at the intial points) and returns new points ax, bx, cx which bracket a minimum of the function. Also returned are the function values at the three points fa, fb, and fc.
//
// INPUT:
//     func : function
//
// OUTPUT:
//
// REQUIRED FUNCTIONS:
//
// NOTES:
//
//========================================================================
//========================================================================
void mnbrak(double *ax, double *bx, double *cx, double *fa, double *fb, double *fc, double (*func)(double))
{

  double ulim, u, r, q, fu, dum;

  *fa = (*func)(*ax);
  *fb = (*func)(*bx);

  // SWITCH ROLES OF a AND b SO THAT WE CAN GO DOWNHILL IN THE DIRECTION FROM a TO b
  if(*fb > *fa)
  {
    SHFT(dum, *ax, *bx, dum);
    SHFT(dum, *fb, *fa, dum);
  }

  // FIRST GUESS FOR c
  *cx = (*bx) + GOLD*(*bx - *ax);
  *fc = (*func)(*cx);

  // KEEP RETURNING TO HERE UNTIL WE BRACKET
  while( *fb > *fc )
  {
    // COMPUTE u BY PARABOLIC EXTRAPOLATION FROM a, b, c
    r = (*bx - *ax)*(*fb - *fc);
    q = (*bx - *cx)*(*fb - *fa);
    u = (*bx) - ((*bx - *cx)*q - (*bx - *ax)*r)/(2.0*SIGN(MAX(fabs(q-r), TINY), q-r)); // TINY is used to prevent any possibly division by 0
    ulim = (*bx) + GLIMIT*(*cx - *bx);

    // WE WON'T GO FARTHER THAN THIS. NOW TO TEST VARIOUS POSSIBILITIES ...

    // PARABOLIC u IS BETWEEN b AND c; TRY IT
    if( (*bx - u)*(u - *cx) > 0.0 )
    {
      fu = (*func)(u);
      if(fu < *fc) // got a minimum between b and c
      {
        *ax = (*bx);
        *bx = u;
        *fa = (*fb);
        *fb = fu;
        return;
      }
      else if( fu > *fb ) // got a minimum between a and u
      {
        *cx = u;
        *fc = fu;
        return;
      }

      // PARABOLIC FIT WAS NO USE. USE DEFAULT MAGNIFICATION
      u = (*cx) + GOLD*(*cx - *bx);
      fu = (*func)(u);
    }
    // PARABOLIC FIT BETWEEN c AND ITS ALLOWED VALUE
    else if( (*cx - u)*(u - ulim) > 0.0 )
    {
      fu = (*func)(u);
      if( fu < *fc )
      {
        SHFT(*bx, *cx, u, *cx + GOLD*(*cx - *bx));
        SHFT(*fb, *fc, fu, (*func)(u));
      }

    }
    // LIMIT PARABOLIC u TO MAXIMUM ALLOWED VALUE
    else if( (u - ulim)*(ulim - *cx) >= 0.0 )
    {
      u = ulim;
      fu = (*func)(u);
    }
    // REJECT PARABOLIC u, USE DEFAULT MAGNIFICATION
    else
    {
      u = (*cx) + GOLD*(*cx - *bx);
      fu = (*func)(u);
    }

    // ELIMINATE OLDEST POINT AND CONTINUE
    SHFT(*ax, *bx, *cx, u);
    SHFT(*fa, *fb, *fc, fu);
  } // end while( *fb > *fc)


  return;
}


//========================================================================
//========================================================================
//
// NAME: double brent(double ax, double bx, double cx, double (*func)(double), double tol, double *xmin)
//
// DESC: Given a function func, and given a bracketing triplet of abscissas ax, bx, cx (such that bx is between ax and cx, and f(bx) is less than both f(ax) and f(cx)), this routine isolates the minimum to a fractional precision of about tol using Brent's method. The abscissa of the minimum is returned as xmin, and the minimum function value is returned as brent, the returned function value.
//
// INPUT:
//     ax, bx, cx : triplet of abscissas (such that bx is between ax and cx, and f(bx) is less than both f(ax) and f(bx))
//     func : function
//
// OUTPUT:
//
// REQUIRED FUNCTIONS:
//
// NOTES:
//
//========================================================================
//========================================================================
double brent(double ax, double bx, double cx, double (*func)(double), double tol, double *xmin)
{
  double a, b, d, etemp, fu, fv, fw, fx, p, q, r, tol1, tol2, u, v, w, x, xm;

  double e = 0.0; // this will be the distance moved on the step before last

  // a AND b MUST BE IN ASCENDING ORDER, THOUGH THE INPUT ABSCISSAS NEED NOT BE
  a = ((ax < cx) ? ax : cx);
  b = ((ax > cx) ? ax : cx);

  // INITIALIZATIONS ...
  x = w = v = bx;
  fw = fv = fx = (*func)(x);

  // MAIN PROGRAM LOOP
  for(int iter = 1; iter <= ITMAX_BRENT; ++iter)
  {
    xm = 0.5*(a + b);
    tol2 = 2.0*(tol1 = tol*fabs(x) + ZEPS);

    // TEST FOR DONE
    if( fabs(x - xm) <= (tol2 - 0.5*(b-a)) )
    {
      // ARRIVE HERE READY TO EXIT WITH THE BEST VALUES
      *xmin = x;
      return fx;
    }

    // CONSTRUCT A TRIAL PARABOLIC FIT
    if( fabs(e) > tol1 )
    {
      r = (x - w)*(fx - fv);
      q = (x - v)*(fx - fw);
      p = (x - v)*q - (x - w)*r;
      q = 2.0*(q - r);
      if(q > 0.0)
      {
        p = -p;
      }
      q = fabs(q);
      etemp = e;
      e = d;
      // DETERMINE THE ACCEPTABILITY OF THE PARABOLIC FIT. HERE WE TAKE THE GOLDEN SECTION STEP INTO THE LARGER OF THE TWO SEGMENTS
      if( fabs(p) >= fabs(0.5*q*etemp) || p <= q*(a - x) || p >= q*(b - x) )
      {
        d = CGOLD*(e = (x >= xm ? a-x : b-x));
      }
      else
      {
        // TAKE THE PARABOLIC STEP
        d = p/q;
        u = x + d;

        if( u-a < tol2 || b-u < tol2)
        {
          d = SIGN(tol1, xm-x);
        }
      } // end if( fabs(p) >= ...
    } // end if( fabs(e) > tol1 )
    else
    {
      d = CGOLD*(e = (x >= xm ? a-x : b-x));
    }

    u = (fabs(d) >= tol1 ? x+d : x+SIGN(tol1, d));

    fu = (*func)(u); // this is the one function evaluation per iteration

    // DECIDE WHAT TO DO WITH FUNCTION EVALUATION. HOUSEKEEPING FOLLOWS:
    if(fu <= fx)
    {
      if( u >= x)
      {
        a = x;
      }
      else
      {
        b = x;
      }

      SHFT(v, w, x, u);
      SHFT(fv, fw, fx, fu);
    }
    else
    {
      if(u < x)
      {
        a = u;
      }
      else
      {
        b = u;
      }

      if(fu <= fw || w == x)
      {
        v = w;
        w = u;
        fv = fw;
        fw = fu;
      }
      else if(fu <= fv || v == x || v == w)
      {
        v = u;
        fv = fu;
      }
    } // end if(fu <= fx). DONE WITH HOUSEKEEPING

  }  // ++iter

  cout << "Too many iterations in BRENT! See minimization.cpp." << endl;
  *xmin = x;

  return fx;
}


//========================================================================
//========================================================================
//
// NAME: void linmin(double p[], double xi[], int n, double *dret, double (*func)() )
//
// DESC: given an n dimensional point p[0,...,n-1] and an n dimensional direction xi[0,...,n-1], moves and resets p to where the function func(p) takes on a minimum along the direction xi from p, and replaces xi by the actual vector displacement that p was moved. Also returns as dret the value of func at the returned location p. This is actually all accomplished by calling the routines mnbrak and brent
//
// INPUT:
//     p[0,...,n-1] : n-dimensional point
//     xi[0,...,n-1] : n-dimensional direction
//     func : function of p which is to be minimized
//
// OUTPUT:
//     p : the location of the minimum
//     dret : the minimum value of the function func
//
// REQUIRED FUNCTIONS:
//     mnbrak
//     brent
//     double d1dim(double x)
//
// NOTES:
//
//========================================================================
//========================================================================

  // DEFINING DECLARATIONS
  // i. a number of these are used as external variables for d1min below
  int ncom = 0;
  //double *pcom = 0.0;
  //double *xicom = 0.0;
  double *pcom;
  double *xicom;
  double (*nrfunc)(double []);
void linmin(double p[], double xi[], int n, double *dret, double (*func)(double []) )
//void linmin(double p[], double xi[], int n, double & dret, double (*func)(double []) )
{
/*
  // DEFINING DECLARATIONS
  // i. a number of these are used as external variables for d1min below
  int ncom = 0;
  //double *pcom = 0.0;
  //double *xicom = 0.0;
  double *pcom;
  double *xicom;
  double (*nrfunc)(double []);
*/
  double xx, xmin, fx, fb, fa, bx, ax;

  // DEFINE THE GLOBAL VARIABLES
  ncom = n;
  pcom = new double [n];
  xicom = new double [n];
  nrfunc = func;
  for(int j = 0; j < n; ++j)
  {
    pcom[j] = p[j];
    xicom[j] = xi[j];
  }

  // INITIAL GUESS FOR BRACKETS
  ax = 0.0;
  xx = 1.0;
  bx = 2.0;

  mnbrak(&ax, &xx, &bx, &fa, &fx, &fb, d1dim);
  *dret = brent(ax, xx, bx, d1dim, TOL_LINMIN, &xmin);

  // CONSTRUCT THE VECTOR RESULTS TO RETURN
  for(int j = 0; j < n; ++j)
  {
    xi[j] *= xmin;
    p[j] += xi[j];
  }

  // CLEANUP AND RETURN
  delete [] xicom;
  delete [] pcom;

  return;
}

//========================================================================
//========================================================================
//
// NAME: double d1dim(double x)
//
// DESC: Must accompany linmin
//
// INPUT:
//     x :
//
// OUTPUT:
//     d :
//
// NOTES:
//
//========================================================================
//========================================================================
double d1dim(double x)
{
  // DEFINED IN linmin
  // ! these are part of the same file now, so
  //extern int ncom;
  //extern double *pcom, *xicom, (*nrfunc)(double []);

  // BEGIN ROUTINE
  double d, *xt;
  xt = new double [ncom];

  for(int j = 0; j < ncom; ++j)
  {
    xt[j] = pcom[j] + x*xicom[j];
  }

  d = (*nrfunc)(xt);

  delete [] xt;

  return d;
}

//========================================================================
//========================================================================
//
// NAME: void powell(double p[], double **xi, int n, double dtol, int *iter, double *dret, double (*func)(double []) )
//
// DESC: Minimization of a function func of n variables.
//
// INPUT:
//     p[0,...,n-1] : initial starting point
//     xi[0,...n-1][0,...,n-1] : an initial matrix, whose columns contain the initial set of directions (usually the n unit vectors)
//     dtol : the fractional tolerance in the function value such that failure to decrease by more than this amount on one iteration signals doneness
//     func : function of n variables
//
// OUTPUT:
//     p : set to the best point found
//     xi : the then-current direction set
//     dret : the returned function value at p
//     iter : number of iterations taken
//
// SUBROUTINES REQ:
//     void linmin(double p[], double xi[], int n, double *dret, double (*func)() )
//
// NOTES:
//     i.
//
//========================================================================
//========================================================================
void powell(double p[], double **xi, int n, double dtol, int *iter, double *dret, double (*func)(double []) )
{
  //int ibig;
  double fptt, fp, del;
  double *pt, *ptt, *xit;
  pt = new double [n];
  ptt = new double [n];
  xit = new double [n];

  *dret = (*func)(p);

  // SAVE THE INITIAL POINT
  for(int j = 0; j < n; ++j)
  {
    pt[j] = p[j];
  }

  // MAIN ITERATIONS
  for(*iter = 1;;++(*iter))
  {
    fp = (*dret);
    int ibig = 0;
    del = 0.0; // will be the biggest function decrease

    // IN EACH ITERATION, LOOP OVER ALL DIRECTIONS IN THE SET
    for(int i = 0; i < n; ++i)
    {
      // COPY THE DIRECTION
      for(int j = 0; j < n; ++j)
      {
        xit[j] = xi[j][i];
      }

      fptt = (*dret);

      // MINIMIZE ALONG IT AND ...
      linmin(p, xit, n, dret, func);

      // ... RECORD IT IF IT IS THE LARGEST DECREASE SO FAR
      if( fabs(fptt - (*dret)) > del )
      {
        del = fabs( fptt - (*dret) );
        ibig = i;
      }
    } // ++i

    // TERMINATION CRITERION
    if( 2.0*fabs(fp - (*dret)) <= dtol*(fabs(fp) + fabs(*dret)))
    {
      break;

      // NumRec cleans up and returns in here
      //delete [] xit;
      //delete [] ptt;
      //delete [] pt;
      //return;
    }

    // OUTPUT ERROR IF WE TOOK TOO MANY STEPS AND BREAK
    if(*iter == ITMAX_POWELL)
    {
      cout << "Too many iterations in routine POWELL! See jminimization.cpp." << endl;
      break; // NumRec does not break here
    }

    // CONSTRUCT THE EXTRAPOLATED POINT AND THE AVERAGE DIRECTION MOVED. SAVE THE OLD STARTING POINT
    for(int j = 0; j < n; ++j)
    {
      ptt[j] = 2.0*p[j] - pt[j];
      xit[j] = p[j] - pt[j];
      pt[j] = p[j];
    }

    fptt = (*func)(ptt); // Function value at extrapolated point

    if( fptt < fp )
    {
      double t = 2.0*(fp - 2.0*(*dret) + fptt)*square(fp - (*dret) - del) - del*square(fp - fptt);

      if(t < 0.0)
      {
        // MOVE TO THE MINIMUM OF THE NEW DIRECTION
        linmin(p, xit, n, dret, func);

        // SAVE THE NEW DIRECTION
        for(int j = 0; j < n; ++j)
        {
          xi[j][ibig] = xit[j];
        }
      }
    }

  } // ++(*iter)

  // CLEANUP AND RETURN
  delete [] xit;
  delete [] ptt;
  delete [] pt;

  return;
}


// !!! RECHECK THIS ROUTINE AGAINST NUMER RECIPES BEFORE USING IT !!!
//========================================================================
//========================================================================
//
// NAME: void dfpmin(double p[], int n, double dtol, int *iter, double *dret, double (*func)(), void (*dfunc)() )
//
// DESC: given a starting point p[0,...,n-1] the Broyden-Fletcher-Goldfarb-Shanno variant of Davidon-Fletcher-Powell minimization is performed on a function func, using its gradient as calculated by a routine dfunc. ! The function func and its gradient dfunc must be able to be calculated. !
//
// INPUT:
//     p[0,...,n-1] : starting point
//     dtol : convergence requirement on the function value
//     func : function of p which is to be minimized
//     dfunc : gradient of the function func
//
// OUTPUT:
//     p : the location of the minimum
//     iter : the number of iterations that were performed
//     dret : the minimum value of the function
//
// SUBROUTINES REQ:
//     void linmin(double p[], double xi[], int n, double *dret, double (*func)() )
//
// NOTES:
//     i. the routine linmin is called to perform line minimizations
//
//========================================================================
//========================================================================
void dfpmin(double p[], int n, double dtol, int *iter, double *dret, double (*func)(double []), void (*dfunc)(double [], double []) )
{
  int ITMAX = 200; // maximum allowed number of iterations; small number to rectify special case of converging to exactly zero function value
  double EPS = 1.0e-10;

  // ALLOCATE MEMORY
  double fp, fae, fad, fac;
  double *xi, *g, *dg, *hdg;
  xi = new double [n];
  g = new double [n];
  dg = new double [n];
  hdg = new double [n];
  double **hessin;
  hessin = new double * [n];
  for(int i = 0; i < n; ++i)
  {
    hessin[i] = new double [n];
  }

  bool convg = false;

  // CALCULATE STARTING FUNCTION VALUE AND GRADIENT
  fp = (*func)(p);
  (*dfunc)(p, g);

  // INITIALIZE THE INVERSE HESSIAN TO THE UNIT MATRIX
  for(int i = 0; i < n; ++n)
  {
    for(int j = 0; j < n; ++n)
    {
      hessin[i][j] = 0.0;
    }
    hessin[i][i] = 1.0;
    // INITIALIZE LINE DIRECTION
    xi[i] = -g[i];
  }

  // MAIN LOOP OVER THE ITERATIONS
  for(int its = 1; its <= ITMAX; ++its)
  {
    *iter = its;

    linmin(p, xi, n, dret, func);

    // THIS IS THE NORMAL RETURN
    if( 2.0*fabs(*dret - fp) <= dtol*(fabs(*dret)+fabs(fp)+EPS) )
    {
      convg = true;
      break; // NumRec deletes memory in here and returns
    }

    // SAVE THE OLD FUNCTION VALUE
    fp = (*dret);

    // SAVE THE OLD GRADIENT
    for(int i = 0; i < n; ++i)
    {
      dg[i] = g[i];
    }

    // GET NEW FUNCTION AND GRADIENT
    *dret = (*func)(p);
    (*dfunc)(p, g);

    // COMPUTE DIFFERENCE OF GRADIENTS
    for(int i = 0; i < n; ++i)
    {
      dg[i] = g[i] - dg[i];
    }

    // COMPUTE DIFFERENCE TIMES CURRENT GRADIENT
    for(int i = 0; i < n; ++i)
    {
      hdg[i] = 0.0;

      for(int j = 0; j < n; ++j)
      {
        hdg[i] += hessin[i][j]*dg[j];
      }
    }

    // COMPUTE DOT PRODUCTS FOR THE DENOMINATORS
    fac = fae = 0.0;

    for(int i = 0; i < n; ++i)
    {
      fac += dg[i]*xi[i];
      fae += dg[i]*hdg[i];
    }

    // MAKE THE DENOMINATORS MULTIPLICATIVE
    fac = 1.0/fac;
    fad = 1.0/fae;

    for(int i = 0; i < n; ++i)
    {
      dg[i] = fac*xi[i] - fad*hdg[i];
    }

    // ! BFGS UPDATE
    // ! i. this is the only part this makes this different from DFP
    for(int i = 0; i < n; ++i)
    {
      for(int j = 0; j < n; ++j)
      {
        hessin[i][j] += fac*xi[i]*xi[j] - fad*hdg[i]*hdg[j] + fae*dg[i]*dg[j];
      }
    }

    // CALCULATE THE DIRECTION TO GO
    for(int i = 0; i < n; ++i)
    {
      xi[i] = 0.0;
      for(int j = 0; j < n; ++j)
      {
        xi[i] -= hessin[i][j]*g[j];
      }
    }

  } // ++its

  // OUTPUT AN ERROR IF WE DID NOT CONVERGE
  if(!convg)
  {
    cout << "too many iterations in dfpmin()!" << endl;
  }

  // CLEANUP
  for(int i = 0; i < n; ++i)
  {
    delete [] hessin[i];
  }
  delete [] hessin;

  delete [] xi;
  delete [] g;
  delete [] dg;
  delete [] hdg;


  return;
}



// TODO: this subroutine really needs updating:
// TODO: ... to not use the old arrays[]
// TODO: ... to direct output to a better directory
//========================================================================
//========================================================================
//
// NAME: void sim_anneal( std::vector<double> &x, int n, int xtype[], double a[], double b[], std::function<double(const std::vector<double> &)> func, double T, double v[], double eps, int N_T, int N_S, bool silent)
//
// DESC: Minimizes a function func(x) of n constrained variables using simulated annealing.
//
// INPUT:
//     x[0,...,n-1]     : starting vector in R^n
//     n                : number of elements of x
//     xtype[0,...,n-1] : data type of x (0 == int or else == double)
//     a[0,...,n-1]     : min range of x
//     b[0,...,n-1]     : max range of x
//     func(x)          : function to be minimized
//     T                : temp for simulated annealing (< 0 for default)
//     v[0,...,n-1]     : initial step vector
//     eps              : convergence criterion
//     N_T              : number of steps at a given T (< 0 for default)
//     N_S              : number of steps for a given step-size (< 0 for default)
//     silent           : silent mode (do not provide output to a file)
//
// OUTPUT:
//     x[0,...,n-1]     : the location of the minimum
//
// NOTES:
//     ! This routine is based upon the article A. Corana et al. ACM
//          Trans. of Math. Software 13, 262 (1987)
//     ! A temperature reduction occurs every N_S*N_T cycles of moves along every direction and after N_T step adjustments
//
//========================================================================
//========================================================================
void sim_anneal( std::vector<double> &x, int n, int xtype[], double a[], double b[], std::function<double(const std::vector<double> &)> func, double T, double v[], double eps, int N_T, int N_S, bool silent)
{
    // ASSIGN REASONABLE VALUES OF SIMULATED ANNEALING PARAMETERS
    // ! these were given in the Corana article
    int MAXITER = 9999;
    if( N_S < 0 ) { N_S = 20; }
    if( N_T < 0 ) { N_T = max(100, 5*n); }
    double *c = new double [n]; // c[u] controls the step variation along the uth direction
    for(int i = 0; i < n; ++i) { c[i] = 2.0; }
    int N_eps = 4;
    // ! the Trans. Math Software paper recommends 0.85 for r_T, but I read in another paper that 0.95 was the original scale proposed and the final value of f is dependent on the cooling rate -- that being said, 0.95 seems to cool much too slowly
    double r_T = 0.85;

    double *f, f_i, f_opt;
    f = new double [MAXITER];
    f_i = f_opt = func(x);
    for(int i = 0; i < MAXITER; ++i) { f[i] = f_i; }
    double *x_opt = new double [n];
    for(int i = 0; i < n; ++i) { x_opt[i] = x[i]; }
    int *nu = new int [n];
    for(int i = 0; i < n; ++i) { nu[i] = 0; }

    // DETERMINE THE OPTIMUM TEMPERATURE IF NONE PROVIDED
    if( T < 0 )
    {
        T = sim_anneal_get_Topt(n, xtype, a, b, func, 999);
    }


    //=========================================================
    // OPEN A FILE TO OUTPUT SIMULATED ANNEALING INFO
    //=========================================================

    ofstream file_out;
    if( !silent )
    {
        file_out.open("./sim_annealing.dat",ios::app);

        file_out << "** SIMULATED ANNEALING **" << endl;
        file_out << endl;
        file_out << "     Number of param: " << n << endl;
        file_out << "     Starting T:      " << T << endl;
        file_out << "     Convg criterion: " << eps << endl;
        file_out << endl;
        file_out << "PARAMETERS:" << endl;
        file_out << "     MAXITER        : " << MAXITER << endl;
        file_out << "     N_T            : " << N_T << endl;
        file_out << "     N_S            : " << N_S << endl;
        file_out << "     r_T            : " << r_T << endl;
        file_out << endl;
        file_out << "STARING EVAL:" << endl;
        file_out << "     f_i            : " << f_i << endl;
        file_out << "     x              :    " << endl;
        for(int i = 0; i < n; ++i) { file_out << "              x[" << i << "]:=" << x[i] << endl;  }
        file_out << endl;
        file_out << "BEGINNING ..." << endl;
        file_out << endl;
    }


    //=========================================================
    // START ANNEALING ...
    //=========================================================
    // ! This loop is my addition, to make sure we don't get stuck in an
    // infinite loop -- although, by construction, I am not sure if the
    // algorithm can suffer from this
    int k;
    for(k = 0; k < MAXITER; ++k)
    {
        if( !silent )
        {
            file_out << "ITERATION " << k << endl;
            file_out << "     T=" << T << endl;
        }

        // TEMPERATURE CYCLES ...
        for(int m = 0; m < N_T; ++m)
        {
            // STEP-VECTOR CYCLES
            for(int j = 0; j < N_S; ++j)
            {
                // MOVE CYCLES ...
                for(int h = 0; h < n; ++h)
                {
                    //=========================================================
                    // STEP 1: GENERATE A RANDOM STEP
                    //=========================================================

                    // GENERATE A RANDOM NUMBER IN THE RANGE [-1, 1]
                    double r = rand_num_uniform_Mersenne_twister(-1.0, 1.0);

                    // GET RANDOM STEP
                    // ! the reason for this step is because x[h] may be an int
                    // ! you could probably also do this is a different way, by
                    // generating a random integer of -1 or 1, but I think this
                    // way gives a greater range of steps
                    double step = r*v[h];
                    if( xtype[h] == 0 ) { step = floor(step + 0.5); }

                    // TAKE STEP ALONG DIRECTION h
                    double xnew = x[h] + step;


                    //=========================================================
                    // STEP 2: CHECK THAT xnew IS IN THE CORRECT RANGE ...
                    //=========================================================

                    // ... IF NOT, RETRY ANOTHER STEP ALONG h
                    if( (xnew < a[h]) || (xnew > b[h]) )
                    {
                        // NOTE: Update 2/20/18: If a move is out of bounds, reject it and continue.
                        //
                        // NOTE: ... This is consistent with Markov-chain Monte Carlo.
                        //h--;
                        continue;
                    }


                    //=========================================================
                    // STEP 3: COMPUTE f' = f(x') AND POSSIBLY ACCEPT
                    //=========================================================

                    // ASSIGN xnew TO x (BUT STORE VALUE OF x[h] FIRST)
                    double xtmp = x[h];
                    x[h] = xnew;

                    // COMPUTE f' = f(x')
                    double f_new = func(x);

//                    cout << "f_new: " << f_new << endl;

                    // IF f' <= f_i, ACCEPT IMMENDIATELY ...
                    if( f_new <= f_i )
                    {
                        // STORE x' AND f'
                        // ! we have already stored xnew into x
                        f_i = f_new;

                        nu[h]++;

                        // CHECK FOR OPTIMUM VALUE
                        if( f_new < f_opt )
                        {
                            // STORE x_opt AND f_opt
                            for(int hh = 0; hh < n; ++hh) { x_opt[hh] = x[hh]; }
                            f_opt = f_new;

                            if( !silent )
                            {
                                file_out << "     f_opt:    " << f_opt << endl;
                                file_out << "     x_opt:    " << endl;
                                for(int i = 0; i < n; ++i) { file_out << "          x_opt[" << i << "]:=" << x_opt[i] << endl;  }
                            }
                        }
                    }
                    // ... ELSE ACCEPT ACCORDING TO METROPOLIS MOVE
                    else
                    {
                        double p = exp((f_i - f_new)/T);
                        double pp = rand_num_uniform_Mersenne_twister(0.0, 1.0);


                        // ACCEPT?
                        if( pp < p )
                        {
                            // STORE x' AND f'
                            // ! we have already stored xnew into x
                            f_i = f_new;

                            nu[h]++;
                        }
                        else
                        {
                            // STORE BACK ORIGINAL x[h]
                            x[h] = xtmp;
                        }
                    } // end if


                    //=========================================================
                    // STEP 4: ADD TO h AND j
                    //=========================================================

                } // ++h
            } // ++j

            //=========================================================
            // STEP 5: UPDATE STEP VECTOR v_m
            //=========================================================
            // ! The reason for such variations in step length is to try
            // and maintain the average percentage of accepted moves at
            // about 1/2 of the total number of moves

            for(int u = 0; u < n; ++u)
            {
                if( nu[u] > (0.6*static_cast<double>(N_S)) )
                {
                    v[u] *= ( 1.0 + c[u]*( (static_cast<double>(nu[u])/static_cast<double>(N_S) - 0.6)/0.4 ) );

                    // KEEP MAXIMUM STEP SIZE LESS THAN 1/2 OF THE [a,b] RANGE
                    v[u] = min( v[u], (b[u] - a[u])/2.0);
                }
                else if( nu[u] < (0.4*static_cast<double>(N_S)) )
                {
                    v[u] /= ( 1.0 + c[u]*( 0.4 - static_cast<double>(nu[u])/static_cast<double>(N_S) )/0.4 );
                }

                nu[u] = 0;
            } // ++h
        } // ++m


        //=========================================================
        // STEP 6: REDUCE THE TEMPERATURE
        //=========================================================
        T *= r_T;
        f[k] = f_i;

        if( !silent ) { file_out << "     f[" << k << "]: " << f[k] << endl; }

        //=========================================================
        // STEP 7: CHECK RESULTS AGAINST TERMINATING CRITERION
        //=========================================================

        // ASSIGN FLAG INDICATING THAT WE SHOULD STOP
        bool stop = true;

        // NOW CHECK IF: |f_k - f_{k-u}| <= eps, u=1,...,N_eps
        for(int u = 1; u <= N_eps; ++u)
        {
            if( fabs( f[k] - f[k-u] ) > eps )
            {
                stop = false;
                break;
            }
        } // ++u

        // NOW CHECK IF: f_k - f_opt <= eps
        if( (f[k] - f_opt) > eps ) { stop = false; }

        // STOP? ...
        if( stop ) { break; }

        // IF NOT, UPDATE x AND f
        for(int h = 0; h < n; ++h) { x[h] = x_opt[h]; }
        f_i = f_opt;
    } // ++k

    // DO A QUICK CHECK TO MAKE SURE WE CONVERGED
    if( k == (MAXITER-1) )
    {
        cout << "Simulated annealing in sim_anneal() (jminimization) did NOT converge! PROCEED FURTHER AT OWN RISK!" << endl;
    }

    //=========================================================
    // PROVIDE SOME FINAL OUTPUT
    //=========================================================

    if( !silent )
    {
        file_out << "FINAL INFO:" << endl;
        file_out << "     f_opt:    " << f_opt << endl;
        file_out << "     x_opt:    " << endl;
        for(int i = 0; i < n; ++i) { file_out << "          x_opt[" << i << "]:=" << x_opt[i] << endl;  }
        file_out << "     Ending T: " << T << endl;
        file_out << endl;
        file_out << "** ENDING OUTPUT **" << endl;
    }


    //=========================================================
    // CLEANUP & RETURN
    //=========================================================

    delete [] x_opt;
    delete [] f;

    delete [] nu;
    delete [] c;

    if( !silent ) { file_out.close(); }

    return;
}




//========================================================================
//========================================================================
//
// NAME: double sim_anneal_get_Topt(int n, int xtype[], double a[], double b[], std::function<double(const std::vector<double> &)> func, int m)
//
// DESC: Minimizes a function func(x) of n constrained variables using simulated annealing.
//
// INPUT:
//     x[0,...,n-1]     : starting vector in R^n
//     n                : number of elements of x
//     xtype[0,...,n-1] : data type of x (0 == int or else == double)
//     a[0,...,n-1]     : min range of x
//     b[0,...,n-1]     : max range of x
//     func(x)          : function to be minimized
//     T                : temp for simulated annealing
//     v[0,...,n-1]     : initial step vector
//     eps              : convergence criterion
//     N_T              : number of steps at a given T
//     N_S              : number of steps for a given step-size
//     silent           : silent mode (do not provide output to a file)
//
// OUTPUT:
//     return           : optimum temperature
//
// NOTES:
//     ! this routine is based upon the article by Walid Ben-Ameur:
//          Computational Optimization and Applications 29, 369--385 (2004)
//
//========================================================================
//========================================================================
double sim_anneal_get_Topt(int n, int xtype[], double a[], double b[], std::function<double(const std::vector<double> &)> func, int m)
{
    // ! 10^-3 is the value given in the Ben-Ameur paper
    double EPS = 1.0E-3;
    // ! an acceptance rate of 80% is usually desired
    double X0  = 0.8;
    // ! a p of 1.0 will converge almost all of the time, but increasing to 2 will ENSURE convergence
    double p = 2.0;

    //*********************************************************

    double Topt;

    std::vector<double> Emin(m), Emax(m);


    //=========================================================
    // GENERATE *MAXIMUM* STEP VECTORS TO BE 1/2 OF [a,b] RANGE
    //=========================================================

    std::vector<double> v(n);

    for(int j = 0; j < n; ++j) { v[j] = (b[j] - a[j])/2.0; }


    //=========================================================
    // GENERATE A RANDOM SET OF m POSITIVE TRANSITIONS
    //=========================================================

    for(int i = 0; i < m; ++i)
    {
        std::vector<double> xinit(n);
        std::vector<double> xfinal(n);

        // SET AN INITIAL RANDOM VECTOR xinit
        for(int j = 0; j < n; ++j)
        {
            if( xtype[j] == 0 ) { xinit[j] = static_cast<double>( rand_num_uniform_Mersenne_twister(static_cast<int>(a[j]), static_cast<int>(b[j])) ); }
            else                { xinit[j] = rand_num_uniform_Mersenne_twister(a[j], b[j]); }
        }

        // NOW PICK A RANDOM POSITION IN x TO MOVE
        int pos = rand_num_uniform_Mersenne_twister(0, (n-1));

        // MAKE A RANDOM MOVE
        bool badmove = true;
        double xnew;
        do
        {
            // GENERATE A RANDOM STEP
            double step = rand_num_uniform_Mersenne_twister(-1.0, 1.0)*v[pos];
            if( xtype[pos] == 0 ) { step = floor(step + 0.5); }

            // MAKE MOVE
            xnew = xinit[pos] + step;

            // CHECK THAT THE NEW POSITION IS WITHIN OUR RANGE
            if( (xnew >= a[pos]) && (xnew <= b[pos]) ) { badmove = false; }
        } while( badmove );

        // NOW ASSIGN xfinal
        for(int j = 0; j < n; ++j) { xfinal[j] = xinit[j]; }
        xfinal[pos] = xnew;

        // EVALUATE FUNCTIONS FOR xinit AND xfinal
        double E1 = func(xinit);
        double E2 = func(xfinal);

        if( ( E1 != E1 ) || ( E2 != E2 ) )
        {
            i--;
            continue;
        }

        Emin[i] = min(E1, E2);
        Emax[i] = max(E1, E2);
    } // ++i

    //=========================================================
    // ITERATE TO FIND Topt
    //=========================================================

    Topt = 1.0;

    // SET Topt TO BE THE MAX OF max(|Emax|,|Emin|)
    for(int i = 0; i < m; ++i)
    {
        Topt = max(Topt, fabs(Emin[i]));
        Topt = max(Topt, fabs(Emax[i]));
    }


    // SET Xhat TO X0 SO AS TO *NOT* SCALE Topt ON FIRST PASS
    double Xhat = X0;
    do
    {
        Topt *= pow( log(Xhat)/log(X0), (1.0/p) );

        // CALCULATE Xhat
        Xhat = 0.0;
        double Xnum, Xdenom;
        Xnum = Xdenom = 0.0;
        for(int i = 0; i < m; ++i)
        {
            Xnum   += exp(-Emax[i]/Topt);
            Xdenom += exp(-Emin[i]/Topt);
        }
        Xhat = Xnum/Xdenom;

    } while( fabs(Xhat - X0) > EPS );

    return Topt;
}

