/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <7/29/2013
// LAST UPDATE:  4/11/2015

#include "jARMA.hpp"

// UTILITY
//#include <cstdlib>
#include <vector>
// math
#include <cmath>
// I/O
#include <iostream>

// JSCIENCE
#include "jrandnum.hpp"
#include "jstats.hpp"
#include "jminimization.h"

// GLOBAL VARIABLES FOR jARMA_fit()
int g_p, g_q;
vector<double> g_w, g_resid;


//========================================================================
//========================================================================
//
// NAME: void jARMA_fit(int p, int q, vector<double> w, vector<double> & x)
//
// DESC: Predicts the future value of a stationary time-series with data w and ARMA parameters (phi, theta)
//
// INPUT:
//          p             : autoregressive order of the ARMA model
//          q             : moving average order of the ARMA model
//          w             : time-series data
//
// OUTPUT:
//          x             : [phi_{0},...,phi_{p-1},theta_p,...,theta_{p+q-1}] that minimizes L*(phi,theta)
//
// NOTES:
//     ! minimizes the function L*(phi,theta) w.r.t. (phi,theta)
//     ! simulated annealing is used for the minimization, so that we can bound the range of the parameters. If powell is used (commented out below), then nan results with exact likelihood evaluations (??? from parameters going out of bounds -- not sure)
//     ! the initial state is initialized with a normal distribution with 0 mean and 0.5 standard deviation
//     ! for a discussion of the function to be minimized, see:
//          Gardner et al. Applied Statistics 29, 311--322 (1980)
//     ! the global variables defined above are for use with this subroutine
//     ! IMPORTANT: C++ 0 offsets ARE used AS PASSED TO THIS PROGRAM
//
//========================================================================
//========================================================================
void jARMA_fit(int p, int q, vector<double> w, vector<double> & x, const vector<double> & xa, const vector<double> & xb, double & estddev)
{
    // ! testing on 60 days worth of 5 min data of the S&P 500, 1.0E-6 seems sufficient to converge the results with no noticeable to minimal error & it also does not seem to be too strict
    double TOL = 1.0E-6;
    
    //*********************************************************
    
    int nvar = p + q;
    x.resize(nvar);
    
    //=========================================================
    // SET p & q, & CALCULATE r, ETC.
    //=========================================================
    
    g_p = p;
    g_q = q;
    
    
    //=========================================================
    // SET n & w & RESIDUAL ARRAY
    //=========================================================
    // ! IMPORTANT: here is where data is shifted to out of C++ 0 offset    
    
    g_w = w;
    g_w.insert( g_w.begin(), 0.0 );

    g_resid = vector<double>((g_w.size()+1), 0.0);
    
    
    //***********
    //=========================================================
    // SIMULATED ANNEALING
    //=========================================================
    
    // SET UP STARTING VECTOR & ASSOCIATED VARIABLES

    //double *param = new double [nvar];
    int *xtype    = new int [nvar];
    double *a     = new double [nvar];
    double *b     = new double [nvar];
    double *v     = new double [nvar];
    
    std::vector<double> param(nvar);
//    std::vector<double> param(nvar), a(nvar), b(nvar), v(nvar);
//    std::vector<int> xtype(nvar);
    
    for(int i = 0; i < nvar; ++i)
    {
        param[i] = rand_num_normal_Mersenne_twister(0.0, 0.25);
        xtype[i] = 1;
        a[i]     = xa[i];
        b[i]     = xb[i];
        v[i]     = 0.025;
    }
    
    // DO SIMULATED ANNEALING
    // ! all settings (N_T and N_S) are taken as recommended sim_anneal parameters
    sim_anneal(param, nvar, xtype, a, b, jARMA_minimization_func, -1, v, TOL, -1, -1, false);
    
    // ASSIGN x
    for(int i = 0; i < nvar; ++i) { x[i] = param[i]; }
    

    // CLEANUP
//    delete [] param;
    delete [] xtype;
    delete [] a;
    delete [] b;
    delete [] v;
    //***********
    
/*
    //*********** 
    //=========================================================
    // POWELL
    //=========================================================
    // ! I have noticed powell does NOT always find the global minimum (although it does a decent job), so here nrep passes are made
    
    int nrep = 4;
    
    //*********************************************************
    
    double dmin = 1.0E36;
    
    for(int n = 0; n < nrep; ++n)
    {
        // ALLOCATE MEMORY & SET INITIAL DIRECTION
        int iter;
        double dret;
        double *param = new double [nvar];
        double **xi;
        xi = new double * [nvar];
        for(int i = 0; i < nvar; ++i)
        {
            xi[i] = new double [nvar];
            
            for(int j = 0; j < nvar; ++j) { xi[i][j] = 0.0; }
   
            xi[i][i] = 1.0;
        }
        
        // SET RANDOM INITIAL PARAMETERS
        // ! here a 0 mean with 0.5 stddev randomization is made
        for(int i = 0; i < nvar; ++i) { param[i] = rng_norm_dist_MersTwist(0.0, 0.5); }
        
        // DO POWELL
        powell( param, xi, nvar, TOL, &iter, &dret, jARMA_minimization_func );
        
        // IF THIS IS THE MIN. POINT FOUND, ASSIGN TO x
        if( dret < dmin )
        {
            dmin = dret;
            
            for(int i = 0; i < nvar; ++i) { x[i] = param[i]; }
        }
        
        // CLEANUP
        delete [] param;
        for(int i = 0; i < nvar; ++i) { delete [] xi[i]; }
        delete [] xi;
    } // ++n
    //***********
*/
    
    
    //=========================================================
    // CALCULATE THE STANDARD DEVIATION OR RESIDUALS
    //=========================================================
    // !
    
    std::vector<double> resid;
    for(int i = 0; i < g_w.size(); ++i) { resid.push_back(g_resid[i+1]); }
    // !!! JMM: sample stddev is calculated here, but maybe we want the population???
    estddev = stddev(resid);
    
    
    //=========================================================
    // CLEANUP & RETURN
    //=========================================================
    
        
    return;
}



//========================================================================
//========================================================================
//
// NAME: double jARMA_minimization_func(double *x)
//
// DESC: Function to pass in minimization subroutines (powell, simulated annealing, etc.) to find (phi,theta)
//
// INPUT:
//          x             : [phi_{0},...,phi_{p-1},theta_p,...,theta_{p+q-1}]
//
// OUTPUT:
//          return        : L*(phi,theta) to be minimized w.r.t. (phi,theta)
//
// NOTES:
//     ! see:
//          Gardner et al. Applied Statistics 29, 311--322 (1980)
//     ! IMPORTANT: C++ 0 offsets ARE used as passed to this program
//
//========================================================================
//========================================================================
double jARMA_minimization_func(const std::vector<double> &x)
{
    // CALCULATE r & np
    int r = max(g_p, (g_q+1));
    int np = r*(r + 1)/2;
    
    
    //=========================================================
    // ASSIGN phi & theta FROM x
    //=========================================================
    // ! IMPORTANT: here is where data is shifted to out of C++ 0 offset
    
    vector<double> phi((r+1), 0.0), theta((r+1), 0.0);
    
    for(int i = 0; i < g_p; ++i) { phi[i+1] = x[i]; }

    for(int i = 0; i < g_q; ++i) { theta[i+1] = x[i+g_p]; }
    
    
    //=========================================================
    // PARSE STARMA THEN KARMA
    //=========================================================
    
    // ALLOCATE SOME MEMORY (& RE-ZERO THE RESIDUAL ARRAY)
    vector<double> A(r+1, 0.0), P(np+1, 0.0), V(np+1, 0.0);
    g_resid = vector<double>((g_w.size()+1), 0.0);
    
    double sumlog, ssq;
    sumlog = ssq = 0.0;
    int ifault;
    
    
    STARMA(g_p, g_q, phi, theta, A, P, V, ifault);
    
    if( ifault != 0 ) { std::cout << "Error in jARMA_minimization_func(): ifault != 0 (" << ifault << ") upon return from STARMA!" << endl; }
    
    // ! use nit = 1 and a delta of ~0.001 if the exact likelihood with negative delta takes too long
    int nit = 0;
    double delta = -1.0;
    int iupd = 1; // necessary when P0 has been obtained from STARMA
    
    KARMA(g_p, g_q, phi, theta, A, P, V, g_w, g_resid, sumlog, ssq, iupd, delta, nit);
    
    
    //=========================================================
    // RETURN L*(phi,theta) = n*log[S(phi,theta)] + sum_{t=1,n}[log(f_t)]
    //=========================================================
    // ! see Eq. (5) in Gardner
    // ! KARMA outputs the second term as sumlog and S(phi,theta) as ssq
    
    return ( static_cast<double>(g_w.size())*log(ssq) + sumlog );
}


//========================================================================
//========================================================================
//
// NAME: double jARMA_predict(int m, int p, int q, vector<double> w, vector<double> x)
//
// DESC: Predicts the future value of a stationary time-series with data w and ARMA parameters (phi, theta)
//
// INPUT:
//          m             : steps ahead to predict (t + m)
//          p             : autoregressive order of the ARMA model
//          q             : moving average order of the ARMA model
//          w             : time-series data
//          x             : [phi_{0},...,phi_{p-1},theta_p,...,theta_{p+q-1}]
//
// OUTPUT:
//          return        : the value of w_{t+m}
//
// NOTES:
//     ! wrapper to perform STARMA, KARMA, and KARFOR from:
//          Gardner et al. Applied Statistics 29, 311--322 (1980)
//     ! IMPORTANT: C++ 0 offsets ARE used AS PASSED TO THIS PROGRAM
//
//========================================================================
//========================================================================
double jARMA_predict(int m, int p, int q, vector<double> w, vector<double> x)
{
    //=========================================================
    // SET r & np
    //=========================================================
    
    int r = max(p, (q+1));
    int np = r*(r + 1)/2;
    
    
    //=========================================================
    // SET n & w
    //=========================================================
    // ! IMPORTANT: here is where data is shifted to out of C++ 0 offset
    
    int n = w.size();
    w.insert( w.begin(), 0.0 );
    
    
    //=========================================================
    // SET phi & theta
    //=========================================================
    
    vector<double> phi((r+1), 0.0), theta((r+1), 0.0);
    
    for(int i = 0; i < p; ++i) { phi[i+1] = x[i]; }
    
    for(int i = 0; i < q; ++i) { theta[i+1] = x[i+p]; }
    
    
    //=========================================================
    // PARSE STARMA & KARMA TO GET A, P, V, AND phi
    //=========================================================
    // ! nit is set to 1 below, to avoid sporadic NaN results that occur sporadically with nit == 0 --- ???I am not sure if this is from bad coefficients or what ??? -- I know that there are constraints on the AR part -- in any case, a comparison of some nit 0 and 1 results shows that the predictions are pretty close for a given model
    
    vector<double> A((r+1), 0.0), P((np+1), 0.0), V((np+1), 0.0);
    int ifault;
    double sumlog, ssq;
    sumlog = ssq = 0.0;   // set to 0, for a first pass
    vector<double> resid((w.size()+1), 0.0);
    int iupd = 1;         // necessary when P0 has been obtained from STARMA
    int nit = 0;
    double delta = -1.0;
    
    STARMA(p, q, phi, theta, A, P, V, ifault);
    
    if( ifault != 0 ) { std::cout << "Error in jARMA_predict(): ifault != 0 (" << ifault << ") upon return from STARMA!" << endl; }

    KARMA(p, q, phi, theta, A, P, V, w, resid, sumlog, ssq, iupd, delta, nit);
    

    //=========================================================
    // NOW PREDICT USING KALFOR
    //=========================================================

    KALFOR(m, p, q, phi, A, P, V);
    
    
    //=========================================================
    // RETURN A[1], THE PREDICTION OF w_{t+m}
    //=========================================================
    
    return A[1];
}


//========================================================================
//========================================================================
//
// NAME: void gen_ARMA_noise(double estddev, int n, std::vector<double> & noise)
//
// DESC: Generates noise for use with an ARMA model (normally-distributed with 0 mean and a std. dev. of estddev)
//
// INPUT:
//          estddev   : standard-deviation of the normal distribution
//          n         : number of points to generate
//
// OUTPUT:
//          noise     : generated noise
//
// NOTES:
//     ! 
//
//========================================================================
//========================================================================
void gen_ARMA_noise(double estddev, int n, std::vector<double> & noise)
{
    noise.resize(n);
    for(int i = 0; i < n; ++i)
    {
        noise[i] = rand_num_normal_Mersenne_twister(0.0, estddev);
    }
}



//************************************************************************
//************************************************************************
//************************************************************************

//========================================================================
//========================================================================
//
// NAME: void STARMA(int p, int q, int r, int np, vector<double> & phi, vector<double> theta, vector<double> & A, vector<double> & P, vector<double> & V, vector<double> thetab, vector<double> xnext, vector<double> xrow, vector<double> rbar, int nrbar, int & ifault)
//
// DESC: Sets the values of V and phi, and obtains the initial values of A and P
//
// INPUT:
//          p             : autoregressive order of the ARMA model
//          q             : moving average order of the ARMA model
//          r             : max(p, (q+1))
//          np            : r*(r+1)/2
//          phi(r)        : phi (in the first p locations)
//          theta(r)      : theta (in the first q locations)
//
// OUTPUT:
//          phi(r)        : first column of T
//          A(r)          : a0
//          P(np)         : P0, stored as a lower-triangular matrix, column by column
//          V(np)         : R*R^T, stored as a lower-triangular matrix, column by column
//          ifault        : fault indicator -- see subroutine for flags
//
//
// NOTES: 
//     ! adaptation of algorithm 154 as subroutine STARMA from:
//          Gardner et al. Applied Statistics 29, 311--322 (1980)
//     ! this routine is not suitable for an AR(1) process -- see inside the program for initialization, in this case
//     ! !!! JMM: C++ offsets are NOT used
//
//========================================================================
//========================================================================
void STARMA(int p, int q, vector<double> & phi, const vector<double> & theta, vector<double> & A, vector<double> & P, vector<double> & V, int & ifault)
{
    //=========================================================
    // CHECK FOR ERRORS
    //=========================================================

    ifault = 0;
    
    if( p < 0 )                  { ifault = 1; }
    if( q < 0 )                  { ifault += 2; }
    if( (p*p + q*q) == 0 )       { ifault = 4; }     // p == q == 0
    if( (p == 1) && (q == 0) )   { ifault = 8; }     // p == 1, q == 0 -- see below

    if( ifault != 0 ) { return; }
    
    
    /*
    // *****
    //=========================================================
    // FOR AN AR(1) PROCESS, THE FOLLOWING SHOULD BE USED FOR INITIALIZATION
    //=========================================================
    
    if( (p == 1) && (q == 0) )
    {
        V[1] = 1.0;
        A[1] = 0.0;
        P[1] = 1.0/( 1.0 - phi[1]*phi[1] );
        
        return;
    }
    // *****
    */
    
    //=========================================================
    // INITIALIZATION
    //=========================================================

    // CALCULATE r, np, & nrbar
    int r     = max(p, (q+1));
    int np    = r*(r+1)/2;
    int nrbar = np*(np-1)/2;
    
    // ALLOCATE WORKSPACES
    vector<double> thetab((np+1), 0.0), xnext((np+1), 0.0), xrow((np+1), 0.0), rbar((nrbar+1), 0.0);

    
    //=========================================================
    // SET A0, V, & PHI
    //=========================================================
    
    for(int i = 2; i <= r; ++i)
    {
        A[i] = 0.0;
        
        if( i > p ) { phi[i] = 0.0; }
        
        V[i] = 0.0;
        
        if( i <= (q+1) ) { V[i] = theta[i-1]; }
    } // LINE 10: ++i
    
    A[1] = 0.0;
    
    if( p == 0 ) { phi[1] = 0.0; }
    
    V[1] = 1.0;
    
    int ind = r;
    
    
    for(int j = 2; j <= r; ++j)
    {
        double Vj = V[j];
        
        for(int i = j; i <= r; ++i)
        {
            ind++;
            V[ind] = V[i]*Vj;
        }
    }
    
    
    //=========================================================
    // FIND P0
    //=========================================================
    // ! the set of equations S*VEC[P0] = VEC[V] is solved for VEC[P0]
    // ! S is generated row by row in the array xnext
    // ! the order of the elements in P is changed, so as to bring more leading zeros into the rows of S, hence achieving a reduction of computing time
    
    if( p != 0 )
    {
        int ind1, indi, indj, ind2, npr, npr1;
        
        int r1 = r - 1;
        int rank = 0;
        int ifail = 0;
        
        double ssqerr = 0.0;
        
        for(int i = 1; i <= nrbar; ++i) { rbar[i] = 0.0; }

        for(int i = 1; i <= np; ++i)
        {
            P[i]      = 0.0;
            thetab[i] = 0.0;
            xnext[i]  = 0.0;
        } // LINE 50: ++i

        ind = 0;
        ind1 = 0;
        npr = np - r;
        npr1 = npr + 1;
        indj = npr1;
        ind2 = npr;
        
        for(int j = 1; j <= r; ++j)
        {
            double phij = phi[j];
            xnext[indj] = 0.0;
            indj++;
            indi = npr1 + j;
            
            for(int i = j; i <= r; ++i)
            {
                ind++;
                double ynext = V[ind];
                double phii = phi[i];
                
                if( j != r )
                {
                    xnext[indj] = -phii;
                    if( i != r )
                    {
                        xnext[indi] -= phij;
                        ind1++;
                        xnext[ind1] = -1.0;
                    }
                }
                
                // LINE 100 ...
                xnext[npr1] = -phii*phij;
                ind2++;
                if( ind2 > np ) { ind2 = 1; }
                xnext[ind2] += 1.0;
                
                double weight = 1.0;
                double recres;
                
                INCLU2(np, weight, xnext, xrow, ynext, P, rbar, thetab, ssqerr, recres, rank, ifail);
                
                xnext[ind2] = 0.0;
                
                if( i != r )
                {
                    xnext[indi] = 0.0;
                    indi++;
                    xnext[ind1] = 0.0;
                }
            } // ++i
        } // LINE 110: ++j
        
        REGRES(np, rbar, thetab, P);
        
        //---------------------------------------------------------
        // NOW RE-ORDER P
        //---------------------------------------------------------
        
        ind = npr;
        
        for(int i = 1; i <= r; ++i)
        {
            ind++;
            xnext[i] = P[ind];
        }
        
        ind = np;
        ind1 = npr;
        
        for(int i = 1; i <= npr; ++i)
        {
            P[ind] = P[ind1];
            ind--;
            ind1--;
        }
        
        for(int i = 1; i <= r; ++i) { P[i] = xnext[i]; }

        return;
        
    }
    // ... ELSE p == 0, AND P0 IS OBTAINED BY BACKSUSBTITUTION FOR A MOVING AVERAGE PROCESS
    else
    {
        // LINE 300 ...
        int indn = np + 1;
        ind = np + 1;
        
        for(int i = 1; i <= r; ++i)
        {
            for(int j = 1; j <= i; ++j)
            {
                ind--;
                P[ind] = V[ind];
                if( j != 1 )
                {
                    indn--;
                    P[ind] += P[indn];
                }
            }
        }
        
        return;
    }
    
    
    // WE SHOULD HAVE ALREADY RETURNED BY NOW ...
    
    return;
}



//========================================================================
//========================================================================
//
// NAME: void KARMA(int p, int q, int r, int np, vector<double> phi, vector<double> theta, vector<double> & A, vector<double> & P, vector<double> V, int n, vector<double> w, vector<double> & resid, double & sumlog, double & ssq, int iupd, double delta, vector<double> E, int & nit)
//
// DESC: Updates A, P, sumlog, and SSQ by inclusion of the data values w[1],...,w[n]. The corresponding values of resid are also obtained.
//
// INPUT:
//          p             : autoregressive order of the ARMA model
//          q             : moving average order of the ARMA model
//          phi(r)        : first column of T
//          theta(r)      : theta (in the first q locations)
//          A(r)          : a0
//          P(np)         : P0
//          V(np)         : R*R^T
//          n             : number of observations
//          w(n)          : observations
//          sumlog        : initial value of sum[log(f_t)] (zero if no previous observation)
//          ssq           : initial value of sum[v_t^2] (zero if no previous observation)
//          iupd          : flag:
//                              1 == prediction equations are by-passed for the first observation. This is necessary when the value of P0 has been obtained from STARMA. In this case, P_{1|0} = P0 and a_{1|0} = a0 and using the prediction equations coded in KARMA would lead to erroneous results. For values other than 1, the prediction equations are not by-passed.
//          delta         : when nit == 0, this parameter determines the level of approximation. Negative delta ensures that the Kalman filter is used for all observations. Otherwise the filter is performed while f_t >= 1 + del, ``quick recursions'' being used thereafter
//          nit           : flag:
//                              when set to zero, see description of delta; for non-zero values, the ``quick recursions'' are performed thoughout, so that a conditional likelihood is obtained
//          
//
// OUTPUT:
//          A(r)          : a_t, where t = t*
//          P(np)         : P_t, where t = t*
//          resid(n)      : standardized prediction errors
//          sumlog        : final value of sum[log(f_t)]
//          ssq           : final value of sum[v_t^2]
//          nit           : number of observations dealt with by the Kalman filter (t*)
//          
//
//
// NOTES:
//     ! adaptation of algorithm 154.1 as subroutine KARMA from:
//          Gardner et al. Applied Statistics 29, 311--322 (1980)
//     ! when FT is less than (1 + delta), quick recursions are used
//     ! !!! JMM: C++ offsets are NOT used
//
//========================================================================
//========================================================================
void KARMA(int p, int q, const vector<double> & phi, const vector<double> & theta, vector<double> & A, vector<double> & P, const vector<double> & V, const vector<double> & w, vector<double> & resid, double & sumlog, double & ssq, int iupd, double delta, int & nit)
{
    // CALCULATE r
    int r  = max(p, (q+1));
    
    // ALLOCATE WORKSPACE
    vector<double> E((r+1), 0.0);     // to store the last q standardized prediction errors
    
    //*********************************************************
    
    int r1 = r - 1;

    for(int i = 1; i <= r; ++i) { E[i] = 0.0; }
    
    int inde = 1;

    int i;
    
    // UPDATE A, P, sumlog, & SSQ ... IF nit != 0, USE QUICK RECURSIONS ...
    if( nit == 0 )
    {
        for(i = 1; i <= w.size(); ++i)
        {
            double wnext = w[i];
            
            //---------------------------------------------------------
            // PREDICTION
            //---------------------------------------------------------
            
            if( (iupd != 1) || (i != 1) )
            {
                // HERE DT = FT - 1.0
                double dt = 0.0;
                
                if( r != 1 ) { dt = P[r+1]; }
                
                if( dt < delta ) { goto L610; }
                
                double A1 = A[1];
                
                if( r != 1 ) { for(int j = 1; j <= r1; ++j) { A[j] = A[j+1]; } }
                
                // LINE 110 ...
                A[r] = 0.0;
                
                if( p != 0 ) { for(int j = 1; j <= p; ++j) { A[j] += ( phi[j]*A1 ); } }
                
                // LINE 200 ...
                int ind = 0;
                int indn = r;
                
                for(int l = 1; l <= r; ++l)
                {
                    for(int j = l; j <= r; ++j)
                    {
                        ind++;
                        
                        P[ind] = V[ind];
                        
                        if( j != r )
                        {
                            indn++;
                            P[ind] += P[indn];
                        }
                    }
                } // LINE 210; ++l
            }
            
            //---------------------------------------------------------
            // UPDATING
            //---------------------------------------------------------
            
            // LINE 300 ...
            double ft = P[1];
            
            double ut = wnext - A[1];
            
            if( r != 1 )
            {
                int ind = r;
                
                for(int j = 2; j <= r; ++j)
                {
                    double G = P[j]/ft;
                    
                    A[j] += ( G*ut );
                    
                    for(int l = j; l <= r; ++l)
                    {
                        ind++;
                        P[ind] -= ( G*P[l] );
                    }
                } // LINE 400: ++J
            }
            
            // LINE 410 ...
            A[1] = wnext;

            for(int l = 1; l <= r; ++l) { P[l] = 0.0; }
            
            resid[i] = ut/sqrt(ft);
            
            E[inde] = resid[i];
            
            inde++;
            
            if( inde > q ) { inde = 1; }
            
            ssq += ( ut*ut/ft );
            
            sumlog += log(ft);
            
        } // LINE 500: ++i
        
        nit = w.size();
        return;
    }
    // ... ELSE,  FOR NON-ZERO VALUES OF NIT, PERFORM QUICK RECURSIONS ...
    else
    {
        i = 1;
L610:
        nit = i - 1;
        
        for(int ii = i; ii <= w.size(); ++ii)
        {
            double et = w[ii];
            int indw = ii;
            
            if( p != 0 )
            {
                for(int j = 1; j <= p; ++j)
                {
                    indw--;
                    
                    if( indw < 1 ) { break; }
                    
                    et -= ( phi[j]*w[indw] );
                }
            }
            
            // LINE 630 ...
            if( q != 0 )
            {
                for(int j = 1; j <= q; ++j)
                {
                    inde--;
                    
                    if( inde == 0 ) { inde = q; }

                    et -= ( theta[j]*E[inde] );
                }
            }
            
            
            // LINE 645 ...
            E[inde] = et;
            resid[ii] = et;
            ssq += ( et*et );
            inde++;
            
            if( inde > q ) { inde = 1; }
            
            
        } // LINE 650: ++ii
        
        
        return;
    }
    
    
    // WE SHOULD HAVE ALREADY RETURNED BY NOW ...
    
    return;
}



//========================================================================
//========================================================================
//
// NAME: void INCLU2(int np, int nrbar, double weight, vector<double> xnext, vector<double> xrow, double ynext, vector<double> & D, vector<double> & rbar, vector<double> & thetab, double & ssqerr, double & recres, int & rank, int & ifault)
//
// DESC: Updates D, rbar, thetab, ssqerr, and rank by the inclusion of xnext and ynext with a specified weight. The corresponding value of recres is calculated.
//
// INPUT:
//          np            :
//          nbar          :
//          weight        :
//          xnext(np)     :
//          xrow(np)      :
//          ynext         :
//          D(np)         :
//          rbar(nrbar)   :
//          thetab(np)    :
//          ssqerr        :
//          recres        :
//          rank          :
//          ifault        :
//
//
// OUTPUT:
//          D(np)         :
//          rbar(nrbar)   :
//          thetab(np)    :
//          ssqerr        :
//          recres        :
//          rank          :
//          ifault        :
//
// NOTES:
//     ! adaptation of algorithm 154.3 as subroutine INCLU2 from:
//          Gardner et al. Applied Statistics 29, 311--322 (1980)
//     ! this is also a revised version of algorithm 75.1 in:
//          Appl. Statist. 3 (1974)
//     ! see remark as R17 Appl. Statist. 3 (1976)
//     ! the values of xnext, ynext, and weight will be conserved
//     ! !!! JMM: C++ offsets are NOT used
//
//========================================================================
//========================================================================
void INCLU2(int np, double weight, const vector<double> & xnext, vector<double> & xrow, double ynext, vector<double> & D, vector<double> & rbar, vector<double> & thetab, double & ssqerr, double & recres, int & rank, int & ifault)
{
    double y  = ynext;
    double wt = weight;

    for(int i = 1; i <= np; ++i) { xrow[i] = xnext[i]; }

    recres = 0.0;
    
    // ERROR CHECK
    if( wt <= 0.0 )
    {
        ifault = 1;
        std::cout << "Error in INCLU2(): weight is <= 0!" << endl;
        return;
    }
    ifault = 0;
    
    int ithisr = 0;
    
    for(int i = 1; i <= np; ++i)
    {
        if( xrow[i] == 0.0 )
        {
            ithisr += ( np - i );
            continue;
        }
        
        // LINE 20 ...
        double xi = xrow[i];
        double di = D[i];
        
        double dpi = di + wt*xi*xi;
        
        D[i] = dpi;
        
        double cbar = di/dpi;
        double sbar = wt*xi/dpi;
        
        wt *= cbar;
        
        double xk;
        
        if( i != np )
        {
            int i1 = i + 1;
            
            for(int k = i1; k <= np; ++k)
            {
                ithisr++;
                xk = xrow[k];
                double rbthis = rbar[ithisr];
                xrow[k] = xk - xi*rbthis;
                rbar[ithisr] = cbar*rbthis + sbar*xk;
            }
        }
        
        // LINE 40 ...
        xk = y;
        y = xk - xi*thetab[i];
        thetab[i] = cbar*thetab[i] + sbar*xk;
        
        if( di == 0.0 )
        {
            rank++;
            return;
        }
    } // LINE 50: ++i
 
    
    ssqerr += ( wt*y*y );
    recres = y*sqrt(wt);

    return;
}


//========================================================================
//========================================================================
//
// NAME: void REGRES(int np, int nrbar, vector<double> rbar, vector<double> thetab, vector<double> & beta)
//
// DESC: Obtains beta by backsubstitution in the triangular system rbar and thetab
//
// INPUT:
//          np            : r*(r+1)/2
//          nrbar         :
//          rbar(nrbar)   :
//          thetab(np)    :
//          beta(np)      :
//
// OUTPUT:
//          beta(np)      :
//
// NOTES:
//     ! adaptation of algorithm 154.4 as subroutine REGRES from:
//          Gardner et al. Applied Statistics 29, 311--322 (1980)
//     ! this is also a revised version of algorithm 75.4 in:
//          Appl. Statist. 3 (1974)
//
//     ! !!! JMM: C++ offsets are NOT used
//
//========================================================================
//========================================================================
void REGRES(int np, const vector<double> & rbar, const vector<double> & thetab, vector<double> & beta)
{
    // CALCULATE nrbar
    int nrbar = np*(np-1)/2;
    
    //*********************************************************
    
    int ithisr = nrbar;
    int im = np;
    
    for(int i = 1; i <= np; ++i)
    {
        double bi = thetab[im];
        
        if( im != np )
        {
            int i1 = i - 1;
            int jm = np;
            
            for(int j = 1; j <= i1; ++j)
            {
                bi -= ( rbar[ithisr]*beta[jm] );
                ithisr--;
                jm--;
            }
        }
        
        // LINE 30 ...
        beta[im] = bi;
        im--;
    } // LINE 50: ++i

    return;
}


//========================================================================
//========================================================================
//
// NAME: void KALFOR(int m, int p, int r, int np, vector<double> phi, vector<double> & A, vector<double> & P, vector<double> V)
//
// DESC: Obtains predictions of A and P, m steps ahead
//
// INPUT:
//          m             : number of steps ahead for which predictor is required
//          p             : autoregressive order of the ARMA model
//          q             : moving average order of the ARMA model
//          phi(r)        : first column of the transition matrix T
//          A(r)          : current value of a_t
//          P(np)         : current value of P_t, stored in lower triangular form, column by column
//          V(np)         : R*R^T, stored in lower triangular form, column by column
//          work(r)       : workspace
//
// OUTPUT:
//          A(r)          : predicted value of a_{t+m}
//          P(np)         : predicted value of P_{t+m}
//
// NOTES:
//     ! adaptation of algorithm 154.2 as subroutine KALFOR from:
//          Gardner et al. Applied Statistics 29, 311--322 (1980)
//     ! !!! JMM: C++ offsets are NOT used
//
//========================================================================
//========================================================================
void KALFOR(int m, int p, int q, const vector<double> & phi, vector<double> & A, vector<double> & P, const vector<double> & V)
{
    // CALCULATE r
    int r  = max(p, (q+1));

    // ALLOCATE WORKSPACE
    vector<double> work((r+1), 0.0);
    
    //*********************************************************
    
    int r1 = r - 1;
    
    for(int l = 1; l <= m; ++l)
    {
        //---------------------------------------------------------
        // PREDICT A
        //---------------------------------------------------------
        
        double A1 = A[1];
        
        if( r != 1 ) { for(int i = 1; i <= r1; ++i) { A[i] = A[i+1]; } }
        
        // LINE 110 ...
        A[r] = 0.0;
        
        if( p != 0 ) { for(int j = 1; j <= p; ++j) { A[j] += ( phi[j]*A1 ); } }
        
        
        //---------------------------------------------------------
        // PREDICT P
        //---------------------------------------------------------
        
        // LINE 200 ...
        for(int i = 1; i <= r; ++i) { work[i] = P[i]; }
        
        int ind = 0;
        
        int ind1 = r;
        
        double dt = P[1];
        
        for(int j = 1; j <= r; ++j)
        {
            double phij   = phi[j];
            double phijdt = phij*dt;
            
            for(int i = j; i <= r; ++i)
            {
                ind++;
                double phii = phi[i];
                
                P[ind] = V[ind] + phii*phijdt;
                
                if( j < r ) { P[ind] += ( work[j+1]*phii ); }
                
                if( i != r )
                {
                    ind1++;
                    P[ind] += ( work[i+1]*phij + P[ind1] );
                }
            }
        } // LINE 220: ++i
    } // LINE 300: ++l
    
    return;
}
