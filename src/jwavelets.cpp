// !!! JMM: I AM NOT SURE IF THE FOLLOIWNG IS WORKING OR NOT -- FURTHER TESTING IS NEEDED BEFORE ANY OF THIS IS USED IN PRODUCTION
// NOTES: (i) An implementation based off of the 1994 and 1995 Donoho & Johnstone papers and their matlab codes is presented below (wave_shrink())


#include "jwavelets.hpp"

// STL
#include <algorithm>

// JSCIENCE
#include "jScience/linalg.hpp" // Vector<>, pnorm()
#include "jstats.hpp"


//************************************************************************
//************************************************************************
// DOCUMENTATION
//************************************************************************
//************************************************************************

//=========================================================
// WAVELET TYPES:
//=========================================================
//     Daub4
//     Daubs(n)
//     Daub4i




//************************************************************************
//************************************************************************
// SUBROUTINES
//************************************************************************
//************************************************************************


//========================================================================
//========================================================================
//
// NAME: void wt1(vector<double> & a, const int isign, Wavelet & wlet)
//
// DESC: One-dimensional discrete wavelet transform:
//                    f(t)
//                    /  \
//                   /    \
//                  A     D_J-1
//                 / \
//                /   \
//               A    D_J-2
//              / \
//             /   \
//            A    D_J-3
//
// INPUT:
//          a               : , size MUST be an integer power of 2
//          isign           :
//          wlet            :
//
// NOTES:
//     ! see p. 703 in Numerical Recipes in C++
//     ! this routine implements the pyramid algorithm, replacing a[0,...,n-1] by its wavelet transform (for isign == 1), or performing the inverse operation (for isign == -1).
//     ! the object wlet, of type Wavelet, is the underlying wavelet filter
//
//========================================================================
//========================================================================
void wt1(std::vector<double> & a, const int isign, Wavelet & wlet)
{
    // CHECK THAT n IS AN INTEGER POWER OF TWO
    if( !is_pow_of_two( a.size() ) ) { std::cout << "Error in wt1(): a is not an integer power of two!" << std::endl; }

    // IF THE SIZE OF a IS LESS THAN 4, WE CAN'T UPSCALE/DOWNSCALE EVEN ONCE
    if( a.size() < 4 ) { return; }

    // WAVELET TRANSFORM ...
    if( isign >= 0 )
    {
        wlet.condition( a, 1 );

        // START AT LARGEST HIERARCHY, AND WORK TOWARD SMALLEST ...
        for( decltype(a.size()) n = a.size(); n >= 4; n>>=1) { wlet.filt( a, isign ); }
    }
    // ... INVERSE ...
    else
    {
        // START AT SMALLEST HIERARCHY, AND WORK TOWARD LARGEST ...
        for( decltype(a.size()) n = 4; n <= a.size(); n<<=1) { wlet.filt( a, isign ); }

        wlet.condition( a, -1 );
    }
}

// DESC: Wrapper to call wt1 with a wavelet name instead of wavelet
void dwt1d(std::vector<double> & a, const int isign, std::string wavelet_nm)
{
    if( wavelet_nm == "Daub4" )        { Daub4 wlet;     wt1(a, isign, wlet); }
    else if( wavelet_nm == "Daubs4" )  { Daubs wlet(4);  wt1(a, isign, wlet); }
    else if( wavelet_nm == "Daubs12" ) { Daubs wlet(12); wt1(a, isign, wlet); }
    else if( wavelet_nm == "Daubs20" ) { Daubs wlet(20); wt1(a, isign, wlet); }
    else if( wavelet_nm == "Daub4i" )  { Daub4i wlet;    wt1(a, isign, wlet); }
    else                               { std::cout << "dwt1d(): Wavelet " << wavelet_nm << " is not in database!" << std::endl; }

}



/*
// !!! JMM: not checked yet -- see p. 712 in Numerical Recipes in C++ to check the following
void wtn(VecDoub_IO &a, VecInt_I &nn, const Int isign, Wavelet &wlet)
{
	Int idim,i1,i2,i3,k,n,nnew,nprev=1,nt,ntot=1;
	Int ndim=nn.size();
	for (idim=0;idim<ndim;idim++) ntot *= nn[idim];
	if (ntot&(ntot-1)) throw("all lengths must be powers of 2 in wtn");
	for (idim=0;idim<ndim;idim++) {
		n=nn[idim];
		VecDoub wksp(n);
		nnew=n*nprev;
		if (n > 4) {
			for (i2=0;i2<ntot;i2+=nnew) {
				for (i1=0;i1<nprev;i1++) {
					for (i3=i1+i2,k=0;k<n;k++,i3+=nprev) wksp[k]=a[i3];
					if (isign >= 0) {
						wlet.condition(wksp,n,1);
						for(nt=n;nt>=4;nt >>= 1) wlet.filt(wksp,nt,isign);
					} else {
						for(nt=4;nt<=n;nt <<= 1) wlet.filt(wksp,nt,isign);
						wlet.condition(wksp,n,-1);
					}
					for (i3=i1+i2,k=0;k<n;k++,i3+=nprev) a[i3]=wksp[k];
				}
			}
		}
		nprev=nnew;
	}
}
*/



//========================================================================
//========================================================================
//
// NAME: void denoise_signal_DWT2(vector<double> & signal, std::string wavelet_nm, int J0)
//
// DESC: Denoise a signal using SureShrink (adaptive threshold selection using priciple of SURE)
//
// INPUT:
//          signal          : signal to denoise, length MUST be 2^J
//          wavelet_nm      : wavelet family to use
//          L               : level to denoise to
//
// NOTES:
//     ! see:
//          Donoho & Johnstone ``Ideal spatial adaption by wavelet shrinkage'' Biometrika 81, 425--455 (1994)
//          Donoho & Johnstone ``Adapting to Unknown Smoothness via Wavelet Shrinkage'' 1995
//     ! in DJ1994, it is remarked that the wavelets at levels J < L do not have vanishing means, and so the coefficients should not generally cluster around 0. Hence, those coefficients, independent of n, should NOT be shrunken toward 0.
//
//
//========================================================================
//========================================================================
void denoise_signal(vector<double> & signal, std::string wavelet_nm, int L)
{
    // NORMALIZE SIGNAL SO NOISE LEVEL IS 1
    double coeff;
    norm_noise(signal, wavelet_nm, coeff);

    // PERFORM WAVE SHRINKAGE
    wave_shrink( signal, wavelet_nm, L );

    // SCALE DATA BACK
    for( auto &sig : signal ) { sig /= coeff; }
}


//========================================================================
//========================================================================
//
// NAME: void wave_shrink(vector<double> & y, std::string wavelet_nm, int L)
//
// DESC: Smooths noisy data (with noise level 1) by transforming it into the wavelet domain, applying soft thresholding to the wavelet coefficients, and inverse transforming
//
// INPUT:
//          y               : 1D signal, normalized to noise level 1 (see noise_norm()); length MUST be 2^J
//          wavelet_nm      : wavelet family to use
//          L               : low-frequency cutoff for shrinkage; should have L << J
//
// OUTPUT:
//          y               : wavelet transform of estimate, obtained by applying soft thresholding on wavelet coefficients
//
// NOTES:
//     ! this is a general wrapper routine for one of a number of methods described in a number of papers by Donoho and Johnstone. *Only SUREShrink* is currently implemented.
//
//========================================================================
//========================================================================
void wave_shrink(vector<double> & y, std::string wavelet_nm, int L)
{
    // PERFORM FORWARD DWT TO GET COEFFICIENTS (STORED IN PLACE IN y)
    dwt1d(y, 1, wavelet_nm);

    // PERFORM THRESHOLDING
    multi_hybrid(y, L);

    // INVERSE DWT
    dwt1d(y, -1, wavelet_nm);
}


//========================================================================
//========================================================================
//
// NAME: void multi_hybrid(vector<double> & wc, int L)
//
// DESC: Apply shrinkage to wavelet coefficients
//
// INPUT:
//          wc   : wavelet transform of noisy sequence with N(0,1) noise
//          L    : low-frequency cutoff for wavelet transform
//
// NOTES:
//     !
//
//========================================================================
//========================================================================
void multi_hybrid(vector<double> & wc, int L)
{
    // GET THE DYADIC LENGTH OF signal
    int J = dyad_length(wc);

    // FOR EACH LEVEL OF SHRINKAGE ...
    for(int j = (J-1); j > ((J-1)-L); --j)
    {
        // GET THE DYADIC INDICES FOR j
        int i1, i2;
        dyad_indices(j, i1, i2);

        // ASSIGN TMP wc VECTOR
        std::vector<double> wc_tmp;
        for(int i = i1; i <= i2; ++i) { wc_tmp.push_back(wc[i]); }

        // APPLY HYBRID-THRESHOLD METHOD
        hybrid_thresh(wc_tmp);

        // RE-ASSIGN wc_tmp BACK TO wc
        for(int i = i1; i <= i2; ++i) { wc[i] = wc_tmp[i-i1]; }
    } // ++j
}



//========================================================================
//========================================================================
//
// NAME: void hybrid_thresh(vector<double> & y)
//
// DESC: Adaptive threshold selection using principle of Stein's Unbiased Risk Estimate (SURE)
//
// INPUT/OUTPUT:
//          y    : noisy data with std. dev. 1; on return, estimate of mean vector
//
// NOTES:
//     ! see:
//          Donoho & Johnstone ``Adapting to Unknown Smoothness via Wavelet Shrinkage'' 1995
//
//========================================================================
//========================================================================
void hybrid_thresh(vector<double> & y)
{
    // GET THE DYADIC LENGTH OF y
    int J = dyad_length(y);

    // STORE A DOUBLE OF y.size() TO PREVENT MULTIPLE static_cast CALLS BELOW
    double n = static_cast<double>( y.size() );

    // GET UNIVERSAL THRESHOLD
    double univ_thresh = sqrt( 2.0*log(n) );

    // DETERMINE WHETHER TO USE UNIVERSAL THRESHOLD OR SURE METHOD TO SCALE
    double normy = pnorm(Vector<double>(y), 2);
    double eta  = (normy*normy - n)/n;
    double crit = pow( static_cast<double>(J), 1.5 )/sqrt(n);

    // THRESHOLD
    if( eta < crit ) { soft_thresh(y, univ_thresh); }
    else
    {
        double T = min( SURE_thresh(y), univ_thresh );
        soft_thresh(y, T);
    }

}


//========================================================================
//========================================================================
//
// NAME: double SURE_thresh(vector<double> y)
//
// DESC: Adaptive threshold selection using principle of Stein's Unbiased Risk Estimate (SURE)
//
// INPUT:
//          y       : noisy data with std. dev. 1
//
// OUTPUT:
//          return  : value of threshold
//
// NOTES:
//     !
//
//========================================================================
//========================================================================
double SURE_thresh(vector<double> y)
{
    // SORT y BY |y|, SQUARE, & STORE
    for( decltype(y.size()) i = 0; i < y.size(); ++i ) { y[i] = fabs(y[i])*fabs(y[i]); }
    std::sort(y.begin(), y.end());

    // CALCULATE CUMULATIVE SUM OF OUR SQUARED & SORTED y
    std::vector<double> b;
    b.push_back(y[0]);
    for( decltype(y.size()) i = 1; i < y.size(); ++i ) { b.push_back(y[i] + b[i-1]); }

    // STORE A DOUBLE OF y.size() TO PREVENT MULTIPLE static_cast CALLS BELOW
    double n = static_cast<double>( y.size() );

    // GENERATE A LINEAR-SPACED ROW VECTOR FROM (n-1) TO 0
    std::vector<double> c;
    for( decltype(y.size()) i = 0; i < y.size(); ++i ) { c.push_back( (n - 1.0) - static_cast<double>(i) ); }

    // CALCULATE s = b + c.*y
    std::vector<double> s;
    for( decltype(y.size()) i = 0; i < y.size(); ++i ) { s.push_back( b[i] + c[i]*y[i] ); }

    // CALCULATE RISK
    // ! the 2.0*static_cast<double>(i+1) calculates 2*v, where v is a row vector from 1 to n
    std::vector<double> risk;
    for( decltype(y.size()) i = 0; i < y.size(); ++i ) { risk.push_back( (n - 2.0*static_cast<double>(i+1) + s[i])/n ); }

    // FIND MINIMUM RISK
    int ibest = -1;
    double min_risk = std::numeric_limits<double>::max();
    for( decltype(y.size()) i = 0; i < y.size(); ++i )
    {
        if( risk[i] < min_risk )
        {
            min_risk = risk[i];
            ibest = i;
        }
    }

    // RETURN SURE_thresh
    return sqrt( y[ibest] );
}


//========================================================================
//========================================================================
//
// NAME: void soft_thresh(vector<double> & y, double t)
//
// DESC: Apply soft threshold
//
// INPUT:
//          y    : noisy data; on return, x = sign(y)*(|y| - t)_+
//          t    : threshold
//
// NOTES:
//     !
//
//========================================================================
//========================================================================
void soft_thresh(vector<double> & y, double t)
{
    for( decltype(y.size()) i = 0; i < y.size(); ++i )
    {
        double res = fabs(y[i]) - t;
        res = (res + fabs(res))/2.0;
        y[i] = sign(y[i])*res;
    }
}


//========================================================================
//========================================================================
//
// NAME: void norm_noise(vector<double> & signal, double & coeff)
//
// DESC: Estimate the noise level and normalize signal to noise level 1
//
// INPUT:
//          signal          : 1D signal, on output scaled so wavelet coefficients at the finest level have a median absolute deviation of 1
//          wavelet_nm      : wavelet name
//          coeff           : estimation of 1/sigma
//
// NOTES:
//     ! see:
//          Donoho & Johnstone ``Ideal spatial adaption by wavelet shrinkage'' Biometrika 81, 425--455 (1994)
//     ! the noise level (if unknown) is estimated based on the median absolute deviation standard deviation estimate using the detail coefficients at the finest level -- the reason is that, at the finest level, the coefficients are essentially pure noise (with the exception of some of the large coefficients corresponding to signal)
//
//========================================================================
//========================================================================
void norm_noise(vector<double> & signal, std::string wavelet_nm, double & coeff)
{
    // PERFORM FORWARD DWT
    dwt1d(signal, 1, wavelet_nm);

    // GET THE COEFFICIENTS AT THE FINEST LEVEL OF DETAIL (J-1) (THIS IS FROM THE FIRST DWT PASS)
    std::vector<double> detJ1_coeff;
    for( decltype(signal.size()) i = 0; i < (signal.size()/2); ++i) { detJ1_coeff.push_back( signal[(signal.size()/2)+i] ); }

    // GET THE MEDIAN DETAIL COEFFICIENT AT LEVEL J ...
    double median_detJ1_coeff = median( detJ1_coeff );

    // ... GET THE ABSOLUTE DEVIATIONS FROM THE MEDIAN ...
    std::vector<double> absdev_detJ1_coeff;
    for( auto c : detJ1_coeff )
    {
        absdev_detJ1_coeff.push_back( fabs(c - median_detJ1_coeff) );
    }

    // ... GET THE MEDIAN ABSOLUTE DEVIATION ...
    double med_absdev = median( absdev_detJ1_coeff );

    // ... AND FINALLY CALCULATE sigma
    // ! the coefficient includes a scale factor 0.6745 to rescale the numerator so that sigma is also a suitable estimator for the standard deviation for Gaussian white noise
    if( med_absdev != 0.0 ) { coeff = 0.6745/med_absdev; }
    else                    { coeff = 1.0; }

    // NOW INVERSE DWT
    dwt1d(signal, -1, wavelet_nm);

    // AND FINALLY SCALE SIGNAL
    for( auto &sig : signal ) { sig *= coeff; }
}




