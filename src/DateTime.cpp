/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/1/2013
// LAST UPDATE: 5/16/2014

#include "DateTime.hpp"

// STL
#include <ctime>        // std::time_t
#include <iostream>     // std::cout, std::endl
#include <string>       // std::string
#include <sstream>      // std::stringstream

// THIS
#include "date.hpp"


// note: the default constructor sets to the Unix epoch
DateTime::DateTime() : DateTime(1970, 1, 1, 0, 0, 0, TIME_ZONE::UTC) {};

DateTime::DateTime(unsigned int Y, unsigned int M, unsigned int D) : DateTime(Y, M, D, 0, 0, 0, TIME_ZONE::UTC) {};

DateTime::DateTime(unsigned int Y, unsigned int M, unsigned int D, TIME_ZONE TZ) : DateTime(Y, M, D, 0, 0, 0, TZ) {};

DateTime::DateTime(unsigned int Y, unsigned int M, unsigned int D, unsigned int h, unsigned int m, unsigned int s, TIME_ZONE TZ)
{
    YYYY     = Y;
    MM       = M;
    DD       = D;
    
    hh       = h;
    mm       = m;
    ss       = s;
    
    TZD      = TZ;
}

DateTime::DateTime(std::time_t timestamp)
{
    struct tm* ptm;
    
    ptm = gmtime(&timestamp);
    
    YYYY     = ptm->tm_year + 1900;
    MM       = ptm->tm_mon + 1;
    DD       = ptm->tm_mday;
    
    hh       = ptm->tm_hour;
    mm       = ptm->tm_min;
    ss       = ptm->tm_sec;
    
    TZD      = TIME_ZONE::UTC;
}


//========================================================================
// NAME: DateTime(const std::string str)
//
// DESC: Initializes a DateTime with a date/time in string format.
//
// NOTES:
//     ! Valid formats for str include:
//          YYYYMMDD
//          YYYY-MM-DD
//          YYYY-MM-DDThh:mm:ss (UTC implied)
//          YYYY-MM-DDThh:mm:ssTZD
//
//========================================================================
DateTime::DateTime(const std::string str)
{
    if( str.size() < 8 )
    {
        std::cout << "Error in DateTime::DateTime(const std::string str): String format is clearly wrong! str = " << str << std::endl;
        exit(0);
    }
    // ----- YYYYMMDD -----
    else if( str.size() == 8 )
    {
        std::stringstream sstream(str);
        
        std::string year, month, day;
        year.resize(4);
        month.resize(2);
        day.resize(2);
        
        sstream.read( &year[0],  4 );
        sstream.read( &month[0], 2 );
        sstream.read( &day[0],   2 );
        
        YYYY = std::stoi(year);
        MM   = std::stoi(month);
        DD   = std::stoi(day);
        
        hh   = 0;
        mm   = 0;
        ss   = 0;
        
        TZD = DateTime::TIME_ZONE::UTC;
    }
    // ----- ANY OTHER FORMAT -----
    else
    {
        std::stringstream sstream(str);
        std::string tmp;
        
        getline(sstream, tmp, '-');
        YYYY = std::stoi(tmp);
        
        getline(sstream, tmp, '-');
        MM   = std::stoi(tmp);
        
        tmp.resize(2);
        sstream.read( &tmp[0], 2 );
        DD   = std::stoi(tmp);
        
        if( str.size() > 10 )
        {
            getline(sstream, tmp, 'T'); // strip the 'T'
            
            getline(sstream, tmp, ':');
            hh = std::stoi(tmp);
            
            getline(sstream, tmp, ':');
            mm = std::stoi(tmp);
            
            tmp.resize(2);
            sstream.read( &tmp[0], 2 );
            ss = std::stoi(tmp);
            
            // << JMM >> :: I NEED A BETTER WAY TO CONVERT A STRING TO AN enum
            getline(sstream, tmp);
            if( tmp == "ET" )
            {
                TZD = DateTime::TIME_ZONE::ET;
            }
            else if( !((tmp == "UTC") || tmp.empty()) )
            {
                std::cout << "Error in string_to_DateTime(): Time zone not recognized!" << std::endl;
                exit(0);
            }
            else
            {
                TZD = DateTime::TIME_ZONE::UTC;
            }
        }
        else
        {
            hh = 0;
            mm = 0;
            ss = 0;
            
            TZD = DateTime::TIME_ZONE::UTC;
        }
    }
}


DateTime::~DateTime() {};


//========================================================================
//========================================================================
//
// NAME: std::time_t DateTime_to_timestamp(const ISO_8601 &date_time) const
//
// DESC: Calculates the UNIX timestamp for an ISO_8601 time.
//
//========================================================================
//========================================================================
std::time_t DateTime::to_timestamp() const
{
    if( TZD != DateTime::TIME_ZONE::UTC )
    {
        std::cout << "(Temporary) Error in DateTime::to_timestamp(): Object time zone MUST be UTC!" << std::endl;
        exit(0);
    }
    
    int ndays = days_since_00010101(YYYY, MM, DD) - days_since_00010101(1970, 1, 1);
    
    return static_cast<std::time_t>( ndays*86400 + hh*3600 + mm*60 + ss );
}


//========================================================================
//========================================================================
//
// NAME: unsigned int DateTime::day_of_week()
//
// DESC: Returns the day of the week (in the Gregorian calendar) that the DateTime corresponds to, in its timezone (1 == Sunday, ..., 7 == Saturday).
//
// NOTES:
//     ! This is a C++ adaptation of the Tomohiko Sakamoto algorithm (good for any Gregorian date); see:
//          http://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week
//
//========================================================================
//========================================================================
unsigned int DateTime::day_of_week() const
{
    unsigned int W;
    
    static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
    
    unsigned int y = YYYY;
    if( MM < 3 )
    {
        --y;
    }

    W = (y + y/4 - y/100 + y/400 + t[MM-1] + DD) % 7;
    ++W; // convert from 0 -- 6 to 1 -- 7
    
    return W;
}


//========================================================================
//========================================================================
//
// NAME: std::string DateTime::to_YYYYMMDD() const
//
// DESC: Convertes the DateTime structure to a string in the format "YYYY-MM-DD".
//
//========================================================================
//========================================================================
std::string DateTime::to_YYYYMMDD() const
{
    // << JMM: change to used padding, rather than all the if() checks >>
    
    std::string yr, mo, d;
    
    if( YYYY < 1000 )
    {
        if( YYYY < 100 )
        {
            if( YYYY < 10 )
            {
                if( YYYY == 0 )
                {
                    yr = "0000";                   
                }
                else
                {
                    yr = "000" + std::to_string(YYYY);
                }
            }
            else
            {
                yr = "00" + std::to_string(YYYY);
            }
        }
        else
        {
            yr = "0" + std::to_string(YYYY);
        }
    }
    else
    {
        yr = std::to_string(YYYY);
    }
    
    if( MM < 10 )
    {
        mo = "0" + std::to_string(MM);
    }
    else
    {
        mo = std::to_string(MM);
    }
    
    if( DD < 10 )
    {
        d = "0" + std::to_string(DD);
    }
    else
    {
        d = std::to_string(DD);
    }
    
    return (yr + "-" + mo + "-" + d);
    
/*
    // note: these designators are NOT ISO 8601, and as such, I don't want to hardwire them (e.g., using a boost map) at the present time 
    std::string sTZ;
    switch(TZD)
    {
        case TIME_ZONE::UTC:
            sTZ = "UTC";
            break;
        case TIME_ZONE::ET:
            sTZ = "ET";
            break;
        default:
            std::cout << "Error in DateTime::to_string(): Time zone not recognized!" << std::endl;
            exit(0);
    }
    
    return ( std::to_string(YYYY) + "-" + std::to_string(MM) + "-" + std::to_string(DD) + "T" + std::to_string(hh) + ":" + std::to_string(mm) + ":" + std::to_string(ss) + " " + sTZ );
*/
}


//========================================================================
//========================================================================
//
// NAME: std::string DateTime::to_YYYYMMDDThhmmss() const
//
// DESC: Convertes the DateTime structure to a string in the format "YYYY-MM-DDThh:mm:ss".
//
//========================================================================
//========================================================================
std::string DateTime::to_YYYYMMDDThhmmss() const
{
    std::string h, m, s;
    
    if( hh < 10 )
    {
        if( hh == 0 )
        {
            h = "00";
        }
        else
        {
            h = "0" + std::to_string(hh);
        }
    }
    else
    {
        h = std::to_string(hh);
    }
    
    if( mm < 10 )
    {
        if( mm == 0 )
        {
            m = "00";
        }
        else
        {
            m = "0" + std::to_string(mm);
        }
    }
    else
    {
        m = std::to_string(mm);
    }
    
    if( ss < 10 )
    {
        if( ss == 0 )
        {
            s = "00";
        }
        else
        {
            s = "0" + std::to_string(ss);
        }
    }
    else
    {
        s = std::to_string(ss);
    }
    
    return (to_YYYYMMDD() + "T" + h + ":" + m + ":" + s);
}


//========================================================================
//========================================================================
//
// NAME: std::string DateTime::to_YYYYMMDDThhmmssTZD() const
//
// DESC: Convertes the DateTime structure to a string in the format "YYYY-MM-DDThh:mm:ssTZD".
//
//========================================================================
//========================================================================
std::string DateTime::to_YYYYMMDDThhmmssTZD() const
{
    return (to_YYYYMMDDThhmmss() + TZD_to_string.at(TZD));
}


//========================================================================
//========================================================================
//
// NAME: DateTime& DateTime::operator-=(const Duration &dur)
//
// DESC: Performs arithmetic with between DateTime and Duration objects.
//
// << JMM >> :: could perhaps these be made faster by avoiding a conversion to a timestamp?
//
//========================================================================
//========================================================================
DateTime& DateTime::operator+=(const Duration &dur)
{
    *this = DateTime( to_timestamp() + dur.to_timestamp() );
    
    return ( *this );
}

DateTime& DateTime::operator-=(const Duration &dur)
{
    *this = DateTime( to_timestamp() - dur.to_timestamp() );
    
    return ( *this );
}


//========================================================================
//========================================================================
//
// NAME: std::ostream& operator<<(std::ostream &os, const ISO_8601 &t)
//
// DESC: Compares two ISO_8601 structures, t1 and t2, returning whether t1 < t2, etc.
//
//========================================================================
//========================================================================
std::ostream& operator<<(std::ostream &os, const DateTime &t)
{
    os << t.to_YYYYMMDDThhmmssTZD();
    
    return os;
}