/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : ~2006
// LAST UPDATE: 9/17/2013

#include "jfft.h"

// STL
#include <iostream>

//========================================================================
//========================================================================
//
// NAME: void fft(complex<double> f[], int ndim, int nn_in[], int isign, complex<double> f_fft[])
// DESC: Calculate the FFT of a function f [this is like a wrapper function to fourn()]
//
// NOTES:
//     i. both this function and that called fourn() are c++ zero-offset
//
// INPUT:
//     complex<double> f[]:     input function on an ndim grid with uniform spacings
//     int ndim:                dimension of grid over which f is over
//     int nn_in[]:             number of grid points in each direction of the ndim
//     int isign:               either [FFT]^1 or [FFT]^-1 (-1 gives R-->G)
// OUTPUT:
//     complex<double> f_fft[]: FFT of f
//
// TODO:
//     i. ! it may not be the most efficient to assign the complex value array to a bigger fully real array here
//     ii. !!! this routine is quite sloppy
//
//========================================================================
//========================================================================
void fft(complex<double> f[], int ndim, int nn_in[], int isign, complex<double> f_fft[])
{

  // FIRST CALCULATE THE NUMBER OF DATA POINTS
  int npts = nn_in[0];
  for(int i = 1; i < ndim; ++i) { npts *= nn_in[i]; }

  // ALLOCATE ARRAY TO HOLD REAL VALUES FOR FFT
  double *f_real;
  f_real = new double [2*npts]; 

  // ASSIGN COMPLEX rho TO A COMPLETELY REAL ARRAY
  for(int n = 0; n < npts; ++n)
  {
    f_real[2*n] = real( f[n] ); 
    f_real[2*n+1] = imag( f[n] ); 
  }

  // PERFORM FFT
  fourn(f_real, nn_in, ndim, isign);

  // CALCULATE PREFACTOR
  // i. ! I believe we also need to normalize by 1/npts if isign == -1
  double prefact;
  if(isign == -1) { prefact = 1.0/static_cast<double>(npts); }
  else { prefact = 1.0; }

  // ASSIGN OUTPUT ARRAY WITH PREFACTOR
  for(int n = 0; n < npts; ++n)
  {
    f_fft[n] = prefact*complex<double>(f_real[2*n], f_real[2*n+1]);
  }

  // CLEANUP & RETURN
  delete [] f_real;

  return;
}



//========================================================================
//========================================================================
//
// NAME: void fourn(double * data, int nn[], int ndim, int isign)
//
// DESC: Replaces data by its ndim-dimensional discrete Fourier transform, if isign is input as 1. nn[1..ndim-1] is an integer array containing the lengths of each dimension (number of complex values), which MUST all be powers of 2. data is a real array of length twice the product of these lengths, in which the data are stored as in a multidimensional complex array: real and imaginary parts of each element are in consecutive locations, and the rightmost index of the array increases most rapidly as one proceeds along data. For a two-dimensional array, this is equivalent to storing the array by rows. If isign is input as -1, data is replaced by its inverse transform times the product of the lengths of all dimensions.
//
// INPUT:
//     double data[] : 
//     int nn[] : 
//     int ndim : 
//     int isign : 
//
// OUTPUT:
//
// REQUIRED FUNCTIONS:
//
// NOTES:
//     i. this is from Numerical Recipes in C (web book v3 c++, p. 629 -- 630)
//
//========================================================================
//========================================================================
void fourn(double *data, int nn[], int ndim, int isign)
{
  int idim, i1, i2, i3, i2rev, i3rev, ip1, ip2, ip3, ifp1, ifp2;
  int ibit, k1, k2, n, nprev, nrem, ntot=1;
  double tempi, tempr; 
  double theta, wi, wpi, wpr, wr, wtemp; // for trigonometric recurrences

  // TOTAL NO. OF COMPLEX VALUES
  for(idim = 0; idim < ndim; idim++) { ntot *= nn[idim]; }
  if(ntot < 2 || ntot&(ntot-1)) 
  { 
      std::cout << "! must have powers of 2 in fourn in jfft.cpp ; FFT will not work" << endl;
      int er; std::cin >> er;
  }
                              
  nprev = 1;

  // MAIN LOOP OVER THE DIMENSIONS
  for(idim = ndim-1; idim >= 0; idim--) 
  {
    n = nn[idim];
    nrem = ntot/(n*nprev);
    ip1 = nprev << 1;
    ip2 = ip1*n;
    ip3 = ip2*nrem;
    i2rev = 0;

    // BIT-REVERSAL SECTION ...
    for(i2 = 0; i2 < ip2; i2 += ip1) 
    {                                     
      if( i2 < i2rev ) 
      {
        for(i1 = i2; i1 < i2 + ip1 - 1; i1 += 2) 
        {
          for(i3 = i1; i3 < ip3; i3 += ip2) 
          {
            i3rev = i2rev + i3 - i2;

            SWAP(data[i3],data[i3rev]);
            SWAP(data[i3+1],data[i3rev+1]);
          }
        }
      } // end if

      ibit = ip2 >> 1;

      while(ibit >= ip1 && i2rev+1 > ibit) 
      {
        i2rev -= ibit;
        ibit >>= 1;
      } 

      i2rev += ibit;
    } // i2

    // DANIELSON--LANCZOS SECTION ...
    ifp1 = ip1;

    while( ifp1 < ip2 ) 
    {
      ifp2 = ifp1 << 1;

      // INITIALIZE FOR THE TRIGONOMETRIC RECURRENCE
      //theta = isign*6.28318530717959/(ifp2/ip1);
      theta = isign*2.0*PI/(ifp2/ip1);
                                                          
      wtemp = sin(0.5*theta);
      wpr = -2.0*wtemp*wtemp;
      wpi = sin(theta);
      wr = 1.0;
      wi = 0.0;

      for(i3 = 0; i3 < ifp1; i3 += ip1) 
      {
        for(i1 = i3; i1 < i3 + ip1 - 1; i1 += 2) 
        {
          for(i2 = i1; i2 < ip3; i2 += ifp2) 
          {
            // DANIELSON--LANCZOS FORMULA:
            k1 = i2;
            k2 = k1 + ifp1;
            tempr = wr*data[k2] - wi*data[k2+1];
            tempi = wr*data[k2+1] + wi*data[k2];
            data[k2] = data[k1] - tempr;
            data[k2+1] = data[k1+1] - tempi;
            data[k1] += tempr;
            data[k1+1] += tempi;
          }
        }

        // TRIGONOMETRIC RECURRENCE
        wr = (wtemp=wr)*wpr - wi*wpi + wr;
        wi = wi*wpr + wtemp*wpi + wi;
      } // i3

      ifp1 = ifp2;

    } // end while

    nprev *= n;

  } // idim


  return;
}


/*
//========================================================================
//========================================================================
//
// NAME: void fourn(double data[], unsigned long nn[], int ndim, int isign)
//
// DESC: Replaces data by its ndim-dimensional discrete Fourier transform, if isign is input as 1. nn[1..ndim] is an integer array containing the lengths of each dimension (number of complex values), which MUST all be powers of 2. data is a real array of length twice the product of these lengths, in which the data are stored as in a multidimensional complex array: real and imaginary parts of each element are in consecutive locations, and the rightmost index of the array increases most rapidly as one proceeds along data. For a two-dimensional array, this is equivalent to storing the array by rows. If isign is input as -1, data is replaced by its inverse transform times the product of the lengths of all dimensions.
//
// INPUT:
//     double data[] : 
//     unsigned long nn[] : 
//     int ndim : 
//     int isign : 
//
// OUTPUT:
//
// REQUIRED FUNCTIONS:
//
// NOTES:
//     i. this is from Numerical Recipes in C (web num. recip.)
//     ii. ! you must be careful with this, as the arrays are unit offset like in C
//
//========================================================================
//========================================================================
void fourn(double data[], unsigned long nn[], int ndim, int isign)
{
  unsigned long i2rev, i3rev, ip1, ip2, ip3, ifp1, ifp2;
  unsigned long ibit, k1, k2, n, nprev, nrem, ntot;
  double tempi, tempr; 
  double theta, wi, wpi, wpr, wr, wtemp; // for trigonometric recurrences

  // COMPUTE TOTAL NUMBER OF COMPLEX VALUES
  ntot = 1;
  for(int idim = 1; idim <= ndim; ++idim) { ntot *= nn[idim]; }
                              
  nprev = 1;

  // MAIN LOOP OVER THE DIMENSIONS
  for(int idim = ndim; idim >= 1; --idim) 
  {
    n = nn[idim];
    nrem = ntot/(n*nprev);
    ip1 = nprev << 1;
    ip2 = ip1*n;
    ip3 = ip2*nrem;
    i2rev = 1;

    // BIT-REVERSAL SECTION ...
    for(unsigned long i2 = 1; i2 <= ip2; i2 += ip1) 
    {                                     
      if( i2 < i2rev ) 
      {
        for(unsigned long i1 = i2; i1 <= i2 + ip1 - 2; i1 += 2) 
        {
          for(unsigned long i3 = i1; i3 <= ip3; i3 += ip2) 
          {
            i3rev = i2rev + i3 - i2;
            SWAP(data[i3], data[i3rev]);
            SWAP(data[i3+1], data[i3rev+1]);
          }
        }
      } // end if

      ibit = ip2 >> 1;

      while( (ibit >= ip1) && (i2rev > ibit) ) 
      {
        i2rev -= ibit;
        ibit >>= 1;
      } 

      i2rev += ibit;
    } // i2

    // DANIELSON--LANCZOS SECTION ...
    ifp1 = ip1;

    while( ifp1 < ip2 ) 
    {
      ifp2 = ifp1 << 1;

      // INITIALIZE FOR THE TRIGONOMETRIC RECURRENCE
      //theta = isign*6.28318530717959/(ifp2/ip1);
      theta = isign*2.0*PI/(ifp2/ip1);
                                                          
      wtemp = sin(0.5*theta);
      wpr = -2.0*wtemp*wtemp;
      wpi = sin(theta);
      wr = 1.0;
      wi = 0.0;

      for(unsigned long i3 = 1; i3 <= ifp1; i3 += ip1) 
      {
        for(unsigned long i1 = i3; i1 <= i3 + ip1 - 2; i1 += 2) 
        {
          for(unsigned long i2 = i1; i2 <= ip3; i2 += ifp2) 
          {
            // DANIELSON--LANCZOS FORMULA:
            k1 = i2;
            k2 = k1 + ifp1;
            tempr = wr*data[k2] - wi*data[k2+1];
            tempi = wr*data[k2+1] + wi*data[k2];
            data[k2] = data[k1] - tempr;
            data[k2+1] = data[k1+1] - tempi;
            data[k1] += tempr;
            data[k1+1] += tempi;
          }
        }

        // TRIGONOMETRIC RECURRENCE
        wr = (wtemp=wr)*wpr - wi*wpi + wr;
        wi = wi*wpr + wtemp*wpi + wi;
      } // i3

      ifp1 = ifp2;

    } // end while

    nprev *= n;

  } // idim


  return;
}
*/


/*
//complex<double> fft_RtoG(int, int, int, int, int, int, complex<double> (*func)(int, int, int));
// !!! THIS IS RIDICULOUSLY SLOW !!!
//========================================================================
//========================================================================
//
// NAME: double energy_get_rho(int g)
// DESC: Calculate the electron--electron direct Coulomb interaction EH
//
// NOTES:
//     i. the density rho in reciprocal space is:
//     ii. !!! I am really not sure if this is how I want to do things -- I could have func be a function of R instead of 3 ints
//
//
//========================================================================
//========================================================================
complex<double> fft_RtoG(int N1, int N2, int N3, int m1, int m2, int m3, complex<double> (*func)(int, int, int))
{
  complex<double> fft = COMPLEXZERO;

  complex<double> e1, e2, e3;

  complex<double> exp1, exp2, exp3;
  exp3 = -(2.0*PI*COMPLEXJ/static_cast<double>(N3))*static_cast<double>(m3);
  exp2 = -(2.0*PI*COMPLEXJ/static_cast<double>(N2))*static_cast<double>(m2);
  exp1 = -(2.0*PI*COMPLEXJ/static_cast<double>(N1))*static_cast<double>(m1); 

  for(int n3 = 0; n3 < N3; ++n3)
  {
  //cout << "n3: " << n3 << endl;

    e3 = exp( exp3*static_cast<double>(n3) );
    for(int n2 = 0; n2 < N2; ++n2)
    {
      e2 = exp( exp2*static_cast<double>(n2) );
      for(int n1 = 0; n1 < N1; ++n1)
      {
  //cout << "n1: " << n1 << endl;
        e1 = exp( exp1*static_cast<double>(n1) );

        //VECTOR_DOUBLE rvec = g_simbox_vec_a*(n1/g_fft_Na) + g_simbox_vec_b*(n2/g_fft_Nb) + g_simbox_vec_c*(n3/g_fft_Nc);
        //fft += ( e1*e2*e3*energy_get_rho_R(rvec) );
        fft += ( e1*e2*e3*func(n1, n2, n3) );
      }
    }
  }

  fft *= ( 1.0/static_cast<double>(N1*N2*N3) );

  return fft;
}
*/

