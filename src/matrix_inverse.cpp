/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 4/30/2014
// LAST UPDATE: 5/27/2014


// STL
#include <iostream> // std::cout, std::endl
#include <vector>   // std::cout, std::endl

// jScience
#include "Matrix.hpp"
#include "SVD.hpp"


void SVD_inverse(const Matrix<double> &A, Matrix<double> &Ainv);


// << JMM: Why am I even using this instead of JUST pseudoinverse? >>
//========================================================================
//========================================================================
//
// NAME: void matrix_inverse(const Matrix<double> &A, Matrix<double> &Ainv) 
//
// DESC: Calculate the inverse of the matrix A, << JMM >> using the most robust and efficient method for its type.
//
//========================================================================
//========================================================================
void matrix_inverse(const Matrix<double> &A, Matrix<double> &Ainv)
{
    // << JMM >> :: A little more testing is needed, but it seems like at least LU is working 
    
    // << JMM >> :: just calling LU decomposition is temporary, as we could check whether we could use Cholesky, etc.
    LU_decomposition(A, Ainv);
    
    //SVD_inverse(A, Ainv);
    
    // **************
    Matrix<double> A_Ainv;
    matrix_multiply(A, Ainv, A_Ainv);
    
    std::cout << "A_Ainv.get_nrows(): " << A_Ainv.get_nrows() << std::endl;
    std::cout << "A_Ainv.get_ncols(): " << A_Ainv.get_ncols() << std::endl;
    
    for( int i = 0; i < A_Ainv.get_nrows(); ++i )
    {
        for( int j = 0; j < A_Ainv.get_ncols(); ++j )
        {
            std::cout << "A_Ainv(" << i << ", " << j << "): " << A_Ainv(i, j) << std::endl;
        }
        
        std::cout << std::endl;
    }
    
    int pp; std::cin >> pp;
    // **************
    
   
    Matrix<double> Atmp(4, 4);
    Atmp(0, 0) = 1;
    Atmp(1, 0) = 5;
    Atmp(2, 0) = 7;
    Atmp(3, 0) = 0;
    
    Atmp(0, 1) = 2;
    Atmp(1, 1) = 8.3;
    Atmp(2, 1) = 4;
    Atmp(3, 1) = 8;
    
    Atmp(0, 2) = 0;
    Atmp(1, 2) = 7;
    Atmp(2, 2) = 9;
    Atmp(3, 2) = 1.5;
    
    Atmp(0, 3) = 5;
    Atmp(1, 3) = 2;
    Atmp(2, 3) = 3;
    Atmp(3, 3) = 5;
    
    Matrix<double> Atmpinv;
    //SVD_inverse(Atmp, Atmpinv);
    LU_decomposition(Atmp, Atmpinv);
    
    // **************
    std::cout << "Atmpinv.get_nrows(): " << Atmpinv.get_nrows() << std::endl;
    std::cout << "Atmpinv.get_ncols(): " << Atmpinv.get_ncols() << std::endl;
    
    for( int i = 0; i < Atmpinv.get_nrows(); ++i )
    {
        for( int j = 0; j < Atmpinv.get_ncols(); ++j )
        {
            std::cout << "Atmpinv(" << i << ", " << j << "): " << Atmpinv(i, j) << std::endl;
        }
        
        std::cout << std::endl;
    }
    
    int pp2; std::cin >> pp2;
    // **************
    
    
    Matrix<double> Atmp_Atmpinv;
    matrix_multiply(Atmp, Atmpinv, Atmp_Atmpinv);
    
    std::cout << "Atmp_Atmpinv.get_nrows(): " << Atmp_Atmpinv.get_nrows() << std::endl;
    std::cout << "Atmp_Atmpinv.get_ncols(): " << Atmp_Atmpinv.get_ncols() << std::endl;
    
    for( int i = 0; i < Atmp_Atmpinv.get_nrows(); ++i )
    {
        for( int j = 0; j < Atmp_Atmpinv.get_ncols(); ++j )
        {
            std::cout << "Atmp_Atmpinv(" << i << ", " << j << "): " << Atmp_Atmpinv(i, j) << std::endl;
        }
        
        std::cout << std::endl;
    }
    
    int pp4; std::cin >> pp4;
    // **************
    
}


// << JMM >> :: this routine needs checking
//========================================================================
//========================================================================
//
// NAME: void matrix_inverse(const Matrix<double> &A, Matrix<double> &Ainv)
// 
// DESC: Calculate the inverse (or approximate inverse) of the matrix A, using SVD
//
// NOTES:
//     ! See:
//          http://www.netlib.no/netlib/lapack/double/dgesvd.f
//
//========================================================================
//========================================================================
void SVD_inverse(const Matrix<double> &A, Matrix<double> &Ainv)
{
    if( A.get_nrows() != A.get_ncols() )
    {
        std::cout << "Error in SVD_inverse(): Cannot use SVD to calculate the inverse of a non-square matrix!" << std::endl;
        exit(0);
    }
    
    
    // CALCULATE U, S, S^-1, VT, & V ...
    Matrix<double> U, VT;
    std::vector<double> S;
    SVD(A, U, S, VT);
    
    Matrix<double> Sinv( A.get_nrows(), A.get_ncols() );
    for( unsigned int i = 0; i < A.get_nrows(); ++i )
    {
        Sinv(i, i) = 1.0/S[i];
    }

    Matrix<double> UT;
    matrix_transpose(U, UT);
    
    Matrix<double> V;
    matrix_transpose(VT, V);
    
    
    // CALCULATE Ainv = V*(S^-1)*UT ...
    Matrix<double> SinvUT;
    matrix_multiply(Sinv, UT, SinvUT);

    matrix_multiply(V, UT, Ainv);
}
