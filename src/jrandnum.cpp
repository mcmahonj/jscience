#include "jrandnum.hpp"

// STL
#include <algorithm>  // std::generate_n
#include <array>      // std::array
#include <functional> // std::ref
#include <numeric>    // std::iota
#include <random>     // std::mt19937
#include <utility>    // std::swap
#include <vector>     // std::vector


static std::mt19937 g_rng;
static bool g_rng_seeded = false;


//
// DESC: Create and seed a 32-bit Mersenne twister (psuedo-)random number generator.
//
std::mt19937 prng_Mersenne_twister()
{
    std::mt19937 _prng;

    seed_Mersenne_twister(
                          _prng
                          );

    return _prng;
}

//
// DESC: Seeds the Mersenne twister PRNG.
//
// TODO: NOTE: Should allow a real random number from random.org to be passed here
//
void seed_Mersenne_twister(
                           std::mt19937 &_prng
                           )
{
    const int RNG_NBURN = 99999;

    //*********************************************************

    // GENERATE A SEED SEQUENCE WITH ENOUGH RANOM DATA TO FILL THE std:mt19937 STATE
    std::array<int, std::mt19937::state_size> seed_data;
    std::random_device rd;
    std::generate_n( seed_data.data(), seed_data.size(), std::ref(rd) );
    std::seed_seq seq(std::begin(seed_data), std::end(seed_data));

    // SEED THE RNG
    _prng = std::mt19937(seq);

    // BURN A NUMBER OF VALUES
    std::normal_distribution<double> normal_dist(0.0, 1.0);
    for(int i = 0; i < RNG_NBURN; ++i)
    {
        normal_dist(_prng);
    }
}


//
// DESC: Produces a (pseudo) random number, distributed uniformly on the interval [a, b].
//
// NOTES:
//     ! These routines cannot be (easily or efficiently) templated, because they call different STL functions depending on type.
//
double rand_num_uniform_Mersenne_twister(double a, double b)
{
    if( !g_rng_seeded )
    {
        seed_Mersenne_twister(
                              g_rng
                              );

        g_rng_seeded = true;
    }

    std::uniform_real_distribution<> dis(a, b);

    return dis(g_rng);
}

int rand_num_uniform_Mersenne_twister(int a, int b)
{
    if( !g_rng_seeded )
    {
        seed_Mersenne_twister(
                              g_rng
                              );

        g_rng_seeded = true;
    }

    std::uniform_int_distribution<> dis(a, b);

    return dis(g_rng);
}


//
// DESC: Produces a (pseudo) random number, normally distributed.
//
double rand_num_normal_Mersenne_twister(double mean, double stddev)
{
    if( !g_rng_seeded )
    {
        seed_Mersenne_twister(
                              g_rng
                              );

        g_rng_seeded = true;
    }

    std::normal_distribution<double> normal_dist(mean, stddev);

    return normal_dist(g_rng);
}


//
// DESC: Generates n random uniqe ints between min and max according to the Knuth--Fisher--Yates algorithm, storing the results in ints.
//
// NOTES:
//     ! << JMM >> :: COULD PROBABLY BE USED IN A MORE GENERAL WAY TO SHUFLLE
//
void n_unique_rand_ints(int n, int min, int max, std::vector<int> &ints)
{
    ints.clear();

    std::vector<int> idx( (max - min + 1) );
    std::iota( idx.begin(), idx.end(), min );

    int maxr = idx.size() - 1;

    for( int i = 0; i < n; ++i )
    {
        int r = rand_num_uniform_Mersenne_twister( 0, maxr );

        std::swap( idx[r], idx[maxr] );

        --maxr;
    }

    for( int i = 0; i < n; ++i )
    {
        ints.push_back( idx[maxr+1 + i] );
    }
}
