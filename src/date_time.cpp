// STL
#include <chrono>   // std::chrono
#include <cstdlib>  // std::getenv
#include <ctime>    // std::time_t, std::tzset
#include <iostream> // std::cout, std::endl
#include <sstream>  // std::stringstream
#include <string>   // std::string

// this
#include "date.hpp"
#include "date_time.hpp"
#include "DateTime.hpp"
#include "Duration.hpp"


const DateTime DateTime_RD(0001, 01, 01, 00, 00, 00, DateTime::TIME_ZONE::UTC);
const DateTime DateTime_future(2112, 06, 17, 00, 00, 00, DateTime::TIME_ZONE::UTC);


//const int EST_shift = -432000; // UTC-05:00
//const int EDT_shift = -345600; // UTC-04:00


//========================================================================
//========================================================================
//
// NAME: DateTime current_DateTime(DateTime::TIME_ZONE TZ)
//
// DESC: Gets the current DateTime at a specified time zone.
//
// NOTES:
//     ! For a listing of time zones in the tz database, see:
//          http://en.wikipedia.org/wiki/List_of_tz_database_time_zones
//
//     << JMM >> :: In a way, this is "hacky" as, in a way, it simply is a wrapper to the gmtime and localtime routines, and should be updated -- not sure how efficient this is
//     << JMM >> :: furthermore, this is NOT thread safe
//     << JMM >> :: finally, I am not sure this is portable at all???
//
//========================================================================
//========================================================================
DateTime current_DateTime(DateTime::TIME_ZONE TZ)
{
    //auto sec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();

    std::time_t rawtime;
    std::time(&rawtime);
    struct tm *timeinfo;

    if( TZ == DateTime::TIME_ZONE::UTC )
    {
        timeinfo = std::gmtime( &rawtime );
    }
    else
    {
        std::string newTZ;

        switch(TZ)
        {
            case DateTime::TIME_ZONE::ET:
                newTZ = "EST5EDT";
                break;
            default:
                std::cout << "Error in is_DST_in_effect(): Time zone not recognized!" << std::endl;
                exit(0);
        }

        char* oldTZ = std::getenv("TZ");

#if defined(_WIN32)
        _putenv_s("TZ", newTZ.c_str());
#else
        setenv("TZ", newTZ.c_str(), 1);
#endif // defined

        tzset();

        timeinfo = std::localtime( &rawtime );

        if( oldTZ )
        {
#if defined(_WIN32)
            _putenv_s("TZ", oldTZ);
#else
            setenv("TZ", oldTZ, 1);
#endif // defined
        }
        else
        {
#if defined(_WIN32)
            _putenv_s("TZ", "");
#else
            unsetenv("TZ");
#endif // defined
        }
        tzset();
    }

    return DateTime( (timeinfo->tm_year + 1900), (timeinfo->tm_mon + 1), timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, TZ );
}


/*
//========================================================================
//========================================================================
//
// NAME: bool is_DST_in_effect(DateTime::TIME_ZONE TZ)
//
// DESC:
//
//========================================================================
//========================================================================
bool is_DST_in_effect(DateTime::TIME_ZONE TZ)
{
    if( TZ == DateTime::TIME_ZONE::UTC )
    {
        return false;
    }

    char* newTZ;

    switch(TZ)
    {
        case DateTime::TIME_ZONE::ET:
            newTZ = "EST5EDT";
            break;
        default:
            std::cout << "Error in is_DST_in_effect(): Time zone not recognized!" << std::endl;
            exit(0);
    }

    char* oldTZ = std::getenv("TZ");

    std::setenv("TZ", newTZ, 1);
    std::tzset();

    std::time_t rawtime;
    std::time(&rawtime);
    struct tm *timeinfo;
    timeinfo = std::localtime( &rawtime );

    std::setenv("TZ", oldTZ, 1);
    std::tzset();

    if( timeinfo.tm_isdst > 0 )
    {
        return true;
    }
    else
    {
        return false;
    }
}
*/


//========================================================================
//========================================================================
//
// NAME: bool is_same_day(const DateTime &dt1, const DateTime &dt2)
//
// DESC: Checks whether two DateTime structures correspond to the same day.
//
//========================================================================
//========================================================================
bool is_same_day(const DateTime &dt1, const DateTime &dt2)
{
    if( dt1.TZD != dt2.TZD )
    {
        std::cout << "Error in is_same_day(): Time zones are not the same, the question is thus ill-posed!" << std::endl;
        exit(0);
    }

    if( (dt1.YYYY == dt2.YYYY) && (dt1.MM == dt2.MM) && (dt1.DD == dt2.DD) )
    {
        return true;
    }
    else
    {
        return false;
    }
}


//========================================================================
//========================================================================
//
// NAME: bool is_leap_year( const unsigned int YYYY )
//
// DESC: Checks whether a year is a leap year.
//
// NOTES:
//     ! See: https://support.microsoft.com/kb/214019/EN-US
//
//========================================================================
//========================================================================
bool is_leap_year( const unsigned int YYYY )
{
    if( (YYYY % 4) == 0 )
    {
        if( (YYYY % 100) == 0 )
        {
            if( (YYYY % 400) == 0 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}


//========================================================================
//========================================================================
//
// NAME: unsigned int ndays_in_month( const unsigned int MM, const unsigned int YYYY )
//
// DESC: Returns the number of days in a month, in a given year.
//
//========================================================================
//========================================================================
unsigned int ndays_in_month( const unsigned int MM, const unsigned int YYYY )
{
    switch(MM)
    {
        case 1:
            return 31;
            break;
        case 2:
            if( is_leap_year(YYYY) )
            {
                return 29;
            }
            else
            {
                return 28;
            }
            break;
        case 3:
            return 31;
            break;
        case 4:
            return 30;
            break;
        case 5:
            return 31;
            break;
        case 6:
            return 30;
            break;
        case 7:
            return 31;
            break;
        case 8:
            return 31;
            break;
        case 9:
            return 30;
            break;
        case 10:
            return 31;
            break;
        case 11:
            return 30;
            break;
        case 12:
            return 31;
            break;
        default:
            break;
    }
}


//========================================================================
//========================================================================
//
// NAME: DateTime floor(const DateTime &dt, const Duration &dur)
//       DateTime ceiling(const DateTime &dt, const Duration &dur)
//
// DESC: "Rounds" a DateTime object up or down by Duration dur.
//
//========================================================================
//========================================================================
DateTime floor(const DateTime &dt, const Duration &dur)
{
    if( (dur.nY > 0) || (dur.nM > 0) || (dur.nW > 0) || (dur.nD > 0) )
    {
        std::cout << "Error in floor(): Cannot calculate for ( (dur.nY > 0) || (dur.nM > 0) || (dur.nW > 0) || (dur.nD > 0) ); cases are ambiguous!" << std::endl;
        exit(0);
    }

    if( dur.nh > 0 )
    {
        if( (24 % dur.nh) != 0 )
        {
            std::cout << "Error in floor(): Invalid interval: ( (24 % dur.nh) != 0 )!" << std::endl;
            exit(0);
        }

        return DateTime(dt.YYYY, dt.MM, dt.DD, (dt.hh - (dt.hh % dur.nh)), 0.0, 0.0, dt.TZD);
    }
    else if( dur.nm > 0 )
    {
        if( (60 % dur.nm) != 0 )
        {
            std::cout << "Error in floor(): Invalid interval: ( (60 % dur.nm) != 0 )!" << std::endl;
            exit(0);
        }

        return DateTime(dt.YYYY, dt.MM, dt.DD, dt.hh, (dt.mm - (dt.mm % dur.nm)), 0.0, dt.TZD);
    }
    else if( dur.ns > 0 )
    {
        if( (60 % dur.ns) != 0 )
        {
            std::cout << "Error in floor(): Invalid interval: ( (60 % dur.ns) != 0 )!" << std::endl;
            exit(0);
        }

        return DateTime(dt.YYYY, dt.MM, dt.DD, dt.hh, dt.mm, (dt.ss - (dt.ss % dur.ns)), dt.TZD);
    }

    std::cout << "Error in floor(): Conversion was not successful!" << std::endl;
    exit(0);
}


DateTime ceiling(const DateTime &dt, const Duration &dur)
{
    return ( floor(dt, dur) + dur );
}
