/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "jsvd.h"

//************************************************************************
//			CLASSES / STRUCTS
//************************************************************************

//************************************************************************
//			FULL CODE GLOBAL VARIABLES
//************************************************************************

//========================================================================
//========================================================================
// NAME: void svdcmp( double **a, int m, int n, double w[], double **v )
// DESC: Given a matrix a[1..m][1..n], this routine computes its singular value 
//     decomposition, A = U*W*V^T. The matrix U replaces a on output. The diagonal 
//     matrix of singular values W is output as a vector w[1..n]. The matrix V 
//     (not the transpose V^T) is output as v[1..n][1..n].
// INPUT:     
// OUTPUT:
// REQUIRED FUNCTIONS:
// NOTES: 
//     ! this can be found on pp. 67 -- 70 in "Num. Recip. in C"
//     ! this DOES NOT use the 0 offset arrays of c++, but rather the 1...n range
//     of c, due to the complexity of this routine
//     ! IMPORTANT: it is recommended that LAPACK SVD be used in leau of this subroutine -- it will be faster and numerically more stable
//========================================================================
//========================================================================
void svdcmp( double **a, int m, int n, double w[], double **v )
{
    int MAXITS = 99999;
    
    int l, nm;
    double anorm, c, f, g, h, s, scale, x, y, z, *rv1;
    rv1 = new double [n+1];
    
    // HOUSEHOLDER REDUCTION TO BI-DIAGONAL FORM
    g = scale = anorm = 0.0;
    
    for(int i = 1; i <= n; ++i) 
    {
        l = i + 1;
        
        rv1[i] = scale*g;
        
        g = s = scale = 0.0;
        
        if( i <= m ) 
        {
            for(int k = i; k <= m; ++k) { scale += fabs(a[k][i]); }
            
            if (scale) 
            {
                for(int k = i; k <= m; ++k) 
                {
                    a[k][i] /= scale;
                    s += (a[k][i]*a[k][i]);
                }
                
                f = a[i][i];
                g = -SIGN(sqrt(s), f);
                h = f*g - s;
                a[i][i] = f - g;
                
                for(int j = l; j <= n; ++j) 
                {
                    s = 0.0;
                    
                    for(int k = i; k <= m; ++k) { s += (a[k][i]*a[k][j]); }
                    
                    f = s/h;
                    
                    for(int k = i; k <= m; ++k) { a[k][j] += (f*a[k][i]); }
                }
                
                for(int k = i; k <= m; ++k) { a[k][i] *= scale; }
            }
        }
        
        w[i] = scale*g;
        g = s = scale = 0.0;
        
        if( (i <= m) && (i != n) ) 
        {
            for(int k = l; k <= n; ++k) { scale += fabs(a[i][k]); }
            
            if( scale )
            {
                for(int k = l; k <= n; ++k) 
                {
                    a[i][k] /= scale;
                    s += (a[i][k]*a[i][k]);
                }
                
                f = a[i][l];
                g = -SIGN(sqrt(s), f);
                h = f*g - s;
                a[i][l] = f - g;
                
                for(int k = l; k <= n; ++k) { rv1[k] = a[i][k]/h; }
                
                for(int j = l; j <= m; ++j) 
                {
                    s = 0.0;
                    for(int k = l; k <= n; ++k) { s += ( a[j][k]*a[i][k] ); }
                    
                    for(int k = l; k <= n; ++k) { a[j][k] += ( s*rv1[k] ); }
                }
                
                for(int k = l; k <= n; ++k) { a[i][k] *= scale; }
            }
        } // end if( (i <= m) && (i != n) )
        
        anorm = dmax( anorm, (fabs(w[i]) + fabs(rv1[i])) );
    } // ++i
    
    // ACCUMULATION OF RIGHT-HAND TRANSFORMATIONS
    for(int i = n; i >= 1; --i) 
    {
        if( i < n ) 
        {
            if( g ) 
            {
                // DOUBLE DIVISION TO AVOID POSSIBLE UNDERFLOW
                for(int j = l; j <= n; ++j) { v[j][i] = (a[i][j]/a[i][l])/g; }

                for(int j = l; j <= n; ++j) 
                {
                    s = 0.0;
                    for(int k = l; k <= n; ++k) { s += (a[i][k]*v[k][j]); }
                    
                    for(int k = l; k <= n; ++k) { v[k][j] += (s*v[k][i]); }
                }
            }
            
            for(int j = l; j <= n; ++j) { v[i][j] = v[j][i] = 0.0; }
        }
        
        v[i][i] = 1.0;
        g = rv1[i];
        l = i;
    } // ++i
    
    // ACCUMULATION OF LEFT-HAND TRANSFORMATIONS
    for(int i = imin(m, n); i >= 1; --i)
    {
        l = i + 1;
        g = w[i];
    
        for(int j = l; j <= n; ++j) { a[i][j] = 0.0; }
        
        if( g ) 
        {
            g = 1.0/g;
            
            for(int j = l; j <= n; ++j) 
            {
                s = 0.0;
                for(int k = l; k <= m; ++k) { s += (a[k][i]*a[k][j]); }
                
                f = (s/a[i][i])*g;
                
                for(int k = i; k <= m; ++k) { a[k][j] += (f*a[k][i]); }
            }
            
            for(int j = i; j <= m; ++j) { a[j][i] *= g; }
            
        } 
        else 
        {
            for(int j = i; j <= m; ++j) { a[j][i] = 0.0; }   
        }
        
        
        ++a[i][i];
    }
    
    // DIAGONALIZATION OF THE BI-DIAGONAL FORM: ...
    // ... LOOP OVER SINGULAR VALUES, AND OVER ALLOWED ITERATIONS
    for(int k = n; k >= 1; --k) 
    {
        for(int its = 1; its <= MAXITS; ++its) 
        {
            int flag = 1;
            
            // TEST FOR SPLITTING ...
            for(l = k; l >= 1; --l) 
            { 
                nm = l - 1;
                // ! note that rv1[1] is always zero.
                if( static_cast<double>( fabs(rv1[l]) + anorm ) == anorm ) 
                {
                    flag = 0;
                    break;
                }
                
                if( static_cast<double>( fabs(w[nm]) + anorm ) == anorm ) { break; }
            }
            
            if( flag ) 
            {
                // CANCELLATION OF rv1[l], IF l > 1
                c = 0.0; 
                s = 1.0;
                
                for(int i = l; i <= k; ++i) 
                {
                    f = s*rv1[i];
                    rv1[i] = c*rv1[i];
                    
                    if( static_cast<double>(fabs(f) + anorm) == anorm ) { break; }
                    
                    g = w[i];
                    h = pythag(f, g);
                    w[i] = h;
                    h = 1.0/h;
                    c = g*h;
                    s = -f*h;
                    
                    for(int j = 1; j <= m; ++j) 
                    {
                        y = a[j][nm];
                        z = a[j][i];
                        a[j][nm] = y*c + z*s;
                        a[j][i] = z*c - y*s;
                    }
                }
            } // end if( flag )
            
            z = w[k];
            
            // CONVERGENCE?
            if( l == k ) 
            { 
                // SINGULAR VALUE IS MADE NON-NEGATIVE
                if( z < 0.0 ) 
                {
                    w[k] = -z;
                    
                    for(int j = 1; j <= n; ++j) { v[j][k] = -v[j][k]; }
                }
                
                break;
            }
            
            if( its == MAXITS ) 
            {
                cout << "No convergence in " << MAXITS << " svdcmp iterations in svdcmp() in jsvd.cpp in JSCIENCE!" << endl;
                exit(2);
            }
                
            // SHIFT FROM BOTTOM 2-BY-2 MINOR
            x = w[l]; 
            nm = k - 1;
            y = w[nm];
            g = rv1[nm];
            h = rv1[k];
            f = ( (y - z)*(y + z) + (g - h)*(g + h) )/(2.0*h*y);
            g = pythag(f, 1.0);
            f = ( (x - z)*(x + z) + h*( (y/(f + SIGN(g, f))) - h) )/x; 
            
            // NEXT QR TRANSFORMATION:
            c = s = 1.0;            
            for(int j = l; j <= nm; ++j) 
            {
                int i = j + 1;
                g = rv1[i];
                y = w[i];
                h = s*g;
                g = c*g;
                z = pythag(f, h);
                rv1[j] = z;
                c = f/z;
                s = h/z;
                f = x*c + g*s;
                g = g*c - x*s;
                h = y*s;
                y *= c;
                
                for(int jj = 1; jj <= n; ++jj) 
                {
                    x = v[jj][j];
                    z = v[jj][i];
                    v[jj][j] = x*c + z*s;
                    v[jj][i] = z*c - x*s;
                }
                
                z = pythag(f, h);
                
                // ROTATION CAN BE ARBITRARY IF z = 0
                w[j] = z;
                
                if( z ) 
                {
                    z = 1.0/z;
                    c = f*z;
                    s = h*z;
                }
                
                f = c*g + s*y;
                x = c*y - s*g;
                
                for(int jj = 1; jj <= m; ++jj) 
                {
                    y = a[jj][j];
                    z = a[jj][i];
                    a[jj][j] = y*c + z*s;
                    a[jj][i] = z*c - y*s;
                }
            }
            
            rv1[l] = 0.0;
            rv1[k] = f;
            w[k] = x;
        }
    }
    
    delete [] rv1;
    
    return;
}


//========================================================================
//========================================================================
// NAME: void svbksb( double **u, double w[], double **v, int m, int n, 
//     double b[], double x[] )
// DESC: Solves A*X = B for a vector X, where A is speciﬁed by the arrays 
//     u[1..m][1..n], w[1..n], v[1..n][1..n] as returned by svdcmp(). m and n are 
//     the dimensions of a, and will be equal for square matrices. b[1..m] is the 
//     input right-hand side. x[1..n] is the output solution vector. No input 
//     quantities are destroyed, so the routine may be called sequentially with 
//     diﬀerent b’s.
// INPUT:     
// OUTPUT:
// REQUIRED FUNCTIONS:
// NOTES: 
//     ! this can be found on p. 64 in "Num. Recip. in C"
//     ! this DOES NOT use the 0 offset arrays of c++, but rather the 1...n range
//     of c, due to the complexity of svdcmp() above
//========================================================================
//========================================================================
void svbksb( double **u, double w[], double **v, int m, int n, double b[], double x[] )
{
    double s, *tmp;
    tmp = new double [n+1];

    // CALCULATE U^T*B
    for(int j = 1; j <= n; ++j) 
    {
        s = 0.0;
        // NONZERO RESULT ONLY IF wj IS NONZERO ...
        if( w[j] ) 
        {
            for(int i = 1; i <= m; ++i) { s += (u[i][j]*b[i]); }
            
            // THIS IS THE DIVIDE BY wj ...
            s /= w[j];
        }
        
        tmp[j] = s;
    }
    
    // MATRIX MULTIPLY BY V TO GET ANSWER
    for(int j = 1; j <= n; ++j) 
    {
        s = 0.0;
        
        for(int jj = 1; jj <= n; ++jj) { s += (v[j][jj]*tmp[jj]); }
        
        x[j] = s;
    }
    
    delete [] tmp;
    
    return;
}




//========================================================================
//========================================================================
// NAME: void svdfit( double x[], double y[], double sig[], int ndata, double a[], int ma, double **u, double **v, double w[], double *chisq, void (*funcs)(double, double [], int) )
// DESC: Given a set of data points x[1..ndata],y[1..ndata] with individual standard 
//     deviations sig[1..ndata], use chi^2 minimization to determine the coeﬃcients 
//     a[1..ma] of the ﬁtting function {y = sum_i[a_i*afunc_i(x)]}. Here we solve the 
//     ﬁtting equations using singular value decomposition of the ndata by ma matrix,
//     as in Section 2.6 of Num. Recip. in C. Arrays u[1..ndata][1..ma], 
//     v[1..ma][1..ma], and w[1..ma] provide workspace on input; on output they deﬁne
//     the singular value decomposition, and can be used to obtain the covariance 
//     matrix. The program returns values for the ma ﬁt parameters a, and chi^2, 
//     chisq. The user supplies a routine funcs(x,afunc,ma) that returns the ma basis 
//     functions evaluated at x = X in the array afunc[1..ma].
// INPUT:     
// OUTPUT:
// REQUIRED FUNCTIONS:
// NOTES:
//     ! IMPORTANT: The hardwired TOL here *can* cause trouble if your coefficients require a smaller precision
//     ! this can be found on p. 678 in Num. Recip. in C
//     ! this DOES NOT use the 0 offset arrays of c++, but rather the 1...n range
//     of C, due to the complexity of svdcmp() above
//========================================================================
//========================================================================
void svdfit( double x[], double y[], double sig[], int ndata, double a[], int ma, double **u, double **v, double w[], double *chisq, void (*funcs)(double, double [], int) )
{
    // TOL FOR svdfit() ON p. 678 IN "Num. Recip. in C"
    // ! default value for single precision and variables scaled to order unity
    // !!! JMM: should probably be updated for double precision
    const double TOL = 1.0e-20;
    
    //*********************************************************
    
    double wmax, tmp, thresh, sum, *b, *afunc;
    b = new double [ndata+1];
    afunc = new double [ma+1];
    
    // ACCUMULATE COEFFICIENTS OF THE FITTING MATRIX
    for(int i = 1; i <= ndata; ++i) 
    {
        (*funcs)( x[i], afunc, ma );
        tmp = 1.0/sig[i];
        
        for(int j = 1; j <= ma; ++j) { u[i][j] = afunc[j]*tmp; }
        
        b[i] = y[i]*tmp;
    }
    
    // SINGULAR VALUE DECOMPOSITION
    svdcmp(u, ndata, ma, w, v);
    
    // EDIT THE SINGULAR VALUES, GIVEN TOL FROM THE #define STATEMENT, 
    // BETWEEN HERE ....
    wmax = 0.0;
    
    for(int j = 1; j <= ma; ++j)
    {
        if( w[j] > wmax ) { wmax = w[j]; }
    }
    
    thresh = TOL*wmax;
    
    for(int j = 1; j <= ma; ++j)
    {
        if( w[j] < thresh ) { w[j] = 0.0; }
    }
    // ... AND HERE
    
    svbksb(u, w, v, ndata, ma, b, a);
    
    // EVALUATE CHI-SQUARE
    *chisq = 0.0;
    
    for(int i = 1; i <= ndata; ++i) 
    {
        (*funcs)( x[i], afunc, ma );
        
        sum = 0.0;
        for(int j = 1; j <= ma; ++j) { sum += (a[j]*afunc[j]); }
        
        *chisq += ( tmp = (y[i] - sum)/sig[i], tmp*tmp );
    }
    
    delete [] afunc;
    delete [] b;
    
    return;
}

//========================================================================
//========================================================================
// NAME: void svdvar( double **v, int ma, double w[], double **cvm )
// DESC: To evaluate the covariance matrix cvm[1..ma][1..ma] of the fit for ma
//     parameters obtained by svdfit, call this routine with matrices 
//     v[1..ma][1..ma], w[1..ma] as returned from svdfit.
// INPUT:     
// OUTPUT:
// REQUIRED FUNCTIONS:
// NOTES: 
//     ! this can be found on p. 679 in "Num. Recip. in C"
//     ! this DOES NOT use the 0 offset arrays of c++, but rather the 1...n range
//     of C, due to the complexity of svdcmp() above
//========================================================================
//========================================================================
void svdvar( double **v, int ma, double w[], double **cvm )
{
    double sum, *wti;
    wti = new double [ma+1];

    for(int i = 1; i <= ma; ++i) 
    {
        wti[i] = 0.0;
        
        if( w[i] ) { wti[i] = 1.0/(w[i]*w[i]); } 
    }
    
    // SUM CONTRIBUTIONS TO COVARIANCE MATRIX (15.4.20)
    for(int i = 1; i <= ma; ++i) 
    { 
        for(int j = 1; j <= i; ++j) 
        {
            sum = 0.0;
            
            for(int k = 1; k <= ma; ++k) { sum += (v[i][k]*v[j][k]*wti[k]); }
            
            cvm[j][i] = cvm[i][j] = sum; 
        }
    }
    
    delete [] wti;
    
    return;
}




//========================================================================
//========================================================================
// NAME: double pythag(double a, double b)
// DESC: Computes (a^2 + b^2)^1/2 without destructive underﬂow or overﬂow.
// INPUT:     
// OUTPUT:
// REQUIRED FUNCTIONS:
// NOTES: 
//     ! this can be found on pp. 70 in Num. Recip. in C
//     !!! JMM: this should be moved
//========================================================================
//========================================================================
double pythag(double a, double b)
{
    double absa, absb;
    absa = fabs(a);
    absb = fabs(b);
    if( absa > absb ) { return ( absa*sqrt(1.0 + square(absb/absa)) ); }
    else { return ( absb == 0.0 ? 0.0 : absb*sqrt(1.0 + square(absa/absb)) ); }
}
