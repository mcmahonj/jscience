/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 8/15/2013
// LAST UPDATE: 5/27/2013

#ifdef WCURL

#include "CJCurlM.hpp"

// STL
#include <iostream>


size_t writefunc(void *ptr, size_t size, size_t nmemb, std::string *stream);


CJCurlM::CJCurlM()
{
    content.clear();
}


CJCurlM::~CJCurlM() {};


//========================================================================
//========================================================================
//
// NAME: void CJCurlM::add_HTTP_request(std::string url)
//
// DESC: Adds a url for HTTP downloading.
//
//========================================================================
//========================================================================
void CJCurlM::add_HTTP_request(std::string url)
{
    HTTP_urls.push_back(url);
}


//========================================================================
//========================================================================
//
// NAME: void CJCurlM::update()
//
// DESC: Update the content vector by activating the transfers.
//
//========================================================================
//========================================================================
void CJCurlM::update()
{
    // CLEAR CONTENTS OF POSSIBLE PRIOR CALLS ...
    content.clear();
    content.resize(HTTP_urls.size());

    
    // INIT HANDLES ...
    CURL *handles[HTTP_urls.size()];
    CURLM *multi_handle;
    
    /* Allocate one CURL handle per transfer */
    for( decltype(HTTP_urls.size()) i = 0; i < HTTP_urls.size(); ++i )
    {
        handles[i] = curl_easy_init();
        
        curl_easy_setopt(handles[i], CURLOPT_URL,            HTTP_urls[i].c_str());
        curl_easy_setopt(handles[i], CURLOPT_WRITEFUNCTION,  writefunc);
        curl_easy_setopt(handles[i], CURLOPT_WRITEDATA,     &content[i]);
    }
    
    multi_handle = curl_multi_init();
    
    for( decltype(HTTP_urls.size()) i = 0; i < HTTP_urls.size(); ++i )
    {
        curl_multi_add_handle(multi_handle, handles[i]);
    }
    
    
    // PERFORM THE MULTI-TRANSFER ...
    int still_running; 
    
    curl_multi_perform(multi_handle, &still_running);
    
    do
    {
        struct timeval timeout;
        int rc; /* select() return code */
        
        fd_set fdread;
        fd_set fdwrite;
        fd_set fdexcep;
        int maxfd = -1;
        
        long curl_timeo = -1;
        
        FD_ZERO(&fdread);
        FD_ZERO(&fdwrite);
        FD_ZERO(&fdexcep);
        
        /* set a suitable timeout to play around with */
        timeout.tv_sec  = 30;
        timeout.tv_usec = 0;
        
        curl_multi_timeout(multi_handle, &curl_timeo);
        if(curl_timeo >= 0)
        {
            timeout.tv_sec = curl_timeo / 1000;
            if(timeout.tv_sec > 1)
            {
                timeout.tv_sec = 1;
            }
            else
            {
                timeout.tv_usec = (curl_timeo % 1000) * 1000;
            }
        }
        
        /* get file descriptors from the transfers */
        curl_multi_fdset(multi_handle, &fdread, &fdwrite, &fdexcep, &maxfd);
        
        /* In a real-world program you OF COURSE check the return code of the
         function calls.  On success, the value of maxfd is guaranteed to be
         greater or equal than -1.  We call select(maxfd + 1, ...), specially in
         case of (maxfd == -1), we call select(0, ...), which is basically equal
         to sleep. */
        
        rc = select(maxfd+1, &fdread, &fdwrite, &fdexcep, &timeout);
        
        switch(rc)
        {
            case -1:
                /* select error */
                break;
            case 0: /* timeout */
            default: /* action */
                curl_multi_perform(multi_handle, &still_running);
                break;
        }
    } while(still_running);
    
/*
    // SEE HOW THE TRANSFERS WENT ...
    
    CURLMsg *msg;
    int msgs_left;

    while ((msg = curl_multi_info_read(multi_handle, &msgs_left)))
    {
        if (msg->msg == CURLMSG_DONE)
        {
            int idx, found = 0;

            for( decltype(HTTP_urls.size()) idx = 0; idx < HTTP_urls.size(); ++idx )
            {
                found = (msg->easy_handle == handles[idx]);
                if(found)
                    break;
            }
            
            switch (idx) {
                case HTTP_HANDLE:
                    printf("HTTP transfer completed with status %d\n", msg->data.result);
                    break;
                case FTP_HANDLE:
                    printf("FTP transfer completed with status %d\n", msg->data.result);
                    break;
            
            }
        }
    }
*/    
    
    // CLEANUP ...
    curl_multi_cleanup(multi_handle);

    for( decltype(HTTP_urls.size()) i = 0; i < HTTP_urls.size(); ++i )
    {
        curl_easy_cleanup(handles[i]);
    }
}


//========================================================================
//========================================================================
//
// NAME: size_t writefunc(void *ptr, size_t size, size_t nmemb, std::string *stream)
//
// DESC: Write-function for a CURL handle to use to write content.
//
//========================================================================
//========================================================================
size_t writefunc(void *ptr, size_t size, size_t nmemb, std::string *stream)
{
    size_t data_size = size*nmemb;
    stream->append((char *)ptr,data_size);
    
    return data_size;
}


#endif