/*
 Copyright 2014-Present Algorithms in Motion LLC

 This file is part of jScience.

 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 8/11/2013
// LAST UPDATE: 10/9/2015

#include "jutility.hpp"

// STL
#include <algorithm>     // std::remove_if
#include <cmath>
#include <iostream>      // std::cout, std::endl
#include <numeric>

//

//========================================================================
//========================================================================
//
// NAME: int dyad_length(vector<double> x)
//
// DESC: Finds the dyadic length of a vector
//
// INPUT:
//          x          : vector of length 2^J (hopefully)
//
// OUTPUT:
//          J          : dyadic length of x -- least power of two greater than size
//
// NOTES:
//     !
//
//========================================================================
//========================================================================
int dyad_length(std::vector<double> x)
{
    int J = std::ceil( std::log(x.size())/std::log(2) );
    if( std::pow(2, J) != x.size() ) { std::cout << "Error in dyad_length(): n != 2^J!" << std::endl; }

    return J;
}


//========================================================================
//========================================================================
//
// NAME: void dyad_indices(int j, int & i1, int & i2)
//
// DESC: Fetches indices of jth-dyad of a vector (e.g., 1D wavelet)
//
// INPUT:
//          j    : dyad index
//
// OUTPUT:
//          i1   : start index
//          i2   : end index
//
// NOTES:
//     ! the dyadic vector should be stored according to (e.g., wavelet coefficients in Numerical Recipes in C++ -- see the notes in wt1 in jwavelets.cpp):
//          A0,D0,D1,...D_J-2,D_J-1
//
//========================================================================
//========================================================================
void dyad_indices(int j, int & i1, int & i2)
{
    i1 = std::pow(2, j);
    i2 = std::pow(2, (j+1)) - 1;
}


//========================================================================
//========================================================================
//
// NAME: int indx_map_3Dto1D(int i, int j, int k, int nj, int nk)
//
// DESC: ``Flatten'' a 3D vector of size [ni*nj*nk] with elements [i][j][k] to 1D. Note that ni is NOT needed for this.
//
// << JMM >> :: THIS  IS STORED POORLY FOR C++
//
//========================================================================
//========================================================================
int indx_map_3Dto1D(int i, int j, int k, int nj, int nk)
{
    return (i + nj*(j + nk*k));
}
