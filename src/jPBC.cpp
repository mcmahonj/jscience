/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "jPBC.h"


//========================================================================
//========================================================================
// NAME: double dist_min_img_orthorhombic(VECTOR_DOUBLE vec1, VECTOR_DOUBLE vec2)
// DESC: Calculates and returns the distance between two vectors using the
//          minimum image convention (for periodic boundary conditions) for
//          an orthorhombic box
// NOTES: 
//     ! for non-orthorhombic, we could just use projections along the lattice
//     vectors, but this will be very computationally demanding
//========================================================================
//========================================================================
double dist_MinImg_orthorhombic(const VECTOR_DOUBLE & vec1, const VECTOR_DOUBLE & vec2, const VECTOR_DOUBLE Rvec[3])
{
    double dx, dy, dz;
    
    dx = vec1.x - vec2.x;
    if( dx > Rvec[0].x/2.0 ) { dx -= Rvec[0].x; }
    if( dx < -Rvec[0].x/2.0 ) { dx += Rvec[0].x; }    
    
    dy = vec1.y - vec2.y;
    if( dy > Rvec[1].y/2.0 ) { dy -= Rvec[1].y; }
    if( dy < -Rvec[1].y/2.0 ) { dy += Rvec[1].y; }
    
    dz = vec1.z - vec2.z;
    if( dz > Rvec[2].z/2.0 ) { dz -= Rvec[2].z; }
    if( dz < -Rvec[2].z/2.0 ) { dz += Rvec[2].z; }
    
    // *****
    /*
    // !!! JMM this looks conceptually simpler (but does not seem any computationally less demanding) when using orthorhombic lattice vectors of unit length
    double RXIJ = qr[i].x - qr[j].x;
    double RYIJ = qr[i].y - qr[j].y;
    double RZIJ = qr[i].z - qr[j].z;
    
    RXIJ = RXIJ - static_cast<double>( round(RXIJ) );
    RYIJ = RYIJ - static_cast<double>( round(RYIJ) );
    RZIJ = RZIJ - static_cast<double>( round(RZIJ) ); 
    */
    // *****
    
    return sqrt( dx*dx + dy*dy + dz*dz );
}



