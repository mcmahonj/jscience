
#if defined(WLAPACKLINUX) || defined(WLAPACKMAC)

// STL
#include <vector> // std::vector

// LAPACK/BLAS
#if defined(WLAPACKLINUX)
#include <lapacke/lapacke.h>
#elif defined(WLAPACKLINUX2)
#include <lapacke.h>
#elif defined(WLAPACKMAC)
#include <Accelerate/Accelerate.h>
#endif

//#include <Accelerate/Accelerate.h>
//#include <lapacke/lapacke.h>

// THIS
#include "Matrix.hpp" // Matrix


//========================================================================
//========================================================================
//
// NAME: void realSymmetric_to_tridiagonal(std::vector<std::vector<double>> A)
//
// DESC: Reduces a real symmetric matrix A to a real symmetric tridiagonal form T. On return, D contains the N diagonal elements of T, D(i) = A(i,i), and E contains the (N-1) off-diagonal elements, E(i) = A(i,i+1).
//
// NOTES:
//     ! A MUST be a real symmetric matrix on input -- a verification is NOT made.
//     ! A can be passed as a complete matrix or just in upper-triangle form, but NOT lower-triangle form.
//
//========================================================================
//========================================================================
void realSymmetric_to_tridiagonal(const Matrix<double> &A, std::vector<double> &D, std::vector<double> &E)
{
    // SET THAT THE UPPER TRIANGLE OF A IS STORED
    char UPLO = 'U';
     
    // ORDER (No. COLUMNS) AND LEADING DIMENSION (No. ROWS) OF A
    int N   = A.get_ncols();
    int LDA = A.get_nrows();

    // STORE A *IN COLUMN-MAJOR ORDER*
    std::vector<double> A_CMO;
    Matrix_to_ColMajOrd(A, A_CMO);

    // DIAGNOAL (D) AND OFF-DIAGONAL (E) ELEMENTS OF TRIDIAGONAL MATRIX
    D.resize(N);
    E.resize(N-1);
    
    // SCALAR FACTORS OF THE ELEMENTARY REFLECTORS
    std::vector<double> TAU(N-1);
    
    // << JMM >> :: A CALL TO ILAENV MAY BE BEST HERE TO GET THE OPTIMAL BLOCKSIZE
    // WORK ARRAY, WITH DIMENSION
    int NB = 32;
    int LWORK = N*NB;
    std::vector<double> WORK( std::max(1, LWORK) );
    
    // INFO RE. SUCCESSFUL EXIT
    int INFO;
    
    // CALL DGEEV
    dsytrd_( &UPLO, &N, &*A_CMO.begin(), &LDA, &*D.begin(), &*E.begin(), &*TAU.begin(), &*WORK.begin(), &LWORK, &INFO );
    
    if( INFO != 0 )
    {
        std::cout << "Error in get_eigenvalues_and_eigenvectors(): INFO did not return as 0! INFO = " << INFO << "." << std::endl;
        exit(0);
    }
}


#endif
