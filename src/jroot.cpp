/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 9/16/2013
// LAST UPDATE: 9/17/2013

#include "jroot.hpp"


// STL
#include <cmath>
#include <iostream>


//========================================================================
//========================================================================
//
// NAME: int zbrac( std::function<double(double)>func, double &x1, double &x2)
//
// DESC: Given a function func and an initial guessed range x1 to x2, this routine expands the range geometrically until a root is bracketed by the returned values x1 and x2 (in which case, zbrac returns 1) or until the range becomes unacceptably large (in which case, zbrac returns 0). Success is guaranteed for a function which has opposite signs for sufficiently large and small arguments.
//
// NOTES:
//     ! see p. 260 in Numerical Recipes in C
//
//========================================================================
//========================================================================
int zbrac( std::function<double(double)>func, double &x1, double &x2)
{
    const double FACTOR = 1.6;
    const int    NTRY   = 50;
    
    //*********************************************************
    
    if( x1 == x2 )
    {
        std::cout << "Error in zbrac(): Bad initial range, x1 == x2!" << std::endl;
        exit(0);
    }
    
    double f1, f2;
    
    f1 = func(x1);
    f2 = func(x2);
    
    for( int j = 0; j < NTRY; ++j )
    {
        if( (f1*f2) < 0.0 )
        {
            return 1;
        }
        
        if( std::fabs(f1) < std::fabs(f2) )
        {
            f1 = func( x1 += FACTOR*(x1 - x2) );
        }
        else
        {
            f2 = func( x2 += FACTOR*(x2 - x1) );
        }
    }
    
    return 0;
}


//========================================================================
//========================================================================
//
// NAME: void zbrak( std::function<double(double)>func, double x1, double x2, int n, std::vector<double> &xb1, std::vector<double> &xb2, int &nb)
//
// DESC: Given a function func defined on the interval x1 to x2, subdivide the interval into n equally spaced segments and search for zero crossings of the function. nb is input as the maximum number of roots sought, and is reset to the number of bracketing pairs xb1[0...nb-1], xb2[0...nb-1] that are found.
//
// NOTES:
//     ! see p. 261 in Numerical Recipes in C
//
//========================================================================
//========================================================================
void zbrak( std::function<double(double)>func, double x1, double x2, int n, std::vector<double> &xb1, std::vector<double> &xb2, int &nb)
{
    xb1.clear();
    xb2.clear();
    
    int nbb = nb;
    nb      = 0;
    
    double x, dx, fp;
    dx = (x2 - x1)/static_cast<double>(n);
    fp = func( x = x1 );
    
    // LOOP OVER ALL INTERVALS ...
    for(int i = 0; i < n; ++i )
    {
        double fc = func( x += dx );
        
        // IF A SIGN CHANGE OCCURS, THEN RECORD VALUES FOR THE BOUNDS ...
        if( (fc*fp) < 0.0 )
        {
            ++nb;
            xb1.push_back( x - dx );
            xb2.push_back( x );
        }
        
        if( nbb == nb ) { return; }
        
        fp = fc;
    }
}


//========================================================================
//========================================================================
//
// NAME: double rtsafe( std::function<void(double, double &, double &)>funcd, double x1, double x2, double xacc )
//
// DESC: Using a combination of Newton--Raphson and bisection, find the root of a function bracketed between x1 and x2. The root (value returned) will be refined until its accuracy is known within +/-xacc. funcd is a user-supplied routine that provides both the function value and the first derivative of the function.
//
// NOTES:
//     ! see pp. 273 -- 274 in Numerical Recipes in C
//
//========================================================================
//========================================================================
double rtsafe( std::function<void(double, double &, double &)>funcd, double x1, double x2, double xacc )
{
    const int MAXIT = 100; // MAXIMUM ALLOWED NUMBER OF ITERATIONS
    
    //*********************************************************
    
    double rts;
    
    double f, fl, fh, df;
    double xl, xh, dx, dxold;
    
    funcd(x1, fl, df);
    funcd(x2, fh, df);
    
    if( (fl*fh) >= 0.0 )
    {
        std::cout << "Error in rtsafe(): Root MUST be bracketed!" << std::endl;
        exit(0);
    }
    
    // ORIENT THE SEARCH SO THAT f(x1) < 0
    if( fl < 0.0 )
    {
        xl = x1;
        xh = x2;
    }
    else
    {
        xh = x1;
        xl = x2;
        
        double swap = fl;
        fl = fh;
        fh = swap;
    }
    
    // INITIALIZE THE GUESS FOR ROOT ...
    rts = 0.5*(x1 + x2);
    // ... THE ``STEP-SIZE BEFORE LAST'' ...
    dxold = std::fabs(x2 - x1);
    // ... AND THE LAST STEP ...
    dx = dxold;
    
    funcd(rts, f, df);
    
    // LOOP OVER ALLOWED ITERATIONS ...
    for( int j = 0; j < MAXIT; ++j )
    {
        // BISECT IF NEWTON OUT OF RANGE, OR NOT DECRCEASING FAST ENOUGH ...
        if( (((rts - xh)*df - f)*((rts - xl)*df - f) >= 0.0)
           || (std::fabs(2.0*f) > std::fabs(dxold*df)) )
        {
            dxold = dx;
            dx = 0.5*(xh - xl);
            rts = xl + dx;
            
            // IF CHANGE IN ROOT IS NEGLIGABLE ...
            if( xl == rts )
            {
                return rts;
            }
        }
        // ... ELSE NEWTON STEP ACCEPTABLE, TAKE IT ...
        else
        {
            dxold = dx;
            dx = f/df;
            double temp = rts;
            rts -= dx;
            
            if( temp == rts )
            {
                return rts;
            }
        }
        
        // CONVERGENCE CRITERION
        if( std::fabs(dx) < xacc )
        {
            return rts;
        }

        // THE ONE NEW FUNCTION EVALUATION PER ITERATION ...
        funcd(rts, f, df);
        
        // MAINTAIN THE BRACKET ON THE ROOT ...
        if( f < 0.0 )
        {
            xl = rts;
            fl = f;
        }
        else
        {
            xh = rts;
            fh = f;
        }
    }
    
    std::cout << "Error in rtsafe(): Maximum number of iterations exceeded!" << std::endl;
    exit(0);
}


