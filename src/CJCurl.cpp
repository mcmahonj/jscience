/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <7/22/2013
// LAST UPDATE:  5/27/2014

#ifdef WCURL

#include "CJCurl.hpp"

//========================================================================
//========================================================================
//
// NAME: CURLcode CJCurl::Fetch(string url)
//
// DESC: Fetches data from a url (headers, content, etc.)
//
// NOTES:
//     !
//
//========================================================================
//========================================================================
CURLcode CJCurl::Fetch(string url)
{
    // CLEAR CONTENTS OF POSSIBLE PRIOR Fetch() CALLS
    mHttpStatus = 0;
    mHeaders.clear();    
    mContent.clear();
    
    // SETUP CALLBACKS FOR PROCESSING CONTENT
    curl_easy_setopt(pCurlHandle, CURLOPT_WRITEFUNCTION, HttpContent);
    curl_easy_setopt(pCurlHandle, CURLOPT_HEADERFUNCTION, HttpHeader);
    curl_easy_setopt(pCurlHandle, CURLOPT_WRITEDATA, this);
    curl_easy_setopt(pCurlHandle, CURLOPT_WRITEHEADER, this);
    
    // SET THE URL
    curl_easy_setopt(pCurlHandle, CURLOPT_URL, url.c_str());
    
    // GET CONTENT
    CURLcode curlErr = curl_easy_perform(pCurlHandle);
    if (curlErr == CURLE_OK){
        
        // assuming everything is ok, get the content type and status code
        char* content_type = NULL;
        if ((curl_easy_getinfo(pCurlHandle, CURLINFO_CONTENT_TYPE, &content_type)) == CURLE_OK)
            mType = string(content_type);
        
        unsigned int http_code = 0;
        if((curl_easy_getinfo (pCurlHandle, CURLINFO_RESPONSE_CODE,&http_code)) == CURLE_OK)
            mHttpStatus = http_code;
        
    }
    return curlErr;
}


//========================================================================
//========================================================================
//
// NAME: size_t CJCurl::HttpHeader(void * ptr, size_t size, size_t nmemb, void* stream) and size_t CJCurl::HttpContent(void * ptr, size_t size, size_t nmemb, void* stream)
//
// DESC: Headers a
//
// NOTES:
//     !
//
//========================================================================
//========================================================================
size_t CJCurl::HttpHeader(void * ptr, size_t size, size_t nmemb, void* stream)
{
    CJCurl* handle = (CJCurl*)stream;
	size_t data_size = size*nmemb;
    if (handle != NULL){
        std::string header_line((char *)ptr,data_size);
        handle->mHeaders.push_back(header_line);
    }
    
    return data_size;
}

size_t CJCurl::HttpContent(void * ptr, size_t size, size_t nmemb, void* stream)
{
	CJCurl* handle = (CJCurl*)stream;
	size_t data_size = size*nmemb;
    if (handle != NULL){
        handle->mContent.append((char *)ptr,data_size);
    }
    
	return data_size;
}


#endif