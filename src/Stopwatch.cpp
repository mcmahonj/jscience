/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/27/2013
// LAST UPDATE: 12/29/2013

#include "Stopwatch.hpp"


// STL
#include <chrono>   // std::chrono
#include <iostream> // std::cout, std::endl
#include <thread>   // std::this_thread::sleep_for


Stopwatch::Stopwatch()
{
    running = false;
}

Stopwatch::~Stopwatch()
{
    if( running )
    {
        stop();
    }
}


//========================================================================
//========================================================================
//
// NAME: void Stopwatch::start() 
//
// DESC: Starts the stopwatch.
//
//========================================================================
//========================================================================
void Stopwatch::start() 
{
    if( running )
    {
        std::cout << "Warning in StopWatch::start(): Stopwatch is already running! Stopwatch will restart." << std::endl;
    }
    
    tstart = std::chrono::high_resolution_clock::now();

    running = true;
}


//========================================================================
//========================================================================
//
// NAME: std::chrono::nanoseconds Stopwatch::count()
//
// DESC: Returns the count (in nanoseconds) of the stopwatch.
//
//========================================================================
//========================================================================
std::chrono::nanoseconds Stopwatch::count()
{
    std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
    
    if( !running )
    {
        std::cout << "Warning in StopWatch::count(): Stopwatch is not running! 0 will be returned." << std::endl;
        
        tstart = now;
    }
    
    return std::chrono::nanoseconds( std::chrono::duration_cast<std::chrono::nanoseconds>(now - tstart) ) ;
}


//========================================================================
//========================================================================
//
// NAME: void Stopwatch::stop()
//
// DESC: Stops the stopwatch.
//
//========================================================================
//========================================================================
void Stopwatch::stop()
{
    if( !running )
    {
        std::cout << "Warning in StopWatch::stop(): Stopwatch is not running!" << std::endl;
    }
    
    running = false;
}


//========================================================================
//========================================================================
//
// NAME: void Stopwatch::wait(std::chrono::nanoseconds dura)
//
// DESC: Causes the calling thread to wait for a specified time.
//
//========================================================================
//========================================================================
void Stopwatch::wait(std::chrono::nanoseconds dura)
{
    std::this_thread::sleep_for(dura);
}


