/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 11/16/2013
// LAST UPDATE: 3/12/2015


// STL
#include <vector>

// BOOST
#include <boost/math/special_functions/gamma.hpp>

// JSCIENCE
#include "jstats.hpp"


//========================================================================
//========================================================================
//
// NAME: void fit(const std::vector<double> &x, const std::vector<double> &y, const std::vector<double> &sig, bool mwt, double &a, double &b, double &siga, double &sigb, double &chi2, double &q)
//
// DESC: Given a set of points x[0,...,n-1], y[0,...,n-1] with standard deviations sig[0,...,n-1], fit them to a straight line y = a + bx by minimizing chi^2. Returned are a,b and their respective probably uncertainties siga and sigb, the chi-square chi^2, and the goodness-of-fit probability q (that the fit would have chi^2 this large or larger). If mwt=false on input, then the standard deviations are assumed to be unavailable: q is returned as 1.0 and the normalization of chi^2 is to unit standard deviation on all points.
//
// NOTES:
//     ! see:
//          pp. 523 -- 528 in Numerical Recipes in C
//
//========================================================================
//========================================================================
void fit(const std::vector<double> &x, const std::vector<double> &y, const std::vector<double> &sig, bool mwt, double &a, double &b, double &siga, double &sigb, double &chi2, double &q)
{
    double sx  = 0.0;
    double sy  = 0.0;
    double st2 = 0.0;
    double ss  = 0.0;
    
    // ACCUMULATE SUMS WITH WEIGHTS ...
    if( mwt )
    {
        for( decltype(x.size()) i = 0; i < x.size(); ++i )
        {
            double wt = 1.0/(sig[i]*sig[i]);
            ss += wt;
            sx += x[i]*wt;
            sy += y[i]*wt;
        }
    }
    // ... OR WITHOUT ...
    else
    {
        for( decltype(x.size()) i = 0; i < x.size(); ++i )
        {
            sx += x[i];
            sy += y[i];
        }
        
        ss = static_cast<double>(x.size());
    }
    
    double sxoss = sx/ss;
    
    b = 0.0;
    
    if( mwt )
    {
        for( decltype(x.size()) i = 0; i < x.size(); ++i )
        {
            double t = (x[i] - sxoss)/sig[i];
            st2 += t*t;
            b   += t*y[i]/sig[i];
        }
    }
    else
    {
        for( decltype(x.size()) i = 0; i < x.size(); ++i )
        {
            double t = x[i] - sxoss;
            st2 += t*t;
            b   += t*y[i];
        }
    }
    
    
    // SOLVE FOR a, b, siga, AND sigb ...
    b /= st2;
    a = (sy - sx*b)/ss;
    
    siga = sqrt( (1.0 + sx*sx/(ss*st2))/ss );
    sigb = sqrt( 1.0/st2 );
    
    
    // CALCULATE chi^2 ...
    chi2 = 0.0;
    if( mwt )
    {
        for( decltype(x.size()) i = 0; i < x.size(); ++i )
        {
            chi2 += (y[i] - a - b*x[i])*(y[i] - a - b*x[i]);
        }
        
        q = 1.0;
        
        // FOR UNWEIGHTED DATA, EVALUATE TYPICAL sig USING chi^2, AND ADJUST THE STANDARD DEVIATIONS ...
        double sigdat = sqrt( chi2/static_cast<double>(x.size() - 2) );
        siga *= sigdat;
        sigb *= sigdat;
    }
    else
    {
        for( decltype(x.size()) i = 0; i < x.size(); ++i )
        {
            chi2 += ((y[i] - a - b*x[i])/sig[i])*((y[i] - a - b*x[i])/sig[i]);
        }
        
        q = boost::math::gamma_q( (0.5*static_cast<double>(x.size()- 2)), (0.5*chi2) );
    }
}
