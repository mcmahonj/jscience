/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <=4/19/2008
// LAST UPDATE:   6/18/2014

#include "jmath_basic.h"

// JSCIENCE
#include "jScience/physics/consts.hpp"


//========================================================================
//========================================================================
//
//	NAME:	double dmin(double d1, double d2)
//	DESC:	Returns the minimum value of two real numbers
//
//	INPUT:
//		double d1:: real number 1
//		double d2:: real number 2
//	
//	OUTPUT:
//		double d1, d2:: the minimum value of two real numbers
//		
//	NOTES:
//
//========================================================================
//========================================================================
double dmin(double d1, double d2)
{

  if(d1 < d2)
  {
    return d1;
  } 
  else
  {
    return d2;
  }

}


//========================================================================
//========================================================================
//
//	NAME:	double dmax(double d1, double d2)
//	DESC:	Returns the maximum value of two real numbers
//
//	INPUT:
//		double d1:: real number 1
//		double d2:: real number 2
//	
//	OUTPUT:
//		double d1, d2:: the maximum value of two real numbers
//		
//	NOTES:
//
//========================================================================
//========================================================================
double dmax(double d1, double d2)
{

  if(d1 > d2)
  {
    return d1;
  } 
  else
  {
    return d2;
  }

}


//========================================================================
//========================================================================
//
//	NAME:	int imin(int i1, int i2)
//	DESC:	Returns the minimum value of two integers
//
//	INPUT:
//		int i1:: integer 1
//		int i2:: integer 2
//	
//	OUTPUT:
//		int i1, i2:: the minimum value of two integers
//		
//	NOTES:
//
//========================================================================
//========================================================================
int imin(int i1, int i2)
{

  if(i1 < i2)
  {
    return i1;
  }
  else
  {
    return i2;
  }

}



//========================================================================
//========================================================================
//
//	NAME:	int imax(int i1, int i2)
//	DESC:	Returns the maximum value of two integers
//
//	INPUT:
//		int i1:: integer 1
//		int i2:: integer 2
//	
//	OUTPUT:
//		int i1, i2:: the maximum value of two integers
//
//	NOTES:
//
//========================================================================
//========================================================================
int imax(int i1, int i2)
{

  if(i1 > i2)
  {
    return i1;
  }
  else
  {
    return i2;
  }

}


//========================================================================
//========================================================================
// NAME:	double dround(double num) 
// DESC:	Rounds the number num to the nearest integer
// NOTES:
//     ! in more recent c++ releases, there appears to be a built in round()
//     function, but I don't know how fully supported it is, nor do I know
//     if it ALWAYS returns an int
//     ! because of the above issues, I will use my own routine which also
//     just returns double
//========================================================================
//========================================================================
double dround(double num) 
{
    return (num > 0.0) ? floor(num + 0.5) : ceil(num - 0.5);
}

//========================================================================
//========================================================================
//
//	NAME:	int ifactorial(int n)
//	DESC:	Calculates the factorial of n and returns the value as an 
//		integer
//
//	INPUT:
//		int n == integer
//
//	OUTPUT:
//		int factorial == n!
//
//	NOTES:	1) This will only works to a certain max n (max int)
//
//
//========================================================================
//========================================================================
int ifactorial(int n)
{
  return static_cast<int>(dfactorial(n));
}


//========================================================================
//========================================================================
//
//	NAME:	double dfactorial(int n)
//	DESC:	Calculates the factorial of n and returns the value as a 
//		real number
//
//	INPUT:
//		int n == integer
//
//	OUTPUT:
//		double factorial == double(n!)
//
//	NOTES:	1) !!!
//		2) There is no support is factorial > double
//
//
//========================================================================
//========================================================================
double dfactorial(int n)
{

  double factorial = 1.0;

  if(n < 2)
  {
    factorial = 1.0;
  }
  else if(n > 30)
  {
    // !!!
    double dn = static_cast<double>(n);
    factorial = sqrt(2.0*PI)*pow(dn, n)*sqrt(dn)*exp(-dn);
  }
  else
  {
    for(int i = n; i >= 2; --i)
    {
      factorial = factorial*static_cast<double>(i);
    }
  }


  return factorial;
}


//========================================================================
//========================================================================
//
//	NAME:	double dtwofac(int l)
//	DESC:	Calculates the double factorial (2l-1)!!=1*3*5*...*(2l-1) for
//		argument l, which can be defined as:
//
//			(2l-1)!!=(2l)!/((2^n)*l!)
//
//	NOTES: 	i. !!! there is a better way to define this with the gamma
//		function
//
//
//========================================================================
//========================================================================
double dtwofac(int n)
{
  return (  dfactorial(2*n)/(   pow(2.0, n)*dfactorial(n)  )  );
}

//========================================================================
//========================================================================
//
//	NAME: bool is_pow_of_two(unsigned long x)
//
//	DESC: Determines whether if a number is a power of 2 (i.e., of dyadic length)
//
//	NOTES:
//     !
//
//========================================================================
//========================================================================
bool is_pow_of_two(unsigned long x)
{
    return( (x != 0) && ((x & (x - 1)) == 0) );
}


//========================================================================
//========================================================================
//
// NAME: double square(double x)
//
// DESC: Returns the square of an argument x
//
// NOTES:
//     ! 
//
//========================================================================
//========================================================================
double square(double x) { return x*x; }

