/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : ~2006
// LAST UPDATE:  9/17/2013


// STL
#include <complex>

// JSCIENCE
#include "jScience/physics/consts.hpp"


//========================================================================
//========================================================================
//
//	NAME:	void getEinc(VECTOR_DOUBLE pos, VECTOR_CD &Einc)
//	DESC:	Get the initial field (Einc) at the point (pos)
//
//	NOTES:	i. the definition of the complex inverse sin is simple:
//                   arcsin(z) = -i*ln*(i*z + sqrt(1 - z^2))
//
//========================================================================
//========================================================================
std::complex<double> complex_asin( std::complex<double> z )
{

  return ( -COMPLEXJ*log( COMPLEXJ*z + sqrt( 1.0 - z*z ) ) );
}

