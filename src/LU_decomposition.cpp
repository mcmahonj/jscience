/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/27/2014
// LAST UPDATE: 5/27/2014

// STL
#include <iostream> // std::cout, std::endl
#include <vector>   // std::vector

#ifdef WLAPACK
// LAPACK/BLAS
#include <Accelerate/Accelerate.h>
#endif

// jScience
#include "Matrix.hpp"


//========================================================================
//========================================================================
//
// NAME: void matrix_inverse(const Matrix<double> &A, Matrix<double> &Ainv)
//
// DESC: Calculate the inverse of the matrix A, using LU decomposition.
//
// NOTES:
//     ! See:
//          http://www.netlib.org/lapack/double/dgetrf.f
//          http://www.netlib.org/lapack/double/dgetri.f
//
//========================================================================
//========================================================================
void LU_decomposition(const Matrix<double> &A, Matrix<double> &Ainv)
{
#ifdef LAPACK
    
    int M = A.get_nrows();
    int N = A.get_ncols();
    
    int LDA = M;
    
    std::vector<double> a;
    Matrix_to_ColMajOrd(A, a);
    
    int INFO;
    
    // FIRST CALL DGETRF ...
    std::vector<int> IPIV( std::min(M, N) );
    
    dgetrf_( &M, &N, &*a.begin(), &LDA, &*IPIV.begin(), &INFO );
    
    // check that return was normal ...
    if( INFO != 0 )
    {
        std::cout << "Error in LU_decomposition():INFO != 0, as returned by DGETRF()!" << std::endl;
        exit(0);
    }
    
    // THEN CALL DGETRI ...
    int LWORK = N*64; // a generous block size of 64 recommended at: http://www.netlib.org/lapack/lug/node120.html
    std::vector<double> WORK( std::max(1,LWORK) );
    
    dgetri_( &N, &*a.begin(), &LDA, &*IPIV.begin(), &*WORK.begin(), &LWORK, &INFO );
    
    // check that return was normal ...
    if( INFO != 0 )
    {
        std::cout << "Error in LU_decomposition():INFO != 0, as returned by DGETRI()!" << std::endl;
        exit(0);
    }
    
    // CONVERT THE COLUM-MAJOR ORDER MATRIX (A INVERSE) BACK TO MATRIX FORM ...
    ColMajOrd_to_Matrix(a, N, Ainv);
    
#else
    
    // << JMM:  Call my own LU (prob NR) here >>
    std::cout << "Temporary error in LU_decomposition(): LAPACK LU() called, but jScience compiled without LAPACK support!" << std::endl;
    exit(0);
    
#endif
}
