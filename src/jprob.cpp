/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : ~2011
// LAST UPDATE:  9/17/2013

#include "jprob.h"

// STL
#include <iostream>


//========================================================================
//========================================================================
// NAME: void get_combin_norep(int S[], int n, int k, int & ncombin, int ** combin)
// DESC: Recursively enumerate the combination of k elements of the set S 
//     with n elements
// NOTES:
//     ! the number of combinations is equal to the binomial coefficient:
//          (n)   (n-1)...(n-k+1)
//          | | = ---------------
//          (k)     k(k-1)...1
//     ! however, the number of non-repeating configurations is less
//     ! the call k runs from 1...n and does not consider c++ offsets, but the 
//     coding (array indices, etc.) do use c++ offsets
//     !!! JMM: this routine DOES NOT consider that multiple values of S could
//     be identical
//========================================================================
//========================================================================
void get_combin_norep(int S[], int n, int k, int & ncombin, int ** combin)
{
    int MAXCOMBIN = 9999;
    
    // ! k=1 is the set S itself ...
    if( k == 1 )
    {
        ncombin = n;
        
        for(int i = 0; i < n; ++i) { combin[i][0] = S[i]; }
    }
    // ! if k>1, get all combinations of order (k-1) and combine with element i
    else if ( k > 1 )
    {
        // ALLOCATE SOME TEMPORARY MEMORY 
        int ncombin_kminus1;
        int **combin_kminus1;
        combin_kminus1 = new int * [MAXCOMBIN];
        for(int i = 0; i < MAXCOMBIN; ++i) { combin_kminus1[i] = new int [k-1]; }
        int *combin_tmp;
        combin_tmp = new int [k];
        
        // GET COMBINATIONS OF 1 LESSER ORDER
        get_combin_norep( S, n, (k-1), ncombin_kminus1, combin_kminus1 );
        
        // FOR EACH ELEMENT i IN SET S, ADD UNIQUE (k-1) COMBINATIONS WITH i
        ncombin = 0;
        for(int i = 0; i < n; ++i)
        {
            // ATTEMPT TO COMBINE EACH (k-1) COMBINATION WITH i
            for(int j = 0; j < ncombin_kminus1; ++j)
            {
                int flag = 1; // flag that this is a unique combination
                
                // FIRST MAKE SURE THAT combin_kminus1[j] DOES NOT INCLUDE S[i] ...
                for(int l = 0; l < (k-1); ++l)
                {
                    if( combin_kminus1[j][l] == S[i] ) 
                    { 
                        flag = 0; 
                        break;
                    }
                }
                
                // ... NOW ASSIGN A TEMPORARY COMBINATION FOR COMPARISON BELOW
                combin_tmp[0] = S[i];
                for(int l = 0; l < (k-1); ++l) { combin_tmp[l+1] = combin_kminus1[j][l]; }
                
                // ... NOW MAKE SURE THIS COMBINATION IS NOT ALREADY INCLUDED, BUT IN 
                //     DIFFERENT ORDER ...
                for(int l = 0; l < ncombin; ++l)
                {
                    int cnt = 0;
                    
                    for(int m = 0; m < k; ++m)
                    {
                        for(int n = 0; n < k; ++n)
                        {
                            if( combin[l][m] == combin_tmp[n] ) { cnt++; }
                        }                        
                    }
                    
                    // if k elements in common, we already have this ...
                    if( cnt == k )
                    {
                        flag = 0;
                        break;
                    }
                }                
                
                // ... IF NO TO BOTH TESTS, ADD TO COMBINATION ARRAY
                if( flag == 1 )
                {
                    for(int l = 0; l < k; ++l) { combin[ncombin][l] = combin_tmp[l]; }                  
                    
                    ncombin++;
                }
            }
        }   
        
        // CLEANUP TEMPORARY MEMORY
        for(int i = 0; i < MAXCOMBIN; ++i) { delete [] combin_kminus1[i]; } 
        delete [] combin_kminus1;
        delete [] combin_tmp;
    }
    else
    {
        std::cout << "Bad call in get_combin_norep(), we cannoy have a set with a negative number of elements!" << std::endl;
        exit(2);
    }
    
    return;
}


