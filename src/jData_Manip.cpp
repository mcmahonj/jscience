/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "jData_Manip.h"

//========================================================================
//========================================================================
// NAME: void smooth_exp(double *x, int nx, double ALPHA)
// DESC: Smoothes data using exponential smoothing
// NOTES: 
//     ! See:
//          http://en.wikipedia.org/wiki/Exponential_smoothing
//     ! Exponential smoothing assumes a stationary, not trending, time
//     series, and thus lags behind a trend, if one exists. If there is a
//     trend, this smoothing DOES NOT do well. 
//     ! Note the lag is also why the size of s is [nx+1] and not [nx];
//     and further note that s[0] is NOT used.
//========================================================================
//========================================================================
void smooth_exp(double *x, int nx, double ALPHA, double *s)
{
    // ASSIGN s1 = x0
    s[1] = x[0];
    
    // NOW CALCULATE: st = ALPHA*x{t-1} + (1 - ALPHA)*s{t-1}
    for(int i = 1; i < (nx+1); ++i) { s[i] = ALPHA*x[i-1] + (1.0 - ALPHA)*s[i-1]; }
    
    // CLEANUP & RETURN
    
    return;
}


//========================================================================
//========================================================================
// NAME: void smooth_Holt_Winters(double *x, int nx, double ALPHA, double BETA, double *s, double *b)
// DESC: Smoothes data using Holt-Winters double exponential smoothing
// NOTES: 
//     ! See:
//          http://en.wikipedia.org/wiki/Exponential_smoothing
//     ! Outperforms exponential smoothing when there is a trend in the 
//     data
//     ! because this accomodates a trend in the data, x should be in order
//     of increasing t
//     ! The C++ convention is used in all arrays.
//     ! As with exponential smoothing, s[0] again has no meaning; but
//     unlike there, a lag DOES NOT exist, and so s[] only extends up to 
//     nx. BUT, in case data is being copied based on this, s[0] is 
//     assigned to x[0] so that the complete set of s has meaning
//     ! Besides smoothing data, one can also forecast:
//          F_{t+m} = s_t + m*b_t
//========================================================================
//========================================================================
void smooth_Holt_Winters(double *x, int nx, double ALPHA, double BETA, double *s, double *b)
{
    // ASSIGN: s0 = x0
    // ! this is not meaningful for the subroutine, but is useful for re-
    // copying data based on this subroutine (see the NOTES above)
    s[0] = x[0];
    
    //*********************************************************
    
    // ASSIGN: s1 = x1
    s[1] = x[1];
    
    // ASSIGN: b1 = x1 - x0
    b[1] = x[1] - x[0];

    // NOW FOR t > 1
    for(int i = 2; i < nx; ++i) 
    { 
        s[i] = ALPHA*x[i] + (1.0 - ALPHA)*(s[i-1] + b[i-1]);
        b[i] = BETA*(s[i] - s[i-1]) + (1.0 - BETA)*b[i-1];
    }
    
    // CLEANUP & RETURN
    
    return;
}
   
