/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 5/4/2014
// LAST UPDATE: 5/5/2014


// STL
#include <complex>         // std::real
#include <iostream>        // std::cout, std::endl
#include <string>          // std::string
#include <vector>          // std::vector

// jScience
#include "jconsts.h"       // PI
#include "EMD.hpp"         // mean_and_amplitude() 
#include "jstats.hpp"      // mean()
#include "jutility.hpp"    // any()
#include "vector.hpp"      // rdivide(), abs()


//========================================================================
//========================================================================
//
// NAME: bool stop_sifting( const std::vector<double> &m, const std::vector<int> &t, const std::vector<double> &sd, const std::vector<double> &sd2, double tol, std::string interp, bool mode_complex, int ndirs, std::vector<double> &envmoy, double &s )
// ---
//       bool stop_sifting_fixe( const std::vector<int> &t, const std::vector<double> &m, std::string interp, bool mode_complex, int ndirs, std::vector<double> &moyenne )
// ---
//       bool stop_sifting_fixe_h( const std::vector<int> &t, const std::vector<double> &m, std::string interp, int fixe_h, bool mode_complex, int ndirs, std::vector<double> &moyenne, int &stop_count )
//
// DESC: Stopping criterion for sifting, depending on the options fixe or fixe_h.
//
//========================================================================
//========================================================================

// DEFAULT STOPPING CRITERION: 
bool stop_sifting( const std::vector<double> &m, const std::vector<int> &t, const std::vector<double> &sd, const std::vector<double> &sd2, double tol, std::string interp, bool mode_complex, int ndirs, std::vector<double> &envmoy, double &s )
{
    bool stop;
    
    std::vector<int> nem, nzm;
    std::vector<double> amp;
    mean_and_amplitude( m, t, interp, mode_complex, ndirs, envmoy, nem, nzm, amp );
    
    std::vector<double> sx = rdivide( abs(envmoy), amp );
    s = mean(sx);
    
    if( (sx.size() != sd.size()) || (sx.size() != sd2.size()) )
    {
        std::cout << "Error in stop_sifting(): (sx.size() != sd.size()) || (sx.size() != sd2.size())!" << std::endl;
        exit(0);
    }
    
    // mean(sx > sd) ...
    double sx_gt_sd = 0.0;
    for( auto i = 0; i < sx.size(); ++i )
    {
        if( sx[i] > sd[i] )
        {
            sx_gt_sd += 1.0;
        }
    }
    sx_gt_sd /= static_cast<double>(sx.size());
    
    // any(sx > sd2) ...
    // note: it is just as quick to do this check manually, rather than convert to (sx-sd2) and call any()
    bool sx_gt_sd2 = false;
    for( auto i = 0; i < sx.size(); ++i )
    {
        if( sx[i] > sd2[i] )
        {
            sx_gt_sd2 = true;
            break;
        }
    }
    
    // DETERMINE AND RETURN STOP
    stop = !( ((sx_gt_sd > tol) || sx_gt_sd2) && all(nem, ([](int x){return (x > 2)})) );

    if( !mode_complex )
    {
        // << JMM >> :: I think the latter check is correct (abs(nzm-nem) > 1), as this is what occurs in the subroutine for fixe_h
        stop = stop && !all(abs(nzm - nem), ([](int x){return (x > 1)}));
    }
    
    return stop;
}


// STOPPING CRITERION FOR OPTION fixe:
bool stop_sifting_fixe( const std::vector<int> &t, const std::vector<double> &m, std::string interp, bool mode_complex, int ndirs, std::vector<double> &moyenne )
{
    // always return false ...

    std::vector<int> nem, nzm;
    std::vector<double> amp;
    mean_and_amplitude( m, t, interp, mode_complex, ndirs, moyenne, nem, nzm, amp );
    
    return false;
}

// DEFAULT STOPPING CRITERION FOR OPTION fixe_h:
bool stop_sifting_fixe_h( const std::vector<int> &t, const std::vector<double> &m, std::string interp, int fixe_h, bool mode_complex, int ndirs, std::vector<double> &moyenne, int &stop_count )
{
    bool stop;
    
    std::vector<int> nem, nzm;
    std::vector<double> amp;
    mean_and_amplitude( m, t, interp, mode_complex, ndirs, moyenne, nem, nzm, amp );
    
    if( all(abs(nzm - nem), ([](int x){return (x > 1)})) )
    {
        stop = false;
        stop_count = 0;
    }
    else
    {
        ++stop_count;
        stop = ( stop_count == fixe_h );
    }

    return stop;
}





