/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 5/3/2014
// LAST UPDATE: 5/2/2014


// STL
#include <iostream>        // std::cout, std::endl
#include <string>          // std::string
#include <vector>          // std::vector

// jScience
#include "EMD.hpp"         // EMD()
#include "EMDOptions.hpp"  // EMD()


//========================================================================
//========================================================================
//
// NAME: void EMD_trend_filtering( const std::vector<double> &x, std::string interpolation )
//
// DESC: Determine the trend of a data series x, using empirical mode decompositio (EMD) trend filtering.
//
// INPUT:
//     x      : Data.
//     interp : 'linear' or 'spline' interpolation for EMD. 'linear' leads to more piecewise linear trends.
//
// OUTPUT:
//     trend  : Estimated trend.
//
// NOTES:
//     ! This is an adpatation of the Matlab code by Moghtaderi et al. (see /misc in jScience)
//     ! See the following and associated references:
//          A. Moghtaderi, P. Borgnat, and P. Flandrin, "TREND FILTERING: EMPIRICAL MODE DECOMPOSITIONS VERSUS l1 AND HODRICK-PRESCOTT", Advances in Adaptive Data Analysis 3, 41-61 (2011).
//
//========================================================================
//========================================================================
void EMD_trend_filtering( const std::vector<double> &x, std::string interp, std::vector<double> &trend )
{
    trend.clear();
    
    
    // X0 in example_EMD_trend_filtering() ...
    
    // X1 calling EgRtComp() ...
    
    
    OPTIONS.INTERP = oint;
    OPTIONS.MAXITERATIONS = 50;
    
    options.interp   = interp;
    options.max_iter = 50;
    
    std::vector<std::vector<double>> IMF;
    EMD(x, options, IMF);
    
    // DETERMINE SIZE OF MODES AND POINTS ...
    auto L = imf.size();
    auto N = imf[0].size();
    
    if( N != x.size() )
    {
        std::cout << "N != x.size()!" << std::endl;
    }
    
    
    // GET min_InC ...
/*
    std::vector<int> InC;
    int k = 0;
    
    for( auto l = 0; l < L; ++l )
    {
        
        
    }
*/    
    int min_InC = 0;
    
    
    // X1 back from calling EgRtComp() ...
    
    
    // ESTIMATE THE TREND ...
    trend.resize(N, 0.0);
    
    for( auto l = MInC; l < L; ++l )
    {
        for( auto n = 0; n < N; ++n )
        {
            trend[n] += imf[l][n];
        }
    }
    
    
    
    
}