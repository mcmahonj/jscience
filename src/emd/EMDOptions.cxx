/*
 Copyright 2013--Present JSCIENCE
 
 This file is distributed under the terms of the JSCIENCE License.
 
 You should have received a copy of the JSCIENCE License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 5/3/2014
// LAST UPDATE: 5/3/2014

#include "EMDOptions.hpp"


EMDOptions::EMDOptions()
{
    interp   = "spline";
    
    max_iter = 50;
}

EMDOptions::~EMDOptions() {};
