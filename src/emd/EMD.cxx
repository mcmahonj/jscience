/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 5/3/2014
// LAST UPDATE: 5/4/2014


// STL
#include <complex>         // std::real
#include <iostream>        // std::cout, std::endl
#include <vector>          // std::vector

// jScience
#include "jconsts.h"       // PI
#include "EMD.hpp"         // stop_EMD(), 
#include "EMDOptions.hpp"  // EMDOptions
#include "jutility.hpp"    // any()


//========================================================================
//========================================================================
//
// NAME: void EMD_trend_filtering( const std::vector<double> &x, std::string interpolation )
//
// DESC: Determine the trend of a data series x, using empirical mode decompositio (EMD) trend filtering.
//
// INPUT:
//     x      : Data.
//     interp : 'linear' or 'spline' interpolation for EMD. 'linear' leads to more piecewise linear trends.
//
// OUTPUT:
//     trend  : Estimated trend.
//
// NOTES:
//     ! This is an adpatation of the Matlab code by Moghtaderi et al. (see /misc in jScience)
//     ! See the following and associated references:
//          A. Moghtaderi, P. Borgnat, and P. Flandrin, "TREND FILTERING: EMPIRICAL MODE DECOMPOSITIONS VERSUS l1 AND HODRICK-PRESCOTT", Advances in Adaptive Data Analysis 3, 41-61 (2011).
//
//========================================================================
//========================================================================
void EMD( const std::vector<double> &x, const EMDOptions &options, std::vector<std::vector<double>> &IMF )
{
    bool mode_complex = false;
    
    
    [x,t,sd,sd2,tol,MODE_COMPLEX,ndirs,display_sifting,sdt,sd2t,r,imf,k,nbit,NbIt,MAXITERATIONS,FIXE,FIXE_H,MAXMODES,INTERP,mask] = init(varargin{:});
    
    // MAIN LOOP: 3 EXTREMA NEEDED ...

    while( !stop_EMD(r, mode_complex, ndirs) && ((k < (max_modes+1)) || (max_modes == 0)) && !any(mask) )
    {
        // SET MODES ...
        m  = r; // current mode
        mp = m; // mode at previous iteration
        
        // COMPUTE MEAN AND STOPPING CRITERION ...
        // *****
        /*
        if( fixe )
        {
            [stop_sift,moyenne] = stop_sifting_fixe(t,m,INTERP,MODE_COMPLEX,ndirs);
        }
        else if( fixe_h )
        {
            stop_count = 0;
            [stop_sift,moyenne] = stop_sifting_fixe_h(t,m,INTERP,stop_count,FIXE_H,MODE_COMPLEX,ndirs);
        }
        else
        {
            [stop_sift,moyenne] = stop_sifting(m,t,sd,sd2,tol,INTERP,MODE_COMPLEX,ndirs);
        }
        */
        [stop_sift,moyenne] = stop_sifting(m,t,sd,sd2,tol,INTERP,MODE_COMPLEX,ndirs);
        // *****
        
/*
        % in case the current mode is so small that machine precision can cause
        % spurious extrema to appear
        if (max(abs(m))) < (1e-10)*(max(abs(x)))
            if ~stop_sift
                warning('emd:warning','forced stop of EMD : too small amplitude')
            else
                    disp('forced stop of EMD : too small amplitude')
            end
            break
        end
*/                    
        
        // SIFTING LOOP ...
        while( !stop_sift && (nbit < MAXITERATIONS) )
        {
/*
            if( !mode_complex && (nbit > (MAXITERATIONS/5)) && (mod(nbit,floor(MAXITERATIONS/10))==0) && !fixe && (nbit > 100) )
            {
                disp(['mode ',int2str(k),', iteration ',int2str(nbit)])
                if exist('s','var')
                    disp(['stop parameter mean value : ',num2str(s)])
                end
                [im,iM] = extr(m);
                disp([int2str(sum(m(im) > 0)),' minima > 0; ',int2str(sum(m(iM) < 0)),' maxima < 0.'])
            }
*/
            
            // SIFTING ...
            m -= moyenne;
            
            
            // COMPUTE MEAN AND STOPPING CRITERION ...
            // *****
            /*
            if( fixe )
            {
                [stop_sift,moyenne] = stop_sifting_fixe(t,m,INTERP,MODE_COMPLEX,ndirs);
            }
            else if( fixe_h )
            {
                [stop_sift,moyenne,stop_count] = stop_sifting_fixe_h(t,m,INTERP,stop_count,FIXE_H,MODE_COMPLEX,ndirs);
            }
            else
            {
                [stop_sift,moyenne,s] = stop_sifting(m,t,sd,sd2,tol,INTERP,MODE_COMPLEX,ndirs);
            }
            */
            [stop_sift,moyenne,s] = stop_sifting(m,t,sd,sd2,tol,INTERP,MODE_COMPLEX,ndirs);
            // *****
            
            
            /*
             % display
             if display_sifting && ~MODE_COMPLEX
             NBSYM = 2;
             [indmin,indmax] = extr(mp);
             [tmin,tmax,mmin,mmax] = boundary_conditions(indmin,indmax,t,mp,mp,NBSYM);
             envminp = interp1(tmin,mmin,t,INTERP);
             envmaxp = interp1(tmax,mmax,t,INTERP);
             envmoyp = (envminp+envmaxp)/2;
             if FIXE || FIXE_H
             display_emd_fixe(t,m,mp,r,envminp,envmaxp,envmoyp,nbit,k,display_sifting)
             else
             sxp=2*(abs(envmoyp))./(abs(envmaxp-envminp));
             sp = mean(sxp);
             display_emd(t,m,mp,r,envminp,envmaxp,envmoyp,s,sp,sxp,sdt,sd2t,nbit,k,display_sifting,stop_sift)
             end
             end
             */
            
            
            mp   = m;
            nbit = nbit + 1;
            NbIt = NbIt + 1;
            
            
/*
            if( (nbit==(MAXITERATIONS-1)) && !fixe && (nbit > 100) )
            {
                if exist('s','var')
                    warning('emd:warning',['forced stop of sifting : too many iterations... mode ',int2str(k),'. stop parameter mean value : ',num2str(s)])
                else
                    warning('emd:warning',['forced stop of sifting : too many iterations... mode ',int2str(k),'.'])
                end
            }
*/
        } // end sifting ...

        
        imf(k,:) = m;
/*
        if display_sifting
            disp(['mode ',int2str(k),' stored'])
        end
*/
        nbits(k) = nbit;
        k = k+1;
        
        r = r - m;
        nbit=0;
    }
    
    
    if any(r) && ~any(mask)
        imf(k,:) = r;
    end
    
    ort = io(x,imf);
    
/*
    if display_sifting
        close
    end
*/
}


