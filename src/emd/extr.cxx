/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 5/4/2014
// LAST UPDATE: 5/5/2014


// STL
#include <algorithm>       // std::sort
#include <cmath>           // std::round
#include <iostream>        // std::cout, std::endl
#include <vector>          // std::vector

// jScience
#include "jutility.hpp"    // append(), find(), any()
#include "vector.hpp"      // diff(), times()


//========================================================================
//========================================================================
//
// NAME: void extr( const std::vector<double> &x, std::vector<int> &ind_min, std::vector<int> &ind_max, std::vector<int> &ind_zer )
//
// DESC: Extracts the indices of extrema of x.
//
//========================================================================
//========================================================================
void extr( const std::vector<double> &x, std::vector<int> &ind_min, std::vector<int> &ind_max, std::vector<int> &ind_zer )
{
    ind_min.clear();
    ind_max.clear();
    ind_zer.clear();
    
    
    //=========================================================
    // CALCULATE indzer ...
    //=========================================================
    
    std::vector<double> x1 = x;
    std::vector<double> x2 = x;
    x1.pop_back();        // x1 = x(1:m-1)
    x2.erase(x2.begin()); // x2 = x(2:m)
    
    std::vector<double> x1x2 = times(x1, x2);
    
    ind_zer = find(x1x2, ([](double x){return (x < 0.0)}) );
    
    // if any(x == 0), iz = find( x==0 )
    std::vector<int> iz = find(x, ([](double x){return (x == 0.0)}) );
    
    if( iz.size() > 0 )
    {
        // CALCULATE ind_z ...
        std::vector<int> ind_z;
        
        if( any( diff(iz), ([](int x){return (x == 1)}) ) )
        {
            // zer = x == 0 ...
            std::vector<int> zer(x.size(), 0);
            for( auto &i & iz )
            {
                zer[i] = 1;
            }
            
            // tmp = [0 zer 0]
            std::vector<int> tmp(1, 0);
            append(zer, tmp);
            tmp.push_back(0);
            // dz = diff([0 zer 0])
            std::vector<int> dz = diff(tmp);
            
            
            std::vector<int> debz = find(dz, ([](int x){return (x == 1)}) );
            // finz = find(dz == -1)-1 ...
            std::vector<int> finz = find(dz, ([](int x){return (x == -1)}) );
            for( auto &f : finz )
            {
                f -= 1;
            }
            
            // ***
            if( debz.size() != finz.size() )
            {
                std::cout << "Error in extr(): debz.size() != finz.size()! Cannot calculate indz." << std::endl;
                exit(0);
            }
            else
            {
                for( auto i = 0; i < debz.size(); ++i )
                {
                    ind_z.push_back( static_cast<int>( std::round( (static_cast<double>(debz[i]) + static_cast<double>(finz[i]))/2.0 ) ) );
                }
            }
            // ***
        }
        else
        {
            ind_z = iz;
        }
        
        // APPEND ind_z TO ind_zer AND SORT ...
        append(ind_z, ind_zer);
        std::sort( ind_zer.begin(), ind_zer.end() );
    }
    
    
    //=========================================================
    // CALCULATE indmin & indmax ...
    //=========================================================
    
    std::vector<double> d = diff(x);

    std::vector<double> d1 = d;
    std::vector<double> d2 = d;
    d1.pop_back();        // d1 = d(1:n-1)
    d2.erase(d2.begin()); // d2 = d(2:n))
    
    std::vector<double> d1d2 = times(d1, d2);
    
    // indmin = find(d1.*d2<0 & d1<0)+1 ...
    // indmax = find(d1.*d2<0 & d1>0)+1 ...
    std::vector<int> tmpd1d2 = find(d1d2, ([](double x){return (x < 0.0)}) );
    for( auto &t : tmpd1d2 )
    {
        if( d1[t] < 0 )
        {
            ind_min.push_back( (t+1) );
        }
        else if( d1[t] > 0  )
        {
            ind_max.push_back( (t+1) );
        }
    }
    
    // note: when two or more successive points have the same value, only one extremum in the middle of the constant area is considered (this only works if the signal is uniformly sampled)
    
    if( any( d, ([](double x){return (x == 0.0)}) ) )
    {
        imin.clear();
        imax.clear();
        
        // bad = (d==0) ...
        std::vector<int> bad(d.size(), 0);
        for( auto i = 0; i < d.size(); ++i )
        {
            if( d[i] == 0.0 )
            {
                bad[i] = 1;
            }
        }
        
        // tmp = [0 bad 0]
        std::vector<int> tmp(1, 0);
        append(bad, tmp);
        tmp.push_back(0);
        // dd = diff([0 bad 0])
        std::vector<int> dd = diff(tmp);
        
        std::vector<int> debs = find(dd, ([](int x){return (x == 1)}) );
        std::vector<int> fins = find(dd, ([](int x){return (x == -1)}) );
        
        
        
        if( debs[0] == 1 )
        {
            if( debs.size() > 1 )
            {
                debs.erase( debs.begin() ); //  debs = debs(2:end)
                fins.erase( fins.begin() ); //  fins = fins(2:end)
                
            }
            else
            {
                debs.clear();
                fins.clear();
            }
        }
        
        if( (debs.size() > 0) && (fins.back() == x.size()) )
        {
            if( debs.size() > 1 )
            {
                debs.pop_back(); // debs = debs(1:(end-1))
                fins.pop_back(); // fins = fins(1:(end-1))
            }
            else
            {
                debs.clear();
                fins.clear();
            }
        }
        
        for( auto k = 0; k < debs.size(); ++k )
        {
            if( d[debs[k]-1] > 0.0 )
            {
                if( d[fins[k]] < 0.0 )
                {
                    imax.push_back( static_cast<int>( std::round( (static_cast<double>(fins[k]) + static_cast<double>(debs[k]))/2.0 ) ) );
                }
            }
            else
            {
                if( d[fins(k)] > 0.0 )
                {
                    imin.push_back( static_cast<int>( std::round( (static_cast<double>(fins[k]) + static_cast<double>(debs[k]))/2.0 ) ) );
                }
            }
        }
        
        // SOFT indmin & indmax ...
        if( imin.size() > 0 )
        {
            append(imin, ind_min);
            std::sort( ind_min.begin(), ind_min.end() );
        }
        
        if( imax.size() > 0 )
        {
            append(imax, ind_max);
            std::sort( ind_max.begin(), ind_max.end() );
        }
    }
}





