/*
 Copyright 2013--Present JMM_PROGNAME
 
 This file is distributed under the terms of the JMM_PROGNAME License.
 
 You should have received a copy of the JMM_PROGNAME License.
 If not, see <JMM_PROGNAME WEBSITE>.
*/
// CREATED    : 5/4/2014
// LAST UPDATE: 5/4/2014


// STL
#include <complex>         // std::real
#include <iostream>        // std::cout, std::endl
#include <vector>          // std::vector

// jScience
#include "jconsts.h"       // PI, COMPLEXJ
#include "EMD.hpp"         // extr()


//========================================================================
//========================================================================
//
// NAME: bool stop_EMD(r,MODE_COMPLEX,ndirs)
//
// DESC: Tests whether there are least three extema, needed in order to continue the decomposition.
//
//========================================================================
//========================================================================
bool stop_EMD( const std::vector<double> &r, bool mode_complex, int ndirs )
{
    if( mode_complex )
    {
        for( int k = 0; k < ndirs; ++k )
        {
            // CALCULATE Re(exp(i*phi)*r) ...
            // phi = (k-1)*pi/ndirs, for k = 1:ndirs ... 
            double phi = static_cast<double>(k)*PI/static_cast<double>(ndirs);
            
            std::vector<double> rrc;
            for( auto &ri : r )
            {
                rrc.push_back( std::real(std::exp(COMPLEXJ*phi)*ri) );
            }
            
            // CALCULATE INDICES OF EXTREMA ...
            std::vector<int> ind_min, ind_max, ind_zer;
            
            extr( rrc, ind_min, ind_max, ind_zer );
            
            if( (ind_min.size() + ind_max.size()) < 3 )
            {
                return true;
            }
        }
    }
    else
    {
        // CALCULATE INDICES OF EXTREMA ...
        std::vector<int> ind_min, ind_max, ind_zer;
        
        extr(r, ind_min, ind_max, ind_zer);

        if( (ind_min.size() + ind_max.size()) < 3 )
        {
            return true;
        }
    }
    
    return false;
}