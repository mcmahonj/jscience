/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/22/2013
// LAST UPDATE: 12/23/2013

#ifdef WLAPACK

// STL
#include <complex>    // std::complex<>
#include <iostream>   // std::cout, std::endl
#include <vector>     // std::vector

// LAPACK/BLAS
#include <Accelerate/Accelerate.h>

// THIS
#include "Matrix.hpp" // Matrix, realSymmetric_to_tridiagonal()


//========================================================================
//========================================================================
//
// NAME: void get_eigenvalues_and_eigenvectors_realSymmetric(const Matrix<double> &A, std::vector<double> &W, Matrix<double> &Z)
//
// DESC: Gets the eigenvalues and eigenvectors of the real, symmetric matrix A. On return, W gives the eigenvalues in ascending order, and Z gives the eigenvectors, with the ith column holding the eigenvector associated with W[i].
//
// NOTES:
//     ! A MUST be real and symmetric -- a verification is NOT made.
//     ! A can be passed as a complete matrix, or in upper-triangle form (but NOT lower-triangle).
//
//========================================================================
//========================================================================
void get_eigenvalues_and_eigenvectors_realSymmetric(const Matrix<double> &A, std::vector<double> &W, Matrix<double> &Z)
{
    // REDUCE MATRIX A TO TRIDIAGONAL FORM
    // note: upon exit of realSymmetric_to_tridiagonal(), E contains (N-1) elements, but dstegr below NEEDS it to contain N elements
    std::vector<double> D; // N diagonal elements of the tridiagonal matrix
    std::vector<double> E; // N-1 subdiagonal elements of the tridiagonal matrix
    realSymmetric_to_tridiagonal(A, D, E);
    E.push_back(0);
    
    // SET TO CALCULATE EIGENVALUES *AND* EIGENVECTORS
    char JOBZ  = 'V';
    
    // SET THAT ALL EIGENVALUES WILL BE FOUND
    char RANGE = 'A';
    
    // ORDER (No. COLUMNS) AND LEADING DIMENSION (No. ROWS) OF A
    int N   = A.get_ncols();
    //int LDA = A.size();
    
    // RANGES OF EIGENVALUES TO SEARCH FOR (NOT REFERENCED WITH RANGE = 'A')
    double VL = 0.0;
    double VU = 0.0;
    int IL    = 0;
    int IU    = 0;
    
    // UNUSED
    double ABSTOL;
    
    // NUMBER OF EIGENVALUES FOUND -- SHOULD BE N FOR RANGE = 'A'
    int M;
    
    // EIGENVALUES
    W.resize(N);

    // EIGENVECTORS
    int LDZ = std::max(1, N); // LEADING DIMENSION OF Z -- FOR JOBZ = 'V', LDZ SHOULD BE N
    std::vector<double> Z_CMO(LDZ*std::max(1,N));
    
    // SUPPORT OF THE EIGENVECTORS IN Z
    std::vector<int> ISUPPZ(2*std::max(1,N));
    
    // WORK VECTORS
    int LWORK = std::max(1,(18*N));
    std::vector<double> WORK(LWORK);
    
    int LIWORK = std::max(1,(10*N));
    std::vector<int> IWORK(LIWORK);

    // INFO
    int INFO;
    
    // CALL DGEEV
    dstegr_(&JOBZ, &RANGE, &N, &*D.begin(), &*E.begin(), &VL, &VU, &IL, &IU, &ABSTOL, &M, &*W.begin(), &*Z_CMO.begin(), &LDZ, &*ISUPPZ.begin(), &*WORK.begin(), &LWORK, &*IWORK.begin(), &LIWORK, &INFO);

    if( INFO != 0 )
    {
        std::cout << "Error in get_eigenvalues_and_eigenvectors_realSymmetric(): INFO did not return as 0! INFO = " << INFO << "." << std::endl;
        exit(0);
    }

    ColMajOrd_to_Matrix(Z_CMO, A.get_ncols(), Z);
}


//========================================================================
//========================================================================
//
// NAME: void get_eigenvalues_and_eigenvectors_realNonSymmetric(const Matrix<double> &A, std::vector<std::complex<double>> &W, Matrix<std::complex<double>> &Z)
//
// DESC: Gets the eigenvalues and eigenvectors of a real, non-symmetric matrix A. On return, W gives the eigenvalues (ordered?), and Z gives the eigenvectors, with the ith column holding the eigenvector associated with W[i].
//
//========================================================================
//========================================================================
void get_eigenvalues_and_eigenvectors_realNonSymmetric(const Matrix<double> &A, std::vector<std::complex<double>> &W, Matrix<std::complex<double>> &Z)
{
    // SET TO CALCULATE ONLY THE RIGHT EIGENVECTORS
    char JOBVL = 'N';
    char JOBVR = 'V';
    
    // ORDER (No. COLUMNS) AND LEADING DIMENSION (No. ROWS) OF A
    int N   = A.get_ncols();
    int LDA = A.get_nrows();
    
    // STORE A IN COLUMN-MAJOR ORDER
    std::vector<double> A_CMO(LDA*N);
    Matrix_to_ColMajOrd(A, A_CMO);
    
    // EIGENVALUES (TMP)
    std::vector<double> WR(N);
    std::vector<double> WI(N);
    
    // EIGENVECTORS (TMP)
    int LDVL = 1;
    std::vector<double> VL(LDVL*N); // NOT REFERENCED FOR JOBVL = 'N'
    
    int LDVR = N;
    std::vector<double> VR(LDVR*N);
    
    // WORK VECTOR
    int LWORK = 4*N;
    std::vector<double> WORK(std::max(1,LWORK));

    // INFO
    int INFO;
    
    // CALL DGEEV
    dgeev_(&JOBVL, &JOBVR, &N, &*A_CMO.begin(), &LDA, &*WR.begin(), &*WI.begin(), &*VL.begin(), &LDVL, &*VR.begin(), &LDVR, &*WORK.begin(), &LWORK, &INFO);
    
    if( INFO != 0 )
    {
        std::cout << "Error in get_eigenvalues_and_eigenvectors_realNonSymmetric(): INFO did not return as 0! INFO = " << INFO << "." << std::endl;
        exit(0);
    }
    
    // CONVERT RETURNS FROM DGEEV TO RETURN FORMAT ...
    W.resize(N);
    for( decltype(W.size()) i = 0; i < W.size(); ++i )
    {
        W[i] = std::complex<double>(WR[i], WI[i]);
    }
    
    Matrix<double> Z_r;
    ColMajOrd_to_Matrix(VR, A.get_ncols(), Z_r);
    
    Z.resize( A.get_nrows(), A.get_ncols() );
    for( unsigned int i = 0; i < Z.get_nrows(); ++i )
    {
        for( unsigned int j = 0; j < Z.get_ncols(); ++j )
        {
            Z(i, j) = std::complex<double>(Z_r(i, j), 0.0);
        }
    }
    
    for( decltype(W.size()) j = 0; j < (W.size()-1); ++j )
    {
        if( W[j] == std::conj(W[j+1]) )
        {
            for( unsigned int i = 0; i < Z.get_nrows(); ++i )
            {
                Z(i, j)     = std::complex<double>( std::real(Z(i, j)), std::real(Z(i, (j+1))) );
                Z(i, (j+1)) = std::conj( Z(i, j) );
            }
            
            ++j;
        }
    }
    
}


#endif