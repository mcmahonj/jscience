/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/23/2013
// LAST UPDATE: 12/23/2013


// STL
#include <string> // std::string
#include <vector> // std::vector


//========================================================================
//========================================================================
//
// NAME: int find_string(std::string string, const std::vector<std::string> &strings)
//
// DESC: Finds a string in a vector of strings, returning -1 if not found.
//
//========================================================================
//========================================================================
int find_string(std::string string, const std::vector<std::string> &strings)
{
    for( auto it = strings.begin(), end = strings.end(); it != end; ++it )
    {
        if( *it == string )
        {
            return std::distance( strings.begin(), it );
        }
    }
    
    return -1;
}




