/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "jEwald.h"


// ! these must be static to ensure they stay inside these routines
static VECTOR_DOUBLE g_Rvec[3];
static VECTOR_DOUBLE *g_Rshift = NULL;
static VECTOR_DOUBLE *g_kvec = NULL;
static double *g_U_1_coeff = NULL;
static double *g_qi = NULL;
static int g_ncharge, g_Rmax, g_lmax, g_nR, g_nR0, g_nk;
static double g_alpha, g_sqrt_alpha, g_U_self;


//========================================================================
//========================================================================
// NAME: void Ewald_init(int, double [], VECTOR_DOUBLE [3], int, double)
// DESC: Initialize quantities needed for Ewald summation, subsequent calls
//     to Ewald() must only supply qr (unless Rvecs, etc. change)
// INPUT:
//     xxx    
// OUTPUT:
//     None
// NOTES: 
//     ! !!! JMM the computational cost of the k-vector sum could be reduced
//     by 2 by only including kx>=0 components and multiplying by 2 -- but
//     this is not implemented yet
//========================================================================
//========================================================================
void Ewald_init(int ncharge_user, double qi_user[], VECTOR_DOUBLE Rvec0[3], int lmax_user, int Rmax_user, double alpha_user)
{
    //int lmax = 7; 
    //*********************************************************
    /*
     // CONVERGENCE PARAMETER FOR EWALD SUMMATION
     // ! estimate the amount of time necessary to do the real space part relative to the k-space part
     static double tauR_div_tauk = 0.05;
     // SET OPTIMUM VALUE OF ALPHA (
     // ! just before Eq. (12.1.37) in Frenkel and Smit
     // ! this assumes that the same number of charges exists on ALL particles (so we can just use the 0 slice 0 particle to estimate this)
     static double alpha = pow((tauR_div_tauk)*PI*PI*PI*static_cast<double>(g_npart)*static_cast<double>(g_particle[0][0].ncharge)/pow(g_a0, 6), (1.0/6.0));
     */
    //********************************************************* 
    // SET DEFAULT ALPHA TO THAT USED IN cp2k, RATHER THAN CALCULATE IT DIRECTLY
    //static double alpha = 0.35*AA_PER_BOHR;    
    //********************************************************* 
    
    double V;
    VECTOR_DOUBLE kvec0[3];    
    

    //=========================================================
    // ASSIGN USER-SUPPLIED VALUES TO Ewald.cpp QUANTITIES
    //=========================================================     
    
    int g_Rmax = Rmax_user;    
    int g_lmax = lmax_user;
    g_alpha = alpha_user;   
    g_ncharge = ncharge_user;    
    
    // ALLOCATE SOME MEMORY  
    // ! the '-1' in g_kvec and g_U_1_coeff comes from the fact that we don't need k=0
    g_Rshift = new VECTOR_DOUBLE [(2*g_Rmax+1)*(2*g_Rmax+1)*(2*g_Rmax+1)];
    g_kvec = new VECTOR_DOUBLE [(2*g_lmax+1)*(2*g_lmax+1)*(2*g_lmax+1)-1];
    g_U_1_coeff = new double [(2*g_lmax+1)*(2*g_lmax+1)*(2*g_lmax+1)-1];  
    g_qi = new double [g_ncharge];  
    
    for(int n = 0; n < g_ncharge; ++n) { g_qi[n] = qi_user[n]; }

    for(int i = 0; i < 3; ++i) { g_Rvec[i] = Rvec0[i]; }
    
    
    //=========================================================
    // CALCULATE QUANTITIES OF SIMULATION BOX ...
    //=========================================================
    
    // GET BOX VOLUME
    // ! the volume of a parallelpiped is V = |a*(b x c)| = |b*(c x a)| = |c*(a x b)|
    V = fabs( dot_product( Rvec0[0], cross_product( Rvec0[1], Rvec0[2] ) ) );   
    
    
    //=========================================================
    // CALCULATE R-VECTORS FOR REAL-SPACE SUM
    //=========================================================

    // ZERO RUNNING TALLLY OF R-VECTORS FOR FOURIER SUM
    g_nR = 0;
    
    // ASSIGN TEMPORARY POSITION FOR THE 0 R-VECTOR
    // ! if this is not over-ridden, an error will eventually occur
    g_nR0 = -1;
    
    for(int r1 = -g_Rmax; r1 <= g_Rmax; ++r1)
    {
        for(int r2 = -g_Rmax; r2 <= g_Rmax; ++r2)
        {
            for(int r3 = -g_Rmax; r3 <= g_Rmax; ++r3)
            {
                // CHECK FOR THE 0 VECTOR
                if( (r1 == 0) && (r2 == 0) && (r3 == 0) ) { g_nR0 = g_nR; }
                
                // CALCULATE SHIFTED R-VECTOR
                g_Rshift[g_nR] = Rvec0[0]*static_cast<double>(r1) + Rvec0[1]*static_cast<double>(r2) + Rvec0[2]*static_cast<double>(r3);                 
                
                // INCREASE RUNNING TALLY 
                g_nR++;
            }
        }
    }        
    
    //=========================================================
    // CALCULATE k-VECTORS (AND RELATED QUANTITIES) AND U_1 COEFFICIENT
    //=========================================================
    
    // CALCULATE REFERENCE k-VECTORS
    kvec0[0] = ( cross_product(Rvec0[1], Rvec0[2]) / dot_product(Rvec0[0], cross_product(Rvec0[1], Rvec0[2]) ) )*2.0*PI;
    kvec0[1] = ( cross_product(Rvec0[2], Rvec0[0]) / dot_product(Rvec0[1], cross_product(Rvec0[2], Rvec0[0]) ) )*2.0*PI;
    kvec0[2] = ( cross_product(Rvec0[0], Rvec0[1]) / dot_product(Rvec0[2], cross_product(Rvec0[0], Rvec0[1]) ) )*2.0*PI;
    
    // ZERO RUNNING TALLLY OF k-VECTORS FOR FOURIER SUM
    g_nk = 0;
    
    // CALCULATE k-VECTORS AND k*k FOR FOURIER SUM
    // ! note that there is no nee
    for(int l1 = -g_lmax; l1 <= g_lmax; ++l1)
    {
        for(int l2 = -g_lmax; l2 <= g_lmax; ++l2)
        {
            for(int l3 = -g_lmax; l3 <= g_lmax; ++l3)
            {
                // SKIP k=0
                // ! see Eq. (12.1.17) in Frenkel and Smit 
                if( (l1 == 0) && (l2 == 0) && (l3 == 0) ) { continue; }
                
                // CALCULATE k-VECTOR
                g_kvec[g_nk] = kvec0[0]*static_cast<double>(l1) + kvec0[1]*static_cast<double>(l2) + kvec0[2]*static_cast<double>(l3);                
                
                // CALCULATE k*k
                double k2 = dot_product(g_kvec[g_nk], g_kvec[g_nk]);
                
                // CALCULATE THE COEFFICIENT FOR U_1
                // ! Eq. (12.1.17) in Frenkel and Smit
                g_U_1_coeff[g_nk] = (1.0/(2.0*V))*(4.0*PI/k2)*exp(-k2/(4.0*g_alpha));                    
                
                // INCREASE RUNNING TALLY 
                g_nk++;
            }
        }
    }
    
    
    //=========================================================
    // CALCULATE OTHER UTILIZED QUANTITIES
    //=========================================================
    
    g_sqrt_alpha = sqrt(g_alpha);
    
    //=========================================================
    // CALCULATE SELF-INTERACTION
    //=========================================================
    // ! Eq. (12.1.22) in Frenkel and Smit
    
    // ZERO TERM
    g_U_self = 0.0;
    
    // SUM SQUARE OF CHARGES
    for(int n = 0; n < g_ncharge; ++n) { g_U_self += ( g_qi[n]*g_qi[n] ); }
    
    // NORMALIZE
    g_U_self *= sqrt( g_alpha/PI );    
    
    
    return;
}



//========================================================================
//========================================================================
// NAME: double Ewald(VECTOR_DOUBLE [])
// DESC: Calculated Ewald summation
// INPUT:
//     VECTOR_DOUBLE qr[] :: charge positions (in a.u.)
// OUTPUT:
//     double Ewald: Ewald energy in Ry*a.u.
// NOTES: 
//     ! calculations are done in Gaussian units and m, and normalized to 
//     Ry at end
//     ! Ewald_init() MUST be called before this
//     !!! I do not know if my erfc_approx() is faster than the c++ erfc(),
//     but I will ASSUME it is
//========================================================================
//========================================================================
double Ewald(VECTOR_DOUBLE qr[])
{   
    double Ewald;
    double U_1, U_short_range; 
    
    //=========================================================
    // INITIAL ERROR CHECK 
    //========================================================= 
    // ! this check just looks for NULL pointers
    
    if( g_kvec == NULL )
    {
        cout << "Error in Ewald() -- (g_kvec == NULL), so 'Ewald_init()' probably never called!" << endl;
        exit(3);        
    }
    
    //=========================================================
    // CALCULATE FOURIER-SPACE SUM
    //=========================================================
    
    complex<double> rhok;
    
    // ZERO U_1
    U_1 = 0.0;

    // SUM U_1
    // ! note that this sum does NOT include k=0, above it was skipped, 
    // as it should be
    for(int k = 0; k < g_nk; ++k)
    {
        // CALCULATE rhok
        // ! Eq. (12.1.18) in Frenkel and Smit
        rhok = COMPLEXZERO;
        for(int n = 0; n < g_ncharge; ++n) 
        { 
            rhok += ( g_qi[n]*exp(COMPLEXJ*dot_product(g_kvec[k], qr[n])) );  
        }
        
        // ADD TERM TO FOURIER-SPACE SUM
        U_1 += ( g_U_1_coeff[k]*norm(rhok) );
    }
     

    //=========================================================
    // CALCULATE REAL-SPACE SUM
    //=========================================================
    
    // ZERO TERM
    U_short_range = 0.0;
    
    // CALCULATE SUM
    // ! Eq. (12.1.24) in Frenkel and Smit  
    // ! note this is ADAPTED from Eq. (12.1.24), so I don't need to sum over all pairs twice
    // and normalize by 0.5 *IF, AND ONLY IF* (g_nR=0)
    // ! based on the algorithm in Allen and Tindsley (NOT Frenkel and smit), the minimum
    // image convention should be used for (g_nR=0)
    // ! this should probably be used for (g_nR!=0), but this will continue to increase the computational cost by a lot (with probably little added benefit)
    // ! (g_nR == 1) corresponds to simply the unit cell with the minimum image convention
    // ! for (g_nR != 1), the minimum image convention is NOT employed
    if(g_nR == 1)
    {
        for(int i = 0; i < (g_ncharge-1); ++i) 
        { 
            // define local quantities for the ith charge to reduce array traverse below 
            double qii = g_qi[i];
            VECTOR_DOUBLE qri = qr[i]; 
            
            for(int j = (i+1); j < g_ncharge; ++j) 
            { 
                // CALCULATE rij USING MINIMUM IMAGE CONVENTION
                double rij = dist_MinImg_orthorhombic(qri, qr[j], g_Rvec); 
            
                U_short_range += ( qii*g_qi[j]*erfc_approx( (g_sqrt_alpha*rij) )/rij );
            } 
        } 
    }
    else
    { 
        // !!! JMM: I am not entirely convinced the following is correct, so quit if user tries to call it
        cout << "Not sure that Rmax_user > 0 is implemented correctly in jEwald.cpp, so I will quit!" << endl;
        exit(3);
        /*
        for(int i = 0; i < g_ncharge; ++i) 
        { 
            // define local quantities for the ith charge to reduce array traverse below 
            double qii = g_qi[i];
            VECTOR_DOUBLE qri = qr[i]; 
            
            for(int j = 0; j < g_ncharge; ++j) 
            { 
                for(int r = 0; r < g_nR; ++r)
                {
                    // SKIP SELF INTERACTION AND DOUBLE COUNTING WITHIN R=0 CELL
                    if( (r == g_nR0) && ( j <= i ) ) { continue; }
                    
                    // CALCULATE rij
                    double rij = get_length( qri - (qr[j] + g_Rshift[r]) );            
                    
                    // ADD TO SUM
                    U_short_range += ( qii*g_qi[j]*erfc_approx( (g_sqrt_alpha*rij) )/rij );                
                    
                }
            } 
        }
        */
    }    
    
    //=========================================================
    // SUM EWALD CONTRIBUTIONS, CONVERT FROM GAUSSIAN TO Ry, & RETURN
    //=========================================================
    
    // SUM CONTRIBUTIONS
    // ! Eq. (12.1.24) in Frenkel and Smit  
    // ! note the spurious contribution U_self is SUBTRACTED
    Ewald = U_1 - g_U_self + U_short_range;
    
    //cout << "U_1: " << U_1 << endl;
    //cout << "g_U_self: " << g_U_self << endl;
    //cout << "U_short_range: " << U_short_range << endl;
    
    // NORMALIZE FROM Gauss TO Ry
    Ewald *= RYD_BOHR_PER_GAUSS_M;  
 
    return Ewald;
}


//========================================================================
//========================================================================
// NAME: void Ewald_cleanup()
// DESC: Cleans up quantities allocated in Ewald_init()
// INPUT:
//     None
// OUTPUT:
//     None
// NOTES: 
//     ! 
//========================================================================
//========================================================================
void Ewald_cleanup()
{
    //=========================================================
    // INITIAL ERROR CHECK 
    //========================================================= 
    // ! this check just looks for NULL pointers
    
    if( g_kvec == NULL )
    {
        cout << "Error in Ewald_cleanup() -- (g_kvec == NULL), so 'Ewald_init()' probably never called!" << endl;
        exit(3);        
    }
    
    //=========================================================
    // DELETE MEMORY AND REASSIGN POINTERS TO NULL
    //========================================================= 
   
    delete [] g_Rshift;
    delete [] g_kvec;
    delete [] g_U_1_coeff;   
    delete [] g_qi; 

    g_Rshift = NULL;
    g_kvec = NULL;
    g_U_1_coeff = NULL;
    g_qi = NULL;
    
    return;
}








