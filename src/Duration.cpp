/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/12/2013
// LAST UPDATE: 5/9/2014

#include "Duration.hpp"

// STL
#include <ctime>    // std::time_t
#include <iostream> // std::istream, std::ostream, std::cout, std::endl

// jScience
#include <ctime>    // std::time_t


Duration::Duration() : Duration(0, 0, 0, 0, 0, 0, 0) {};

Duration::Duration(unsigned int Y, unsigned int M, unsigned int W, unsigned int D, unsigned int h, unsigned int m, unsigned int s)
{   
    nY = Y;
    nM = M;
    nW = W;
    nD = D;
    nh = h;
    nm = m;
    ns = s;
}

// note: constructing with a timestamp, we utilize D, M, and S only, because Y, M, and W can be ambiguous
Duration::Duration(std::time_t t)
{
    nY = 0;
    nM = 0;
    nW = 0;
    
    nD = t/86400;
    t = t % 86400;
    
    nh = t/3600;
    t = t % 3600;
    
    nm = t/60;
    
    ns = t % 60;
}

Duration::~Duration() {};


//========================================================================
//========================================================================
//
// NAME: std::time_t Duration::to_timestamp()
//
// DESC: Calculates the UNIX timestamp for an ISO_8601 Duration.
//
//========================================================================
//========================================================================
std::time_t Duration::to_timestamp() const
{
    if( (nY > 0) || (nM > 0) || (nW > 0) )
    {
        std::cout << "Error in Duration::to_timestamp():( (nY > 0) || (nM > 0) || (nW > 0) ), which, by itself, is ambiguous!" << std::endl;
        exit(0);
    }
    
    return ( static_cast<std::time_t>(86400*nD + 3600*nh + 60*nm + ns) );
}


//========================================================================
//========================================================================
//
// NAME: Duration& Duration::operator+=(const Duration& rhs)
//
// DESC: Arithmetic operators between Duration objects.
//
// NOTES:
//     ! No "rounding" is performed between time-units.
//
//========================================================================
//========================================================================
Duration& Duration::operator+=(const Duration& rhs)
{
    nY += rhs.nY;
    nM += rhs.nM;
    nW += rhs.nW;
    nD += rhs.nD;
    nh += rhs.nh;
    nm += rhs.nm;
    ns += rhs.ns;
    
    return (*this);
}


//========================================================================
//========================================================================
//
// NAME: std::ostream& operator<<(std::ostream& os, const Duration &dur)
//       std::istream& operator>>(std::istream& is, Duration &dur)
//
// DESC: Overloads for the input and output operators.
//
//========================================================================
//========================================================================
std::ostream& operator<<(std::ostream& os, const Duration &dur)
{
    os << "(" << dur.nY << "," << dur.nM << "," << dur.nW << "," << dur.nD << "," << dur.nh << "," << dur.nm << "," << dur.ns << ")";

    return os;
}

std::istream& operator>>(std::istream& is, Duration &dur)
{
    int nY = -1;
    int nM = -1;
    int nW = -1;
    int nD = -1;
    int nh = -1;
    int nm = -1;
    int ns = -1;   
    
    std::string val;
    
    getline(is, val, '(');
    getline(is, val, ',');
    nY = std::stoi(val);
    getline(is, val, ',');
    nM = std::stoi(val);
    getline(is, val, ',');
    nW = std::stoi(val);
    getline(is, val, ',');
    nD = std::stoi(val);
    getline(is, val, ',');
    nh = std::stoi(val);
    getline(is, val, ',');
    nm = std::stoi(val);
    getline(is, val, ',');
    ns = std::stoi(val);
    getline(is, val, ')');
    
    if( (nY < 0) || (nM < 0) || (nW < 0) || (nD < 0) || (nh < 0) || (nm < 0) || (ns < 0) )
    {
        is.setstate(std::ios::failbit);
    }
    else
    {
        dur = Duration(nY, nM, nW, nD, nh, nm, ns);
    }
    
    return is;
}


//========================================================================
//========================================================================
// CONSTANTS
//========================================================================
//========================================================================

const Duration Duration_1Day(0, 0, 0, 1, 0, 0, 0);
const Duration Duration_1Hour(0, 0, 0, 0, 1, 0, 0);
const Duration Duration_30Mins(0, 0, 0, 0, 0, 30, 0);
const Duration Duration_15Mins(0, 0, 0, 0, 0, 15, 0);
const Duration Duration_5Mins(0, 0, 0, 0, 0, 5, 0);
const Duration Duration_3Mins(0, 0, 0, 0, 0, 3, 0);
const Duration Duration_2Mins(0, 0, 0, 0, 0, 2, 0);
const Duration Duration_1Min(0, 0, 0, 0, 0, 1, 0);
const Duration Duration_30Secs(0, 0, 0, 0, 0, 0, 30);
const Duration Duration_15Secs(0, 0, 0, 0, 0, 0, 15);
const Duration Duration_5Secs(0, 0, 0, 0, 0, 0, 5);
const Duration Duration_1Sec(0, 0, 0, 0, 0, 0, 1);
const Duration Duration_none(0, 0, 0, 0, 0, 0, 0);





