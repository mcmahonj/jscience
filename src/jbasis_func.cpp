/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 8/4/2013
// LAST UPDATE: 8/21/2013


#include "jbasis_func.hpp"

//=========================================================
// ERROR CHECK
//=========================================================


//========================================================================
//========================================================================
//
// NAME: double GaussianRBF_kernel(vector<double> X, vector<double> XP, double sigma)
//
// DESC: Calculates the Gaussian radial basis function kernel function k(X,XP) = exp(-||X - XP||^2/(2*sigma^2))
//
// INPUT:
//          X             : feature vector 1
//          XP            : feature vector 2
//          sigma         : standard deviation of distribution
//
// OUTPUT:
//          return        : k(X,XP)
//
// NOTES:
//     ! see:
//          http://en.wikipedia.org/wiki/Radial_basis_function_kernel
//          http://en.wikipedia.org/wiki/Euclidean_distance
//
//========================================================================
//========================================================================
double GaussianRBF_kernel(std::vector<double> X, std::vector<double> XP, double sigma)
{
    // ERROR CHECK
    if( X.size() != XP.size() ) { std::cout << "Error in GaussianRBF(): X.size() != XP.size()!" << std::endl; }
    
    // GET SQUARED EUCLIDIAN DISTANCE BETWEEN FEATURE VECTORS
    double d2 = 0.0;
    for( decltype(X.size()) i = 0; i < X.size(); ++i) { d2 += ( (X[i] - XP[i])*(X[i] - XP[i]) ); }
    
    // NOW RETURN k(X,XP) = exp(-||X - XP||^2/(2*sigma^2))
    return ( exp(-d2/(2.0*sigma*sigma)) );
}


//========================================================================
//========================================================================
//
// NAME: double polynomial_kernel(vector<double> X, vector<double> XP, double c, int d)
//
// DESC: Calculates the polynomial kernel function k(X,XP) = ( (X^T)*XP + c )^d
//
// INPUT:
//          X             : feature vector 1
//          XP            : feature vector 2
//          c             : influence-trading constant
//          d             : order of polynomial
//
// OUTPUT:
//          return        : k(X,XP)
//
// NOTES:
//     ! see:
//          http://en.wikipedia.org/wiki/Polynomial_kernel
//
//========================================================================
//========================================================================
double polynomial_kernel(std::vector<double> X, std::vector<double> XP, double c, int d)
{
    // ERROR CHECK
    if( X.size() != XP.size() ) { std::cout << "Error in polynomial_kernel(): X.size() != XP.size()!" << std::endl; }
    
    // GET (X^T)*XP
    double XTXP = 0.0;
    for( decltype(X.size()) i = 0; i < X.size(); ++i) { XTXP += ( X[i]*XP[i] ); }
    
    // NOW RETURN k(X,XP) = ( (X^T)*XP + c )^d
    return ( pow( (XTXP + c), d ) );
}



