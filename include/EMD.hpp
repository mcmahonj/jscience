/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/1/2014
// LAST UPDATE: 5/4/2014

#ifndef EMD_H
#define EMD_H


// STL
#include <string>     // std::string
#include <vector>     // std::vector

// jScience
#include "EMDOptions" // EMDOptions


void EMD( const std::vector<double> &x, const EMDOptions &options, std::vector<std::vector<double>> &IMF );

bool stop_EMD( const std::vector<double> &r, bool mode_complex, int ndirs );
bool stop_sifting( const std::vector<double> &m, const std::vector<int> &t, const std::vector<double> &sd, const std::vector<double> &sd2, double tol, std::string interp, bool mode_complex, int ndirs, std::vector<double> &envmoy, double &s );
bool stop_sifting_fixe( const std::vector<int> &t, const std::vector<double> &m, std::string interp, bool mode_complex, int ndirs, std::vector<double> &moyenne );
bool stop_sifting_fixe_h( const std::vector<int> &t, const std::vector<double> &m, std::string interp, int fixe_h, bool mode_complex, int ndirs, std::vector<double> &moyenne, int &stop_count );

void extr( const std::vector<double> &x, std::vector<int> &ind_min, std::vector<int> &ind_max, std::vector<int> &ind_zer );

void EMD_trend_filtering( const std::vector<double> &x, std::string interpolation, std::vector<double> &trend );


#endif
