/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/12/2013
// LAST UPDATE: 5/9/2014

#ifndef DURATION_H
#define DURATION_H


// STL
#include <ctime>    // std::time_t
#include <iostream> // std::istream, std::ostream


//=========================================================
// ISO 8601 DURATION
// NOTES:
//     ! See:
//          http://en.wikipedia.org/wiki/ISO_8601#Durations
//     ! A designator of 'nm' is used in place of the somewhat ambiguos 'nM' in ISO 8601
//=========================================================
class Duration
{
    
public:
    
    unsigned int nY;
    unsigned int nM;
    unsigned int nW;
    unsigned int nD;
    unsigned int nh;
    unsigned int nm;
    unsigned int ns;
    
    Duration();
    Duration(unsigned int Y, unsigned int M, unsigned int W, unsigned int D, unsigned int h, unsigned int m, unsigned int s);
    Duration(std::time_t t);
    ~Duration();
    
    std::time_t to_timestamp() const;
    
    Duration& operator+=(const Duration& rhs);
    
    template<typename T>
    Duration& operator*=(const T &mult);
    template<typename T>
    Duration& operator/=(const T &rhs);
    
    friend std::ostream& operator<<(std::ostream& os, const Duration &dur);
    friend std::istream& operator>>(std::istream& is, Duration &dur);
};

#include "../tpp/Duration.tpp"


//========================================================================
//========================================================================
//
// NAME: inline bool operator==(const Duration &d1, const Duration &d2)
//       inline bool operator!=(const Duration &d1, const Duration &d2)
//
// DESC: Compares two Duration structures, d1 and d2, returning whether d1 is equal (or not) to d2.
//
//========================================================================
//========================================================================
inline bool operator==(const Duration &d1, const Duration &d2)
{
    if( (d1.nY == d2.nY) && (d1.nM == d2.nM) && (d1.nW == d2.nW) && (d1.nD == d2.nD) && (d1.nh == d2.nh) && (d1.nm == d2.nm) && (d1.ns == d2.ns) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

inline bool operator!=(const Duration &d1, const Duration &d2)
{
    return !operator==(d2, d1);
}


//========================================================================
//========================================================================
//
// NAME: inline bool operator< (const Duration &d1, const Duration &d2)
//       inline bool operator> (const Duration &d1, const Duration &d2)
//       inline bool operator<=(const Duration &d1, const Duration &d2)
//       inline bool operator>=(const Duration &d1, const Duration &d2)
//
// DESC: Compares two Duration structures, d1 and d2, returning whether d1 < d2, etc.
//
// NOTES:
//     ! This comparison is a little ambiguous, since one day could be longer than another (e.g., DST), but the comparison below considers just the more general case.
//
//========================================================================
//========================================================================
inline bool operator< (const Duration &d1, const Duration &d2)
{
    // << JMM >> :: would it be faster here to convert to timestamp and make a single comparison?
    
    if( d1.nY < d2.nY )
    {
        return true;
    }
    else if( d1.nY > d2.nY  )
    {
        return false;
    }
    else
    {
        if( d1.nM < d2.nM )
        {
            return true;
        }
        else if( d1.nM > d2.nM  )
        {
            return false;
        }
        else
        {
            if( d1.nD < d2.nD )
            {
                return true;
            }
            else if( d1.nD > d2.nD  )
            {
                return false;
            }
            else
            {
                if( d1.nh < d2.nh )
                {
                    return true;
                }
                else if( d1.nh > d2.nh  )
                {
                    return false;
                }
                else
                {
                    if( d1.nm < d2.nm )
                    {
                        return true;
                    }
                    else if( d1.nm > d2.nm  )
                    {
                        return false;
                    }
                    else
                    {
                        if( d1.ns < d2.ns )
                        {
                            return true;
                        }
                        else if( d1.ns > d2.ns  )
                        {
                            return false;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }
    }
}

inline bool operator> (const Duration &d1, const Duration &d2)
{
    return operator<(d2, d1);
}

inline bool operator<=(const Duration &d1, const Duration &d2)
{
    return !operator>(d1, d2);
}

inline bool operator>=(const Duration &d1, const Duration &d2)
{
    return !operator<(d1, d2);
}


//========================================================================
//========================================================================
//
// NAME: inline Duration operator+(Duration d1, const Duration &d2)
//
// DESC: Arithmetic operators between Duration objects.
//
//========================================================================
//========================================================================
inline Duration operator+(Duration d1, const Duration &d2)
{
    d1 += d2;
    return d1;
}


//========================================================================
//========================================================================
//
// NAME: template<typename T>
//       inline Duration operator*(const T &mult, Duration dur)
//       inline Duration operator/(Duration dur, const T &rhs)
//
// DESC: Arithmetic operators between a duration and 
//
//========================================================================
//========================================================================
template<typename T>
inline Duration operator*(const T &mult, Duration dur)
{
    dur *= mult;
    return dur;
}

template<typename T>
inline Duration operator/(Duration dur, const T &rhs)
{
    dur /= rhs;
    return dur;
}


//=========================================================
// CONSTANTS
//=========================================================

extern const Duration Duration_1Day;
extern const Duration Duration_1Hour;
extern const Duration Duration_30Mins;
extern const Duration Duration_15Mins;
extern const Duration Duration_5Mins;
extern const Duration Duration_3Mins;
extern const Duration Duration_2Mins;
extern const Duration Duration_1Min;
extern const Duration Duration_30Secs;
extern const Duration Duration_15Secs;
extern const Duration Duration_5Secs;
extern const Duration Duration_1Sec;
extern const Duration Duration_none;


#endif
