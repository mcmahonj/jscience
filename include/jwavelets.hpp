/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 8/9/2013
// LAST UPDATE: 8/29/2013

// DESC:
// NOTES: (i) *Many* of these routines are based on those provided in Numerical Recipes in C++, but have been updated for improved/updated functionality.

#ifndef JWAVELETS_H
#define JWAVELETS_H

//************************************************************************
//************************************************************************
// INCLUDES
//************************************************************************
//************************************************************************

// UTILITY
#include <vector>
// I/O
#include <fstream>

// JSCIENCE
#include "jmath_basic.h"
#include "jutility.hpp"

// !!! JMM
using namespace std;

//************************************************************************
//************************************************************************
// WAVELETS
//************************************************************************
//************************************************************************

// ! see p. 703 in Numerical Recipes in C++
// ! Wavelet is an ``abstract base class'', meaning that it is really only a promis that specific wavelets that derive from it will contain a method call filt, the actual wavelet filter. Wavelet also provides a default, null, pre- and post-conditioning method.
struct Wavelet
{
    virtual void filt( vector<double> & a, const int isign ) = 0;
    virtual void condition( vector<double> & a, const int isign ) {}
};


struct Daub4 : Wavelet
{
    // DESC: Applied the Daubechies 4-coefficient wavelet filter to data vector a[0,...,n-1] (for isign == 1) or applies its transpose (for isign == -1). Used heirachically by routines wt1 and wtn.
    // ! see (13.10.1), (13.10.2), and (13.10.7) in Numerical Recipes in C++
    void filt( vector<double> & a, const int isign )
    {
        const double C0 = 0.4829629131445341;
        const double C1 = 0.8365163037378077;
        const double C2 = 0.2241438680420134;
        const double C3 = -0.1294095225512603;
        
        if( a.size() < 4 ) { return; }
        
        vector<double> wksp( a.size() );
        
        int nh = a.size() >> 1;
        
        // APPLY FILTER ...
        if( isign >= 0 )
        {
            unsigned int i, j;
            for(i=0, j=0; j < (a.size()-3); j+=2, ++i)
            {
                wksp[i]    = C0*a[j] + C1*a[j+1] + C2*a[j+2] + C3*a[j+3];
                wksp[i+nh] = C3*a[j] - C2*a[j+1] + C1*a[j+2] - C0*a[j+3];
            }
            
            wksp[i]    = C0*a[a.size()-2] + C1*a[a.size()-1] + C2*a[0] + C3*a[1];
            wksp[i+nh] = C3*a[a.size()-2] - C2*a[a.size()-1] + C1*a[0] - C0*a[1];
        }
        // ... OR APPLY TRANSPOSE FILTER
        else
        {
            wksp[0] = C2*a[nh-1] + C1*a[a.size()-1] + C0*a[0] + C3*a[nh];
            wksp[1] = C3*a[nh-1] - C0*a[a.size()-1] + C1*a[0] - C2*a[nh];
        
            for(int i=0, j=2; i < (nh-1); ++i)
            {
                wksp[j++] = C2*a[i] + C1*a[i+nh] + C0*a[i+1] + C3*a[i+nh+1];
                wksp[j++] = C3*a[i] - C0*a[i+nh] + C1*a[i+1] - C2*a[i+nh+1];
            }   
        }
        
        a = wksp;
    } // end filt()
};


// DESC: Structure for initializing and using the DAUBn wavelet for any n whose coefficients are provided
// ! There is some arbitrariness in how the wavelets at each heirarchical stage are centered over the data they act on. An alternate centering is commented out below.
// ! for every increase in the number of wavelet coefficients by two, the Daubechies wavelets gain *about* half a derivative of continuity
struct Daubs : Wavelet
{
	int ncof, ioff, joff;
	vector<double> cc, cr;
	//static double c4[4], c12[12], c20[20];
    double c4[4]=
    {0.4829629131445341, 0.8365163037378079,
        0.2241438680420134, -0.1294095225512604};
    double c12[12]=
    {0.111540743350, 0.494623890398, 0.751133908021,
        0.315250351709, -0.226264693965, -0.129766867567,
        0.097501605587, 0.027522865530, -0.031582039318,
        0.000553842201, 0.004777257511, -0.001077301085};
    double c20[20]=
    {0.026670057901, 0.188176800078, 0.527201188932,
        0.688459039454, 0.281172343661, -0.249846424327,
        -0.195946274377, 0.127369340336, 0.093057364604,
        -0.071394147166, -0.029457536822, 0.033212674059,
        0.003606553567, -0.010733175483, 0.001395351747,
        0.001992405295, -0.000685856695, -0.000116466855,
        0.000093588670, -0.000013264203};
    
	Daubs(int n) : ncof(n), cc(n), cr(n)
    {
		ioff = joff = -(n >> 1);
		// ioff = -2; joff = -n + 2; // Alternate centering. (Used by Daub4, above.)
		
        if( n == 4 )       { for(int i = 0; i < n; ++i) { cc[i] = c4[i]; } }
        else if( n == 12 ) { for(int i = 0; i < n; ++i) { cc[i] = c12[i]; } }
        else if( n == 20 ) { for(int i = 0; i < n; ++i) { cc[i] = c20[i]; } }
		else               { throw("n not yet implemented in Daubs"); }
        
		double sig = -1.0;
		
        for(int i = 0; i < n; ++i)
        {
			cr[n-1-i] = sig*cc[i];
			sig = -sig;
		}
	}
    
	//void filt( vector<double> & a, const int isign );
    // DESC: Applies the previously initialized Daubn wavelet filter to data vector a[0,...,n-1] (for isign == 1) or applies its transpose (for isign == -1). Used heirarchically by routines wt1 and wtn.
    void filt( vector<double> & a, const int isign )
    {
        if( a.size() < 4 ) { return; }
        
        vector<double> wksp(a.size(), 0.0);
        
        int nmod = ncof*a.size();    // A positive constant equal to zero % n
        int n1 = a.size() - 1;       // Mask of all bits, since n a power of 2
        int nh = a.size() >> 1;
        
        // APPLY FILTER ...
        if( isign >= 0 )
        {
            for(unsigned int ii = 0, i = 0; i < a.size(); i+=2, ++ii)
            {
                // pointers to be incremented and wrapped around ...
                int ni = i+1+nmod+ioff;
                int nj = i+1+nmod+joff;
                
                for(int k = 0; k < ncof; ++k)
                {
                    // use ``bitwise and'' to wrap around the pointers ...
                    int jf = n1 & (ni+k+1);
                    int jr = n1 & (nj+k+1);
                    
                    wksp[ii]    += cc[k]*a[jf];
                    wksp[ii+nh] += cr[k]*a[jr];
                }
            }
        }
        // ... APPLY TRANSPOSE FILTER ...
        else
        {
            for(unsigned int ii = 0, i = 0; i < a.size(); i+=2, ++ii)
            {
                double ai  = a[ii];
                double ai1 = a[ii+nh];
                
                // see comments above ...
                int ni = i+1+nmod+ioff;
                int nj = i+1+nmod+joff;
                
                for(int k = 0; k < ncof; ++k)
                {
                    int jf = n1 & (ni+k+1);
                    int jr = n1 & (nj+k+1);
                    
                    wksp[jf] += cc[k]*ai;
                    wksp[jr] += cr[k]*ai1;
                }
            }
        }
        
        // COPY RESULTS BACK TO WORKSPACE ...
        a = wksp;
    }
};

/*
double Daubs::c4[4]=
    {0.4829629131445341, 0.8365163037378079,
	 0.2241438680420134, -0.1294095225512604};
double Daubs::c12[12]=
    {0.111540743350, 0.494623890398, 0.751133908021,
	 0.315250351709, -0.226264693965, -0.129766867567,
	 0.097501605587, 0.027522865530, -0.031582039318,
 	 0.000553842201, 0.004777257511, -0.001077301085};
double Daubs::c20[20]=
     {0.026670057901, 0.188176800078, 0.527201188932,
	 0.688459039454, 0.281172343661, -0.249846424327,
	 -0.195946274377, 0.127369340336, 0.093057364604,
	 -0.071394147166, -0.029457536822, 0.033212674059,
	 0.003606553567, -0.010733175483, 0.001395351747,
     0.001992405295, -0.000685856695, -0.000116466855,
	 0.000093588670, -0.000013264203};

// DESC: Applies the previously initialized Daubn wavelet filter to data vector a[0,...,n-1] (for isign == 1) or applies its transpose (for isign == -1). Used heirarchically by routines wt1 and wtn.
void Daubs::filt( vector<double> & a, const int isign )
{
	if( a.size() < 4 ) { return; }
	
    vector<double> wksp(a.size(), 0.0);
    
	int nmod = ncof*a.size();    // A positive constant equal to zero % n
	int n1 = a.size() - 1;       // Mask of all bits, since n a power of 2
	int nh = a.size() >> 1;
	
    // APPLY FILTER ...
    if( isign >= 0 )
    {
		for(int ii = 0, i = 0; i < a.size(); i+=2, ++ii)
        {
            // pointers to be incremented and wrapped around ...
			int ni = i+1+nmod+ioff;        
			int nj = i+1+nmod+joff;
            
			for(int k = 0; k < ncof; ++k)
            {
                // use ``bitwise and'' to wrap around the pointers ...
				int jf = n1 & (ni+k+1);    
				int jr = n1 & (nj+k+1);
                
				wksp[ii]    += cc[k]*a[jf];
				wksp[ii+nh] += cr[k]*a[jr];
			}
		}
	}
    // ... APPLY TRANSPOSE FILTER ...
    else
    {
		for(int ii = 0, i = 0; i < a.size(); i+=2, ++ii)
        {
			double ai  = a[ii];
			double ai1 = a[ii+nh];
            
            // see comments above ...
			int ni = i+1+nmod+ioff;
			int nj = i+1+nmod+joff;
            
			for(int k = 0; k < ncof; ++k)
            {
				int jf = n1 & (ni+k+1);
				int jr = n1 & (nj+k+1);
                
				wksp[jf] += cc[k]*ai;
				wksp[jr] += cr[k]*ai1;
			}
		}
	}
    
    // COPY RESULTS BACK TO WORKSPACE ...
    a = wksp;
}
*/


// ! coefficients of the wavelet filter of Daub4 are modified to produce wavelets that ``live on the interval'' instead of on the circle.


// ! see p. 709 in Numerical Recipes in C++
struct Daub4i : Wavelet
{
    // DESC: Applies the Cohen--Daubechies--Vial 4-coefficient wavelet on the interval filter to data vector a[0,...,n-1] (for isign == 1) or applies its transpose (for isign == -1). Used heirachically by routines wt1 and wtn.
	void filt( vector<double> & a, const int isign )
    {
		const double C0=0.4829629131445341,  C1=0.8365163037378077,
                     C2=0.2241438680420134,  C3=-0.1294095225512603;
		const double R00=0.603332511928053,  R01=0.690895531839104,
                     R02=-0.398312997698228, R10=-0.796543516912183, R11=0.546392713959015,
                     R12=-0.258792248333818, R20=0.0375174604524466, R21=0.457327659851769,
                     R22=0.850088102549165,  R23=0.223820356983114,  R24=-0.129222743354319,
                     R30=0.0100372245644139, R31=0.122351043116799,  R32=0.227428111655837,
                     R33=-0.836602921223654, R34=0.483012921773304,  R43=0.443149049637559,
                     R44=0.767556669298114,  R45=0.374955331645687,  R46=0.190151418429955,
                     R47=-0.194233407427412, R53=0.231557595006790,  R54=0.401069519430217,
                     R55=-0.717579999353722, R56=-0.363906959570891, R57=0.371718966535296,
                     R65=0.230389043796969,  R66=0.434896997965703,  R67=0.870508753349866,
                     R75=-0.539822500731772, R76=0.801422961990337,  R77=-0.257512919478482;
        
        // DUE TO MULTIPLE REFS, JUST ASSIGN n UP FRONT ...
		int n = a.size();
		
        if( n < 8 ) { return; }
        
		vector<double> wksp( n );
        
		int nh = n >> 1;
        
		if (isign >= 0)
        {
			wksp[0]    = R00*a[0]+R01*a[1]+R02*a[2];
			wksp[nh]   = R10*a[0]+R11*a[1]+R12*a[2];
			wksp[1]    = R20*a[0]+R21*a[1]+R22*a[2]+R23*a[3]+R24*a[4];
			wksp[nh+1] = R30*a[0]+R31*a[1]+R32*a[2]+R33*a[3]+R34*a[4];
            
			for(int i = 2, j = 3; j < (n-4); j+=2, ++i)
            {
				wksp[i]    = C0*a[j]+C1*a[j+1]+C2*a[j+2]+C3*a[j+3];
				wksp[i+nh] = C3*a[j]-C2*a[j+1]+C1*a[j+2]-C0*a[j+3];
			}
            
			wksp[nh-2] = R43*a[n-5]+R44*a[n-4]+R45*a[n-3]+R46*a[n-2]+R47*a[n-1];
			wksp[n-2]  = R53*a[n-5]+R54*a[n-4]+R55*a[n-3]+R56*a[n-2]+R57*a[n-1];
			wksp[nh-1] = R65*a[n-3]+R66*a[n-2]+R67*a[n-1];
			wksp[n-1]  = R75*a[n-3]+R76*a[n-2]+R77*a[n-1];
		}
        else
        {
			wksp[0] = R00*a[0]+R10*a[nh]+R20*a[1]+R30*a[nh+1];
			wksp[1] = R01*a[0]+R11*a[nh]+R21*a[1]+R31*a[nh+1];
			wksp[2] = R02*a[0]+R12*a[nh]+R22*a[1]+R32*a[nh+1];
            
			if( n == 8 )
            {
				wksp[3]   = R23*a[1]+R33*a[5]+R43*a[2]+R53*a[6];
				wksp[4]   = R24*a[1]+R34*a[5]+R44*a[2]+R54*a[6];
			}
            else
            {
				wksp[3]   = R23*a[1]+R33*a[nh+1]+C0*a[2]+C3*a[nh+2];
				wksp[4]   = R24*a[1]+R34*a[nh+1]+C1*a[2]-C2*a[nh+2];
				wksp[n-5] = C2*a[nh-3]+C1*a[n-3]+R43*a[nh-2]+R53*a[n-2];
				wksp[n-4] = C3*a[nh-3]-C0*a[n-3]+R44*a[nh-2]+R54*a[n-2];
			}
            
			for(int i = 2, j = 5; i < (nh-3); ++i)
            {
				wksp[j++] = C2*a[i]+C1*a[i+nh]+C0*a[i+1]+C3*a[i+nh+1];
				wksp[j++] = C3*a[i]-C0*a[i+nh]+C1*a[i+1]-C2*a[i+nh+1];
			}
            
			wksp[n-3] = R45*a[nh-2]+R55*a[n-2]+R65*a[nh-1]+R75*a[n-1];
			wksp[n-2] = R46*a[nh-2]+R56*a[n-2]+R66*a[nh-1]+R76*a[n-1];
			wksp[n-1] = R47*a[nh-2]+R57*a[n-2]+R67*a[nh-1]+R77*a[n-1];
		}
        
        a = wksp;
	} // end filt()
    
	void condition( vector<double> & a, const int isign )
    {
		if( a.size() < 4 ) { return; }
        
        double t0, t1, t2, t3;
        
		if( isign >= 0 )
        {
			t0 = 0.324894048898962*a[0]+0.0371580151158803*a[1];
			t1 = 1.00144540498130*a[1];
			t2 = 1.08984305289504*a[a.size()-2];
			t3 = -0.800813234246437*a[a.size()-2]+2.09629288435324*a[a.size()-1];
		}
        else
        {
			t0 = 3.07792649138669*a[0]-0.114204567242137*a[1];
			t1 = 0.998556681198888*a[1];
			t2 = 0.917563310922261*a[a.size()-2];
			t3 = 0.350522032550918*a[a.size()-2]+0.477032578540915*a[a.size()-1];
		}
        
        a[0]=t0; a[1]=t1; a[a.size()-2]=t2; a[a.size()-1]=t3;
	} // end condition()
};




//************************************************************************
//************************************************************************
// SUBROUTINES
//************************************************************************
//************************************************************************

void wt1(vector<double> & a, const int isign, Wavelet & wlet);
void dwt1d(vector<double> & a, const int isign, std::string wavelet_nm);

void denoise_signal(vector<double> & signal, std::string wavelet_nm, int L);
void wave_shrink(vector<double> & y, std::string wavelet_nm, int L);
void multi_hybrid(vector<double> & wc, int L);
void hybrid_thresh(vector<double> & y);
double SURE_thresh(vector<double> y);
void soft_thresh(vector<double> & y, double t);
void norm_noise(vector<double> & signal, std::string wavelet_nm, double & coeff);

#endif	
