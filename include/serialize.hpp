/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/19/2014
// LAST UPDATE: 5/19/2014

#ifndef SERIALIZE_H
#define SERIALIZE_H


// STL
#include <string>   // std::string


//========================================================================
// NAME: template <typename T>
//       bool serialize_read( const std::string filename, T &obj, bool binary = false )
// DESC: Reads an object from file, previously serialized using Boost.
// NOTES:
//     ! The object T MUST contain the serialize() function for Boost.
//     ! The format (text or binary) MUST be the same as that used for serialize_write().
//========================================================================
template <typename T>
bool serialize_read( const std::string filename, T &obj, bool binary = false );

//========================================================================
// NAME: template <typename T>
//       void serialize_write( const std::string filename, T &obj, bool binary = false )
// DESC: Writes a serialized object to file, using Boost, in either text or binary format.
// NOTES:
//     ! The object T MUST contain the serialize() function for Boost.
//     ! For portability (cross-platform, etc.), binary should be left false.
//========================================================================
template <typename T>
void serialize_write( const std::string filename, T &obj, bool binary = false );


#include "../tpp/serialize.tpp"


#endif	
