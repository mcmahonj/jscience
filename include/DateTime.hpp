/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/1/2013
// LAST UPDATE: 5/19/2014

#ifndef DATE_TIME_H
#define DATE_TIME_H


// STL
#include <ctime>   // std::time_t
#include <ostream> // std::ostream
#include <string>  // std::string

// BOOST
#include <boost/assign/list_of.hpp> // boost::assign::map_list_of
#include <boost/unordered_map.hpp>  // boost::unordered_map

// THIS
#include "Duration.hpp"
//#include "ISO_8601.hpp"


// forward declaration of class boost::serialization::access
namespace boost
{
    namespace serialization
    {
        class access;
    }
}


//=========================================================
// ISO 8601 DATE AND TIME FORMAT
// NOTES:
//     ! See:
//          http://en.wikipedia.org/wiki/ISO_8601
//     ! The time-zone specifiers do not STRICTLY conform to ISO 8601
//=========================================================
class DateTime
{
    
public:
    
    unsigned int YYYY;
    unsigned int MM;
    unsigned int DD;
    
    unsigned int hh;
    unsigned int mm;
    unsigned int ss;
    
    enum class TIME_ZONE
    {
        UTC,
        ET,
        TIME_ZONE_END
    };
    TIME_ZONE TZD;
    
    
    DateTime();
    DateTime(unsigned int Y, unsigned int M, unsigned int D);
    DateTime(unsigned int Y, unsigned int M, unsigned int D, TIME_ZONE TZ);
    DateTime(unsigned int Y, unsigned int M, unsigned int D, unsigned int h, unsigned int m, unsigned int s, TIME_ZONE TZ);
    DateTime(std::time_t timestamp);
    //========================================================================
    // NAME: DateTime(const std::string str)
    // DESC: Initializes a DateTime with a date/time in string format.
    // NOTES:
    //     ! Valid formats for str include:
    //          YYYYMMDD
    //          YYYY-MM-DD
    //          YYYY-MM-DDThh:mm:ss (UTC implied)
    //          YYYY-MM-DDThh:mm:ssTZD
    //========================================================================
    DateTime(const std::string str);
    ~DateTime();
    
    std::time_t  to_timestamp() const;
    
    unsigned int day_of_week() const;
    
    std::string  to_YYYYMMDD() const;
    std::string  to_YYYYMMDDThhmmss() const;
    std::string  to_YYYYMMDDThhmmssTZD() const;
    
    DateTime& operator+=(const Duration &dur);
    DateTime& operator-=(const Duration &dur);
    
    
private:
    
    friend class boost::serialization::access;
    
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version __attribute__((unused)))
    {
        ar & YYYY;
        ar & MM;
        ar & DD;
        ar & hh;
        ar & mm;
        ar & ss;
        ar & TZD;
    }
    
};


// note: all of the following designators are NOT ISO 8601, and also since the ISO_8601 class is more of simply a FORMAT, the boost map for time zones is defined here ...

const boost::unordered_map<unsigned int, std::string> DateTime_MMtoString_abbr = boost::assign::map_list_of
    (1,                                    "Jan")
    (2,                                    "Feb")
    (3,                                    "Mar")
    (4,                                    "Apr")
    (5,                                    "May")
    (6,                                    "Jun")
    (7,                                    "Jul")
    (8,                                    "Aug")
    (9,                                    "Sep")
    (10,                                   "Oct")
    (11,                                   "Nov")
    (12,                                   "Dec");

const boost::unordered_map<std::string, unsigned int> DateTime_stringToMM_abbr = boost::assign::map_list_of
    ("Jan",                                1)
    ("Feb",                                2)
    ("Mar",                                3)
    ("Apr",                                4)
    ("May",                                5)
    ("Jun",                                6)
    ("Jul",                                7)
    ("Aug",                                8)
    ("Sep",                                9)
    ("Oct",                                10)
    ("Nov",                                11)
    ("Dec",                                12);

const boost::unordered_map<DateTime::TIME_ZONE, std::string> TZD_to_string = boost::assign::map_list_of
    (DateTime::TIME_ZONE::UTC,             "UTC")
    (DateTime::TIME_ZONE::ET,              "ET");

// TZD TO TIME-ZONE IN TZ DATABASE
// See:
//     http://en.wikipedia.org/wiki/List_of_tz_database_time_zones
const boost::unordered_map<DateTime::TIME_ZONE, std::string> TZD_to_TZ = boost::assign::map_list_of
    (DateTime::TIME_ZONE::UTC,             "UTC")
    (DateTime::TIME_ZONE::ET,              "EST5EDT");


//========================================================================
//========================================================================
//
// NAME: inline bool operator==(const DateTime &t1, const DateTime &t2)
//       inline bool operator!=(const DateTime &t1, const DateTime &t2)
//
// DESC: Compares two DateTime structures, t1 and t2, returning whether t1 is equal (or not) to t2.
//
//========================================================================
//========================================================================
inline bool operator==(const DateTime &t1, const DateTime &t2)
{
    if( t1.TZD != t2.TZD )
    {
        std::cout << "(Temporary) Error in bool operator==(const DateTime &t1, const DateTime &t2): time zones for t1 and t2 MUST be the same!" << std::endl;
        exit(0);
    }
    
    // note: assuming that typical comparisons will be of similar time-periods, comparisons are made in the order of most-likely deviation: s, m, h, etc.
    if( (t1.ss == t2.ss) && (t1.mm == t2.mm) && (t1.hh == t2.hh) && (t1.DD == t2.DD) && (t1.MM == t2.MM) && (t1.YYYY == t2.YYYY) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

inline bool operator!=(const DateTime &t1, const DateTime &t2)
{
    return !operator==(t2, t1);
}

//========================================================================
//========================================================================
//
// NAME: inline bool operator< (const DateTime &t1, const DateTime &t2)
//       inline bool operator> (const DateTime &t1, const DateTime &t2)
//       inline bool operator<=(const DateTime &t1, const DateTime &t2)
//       inline bool operator>=(const DateTime &t1, const DateTime &t2)
//
// DESC: Compares two DateTime structures, t1 and t2, returning whether t1 < t2, etc.
//
//========================================================================
//========================================================================
inline bool operator< (const DateTime &t1, const DateTime &t2)
{
    if( t1.TZD != t2.TZD )
    {
        std::cout << "(Temporary) Error in bool operator<(const DateTime &t1, const DateTime &t2): time zones for t1 and t2 MUST be the same!" << std::endl;
        std::cout << "t1 = " << t1.to_YYYYMMDDThhmmssTZD() << std::endl;
        std::cout << "t2 = " << t2.to_YYYYMMDDThhmmssTZD() << std::endl;
        exit(0);
    }
    
    // << JMM >> :: would it be faster here to convert to timestamp and make a single comparison?
    
    // note: unlike the == operator above, here we must start comparing the times starting at the longest interval: Y, M, D, etc.
    if( t1.YYYY < t2.YYYY )
    {
        return true;
    }
    else if( t1.YYYY > t2.YYYY  )
    {
        return false;
    }
    else
    {
        if( t1.MM < t2.MM )
        {
            return true;
        }
        else if( t1.MM > t2.MM  )
        {
            return false;
        }
        else
        {
            if( t1.DD < t2.DD )
            {
                return true;
            }
            else if( t1.DD > t2.DD  )
            {
                return false;
            }
            else
            {
                if( t1.hh < t2.hh )
                {
                    return true;
                }
                else if( t1.hh > t2.hh  )
                {
                    return false;
                }
                else
                {
                    if( t1.mm < t2.mm )
                    {
                        return true;
                    }
                    else if( t1.mm > t2.mm  )
                    {
                        return false;
                    }
                    else
                    {
                        if( t1.ss < t2.ss )
                        {
                            return true;
                        }
                        else if( t1.ss > t2.ss  )
                        {
                            return false;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }
    }
}

inline bool operator> (const DateTime &t1, const DateTime &t2)
{
    return operator<(t2, t1);
}

inline bool operator<=(const DateTime &t1, const DateTime &t2)
{
    return !operator>(t1, t2);
}

inline bool operator>=(const DateTime &t1, const DateTime &t2)
{
    return !operator<(t1, t2);
}


//========================================================================
//========================================================================
//
// NAME: inline DateTime operator+(DateTime t, const Duration &dur)
//       inline DateTime operator-(DateTime t, const Duration &dur) 
//
// DESC: Operators to add, subtract, etc. a 'Duration' to a 'DateTime' object.
//
//========================================================================
//========================================================================
inline DateTime operator+(DateTime t, const Duration &dur)
{
    return ( t += dur );
}

inline DateTime operator-(DateTime t, const Duration &dur)
{
    return ( t -= dur );
}


// NOTE: See duration_between() in date_time.hpp, for an analogous operator to subtraction between two DateTime objects.


std::ostream& operator<<(std::ostream &os, const DateTime &t);


#endif
