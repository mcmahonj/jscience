/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/23/2013
// LAST UPDATE: 12/23/2013

#ifndef EIGEN_H
#define EIGEN_H


// STL
#include <complex> // std::complex<>
#include <vector>  // std::vector

// THIS
#include "Matrix.hpp"


void get_eigenvalues_and_eigenvectors_realSymmetric(const Matrix<double> &A, std::vector<double> &W, Matrix<double> &Z);
void get_eigenvalues_and_eigenvectors_realNonSymmetric(const Matrix<double> &A, std::vector<std::complex<double>> &W, Matrix<std::complex<double>> &Z);


#endif
