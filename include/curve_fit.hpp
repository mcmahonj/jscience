/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 11/16/2013
// LAST UPDATE: 3/12/2015

#ifndef CURVE_FIT_H
#define CURVE_FIT_H


// STL
#include <vector>


void fit(const std::vector<double> &x, const std::vector<double> &y, const std::vector<double> &sig, bool mwt, double &a, double &b, double &siga, double &sigb, double &chi2, double &q);


#endif
