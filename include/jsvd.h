/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JSVD_H	
#define JSVD_H

//************************************************************************
//			INCLUDE FILES
//************************************************************************

// MATH
#include <cmath>
// I/O
#include <iostream>

#include "jmath_basic.h"

using namespace std;

//************************************************************************
//			MACROS / OTHER DEFS.
//************************************************************************


//************************************************************************
//			CLASSES / STRUCTS
//************************************************************************

//************************************************************************
//			FULL CODE GLOBAL VARIABLES
//************************************************************************

//************************************************************************
//			SUBROUTINE DECLARATIONS
//************************************************************************

// !!! JMM: this should be moved
// ! this can be found on p. JMM in Num. Recip. in C
double pythag(double, double);

void svdcmp( double **a, int m, int n, double w[], double **v );
void svbksb( double **u, double w[], double **v, int m, int n, double b[], double x[] );
void svdvar( double **v, int ma, double w[], double **cvm );

void svdfit( double x[], double y[], double sig[], int ndata, double a[], int ma, double **u, double **v, double w[], double *chisq, void (*funcs)(double, double [], int) );

#endif	
