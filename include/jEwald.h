/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JEWALD_H	
#define JEWALD_H


//************************************************************************
//			INCLUDE FILES
//************************************************************************

// STANDARD	
// utilities
#include <cstdlib>
// math
#include <cmath>
#include <complex>
// I/O
//#include <iostream>

#include "jScience/physics/consts.hpp"
#include "jmath_vector.h"
#include "jmath_special.h"
#include "jPBC.h"


using namespace std;


//************************************************************************
//			CLASSES / STRUCTS
//************************************************************************

void Ewald_init(int, double [], VECTOR_DOUBLE [3], int, int, double);

double Ewald(VECTOR_DOUBLE []);

void Ewald_cleanup();



#endif	
