/*
 Copyright 2014-Present Algorithms in Motion LLC

 This file is part of jScience.

 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 8/11/2013
// LAST UPDATE: 1/1/2015

#ifndef JUTILITY_H
#define JUTILITY_H


// STL
#include <iterator> // std::front_inserter, std::back_inserter
#include <string>   // std::string
#include <vector>   // std::vector
#include <utility>  // std::move


//========================================================================
// NAME: int dyad_length(vector<double> x)
// DESC: Finds the dyadic length of a vector
//========================================================================
int dyad_length(std::vector<double> x);
//========================================================================
// NAME: void dyad_indices(int j, int & i1, int & i2)
// DESC: Fetches indices of jth-dyad of a vector (e.g., 1D wavelet)
//========================================================================
void dyad_indices(int j, int & i1, int & i2);

//========================================================================
// NAME: template <typename T>
//       bool any(const std::vector<T> &x)
//
//       template <typename T, typename F>
//       bool any(const std::vector<T> &x, F func)
// DESC: Determines whether any elements of x are non-zero (no second parameters) or satisfy the function func.
// NOTES:
//     ! This is similar to the MATLAB function any().
//========================================================================
template <typename T>
bool any(const std::vector<T> &x);

template <typename T, typename F>
bool any(const std::vector<T> &x, F func);

#include "../tpp/any.tpp"


//========================================================================
// NAME: template <typename T>
//       bool all(const std::vector<T> &x);
//
//       template <typename T, typename F>
//       bool all(const std::vector<T> &x, F func);
// DESC: Determines whether all elements of x are non-zero (no second parameters) or satisfy the function func.
// NOTES:
//     ! This is similar to the MATLAB function any().
//========================================================================
template <typename T>
bool all(const std::vector<T> &x);

template <typename T, typename F>
bool all(const std::vector<T> &x, F func);

#include "../tpp/all.tpp"


//========================================================================
// NAME: template <typename T, typename F>
//       std::vector<int> find(const std::vector<T> &x, F func)
// DESC: Finds the indices of x that satisfy the function func.
// NOTES:
//     ! This is similar to the MATLAB function find().
//========================================================================
template <typename T, typename F>
std::vector<int> find(const std::vector<T> &x, F func);
#include "../tpp/find.tpp"


//========================================================================
// NAME: int find_string(std::string string, const std::vector<std::string> &strings)
// DESC: Finds a string in a vector of strings, returning -1 if not found.
//========================================================================
int find_string(std::string string, const std::vector<std::string> &strings);


//========================================================================
// NAME: template <typename T> inline append(std::vector<T> &a, std::vector<T> &b)
// DESC: Appends a vector 'a' to 'b'.
// NOTES:
//     ! Upon return, 'a' is unusable -- it is passed by reference to avoid a copy.
//========================================================================
template <typename T> inline void append(std::vector<T> &a, std::vector<T> &b)
{
    b.reserve( b.size() + a.size() );
    std::move( a.begin(), a.end(), std::back_inserter(b) );
}


//========================================================================
// NAME: template <typename T> inline prepend(std::vector<T> &a, std::vector<T> &b)
// DESC: Prepends a vector 'a' to 'b'.
// NOTES:
//     ! If it is known that elements are routinely to be inserted at the beginning of b, std::deque should be used rather than std::vector.
//     ! Upon return, 'a' is unusable -- it is passed by reference to avoid a copy.
//========================================================================
template <typename T> inline void prepend(std::vector<T> &a, std::vector<T> &b)
{
    // note: I think that append followed by assignmet is the fastest operation, as it requires a set of O(1) operations followed by ONE O(n) operation, whereas a continuos insert at the beginning would be multiple O(n) operations
    append(b, a); // O(1)
    b = a;        // O(n)
}

//========================================================================
// NAME: template <typename T> void remove_duplicates(std::vector<T> &v)
//       template <typename T> void remove_duplicates_huge(std::vector<T> &v)
// DESC: Removes duplicate elements from a vector. The vector is sorted, upon return. _huge is used for vectors with a lot of duplicates.
//========================================================================
template <typename T> void remove_duplicates(std::vector<T> &v);
template <typename T> void remove_duplicates_huge(std::vector<T> &v);
#include "../tpp/duplicates.tpp"

//========================================================================
// NAME: template <typename T> void remove_multiple_elements(std::vector<std::vector<T>::size_type> indxs, std::vector<T> &v)
// DESC: Removes multiple elements from a vector. indx MUST not contain duplicate elements -- if so, call remove_duplicates() first.
//========================================================================
template <typename T> void remove_multiple_elements(std::vector<typename std::vector<T>::size_type> indxs, std::vector<T> &v);
#include "../tpp/remove_multiple_elements.tpp"

//========================================================================
// NAME: template <typename T> std::vector<T> get_n_rand_unique_elements(int n, std::vector<T> v)
// DESC: Get n random, unique elements from a vector v.
//========================================================================
template <typename T> std::vector<T> get_n_rand_unique_elements(int n, std::vector<T> v);
#include "../tpp/get_n_rand_unique_elements.tpp"

//========================================================================
// NAME: int indx_map_3Dto1D(int i, int j, int k, int nj, int nk)
// DESC: ``Flatten'' a 3D vector of size [ni*nj*nk] with elements [i][j][k] to 1D. Note that ni is NOT needed for this.
//========================================================================
int indx_map_3Dto1D(int i, int j, int k, int nj, int nk);

//========================================================================
// NAME: template<typename T> auto vector2D_size(std::vector<std::vector<T>> vec2D) -> decltype( std::vector<T>::size_type )
// DESC: Calculate the number of elements in a 2D vector
//========================================================================
template <typename T> auto vector2D_size(std::vector<std::vector<T>> vec2D) -> decltype(vec2D.size());
#include "../tpp/vector2d.tpp"

//========================================================================
// NAME: template <typename T> auto interpolate( T begin, T end, T frac) -> decltype(begin)
// DESC: Interpolates the value frac of the way between two 1D data points
//========================================================================
template <typename T> T interpolate_1D( T begin, T end, double frac);
#include "../tpp/interpolate.tpp"

//========================================================================
// NAME: template <typename T>
//       std::vector<typename std::vector<T>::size_type> sort_indices_asc(  const std::vector<T> &v )
//
//       template <typename T>
//       std::vector<typename std::vector<T>::size_type> sort_indices_desc( const std::vector<T> &v )
//
// DESC: Sort vector vec, returning the sorted indices idx
//========================================================================
template <typename T>
std::vector<typename std::vector<T>::size_type> sort_indices_asc(  const std::vector<T> &v );

template <typename T>
std::vector<typename std::vector<T>::size_type> sort_indices_desc( const std::vector<T> &v );

#include "../tpp/sort.tpp"

//========================================================================
// NAME: template <class T> int find_element_by_name(std::string name, const std::vector<T> &cls)
//       template <class T> int find_element_by_ID(std::string ID, const std::vector<T> &cls)
// DESC: Finds the element number of a vector of classes by name (m_name) or ID (m_ID). A -1 is returned if the element is not found.
//========================================================================
template <class T> int find_element_by_name(std::string name, const std::vector<T> &cls);
template <class T> int find_element_by_ID(std::string ID, const std::vector<T> &cls);
#include "../tpp/find_element.tpp"


#endif
