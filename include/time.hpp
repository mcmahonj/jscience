/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 11/4/2013
// LAST UPDATE: 11/29/2013

#ifndef TIME_H
#define TIME_H


// STL
#include <ctime>  // std::time_t
#include <string> // std::string


//========================================================================
// NAME: std::string timestamp_to_yyyymmdd(std::time_t t)
// DESC: Returns a timestamp in the form YYYY-MM-DD.
// << JMM >> :: this is a temporary routine, since one COULD use std::put_time in C++ 11 (but gcc does not yet have this implemented)
//========================================================================
std::string timestamp_to_yyyymmdd(std::time_t t);

//========================================================================
// NAME: std::string yyyymmdd_to_timestamp(std::time_t t)
// DESC: Returns a timestamp from a string in the form YYYY-MM-DD.
//========================================================================
std::time_t yyyymmdd_to_timestamp(std::string yyyymmdd);

//========================================================================
// NAME: int days_since_00010101(int YYYY, int MM, int DD)
// DESC: Calculates the number of days since 0001-01-01 (Rata Die day one).
//========================================================================
int days_since_00010101(int YYYY, int MM, int DD);


#endif	
