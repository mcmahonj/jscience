/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/12/2013
// LAST UPDATE: 12/12/2013

#ifndef DATE_H
#define DATE_H


//========================================================================
//========================================================================
//
// NAME: int days_since_00010101(int YYYY, int MM, int DD)
//
// DESC: Calculates the number of days since 0001-01-01 (Rata Die day one).
//
// NOTES:
//     ! See:
//          http://mysite.verizon.net/aesir_research/date/rata.htm
//
//========================================================================
//========================================================================
inline int days_since_00010101(int YYYY, int MM, int DD)
{
    if( MM < 3 )
    {
        --YYYY;
        MM += 12;
    }
    
    return ( 365*YYYY + YYYY/4 - YYYY/100 + YYYY/400 + (153*MM - 457)/5 + DD - 306 );
}


#endif	
