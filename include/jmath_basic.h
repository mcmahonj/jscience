/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <=12/18/2008
// LAST UPDATE:   6/18/2014

#ifndef JMATH_BASIC_H
#define JMATH_BASIC_H

// STL
#include <cstdlib>
#include <cmath>	
#include <iostream>

// JSCIENCE
#include "jScience/physics/consts.hpp"


// !!! JMM
// ! this can be found on p. 942 in Num. Recip. in C
#define SIGN(a, b) ( (b) >= 0.0 ? fabs(a) : -fabs(a) )

#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define SHFT(a, b, c, d) (a)=(b);(b)=(c);(c)=(d);


// NUMBER COMPARISON
double dmin(double d1, double d2);
double dmax(double d1, double d2);

int imax(int i1, int i2);
int imin(int i1, int i2);

//========================================================================
// NAME: template <typename T> int sign(T val)
// DESC: Returns the sign of val, also considering 0.
//========================================================================
template <typename T> int sign(T val);
#include "../tpp/sign.tpp"

//========================================================================
// NAME: bool is_pow_of_two(unsigned long x)
// DESC: Determines whether if a number is a power of 2 (i.e., of dyadic length)
//========================================================================
bool is_pow_of_two(unsigned long x);

// ROUND NUMBER TO NEAREST INTEGER
double dround(double); 

// NUMBER MANIPULATION
int ifactorial(int n);
double dfactorial(int n);
double dtwofac(int n);

//========================================================================
// NAME: double square(double x)
// DESC: Returns the square of an argument x
//========================================================================
double square(double x);

 
#endif
