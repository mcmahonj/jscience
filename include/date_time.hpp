/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/1/2013
// LAST UPDATE: 1/13/2015

#ifndef DATE_TIME_ROUTINES_H
#define DATE_TIME_ROUTINES_H


// STL
#include <ctime>        // std::time_t
#include <string>       // std::string

// THIS
#include "DateTime.hpp" // DateTime
#include "Duration.hpp" // Duration


extern const DateTime DateTime_RD;
extern const DateTime DateTime_future;


//========================================================================
// NAME: void switch_time_zone(DateTime::TIME_ZONE TZD)
//       void unswitch_time_zone()
// DESC: Switches (or un-switches) the time zone to that specified by TZD.
//========================================================================
void switch_time_zone(DateTime::TIME_ZONE TZD);
void unswitch_time_zone();


//========================================================================
// NAME: DateTime current_DateTime(DateTime::TIME_ZONE TZ)
// DESC: Gets the current DateTime at a specified time zone.
//========================================================================
DateTime current_DateTime(DateTime::TIME_ZONE TZ);


//========================================================================
// NAME: bool is_same_day(const DateTime &dt1, const DateTime &dt2)
// DESC: Checks whether two DateTime structures correspond to the same day.
//========================================================================
bool is_same_day(const DateTime &dt1, const DateTime &dt2);


//========================================================================
// NAME: bool is_leap_year( const unsigned int YYYY )
// DESC: Checks whether a year is a leap year.
//========================================================================
bool is_leap_year( const unsigned int YYYY );


//========================================================================
// NAME: unsigned int ndays_in_month( const unsigned int MM, const unsigned int YYYY )
// DESC: Returns the number of days in a month, in a given year.
//========================================================================
unsigned int ndays_in_month( const unsigned int MM, const unsigned int YYYY );


//========================================================================
// NAME: DateTime floor(const DateTime &dt, const Duration &dur)
//       DateTime ceiling(const DateTime &dt, const Duration &dur)
// DESC: Checks whether two DateTime structures correspond to the same day.
//========================================================================
DateTime floor(const DateTime &dt, const Duration &dur);
DateTime ceiling(const DateTime &dt, const Duration &dur);


//========================================================================
// NAME: Duration duration_between(const DateTime &dt1, const DateTime &dt2)
// DESC: Returns the duration (in days, hours, mins., or secs.) between two DateTime objects.
//========================================================================
Duration duration_between(const DateTime &dt1, const DateTime &dt2);


#endif	
