/*
 Copyright 2014-Present Algorithms in Motion LLC

 This file is part of jScience.

 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 8/15/2013
// LAST UPDATE: 11/30/2013

#ifndef JCURLM_H
#define JCURLM_H


// STL
#include <string> // std::string
#include <vector> // std::vector

// CURL
#include "curl/curl.h"


//=========================================================
// CJCurlM MULTI-CURL CLASS
// DESC: cURL multi-interface class. Can be used as:
//     CJCurlM:add_request("http://www.website1.com");
//     CJCurlM:add_request("http://www.website2.com");
//     .
//     .
//     .
//     CJCurlM:add_request("http://www.websiten.com");
//     CJCurlM:update();
//
// NOTES:
//     ! The functionality in this class was derived directly from:
//          http://curl.haxx.se/libcurl/c/multi-app.html
//=========================================================
class CJCurlM
{

public:

    std::vector<std::string> content;

    CJCurlM();
    ~CJCurlM();

    void add_HTTP_request(std::string url);
    void update();

private:

    std::vector<std::string> HTTP_urls;

};


#endif


