/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <7/14/2013
// LAST UPDATE:  7/8/2014

#ifndef JMATH_SPECIAL_H
#define JMATH_SPECIAL_H


// STL
#include <cstdlib>
#include <vector>
#include <cmath>
#include <complex>

// jSCIENCE
#include "jScience/physics/consts.hpp"
#include "jmath_basic.h"


//========================================================================
// NAME: double jtanh(double x)
// DESC: Hyperbolic tangent function.
//========================================================================
double jtanh(double x);
//========================================================================
// NAME: double dtanh(double x)
// DESC: Derivative of the hyperbolic tangent function.
//========================================================================
double dtanh(double x);

//========================================================================
// NAME: double jsech(double x)
// DESC: Hyperbolic secent function
//========================================================================
double jsech(double x);


//========================================================================
// NAME: double jerfinv(double z, double tol)
// DESC: Calculates the inverse error function of z
//========================================================================
double jerfinv(double z);

// (APPROXIMATE) COMPLEMENTARY ERROR FUNCTION
double erfc_approx(double);

//========================================================================
// NAME: double probit(double p)
// DESC: Calculates the probit function of p
//========================================================================
double probit(double p);

/*
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


// BESSEL FUNCTIONS
void get_hn(int n, int m, double z, std::complex<double>& hn);

void get_g(double k0, double xpos, double xposp, std::complex<double>& g);
double digamma(int m);
*/


#endif
