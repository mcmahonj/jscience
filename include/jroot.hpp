/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 9/16/2013
// LAST UPDATE: 9/16/2013

#ifndef JROOT_H
#define JROOT_H


// STL
#include <functional>
#include <vector>


int zbrac( std::function<double(double)>func, double &x1, double &x2);
void zbrak( std::function<double(double)>func, double x1, double x2, int n, std::vector<double> &xb1, std::vector<double> &xb2, int &nb);

double rtsafe( std::function<void(double, double &, double &)>funcd, double x1, double x2, double xacc );


#endif	
