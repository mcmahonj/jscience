/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JMINIMIZATION_H	
#define JMINIMIZATION_H

//************************************************************************
//			INCLUDE FILES
//************************************************************************

// MATH
#include <cmath>
// I/O
#include <iostream>
#include <fstream>
#include <functional> // std::function
#include <vector>     // std::vector

#include "jmath_basic.h"
#include "jrandnum.hpp"

// !!! JMM
using namespace std;

//************************************************************************
//			CLASSES / STRUCTS
//************************************************************************

// MNBRACK
// default ratio by which successive intervals are magnified
#define GOLD 1.618034
// maximum magnification allowed for a parabolic-fit step
#define GLIMIT 100.0
#define TINY 1.0e-20

// BRENT
// maximum allowed number of iterations
//#define ITMAX_BRENT 1000 
#define ITMAX_BRENT 100 
// golden ratio
#define CGOLD 0.3819660
// small number to protect against trying to achieve fractional accuracy for a minumum of exactly 0
#define ZEPS 1.0e-10

// LINMIN
//#define TOL_LINMIN 2.0e-8
#define TOL_LINMIN 2.0e-4

// POWELL
// maximum allowed number of iterations
#define ITMAX_POWELL 200 

//************************************************************************
//			FULL CODE GLOBAL VARIABLES
//************************************************************************

//************************************************************************
//			SUBROUTINE DECLARATIONS
//************************************************************************

double brent(double, double, double, double (*)(double), double, double *);

//void linmin(double [], double [], int, double &, double (*)(double []) );
void linmin(double [], double [], int, double *, double (*)(double []) );
double d1dim(double);

void powell(double [], double **, int, double, int *, double *, double (*)(double []) );

// !!! RECHECK THIS ROUTINE AGAINST NUMER RECIPES BEFORE USING IT !!!
void dfpmin(double [], int, double, int *, double *, double (*)(double []), void (*)(double [], double []) );

void sim_anneal( std::vector<double> &x, int n, int xtype[], double a[], double b[], std::function<double(const std::vector<double> &)> func, double T, double v[], double eps, int N_T, int N_S, bool silent);
double sim_anneal_get_Topt(int n, int xtype[], double a[], double b[], std::function<double(const std::vector<double> &)> func, int m);

#endif	
