// TODO: NOTE: The random number stuff should go to stats++.


#ifndef JRANDNUM_H
#define JRANDNUM_H


// STL
#include <random> // std::mt19937
#include <vector> // std::vector<>


// TODO: NOTE: Maybe should add distribution functionality here [see ::train_Real_AdaBoost() for generating distribution according to vector of weights, for example].


//
// DESC: Create and seed a 32-bit Mersenne twister (psuedo-)random number generator.
//
std::mt19937 prng_Mersenne_twister();


//
// DESC: Seeds the Mersenne twister PRNG.
//
void seed_Mersenne_twister(
                           std::mt19937 &_prng
                           );

//========================================================================
// NAME: double rand_num_uniform_Mersenne_twister(double a, double b)
//       int rand_num_uniform_Mersenne_twister(int a, int b)
// DESC: Produces a (pseudo) random number, distributed uniformly on the interval [a, b].
//========================================================================
double rand_num_uniform_Mersenne_twister(double a, double b);
int rand_num_uniform_Mersenne_twister(int a, int b);

//========================================================================
// NAME: double rand_num_normal_Mersenne_twister(double mean, double stddev)
// DESC: Produces a (pseudo) random number, normally distributed.
//========================================================================
double rand_num_normal_Mersenne_twister(double mean, double stddev);

// TODO: the following should return the ints (not use an output argument)
//========================================================================
// NAME: void n_unique_rand_ints(int n, int min, int max, std::vector<int> &ints)
// DESC: Generates n random uniqe ints between min and max according to the Knuth--Fisher--Yates algorithm, storing the results in ints.
//========================================================================
void n_unique_rand_ints(int n, int min, int max, std::vector<int> &ints);


#endif
