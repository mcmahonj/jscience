/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <7/29/2013
// LAST UPDATE:  4/11/2015

#ifndef JARMA_H	
#define JARMA_H


//************************************************************************
//			INCLUDE FILES
//************************************************************************

// UTILITY
#include <vector>

// !!! JMM
using namespace std;


//************************************************************************
//			CLASSES / STRUCTS
//************************************************************************

// FITTING & EVALUATION OF AN ARMA MODEL TO DATA w
void jARMA_fit(int p, int q, vector<double> w, std::vector<double> & x, const std::vector<double> & xa, const std::vector<double> & xb, double & estddev);
double jARMA_minimization_func(const std::vector<double> &x);
double jARMA_predict(int m, int p, int q, std::vector<double> w, std::vector<double> x);
void gen_ARMA_noise(double estddev, int n, std::vector<double> & noise);

// ROUTINES FROM: Gardner et al. Applied Statistics 29, 311--322 (1980)
void STARMA(int p, int q, vector<double> & phi, const vector<double> & theta, vector<double> & A, vector<double> & P, vector<double> & V, int & ifault);
void KARMA(int p, int q, const vector<double> & phi, const vector<double> & theta, vector<double> & A, vector<double> & P, const vector<double> & V, const vector<double> & w, vector<double> & resid, double & sumlog, double & ssq, int iupd, double delta, int & nit);
void INCLU2(int np, double weight, const vector<double> & xnext, vector<double> & xrow, double ynext, vector<double> & D, vector<double> & rbar, vector<double> & thetab, double & ssqerr, double & recres, int & rank, int & ifault);
void REGRES(int np, const vector<double> & rbar, const vector<double> & thetab, vector<double> & beta);
void KALFOR(int m, int p, int q, const vector<double> & phi, vector<double> & A, vector<double> & P, const vector<double> & V);


#endif	
