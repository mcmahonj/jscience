/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 8/4/2013
// LAST UPDATE: 8/4/2013

#ifndef JBASIS_FUNC_H
#define JBASIS_FUNC_H

//************************************************************************
//************************************************************************
// INCLUDES
//************************************************************************
//************************************************************************

// UTILITY
#include <vector>
// MATH
#include <cmath>
// I/O
#include <iostream>

//************************************************************************
//************************************************************************
// SUBROUTINES
//************************************************************************
//************************************************************************

double GaussianRBF_kernel(std::vector<double> X, std::vector<double> XP, double sigma);
double polynomial_kernel(std::vector<double> X, std::vector<double> XP, double c, int d);


#endif	
