/*
 Copyright 2014-Present Algorithms in Motion LLC

 This file is part of jScience.

 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <7/22/2013
// LAST UPDATE:  8/19/2013

// !!! JMM: THIS SHOULD BE RE-WRITTEN LIKE CJCurlM -- NOT SUITABLE AS IS FOR COMMERCIAL CODE

#ifndef JCURL_H
#define JCURL_H

// STANDARD
// utilities
#include <string>
#include <sstream>
#include <vector>
// curl
#include <vector>

#include "curl/curl.h"

using namespace std;

class CJCurl
{

private:

    unsigned int     mHttpStatus;
    vector<string>   mHeaders;
    string           mContent;
    string           mType;
    CURL*            pCurlHandle;
    static size_t    HttpContent(void * ptr, size_t size, size_t nmemb, void * stream);
    static size_t    HttpHeader(void * ptr, size_t size, size_t nmemb, void * stream);

public:

    // CONSTRUCTOR/DESTRUCTOR
    CJCurl():pCurlHandle( curl_easy_init() ) {};
    ~CJCurl() {};

    CURLcode Fetch(string url);

    inline string           Content()    const { return mContent; }
    inline string           Type()       const { return mType; }
    inline unsigned int     HttpStatus() const { return mHttpStatus; }
    inline vector<string>   Headers()    const { return mHeaders; }
};

#endif


