/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/22/2008
// LAST UPDATE: 5/27/2014

#ifndef MATRIXX_H
#define MATRIXX_H


// STL
#include <vector>


//=========================================================
// MATRIX
//=========================================================
template<typename T>
class Matrix
{
     
public:
    
    Matrix();
    Matrix(unsigned int r, unsigned int c);
    ~Matrix();
    
    void resize(unsigned int r, unsigned int c);
    
    unsigned int get_nrows() const;
    unsigned int get_ncols() const;
    
    T& operator()(unsigned int i, unsigned int j);
    T const& operator()(unsigned int i, unsigned int j) const;
    
    //template<typename Y>
    Matrix<T>& operator+=(const Matrix<T> &rhs);
    
private:
    
    unsigned int nr;
    unsigned int nc;
    
    std::vector<T> data;
    
    T& at(unsigned int i, unsigned int j);
    
    template<typename Y>
    friend void Matrix_to_RowMajOrd(const Matrix<Y> &A, std::vector<Y> &a);
    template<typename Y>
    friend void Matrix_to_ColMajOrd(const Matrix<Y> &A, std::vector<Y> &a);
    template<typename Y>
    friend void RowMajOrd_to_Matrix(std::vector<Y> &a, unsigned int nr, Matrix<Y> &A);
    template<typename Y>
    friend void ColMajOrd_to_Matrix(std::vector<Y> &a, unsigned int nc, Matrix<Y> &A);
    
    template<typename Y>
    friend Matrix<Y> operator*(const Y x, Matrix<Y> rhs);
    template<typename Y>
    friend Matrix<Y> operator*(Matrix<Y> lhs, const Y x);
    
};

template<typename T>
Matrix<T> operator+(Matrix<T> A, const Matrix<T> &B);

// copied to linalg/
template<typename T>
std::vector<T> operator*(const Matrix<T> &A, const std::vector<T> &x);

#include "../tpp/Matrix.tpp"


//=========================================================
// SUBROUTINES
//=========================================================
//template<typename T> void Matrix_to_ColMajOrd(const Matrix<T> &A, std::vector<T> &a);
//template<typename T> void Matrix_to_RowMajOrd(const Matrix<T> &A, std::vector<T> &a);

void realSymmetric_to_tridiagonal(const Matrix<double> &A, std::vector<double> &D, std::vector<double> &E);

#include "../tpp/Matrix_to_RowMajOrd.tpp"
#include "../tpp/Matrix_to_ColMajOrd.tpp"

#include "../tpp/RowMajOrd_to_Matrix.tpp"
#include "../tpp/ColMajOrd_to_Matrix.tpp"

// copied to linalg/
template <typename T>
void matrix_transpose( const Matrix<T> &A, Matrix<T> &AT );
#include "../tpp/matrix_transpose.tpp"

void LU_decomposition(const Matrix<double> &A, Matrix<double> &Ainv);

int matrix_pseudoinverse( const Matrix<double> &A, Matrix<double> &Apsinv );
void matrix_inverse(const Matrix<double> &A, Matrix<double> &Ainv);


template <typename T>
void matrix_multiply( const Matrix<T> &A, const Matrix<T> &B, Matrix<T> &C );
#include "../tpp/matrix_multiply.tpp"


template <typename T>
Matrix<T> identity_matrix(const unsigned int n);
#include "../tpp/identity_matrix.tpp"


#endif

