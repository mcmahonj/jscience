/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 12/27/2013
// LAST UPDATE: 12/29/2013

#ifndef STOPWATCH_H
#define STOPWATCH_H


// STL
#include <chrono>   // std::chrono
//#include <cstdint>  // int_fast64_t


//typedef std::chrono::duration<int_fast64_t,std::nano> nanoseconds_t;


//=========================================================
// STOP WATCH
//=========================================================
class Stopwatch
{
    
public:
    
    Stopwatch();
    ~Stopwatch();
    
    void start();
    std::chrono::nanoseconds count();
    void stop();
    
    void wait(std::chrono::nanoseconds dura);
    
private:
    
    bool running;
    
    std::chrono::high_resolution_clock::time_point tstart;
};


#endif
