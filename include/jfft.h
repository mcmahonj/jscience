/*
 Copyright (C) 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JFFT_H	
#define JFFT_H

//************************************************************************
//			INCLUDE FILES
//************************************************************************

// MATH
#include <cmath>
// I/O
//#include <iostream>

// CUSTOM ROUTINES
#include "jScience/physics/consts.hpp"

using namespace std;

//************************************************************************
//			CLASSES / STRUCTS
//************************************************************************

// QUICK ROUTINES
#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr


//************************************************************************
//			FULL CODE GLOBAL VARIABLES
//************************************************************************

//************************************************************************
//			SUBROUTINE DECLARATIONS
//************************************************************************

void fft(complex<double> [], int, int [], int, complex<double> []);
//void fft(complex<double> [], int, int [], int, complex<double> []);

void fourn(double *, int [], int, int);


#endif	
