/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : <7/4/2013
// LAST UPDATE:  11/13/2013

#ifndef JSTATS_H	
#define JSTATS_H

// STL
#include <vector>


//========================================================================
// NAME: template <typename T> T sum(const std::vector<T> &v)
//       double sum_exact(std::vector<double> x)
// DESC: Summation of a vector x.
//========================================================================
template <typename T> T sum(const std::vector<T> &v);
#include "../tpp/sum.tpp"
double sum_exact(std::vector<double> x);



/*
void FastTwoSum(double a, double b, double &x, double &y);
void ExtractVector(double sigma, const std::vector<double> &p, double &tau, std::vector<double> &pp);
void ExtractVectorNew(double sigma0, const std::vector<double> &p, double &sigman, std::vector<double> &pp);
double ufp(double p, const double EPS);
*/
 
//========================================================================
// NAME: double mean(const std::vector<double> &x)
//       double mean_exact(const std::vector<double> &x)
// DESC: Calculates the mean of the vector x.
//========================================================================
double mean(const std::vector<double> &x);
double mean_exact(const std::vector<double> &x);

template <typename T>
std::vector<double> mean(const std::vector<std::vector<T>> &x);
#include "../tpp/mean.tpp"

//========================================================================
// NAME: double median(const std::vector<double> & x)
// DESC: Calculate the median of a vector x.
//========================================================================
// << JMM >> :: should template
double median(std::vector<double> x);

//========================================================================
// NAME: double variance(const std::vector<double> &x)
//       double variance_exact(const std::vector<double> &x)
// DESC: Calculate the variance of a vector x.
//========================================================================
double variance(const std::vector<double> &x);
double variance_exact(const std::vector<double> &x);

//========================================================================
// NAME: double stddev(const std::vector<double> &x)
//       double stddev_exact(const std::vector<double> &x)
// DESC: Calculate the standard deviation of a vector x.
//========================================================================
double stddev(const std::vector<double> &x);
double stddev_exact(const std::vector<double> &x);

//========================================================================
// NAME: void test_statistical_routines()
// DESC: Runs some tests to estimate the accuracy of the statistical routines
//========================================================================
void test_statistical_routines();

/*
//========================================================================
// NAME: double variance( const std::vector<double> & x )
// DESC: Calculate the variance of a vector x.
//========================================================================
// AUTOCORRELATION FUNCTION AT LAG k
double autocorrel_func( int k, double data [], int n );
*/


#endif	
