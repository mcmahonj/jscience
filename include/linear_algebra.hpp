/*
 Copyright 2014-Present Algorithms in Motion LLC
 
 This file is part of jScience.
 
 jScience is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 jScience is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with jScience.  If not, see <http://www.gnu.org/licenses/>.
*/
// CREATED    : 5/2/2014
// LAST UPDATE: 5/2/2014

#ifndef LINEAR_ALGEBRA_H
#define LINEAR_ALGEBRA_H


// STL
#include <vector>     // std::vector

// jScience
#include "Matrix.hpp" // Matrix


int solve_Axb( const Matrix<double> &A, const std::vector<double> &b, std::vector<double> &x );


#endif
